<?php
session_start();
error_reporting(0);
//error_reporting(E_ALL);

require_once "../system/config.php";
require_once "system/config.php";
//require_once "//system/lang.php";
require_once "../system/connector.php";
require_once "system/system.php";
require_once "../system/view.php";
require_once "../system/components.php";
require_once "../system/controller.php";
//require_once "//system/mobile_detect.php";
//require_once "//system/mailer.php";

$db = new Connector;
$db->BaseConnection();

if($_SESSION['s_lang']=='' or $_SESSION['s_lang']==null)
	$_SESSION['s_lang'] = 'pl';

//mobile detect
$config = new config_options;
if($config->mobileAgentDetect==true) {
	$agent_detect = new Mobile_Detect();
	//if($agent_detect->isMobile() or $agent_detect->isTablet())
	if($agent_detect->isMobile())
		$agent = 'mobile';
	else $agent = 'desktop';
} else $agent = 'desktop';

$_SESSION['type'] = '';

$skin = new Skin;

$_SESSION['action'] = 'nauczyciele';

$layout = new Layout('../'.$skin->getTemplatePath());