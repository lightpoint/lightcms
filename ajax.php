<?php
session_start();
error_reporting(0);
//error_reporting(E_ALL);

require_once "system/config.php";
require_once "system/system.php";
require_once "system/connector.php";
require_once "system/controller.php";
require_once "system/view.php";
require_once "system/components.php";
//require_once "system/mobile_detect.php";
require_once "system/mailer.php";

$controller = new Controller;
$var = $controller->GetData();

$db = new Connector;
$db->BaseConnection();

//mobile detect
$config = new config_options;
if($config->mobileAgentDetect==true) {
	$agent_detect = new Mobile_Detect();
	if($agent_detect->isMobile() or $agent_detect->isTablet())
		$agent = 'mobile';
	else $agent = 'desktop';
} else $agent = 'desktop';

$data_module = new Module();
die($data_module->load($var->action,$var->option));
?>