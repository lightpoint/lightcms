<section class="products-pricing-wrap ng-scope">
  <div class="container products-pricing">
    <div class="row">
      <h1>REKOMENDOWANE PAKIETY EDUKACYJNE</h1>
      <div class="text">
        <p></p>
        <p></p>
      </div>
    </div>
    <div style="display: table; border-spacing: 7px;" class="row">
      <div class="educational-pack-tile">
        <div class="pack-top">
          <h3>PAKIET NA START</h3>
          <h3 class="price">4 290 zł</h3>
        </div>
        <div class="pack-body">
          <ul>
            <li>4x Dash i Dot</li>
            <li>1x Zestaw akcesoriów</li>
            <li>1x Łączniki do klocków Lego</li>
            <li>1x Cymbałki</li>
            <li>Scenariusze lekcji</li>
          </ul>
        </div>
      </div>
      <div class="educational-pack-tile">
        <div class="pack-top">
          <h3>PAKIET NA KLASĘ</h3>
          <h3 class="price">11 190 zł</h3>
        </div>
        <div class="pack-body">
          <ul>
            <li>10x Dash i Dot</li>
            <li>5x Zestaw akcesoriów</li>
            <li>5x Łączniki do klocków Lego</li>
            <li>2x Cymbałki</li>
            <li>Scenariusze lekcji</li>
          </ul>
        </div>
      </div>
      <div class="educational-pack-tile">
        <div class="pack-top">
          <h3>PAKIETA NA SZKOŁĘ</h3>
          <h3 class="price">21 900 zł</h3>
        </div>
        <div class="pack-body">
          <ul>
            <li>20x Dash i Dot</li>
            <li>10x Zestaw akcesoriów</li>
            <li>10x Łączniki do klocków Lego</li>
            <li>5x Cymbałki</li>
            <li>Scenariusze lekcji</li>
          </ul>
        </div>
      </div>
    </div>
    <h3 class="offering-group">POJEDYNCZE ROBOTY</h3>
    <div style="display: table; border-spacing: 7px" class="row products-list"><a href="http://www.wonderpolska.pl/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div class="product-image" style="background-image: url(http://static5.wonderpolska.pl/pol_il_DASH-6.jpg);"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Dash</div>
          <div class="product-price">799 zł</div>
        </div></a><a href="http://www.wonderpolska.pl/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div class="product-image" style="background-image: url(http://static2.wonderpolska.pl/pol_il_DASH-I-DOT-7.jpg);"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Dash i Dot</div>
          <div class="product-price">999 zł</div>
        </div></a><a href="http://www.wonderpolska.pl/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div class="product-image" style="background-image: url(http://static1.wonderpolska.pl/pol_il_ZESTAW-WONDER-8.jpg);"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Zestaw Wonder</div>
          <div class="product-price">1299 zł</div>
        </div></a></div>
      
      	<h3 class="offering-group">AKCESORIA</h3>
    <div style="display: table; border-spacing: 7px" class="row products-list"><a href="http://www.wonderpolska.pl/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div class="product-image" style="background-image: url(http://static1.wonderpolska.pl/pol_il_LACZNIKI-DO-KLOCKOW-LEGO-R-10.jpg);"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Łączniki do klocków Lego&reg;</div>
          <div class="product-price">89 zł</div>
        </div></a><a href="http://www.wonderpolska.pl/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div class="product-image" style="background-image: url(http://static1.wonderpolska.pl/pol_il_CYMBALKI-9.png);"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Cymbałki</div>
          <div class="product-price">179 zł</div>
        </div></a><a href="http://www.wonderpolska.pl/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div class="product-image" style="background-image: url(http://static3.wonderpolska.pl/pol_il_ZESTAW-3-AKCESORIOW-1-GRATIS-12.png);"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Zestaw akcesoriów</div>
          <div class="product-price">179 zł</div>
        </div></a></div>
    <div class="row text">
      <p></p>
    </div>
  </div>
</section>