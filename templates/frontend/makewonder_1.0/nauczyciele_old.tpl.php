<!DOCTYPE html>
<html ng-app="teacherPortal" lang="en-US" class="ng-scope">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="icon" type="image/x-icon">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="shortcun icon" type="image/x-icon">
    <meta description="Wonder Workshop Teacher's Portal">
    <meta charset="utf-8">
    <title>Portal dla nauczycieli</title>
    <base href="../">
    <link href="css/teachers.css" rel="stylesheet">
  </head>
  <body>
    <div ng-class="{ 'active': routeLoaded }" class="fade-in active">
      <div class="header-spacer"></div>
      <header ng-controller="headerController as headerCtrl" class="ng-scope">
        <div class="container">
          <div class="logo-wrap"><a href="https://www.makewonder.com/" class="logo">Wonder Workshop</a></div><a ui-sref="home" ui-sref-active="active" class="portal-name active" href="https://teachers.makewonder.com/">portal dla nauczycieli</a>
          <nav>
            <ul>
              <li> <a ui-sref="map" ui-sref-active="active" href="mapa.html" class="">Mapa szkół</a></li>
              <li> <a ui-sref="lessons.browse" ng-class="{ 'active': $state.includes('lessons') }" href="lekcje.html" class="">Plan lekcji</a></li>
              <li> <a ng-click="headerCtrl.requestInfo()" tabindex="1">Dołącz</a></li>
            </ul>
          </nav>
          <div ng-hide="headerCtrl.user.isLoggedIn()" class="header-sign-up">
            <button ng-click="$emit('open login')">Zaloguj się</button>
          </div>
          <div ng-hide="user.isLoggedIn()" class="header-shopping-cart-spacer">
            <div class="header-store-button"><a role="button" href="https://wonderpolska.pl/" class="button orange-button pill-button">Sklep</a></div>
          </div>
          <div class="header-sign-in-spacer">
            <div ww-user-menu="" ng-show="headerCtrl.user.isLoggedIn()" class="header-sign-in signed-in ng-hide"><ul class="user-menu-options" ng-transclude=""> 
              <li class="user-name ng-scope"><img class="user-avatar"><span class="user-name-name"> <span class="ng-binding">Hi,  &nbsp;</span><i class="fa fa-chevron-down user-menu-chevron"></i></span></li>
              <li class="user-menu-option ng-scope"><a href="https://www.makewonder.com/orders" target="_self">My Orders</a></li>
              <li class="user-menu-option ng-scope"><a href="https://www.makewonder.com/account" target="_self">My Account</a></li>
              <li class="user-menu-option ng-scope"><a ng-click="signOut($event)" class="sign-out">Log out</a></li>
            </ul></div>
          </div>
        </div>
      </header>
      <!-- uiView: undefined --><div autoscroll="false" class="ui-view ng-scope">
<section class="hero ng-scope">
  <div class="container">
    <div class="row">
      <div class="span-5">
        <div class="text">
          <h1>Wprowadź roboty do swojej szkoły</h1>
          <p>Dash i Dot wprowadzają dzieci od 5. roku życia w świat robotyki i programowania!</p>
        </div>
      </div>
      <div class="span-7">
        <div class="request-info-modal" ng-hide="user.isLoggedIn()">
  <div ng-click="close()" ng-show="modal" class="close-icon ng-hide">
    <svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
      <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
        <g id="Artboard-1" sketch:type="MSArtboardGroup" transform="translate(-801.000000, -992.000000)" stroke="#999999" stroke-width="2" stroke-linecap="round">
          <path d="M802,993 L816.233,1007.232 M816.233,993 L802,1007.232" id="Imported-Layers" sketch:type="MSShapeGroup"></path>
        </g>
      </g>
    </svg>
  </div>
  
</div>
      </div>
    </div>
  </div>
</section>
<section class="junebug ng-scope">
  <div class="container">
    <div class="row">
      <div class="item"><img src="img/book_icon.png" srcset="/assets/images/home/book_icon_2x.png 2x" alt="Curriculum Included">
        <div class="text">
          <h2 class="header">Gotowe lekcje w pakiecie</h2>
          <p>Udostępniamy nauczycielom gotowe lekcje z wykorzystaniem Dasha i Dota.
Lekcje w j. polskim będą systematycznie dodawane do zakładki ‘Lekcje’ (już niedługo publikacja pierwszej lekcji).<br/>Ponadto, pracujemy nad własnym podręcznikiem przygotowywanym przez polskich nauczycieli, który będzie spełniał wymogi zmian proponowanych przez MEN.</p>
        </div>
      </div>
      <div class="item"><img src="img/crayon_icon.png" srcset="/assets/images/home/crayon_icon_2x.png 2x" alt="Fun and Engaging">
        <div class="text">
          <h2 class="header">Zabawne i wciągające</h2>
          <p>Dash i Dot to osobistości, które rozpalają w dzieciach ciekawość i pewność siebie w trakcie odkrywania możliwości zarówno robota, jak i swoich. Poprzez zabawę robotami uczniowie rozwijają umiejętność współpracy, komunikacji i obsługi urządzeń cyfrowych.</p>
        </div>
      </div>
      <div class="item"><img src="img/cogs_icon.png" srcset="/assets/images/home/cogs_icon_2x.png 2x" alt="Easy to Use">
        <div class="text">
          <h2 class="header">Szybki start</h2>
          <p>Nie potrzebujesz wiedzy z programowania, aby móc zacząć nauczać dzieci z pomocą Dasha i Dota! Wystarczy ściągnąć 1 z 5 dostępnych darmowych aplikacji na tableta (najłatwiejsza jest aplikaca Go), naładować roboty i start! Początkowo pozwól uczniom oswoić się z robotami, by się z nimi zaprzyjaźniły. Uwzględnij czas na swobodną zabawę z Dashem i Dotem, zanim zaczniesz prowadzić pierwszą z przygotowanych przez nas lekcji.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="cta-bar red-cta-bar ng-scope">
  <div class="container">
    <h1>Ponad <strong>1500 szkół podstawowych</strong> z całego świata używa robotów Dash i Dot w trakcie zajęć lekcyjnych.</h1>
  </div>
</section>
<section class="home-quotes ng-scope">
  <div class="container">
    <div style="padding-top: 25px;" class="row">
      <div style="position: relative; min-height: 1px" class="span-8">
        <!-- ngRepeat: quote in quotes --><div ng-repeat="quote in quotes" ng-show="currentQuote === $index" class="quote ng-scope ng-hide">
          <div class="profile-pic"><img ng-src="/assets/images/home/susan.jpg" alt="Susan Prabulos" src="img/susan.jpg"></div>
          <div class="quote-body">
            <div class="quote-quote ng-binding">Dash & Dot have transformed the way my students learn about coding and computer science.</div>
            <div class="author ng-binding"> <span class="author-name ng-binding">Susan Prabulos </span>(technology teacher)<br>Meadow Lane Elementary in Lincoln, NE</div>
          </div>
        </div><!-- end ngRepeat: quote in quotes --><div ng-repeat="quote in quotes" ng-show="currentQuote === $index" class="quote ng-scope ng-hide">
          <div class="profile-pic"><img ng-src="/assets/images/home/smita.jpg" alt="Smita Kolhatkar" src="img/smita.jpg"></div>
          <div class="quote-body">
            <div class="quote-quote ng-binding">They have been instrumental in teaching students empathy, cooperative play, teamwork, patience, resilience, persistence, and taking risks.</div>
            <div class="author ng-binding"> <span class="author-name ng-binding">Smita Kolhatkar </span>(technology teacher)<br>Barron Park Elementary in Palo Alto, CA</div>
          </div>
        </div><!-- end ngRepeat: quote in quotes --><div ng-repeat="quote in quotes" ng-show="currentQuote === $index" class="quote ng-scope">
          <div class="profile-pic"><img ng-src="/assets/images/home/jacob.jpg" alt="Jacob Lee" src="img/jacob.jpg"></div>
          <div class="quote-body">
            <div class="quote-quote ng-binding">Coding concepts are so abstract, but Dot & Dash helped my first graders master the the skill quickly since they could see it all happen with the robots.</div>
            <div class="author ng-binding"> <span class="author-name ng-binding">Jacob Lee </span>(first-grade teacher)<br>Punahou School in Honolulu, HI</div>
          </div>
        </div><!-- end ngRepeat: quote in quotes --><div ng-repeat="quote in quotes" ng-show="currentQuote === $index" class="quote ng-scope ng-hide">
          <div class="profile-pic"><img ng-src="/assets/images/home/michelle.jpg" alt="Michelle Eckstein" src="img/michelle.jpg"></div>
          <div class="quote-body">
            <div class="quote-quote ng-binding">Using Blocky with Dash & Dot, teachers are empowered to bring coding skills into their classroom while addressing Common Core math or ELA standards,</div>
            <div class="author ng-binding"> <span class="author-name ng-binding">Michelle Eckstein </span>(technology teacher)<br>Peak to Peak Charter School in Lafayette, CO</div>
          </div>
        </div><!-- end ngRepeat: quote in quotes -->
      </div>
      <div class="span-4"><img src="img/quotes_dash.png" srcset="/assets/images/home/quotes_dash_2x.png 2x" alt="Get Started with Dash & Dot" class="dash-globe"></div>
    </div>
  </div>
  <div height="50px" style="background-color: #fff; height: 60px" class="row"><a role="button" ui-sref="map" class="button grey-outline-button pill-button" href="https://teachers.makewonder.com/map">Find schools using Dash & Dot!</a></div>
</section>
<section class="coding-curriculum ng-scope">
  <div class="container">
    <div class="row">
      <h1>Coding Curriculum for K-5</h1>
      <div class="text">
        <p>
          Our curriculum makes it easy to integrate coding into school day.
          We provide scaffolding to guide beginners as well as extensions to challenge more experienced coders.
        </p>
        <p>All lessons include detailed lesson procedures, guiding questions, evaluation rubrics. <a href="https://www.dropbox.com/s/5ute9j4y823yva4/WWCurriculumDocument.pdf" target="_blank" style="color: #fff; text-decoration: underline;">Find out more about robotics, coding, and relevance to the Common Core.</a></p>
      </div>
    </div>
    <div class="row lessons-list">
      <!-- ngRepeat: lesson in featuredLessons --><a ui-sref="lessons.lesson({ lessonTitle: lesson.formattedTitle })" ww-track-click="ww-track-click" click-event="click lesson title" click-data="{
  'lesson': 'Dance the Loopedy-Loop'
}" class="lesson-tile ng-isolate-scope" ng-repeat="lesson in featuredLessons" ww-lesson-tile="lesson" href="https://teachers.makewonder.com/lessons/dance-the-loopedyloop">
  <div ng-style="{ 'background-image': 'url(https://s3-us-west-1.amazonaws.com/lesson-plans-prod/images/a42ba2c4d3a3682ef7c5d94392866c26.jpg)' }" class="lesson-tile-image" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-prod/images/a42ba2c4d3a3682ef7c5d94392866c26.jpg');">
    <div class="lesson-categories-list"><span ng-show="lesson.premium" class="label label-premium"><i ng-show="lesson.subscriptionNeeded && !user.isTeacher()" class="fa fa-lock ng-hide"></i>Premium</span></div>
  </div>
  <div class="lesson-tile-bottom">
    <div class="lesson-title-wrap">
      <div class="lesson-title"> 
        <h4 class="ng-binding">Dance the Loopedy-Loop</h4>
        <p class="ng-binding">Coding</p>
      </div>
    </div>
    <div class="lesson-author">
      <div class="author-img">
        <!-- ngIf: lesson.author.avatarURL --><div ng-if="lesson.author.avatarURL" ng-style="{ 'background-image': 'url(' + lesson.author.avatarURL + ')' }" class="img ng-scope" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/14c95751082d74553a5f2ae2b0675e32.jpg');"></div><!-- end ngIf: lesson.author.avatarURL -->
        <!-- ngIf: !lesson.author.avatarURL -->
      </div>
      <div class="author-info">
        <div class="author-name"><span>by </span><strong class="ng-binding">Wonder Workshop</strong></div>
        <div class="author-grades">Grade <span class="ng-binding">3 </span><span ng-show="lesson.gradeLevel.max - lesson.gradeLevel.min" class="ng-binding">- 5</span></div>
      </div>
    </div>
  </div></a><!-- end ngRepeat: lesson in featuredLessons --><a ui-sref="lessons.lesson({ lessonTitle: lesson.formattedTitle })" ww-track-click="ww-track-click" click-event="click lesson title" click-data="{
  'lesson': 'A Series of Unfortunate Events'
}" class="lesson-tile ng-isolate-scope" ng-repeat="lesson in featuredLessons" ww-lesson-tile="lesson" href="https://teachers.makewonder.com/lessons/a-series-of-unfortunate-events">
  <div ng-style="{ 'background-image': 'url(https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/0799b0eb377925e38a0c756140b58643.jpg)' }" class="lesson-tile-image" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/0799b0eb377925e38a0c756140b58643.jpg');">
    <div class="lesson-categories-list"><span ng-show="lesson.premium" class="label label-premium"><i ng-show="lesson.subscriptionNeeded && !user.isTeacher()" class="fa fa-lock ng-hide"></i>Premium</span></div>
  </div>
  <div class="lesson-tile-bottom">
    <div class="lesson-title-wrap">
      <div class="lesson-title"> 
        <h4 class="ng-binding">A Series of Unfortunate Events</h4>
        <p class="ng-binding">Coding, Science, ELA</p>
      </div>
    </div>
    <div class="lesson-author">
      <div class="author-img">
        <!-- ngIf: lesson.author.avatarURL --><div ng-if="lesson.author.avatarURL" ng-style="{ 'background-image': 'url(' + lesson.author.avatarURL + ')' }" class="img ng-scope" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/14c95751082d74553a5f2ae2b0675e32.jpg');"></div><!-- end ngIf: lesson.author.avatarURL -->
        <!-- ngIf: !lesson.author.avatarURL -->
      </div>
      <div class="author-info">
        <div class="author-name"><span>by </span><strong class="ng-binding">Wonder Workshop</strong></div>
        <div class="author-grades">Grade <span class="ng-binding">1 </span><span ng-show="lesson.gradeLevel.max - lesson.gradeLevel.min" class="ng-binding">- 2</span></div>
      </div>
    </div>
  </div></a><!-- end ngRepeat: lesson in featuredLessons --><a ui-sref="lessons.lesson({ lessonTitle: lesson.formattedTitle })" ww-track-click="ww-track-click" click-event="click lesson title" click-data="{
  'lesson': 'Dash’s World Adventure'
}" class="lesson-tile ng-isolate-scope" ng-repeat="lesson in featuredLessons" ww-lesson-tile="lesson" href="https://teachers.makewonder.com/lessons/dashs-world-adventure">
  <div ng-style="{ 'background-image': 'url(https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/46a790d113b307c4855e19797090c3a1.jpg)' }" class="lesson-tile-image" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/46a790d113b307c4855e19797090c3a1.jpg');">
    <div class="lesson-categories-list"><span ng-show="lesson.premium" class="label label-premium ng-hide"><i ng-show="lesson.subscriptionNeeded && !user.isTeacher()" class="fa fa-lock ng-hide"></i>Premium</span></div>
  </div>
  <div class="lesson-tile-bottom">
    <div class="lesson-title-wrap">
      <div class="lesson-title"> 
        <h4 class="ng-binding">Dash’s World Adventure</h4>
        <p class="ng-binding">Coding, Math, ELA, Social Studies</p>
      </div>
    </div>
    <div class="lesson-author">
      <div class="author-img">
        <!-- ngIf: lesson.author.avatarURL --><div ng-if="lesson.author.avatarURL" ng-style="{ 'background-image': 'url(' + lesson.author.avatarURL + ')' }" class="img ng-scope" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/33fe0626aba52cefb0acab0aea6b9912.jpg');"></div><!-- end ngIf: lesson.author.avatarURL -->
        <!-- ngIf: !lesson.author.avatarURL -->
      </div>
      <div class="author-info">
        <div class="author-name"><span>by </span><strong class="ng-binding">Terri Eichholz</strong></div>
        <div class="author-grades">Grade <span class="ng-binding">3 </span><span ng-show="lesson.gradeLevel.max - lesson.gradeLevel.min" class="ng-binding">- 5</span></div>
      </div>
    </div>
  </div></a><!-- end ngRepeat: lesson in featuredLessons --><a ui-sref="lessons.lesson({ lessonTitle: lesson.formattedTitle })" ww-track-click="ww-track-click" click-event="click lesson title" click-data="{
  'lesson': 'The Robot Games'
}" class="lesson-tile ng-isolate-scope" ng-repeat="lesson in featuredLessons" ww-lesson-tile="lesson" href="https://teachers.makewonder.com/lessons/the-robot-games">
  <div ng-style="{ 'background-image': 'url(https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/bf7087f67e56d540f117d30a7e9f5a1c.jpg)' }" class="lesson-tile-image" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/bf7087f67e56d540f117d30a7e9f5a1c.jpg');">
    <div class="lesson-categories-list"><span ng-show="lesson.premium" class="label label-premium"><i ng-show="lesson.subscriptionNeeded && !user.isTeacher()" class="fa fa-lock ng-hide"></i>Premium</span></div>
  </div>
  <div class="lesson-tile-bottom">
    <div class="lesson-title-wrap">
      <div class="lesson-title"> 
        <h4 class="ng-binding">The Robot Games</h4>
        <p class="ng-binding">Coding, Math</p>
      </div>
    </div>
    <div class="lesson-author">
      <div class="author-img">
        <!-- ngIf: lesson.author.avatarURL --><div ng-if="lesson.author.avatarURL" ng-style="{ 'background-image': 'url(' + lesson.author.avatarURL + ')' }" class="img ng-scope" style="background-image: url('https://s3-us-west-1.amazonaws.com/lesson-plans-alpha/images/14c95751082d74553a5f2ae2b0675e32.jpg');"></div><!-- end ngIf: lesson.author.avatarURL -->
        <!-- ngIf: !lesson.author.avatarURL -->
      </div>
      <div class="author-info">
        <div class="author-name"><span>by </span><strong class="ng-binding">Wonder Workshop </strong></div>
        <div class="author-grades">Grade <span class="ng-binding">1 </span><span ng-show="lesson.gradeLevel.max - lesson.gradeLevel.min" class="ng-binding">- 5</span></div>
      </div>
    </div>
  </div></a><!-- end ngRepeat: lesson in featuredLessons -->
    </div>
    <div class="row"><a ui-sref="lessons.browse" role="button" style="margin-right: 1em;" class="button white-button pill-button" href="https://teachers.makewonder.com/lessons">Browse All Lesson Plans</a><a ui-sref="lessons.submit.form" role="button" class="button white-button pill-button" href="https://teachers.makewonder.com/lessons/submit"><i class="fa fa-plus"></i>Create Lesson</a></div>
  </div>
</section>
<section class="getting-started ng-scope">
  <div class="container">
    <div class="row">
      <h1>Getting Started with Dash & Dot</h1>
      <p>New to using coding and robotics? Watch these videos to see Dash & Dot in action!</p>
    </div>
    <div class="row video-list">
      <div class="video-tile"><a ng-click="homeCtrl.playVideo('nRY6KegqyFY')" class="video-preview"><img src="img/coding_and_robotics.jpg" srcset="/assets/images/home/coding_and_robotics_2x.jpg 2x" alt=""><i class="fa fa-play"></i></a><a href="https://teachers.makewonder.com/#">Coding and Robotics</a></div>
      <div class="video-tile"><a ng-click="homeCtrl.playVideo('rheZ5ePOEEc')" class="video-preview"><img src="img/video_getting_started.jpg" srcset="/assets/images/home/video_getting_started_2x.jpg 2x" alt=""><i class="fa fa-play"></i></a><a href="https://teachers.makewonder.com/#">Getting Started</a></div>
      <div class="video-tile"><a ng-click="homeCtrl.playVideo('SR4_6aoBCkU')" class="video-preview"><img src="img/video_robot_olympics.jpg" srcset="/assets/images/home/video_robot_olympics_2x.jpg 2x" alt=""><i class="fa fa-play"></i></a><a href="https://teachers.makewonder.com/#">Robot Olympics with Girls' Science Club</a></div>
    </div>
    <div class="row"><a href="https://www.youtube.com/watch?v=dEpVsT4i3DA&list=PLXSgvv3NnVuTXJLpFGeoluF8jhnXVXEd8" role="button" target="_blank" class="button pill-button browse-videos-button grey-outline-button">Browse More Videos</a></div>
  </div>
</section>
<section class="products-pricing-wrap ng-scope">
  <div class="container products-pricing">
    <div class="row">
      <h1>Educator Packs for the Classroom</h1>
      <div class="text">
        <p>
          Dash & Dot come with a variety of accessories to enhance the educational experience!
          We recommend the classroom pack for the best device to student ratio.
          School packs are great for multiple teachers in the school to share!
        </p>
        <p> <a href="https://store.makewonder.com/pages/quotes" target="_blank">Click here to download a personalized quote</a></p>
      </div>
    </div>
    <div style="display: table; border-spacing: 7px;" class="row">
      <div class="educational-pack-tile">
        <div class="pack-top">
          <h3>Starter Pack</h3>
          <h3 class="price">$960</h3>
        </div>
        <div class="pack-body">
          <ul>
            <li>4 Dash & Dot</li>
            <li>1 Accessories Pack</li>
            <li>1 Building Brick Connectors</li>
            <li>1 Xylophones</li>
            <li>1 Curriculum User</li>
          </ul>
        </div>
      </div>
      <div class="educational-pack-tile">
        <div class="pack-top">
          <h3>Classroom Pack</h3>
          <h3 class="price">$2,620</h3>
        </div>
        <div class="pack-body">
          <ul>
            <li>10 Dash & Dot</li>
            <li>5 Accessories Pack</li>
            <li>5 Building Brick Connectors</li>
            <li>5 Xylophones</li>
            <li>2 Curriculum Users</li>
          </ul>
        </div>
      </div>
      <div class="educational-pack-tile">
        <div class="pack-top">
          <h3>School Pack</h3>
          <h3 class="price">$5,069</h3>
        </div>
        <div class="pack-body">
          <ul>
            <li>20 Dash & Dot</li>
            <li>10 Accessories Pack</li>
            <li>10 Building Brick Connectors</li>
            <li>5 Xylophones</li>
            <li>5 Curriculum Users</li>
          </ul>
        </div>
      </div>
    </div>
    <h3 class="offering-group">Individual Robots</h3>
    <div style="display: table; border-spacing: 7px" class="row products-list"><a href="https://store.makewonder.com/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div ng-style="{ 'background-image': 'url(https://d1my81jgdt4emn.cloudfront.net/images/products/bo_1_300.png)' }" class="product-image" style="background-image: url('https://d1my81jgdt4emn.cloudfront.net/images/products/bo_1_300.png');"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Dash</div>
          <div class="product-price">$149<sup>.99</sup></div>
        </div></a><a href="https://store.makewonder.com/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div ng-style="{ 'background-image': 'url(https://d1my81jgdt4emn.cloudfront.net/images/products/yana_1_300.png)' }" class="product-image" style="background-image: url('https://d1my81jgdt4emn.cloudfront.net/images/products/yana_1_300.png');"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Dot</div>
          <div class="product-price">$49<sup>.99</sup></div>
        </div></a><a href="https://store.makewonder.com/" target="_blank" class="product-tile">
        <div class="product-tile-image">
          <div ng-style="{ 'background-image': 'url(https://d1my81jgdt4emn.cloudfront.net/images/products/all_pack_1_300.png)' }" class="product-image" style="background-image: url('https://d1my81jgdt4emn.cloudfront.net/images/products/all_pack_1_300.png');"></div>
        </div>
        <div class="product-tile-bottom">
          <div class="product-name">Wonder Pack</div>
          <div class="product-price">$279<sup>.99</sup></div>
        </div></a></div>
    <div class="row text">
      <p>Note: Dash & Dot require iPad 3 or newer, iPad Mini, or select Android tablets. Check our&nbsp;<a href="https://www.makewonder.com/compatibility" class="text-link">device compatibility list</a>&nbsp;for more details.</p>
    </div>
  </div>
</section>
<section class="giving ng-scope">
  <div class="container">
    <div class="row"><img src="img/obama.jpg" alt="Obama with Code.org and Wonder Workshop" class="obama">
      <div class="copy">
        <h1>Give Wonder</h1>
        <p>Every child deserves access to a great education. Computer science education is critical to providing children with opportunities relevant to today's world.</p>
      </div>
    </div>
    <div class="give-box-group">
      <div class="giving-group donors-choose"><a href="http://www.donorschoose.org/teachers" target="_blank" class="logo"><img src="img/dc-logo-redux.png" srcset="/assets/images/home/dc-logo-redux_2x.png 2x" alt="Donors Choose" data-pin-nopin="true"></a>
        <div class="copy">
          <p>To expand access and enable passionate teachers looking to bring Dash & Dot into their classrooms, Wonder Workshop will fund the home stretch of your project on <a href="http://www.donorschoose.org/" target="_blank">DonorsChoose.org</a>. Read more about how to start a project and qualify for the matching program <a href="http://blog.makewonder.com/how-to-qualify-your-donors-choose-project-for-worker-workshops-matching-program/" target="_blank">here</a>.</p>
          <div class="row"><a href="http://www.donorschoose.org/teachers" target="_blank" class="button pill-button">Start a Project</a><a href="http://www.donorschoose.org/donors/viewChallenge.html?id=20614600&active=true" target="_blank" class="button pill-button">View Current Projects</a></div>
        </div>
      </div>
      <div class="giving-group pencils-of-promise"><a href="http://pencilsofpromise.org/" target="_blank" class="logo"><img src="img/pencils_of_promise.png" srcset="/assets/images/home/pencils_of_promise_2x.png 2x" alt="Pencils of Promise" data-pin-nopin="true"></a>
        <div class="copy">
          <p>In everything we do, we put children first. We seek ways to have an impact that goes beyond coding. For every robot purchased, we contribute $1 towards providing education to children in developing nations in partnership with Pencils of Promise, an organization that creates schools, programs, and global communities around the common goal of education for all.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section ng-controller="signupCTAController as signupCtrl" ng-hide="signupCtrl.user.isLoggedIn()" class="signup-cta ng-scope">
  <div class="container">
    <h1>Bring Dash & Dot to your classroom today.</h1>
    <button type="button" ng-click="signupCtrl.signUp()" class="orange-button pill-button">Teachers, sign up to get started</button>
  </div>
</section></div>
      <section ng-controller="footerController" class="footer-section ng-scope">
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="footer-link-section">
                <div footer-accordion="" hide-default="" class="footer-link-section-title">Product Info</div>
                <ul>
                  <li> <a href="https://www.makewonder.com/dash">Dash</a></li>
                  <li> <a href="https://www.makewonder.com/dot">Dot</a></li>
                  <li> <a href="https://store.makewonder.com/" target="_self">Store</a></li>
                  <li> <a href="https://developer.makewonder.com/">Developers</a></li>
                  <li> <a href="https://teachers.makewonder.com/">Teachers</a></li>
                  <li><a href="https://www.makewonder.com/start" target="_self">Get Started</a></li>
                  <li><a href="https://www.makewonder.com/play/setup" target="_self">Accessory Setup Guide</a></li>
                </ul>
              </div>
              <div class="footer-link-section">
                <div footer-accordion="" hide-default="" class="footer-link-section-title">Company</div>
                <ul>
                  <li> <a href="https://www.makewonder.com/about">About Us</a></li>
                  <li> <a href="https://www.makewonder.com/blog">Blog</a></li>
                  <li> <a href="https://www.makewonder.com/partners">Partners</a></li>
                  <li><a href="http://goo.gl/forms/jKGCqH2HvP" target="_blank">Resellers</a></li>
                  <li><a href="http://www.shareasale.com/out-viewmerchant.cfm?merchantID=57845" target="_blank">Affiliates</a></li>
                  <li> <a href="https://www.makewonder.com/careers" target="_self">Careers</a></li>
                </ul>
              </div>
              <div class="footer-link-section">
                <div footer-accordion="" hide-default="" class="footer-link-section-title">Inquiries</div>
                <ul>
                  <li> <a href="https://help.makewonder.com/" target="_blank">Help</a></li>
                  <li> <a href="https://help.makewonder.com/" target="_blank">Contact Us</a></li>
                  <li> <a href="mailto:press_inquiries@makewonder.com" target="_blank">Press</a></li>
                  <li> <a href="https://www.makewonder.com/mediakit" target="_blank">Media Kit</a></li>
                  <li> <a href="https://www.makewonder.com/warranty">Warranty</a></li>
                  <li><a href="https://www.makewonder.com/product_support" target="_self">Product Support</a></li>
                </ul>
              </div>
              <div class="footer-newsletter">
                <div class="footer-link-section-title">Join Our Newsletter</div>
                <form method="POST" ng-submit="joinNewsletter()" ng-valid-required="" name="footerNewsletterForm" class="ng-pristine ng-valid-email ng-invalid ng-invalid-required ng-invalid-date">
                  <p ng-show="newsLetterError" class="error newsletter-error fx-fade-normal ng-binding ng-hide"></p>
                  <p class="ng-binding">Stay in the know about new wonder products, events, and events</p>
                  <div style="position: absolute; left: -5000px;">
                    <input type="text" name="b_6c1539b72e697a4dc1503b46b_e5770b11b0" tabindex="-1" value="" ng-model="newsLetterBot" class="ng-pristine ng-untouched ng-valid">
                  </div>
                  <div class="row">
                    <input ng-model="newsletterEmail" type="email" ng-disabled="newsletterSignedUp" placeholder="Your Email Address" required="" class="newsletter-input ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required">
                  </div><small>Verify your age (we do not store this)</small>
                  <div class="row dob-button">
                    <input ng-model="dob" ww-dob-mask="" type="text" ng-disabled="newsletterSignedUp" required="" class="newsletter-input ng-pristine ng-untouched ng-invalid ng-invalid-required ng-invalid-date" placeholder="MM/DD/YYYY">
                    <button ng-disabled="newsletterSignedUp || footerNewsletterForm.$invalid" class="newsletter-button no-shadow-button" disabled="disabled"><span ng-hide="loading">Join</span><span ng-show="loading" class="ng-hide"><i class="fa fa-spinner fa-spin"></i></span></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="retail-locations">Also available at 
            <div><a id="amazon" href="http://www.amazon.com/gp/product/B00SKURVKY/" target="_blank"><img src="img/amazon.png" srcset="https://d1my81jgdt4emn.cloudfront.net/images/footer/amazon_2x.png 2x" alt="Amazon.com"></a><a id="apple-store" href="https://www.apple.com/retail/locator/" target="_blank"><img src="img/apple.png" srcset="https://d1my81jgdt4emn.cloudfront.net/images/footer/apple_2x.png 2x" alt="Apple Retail Store" data-pin-nopin="true"></a></div>
          </div>
          <div class="footer-bottom">
            <div class="social-links"><a href="https://www.facebook.com/makewonder" target="_blank" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/wonderworkshop" target="_blank" class="social-link social-link-twitter sweep-btn"><i class="fa fa-twitter"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a><a href="https://www.makewonder.com/blog" target="_self" class="social-link social-link-blog"><span>the blog</span></a></div>
            <div class="legal-stuff">
              <div class="tos"><a href="https://www.makewonder.com/tos">Terms of Service &nbsp; </a>• <strong><a href="https://www.makewonder.com/privacy">&nbsp; Privacy Policy </a></strong></div>
              <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
            </div>
          </div>
        </div>
      </section>
    </div>
  
</body></html>