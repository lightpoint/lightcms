<!DOCTYPE html>
<html lang="pl" ng-strict-di ng-app="makeWonder.main">
  <head>
    <!--<link href="https://d2eawk67eat224.cloudfront.net/build/stylesheets/style-d998c2a4cb73dd6f4825f9033d02a52f.css" rel="stylesheet"/>-->
    <link href="templates/frontend/makewonder_1.0/css/front.css" rel="stylesheet">
    <link href="templates/frontend/makewonder_1.0/css/unslider.css" rel="stylesheet">
    <link href="templates/frontend/makewonder_1.0/css/unslider-dots.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="/favicon.ico" rel="icon" type="image/x-icon">
    <link href="/favicon.ico" rel="shortcun icon" type="image/x-icon">
    <link rel="alternate" hreflang="de_DE" href="//www.makewonder.de/">
    <link rel="alternate" hreflang="en_US" href="//www.makewonder.com/">
    <link rel="alternate" hreflang="zh_CN" href="//cn.makewonder.com/"><!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
    <script>(function(){'use strict';var f,g=[];function l(a){g.push(a);1==g.length&&f()}function m(){for(;g.length;)g[0](),g.shift()}f=function(){setTimeout(m)};function n(a){this.a=p;this.b=void 0;this.f=[];var b=this;try{a(function(a){q(b,a)},function(a){r(b,a)})}catch(c){r(b,c)}}var p=2;function t(a){return new n(function(b,c){c(a)})}function u(a){return new n(function(b){b(a)})}function q(a,b){if(a.a==p){if(b==a)throw new TypeError;var c=!1;try{var d=b&&b.then;if(null!=b&&"object"==typeof b&&"function"==typeof d){d.call(b,function(b){c||q(a,b);c=!0},function(b){c||r(a,b);c=!0});return}}catch(e){c||r(a,e);return}a.a=0;a.b=b;v(a)}}
function r(a,b){if(a.a==p){if(b==a)throw new TypeError;a.a=1;a.b=b;v(a)}}function v(a){l(function(){if(a.a!=p)for(;a.f.length;){var b=a.f.shift(),c=b[0],d=b[1],e=b[2],b=b[3];try{0==a.a?"function"==typeof c?e(c.call(void 0,a.b)):e(a.b):1==a.a&&("function"==typeof d?e(d.call(void 0,a.b)):b(a.b))}catch(h){b(h)}}})}n.prototype.g=function(a){return this.c(void 0,a)};n.prototype.c=function(a,b){var c=this;return new n(function(d,e){c.f.push([a,b,d,e]);v(c)})};
function w(a){return new n(function(b,c){function d(c){return function(d){h[c]=d;e+=1;e==a.length&&b(h)}}var e=0,h=[];0==a.length&&b(h);for(var k=0;k<a.length;k+=1)u(a[k]).c(d(k),c)})}function x(a){return new n(function(b,c){for(var d=0;d<a.length;d+=1)u(a[d]).c(b,c)})};window.Promise||(window.Promise=n,window.Promise.resolve=u,window.Promise.reject=t,window.Promise.race=x,window.Promise.all=w,window.Promise.prototype.then=n.prototype.c,window.Promise.prototype["catch"]=n.prototype.g);}());

(function(){function l(a,b){document.addEventListener?a.addEventListener("scroll",b,!1):a.attachEvent("scroll",b)}function m(a){document.body?a():document.addEventListener?document.addEventListener("DOMContentLoaded",function c(){document.removeEventListener("DOMContentLoaded",c);a()}):document.attachEvent("onreadystatechange",function k(){if("interactive"==document.readyState||"complete"==document.readyState)document.detachEvent("onreadystatechange",k),a()})};function r(a){this.a=document.createElement("div");this.a.setAttribute("aria-hidden","true");this.a.appendChild(document.createTextNode(a));this.b=document.createElement("span");this.c=document.createElement("span");this.h=document.createElement("span");this.f=document.createElement("span");this.g=-1;this.b.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.c.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";
this.f.style.cssText="max-width:none;display:inline-block;position:absolute;height:100%;width:100%;overflow:scroll;font-size:16px;";this.h.style.cssText="display:inline-block;width:200%;height:200%;font-size:16px;max-width:none;";this.b.appendChild(this.h);this.c.appendChild(this.f);this.a.appendChild(this.b);this.a.appendChild(this.c)}
function x(a,b){a.a.style.cssText="max-width:none;min-width:20px;min-height:20px;display:inline-block;overflow:hidden;position:absolute;width:auto;margin:0;padding:0;top:-999px;left:-999px;white-space:nowrap;font:"+b+";"}function y(a){var b=a.a.offsetWidth,c=b+100;a.f.style.width=c+"px";a.c.scrollLeft=c;a.b.scrollLeft=a.b.scrollWidth+100;return a.g!==b?(a.g=b,!0):!1}function z(a,b){function c(){var a=k;y(a)&&null!==a.a.parentNode&&b(a.g)}var k=a;l(a.b,c);l(a.c,c);y(a)};function A(a,b){var c=b||{};this.family=a;this.style=c.style||"normal";this.weight=c.weight||"normal";this.stretch=c.stretch||"normal"}var B=null,D=null,E=null;function H(){if(null===D){var a=document.createElement("div");try{a.style.font="condensed 100px sans-serif"}catch(b){}D=""!==a.style.font}return D}function I(a,b){return[a.style,a.weight,H()?a.stretch:"","100px",b].join(" ")}
A.prototype.load=function(a,b){var c=this,k=a||"BESbswy",q=0,C=b||3E3,F=(new Date).getTime();return new Promise(function(a,b){null===E&&(E=!!document.fonts);if(E){var J=new Promise(function(a,b){function e(){(new Date).getTime()-F>=C?b():document.fonts.load(I(c,'"'+c.family+'"'),k).then(function(c){1<=c.length?a():setTimeout(e,25)},function(){b()})}e()}),K=new Promise(function(a,c){q=setTimeout(c,C)});Promise.race([K,J]).then(function(){clearTimeout(q);a(c)},function(){b(c)})}else m(function(){function t(){var b;
if(b=-1!=f&&-1!=g||-1!=f&&-1!=h||-1!=g&&-1!=h)(b=f!=g&&f!=h&&g!=h)||(null===B&&(b=/AppleWebKit\/([0-9]+)(?:\.([0-9]+))/.exec(window.navigator.userAgent),B=!!b&&(536>parseInt(b[1],10)||536===parseInt(b[1],10)&&11>=parseInt(b[2],10))),b=B&&(f==u&&g==u&&h==u||f==v&&g==v&&h==v||f==w&&g==w&&h==w)),b=!b;b&&(null!==d.parentNode&&d.parentNode.removeChild(d),clearTimeout(q),a(c))}function G(){if((new Date).getTime()-F>=C)null!==d.parentNode&&d.parentNode.removeChild(d),b(c);else{var a=document.hidden;if(!0===
a||void 0===a)f=e.a.offsetWidth,g=n.a.offsetWidth,h=p.a.offsetWidth,t();q=setTimeout(G,50)}}var e=new r(k),n=new r(k),p=new r(k),f=-1,g=-1,h=-1,u=-1,v=-1,w=-1,d=document.createElement("div");d.dir="ltr";x(e,I(c,"sans-serif"));x(n,I(c,"serif"));x(p,I(c,"monospace"));d.appendChild(e.a);d.appendChild(n.a);d.appendChild(p.a);document.body.appendChild(d);u=e.a.offsetWidth;v=n.a.offsetWidth;w=p.a.offsetWidth;G();z(e,function(a){f=a;t()});x(e,I(c,'"'+c.family+'",sans-serif'));z(n,function(a){g=a;t()});x(n,
I(c,'"'+c.family+'",serif'));z(p,function(a){h=a;t()});x(p,I(c,'"'+c.family+'",monospace'))})})};"undefined"!==typeof module?module.exports=A:(window.FontFaceObserver=A,window.FontFaceObserver.prototype.load=A.prototype.load);}());

		function init() {
	    window.addEventListener('scroll', function(e){
	    	var distanceY = window.pageYOffset || document.documentElement.scrollTop,
          shrinkOn = 200,
          header = document.getElementById("header2");
		      if (distanceY > shrinkOn) {
		      	header.className = "header-2 ng-scope active scrolling";
		      } else {
		      	header.className = "header-2 ng-scope active";
		      	
		      	
		      }
		    });
			}
			window.onload = init();

    </script>
    <meta content="True" name="HandheldFriendly">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0 user-scalable=no">
    <meta name="description" content="Makewonder">
    <title>Wonder Workshop</title>

  </head>
  <body>
    <script>
      // this is down here instead of in the head b/c I want to be able to add a class to the body, which has to exist first
      document.body.className = "fonts-not-loaded";
      
      // added this b/c FFO bugs have been elusive, so I want to be able to easily get some log info if they ever occur again
      window.fontFaceObserverDiagnostics = [];
      
      // in case neither the success or failure event from FFO ever fires for some reason. DOMContentLoaded typically occurs quite late in the loading process, after FFO should have fired
      document.addEventListener("DOMContentLoaded", function(event) {
        window.fontFaceObserverDiagnostics.push('Show buttons b/c DOMContentLoaded');
        document.body.className = "";
      });
      
      var Bariol = new FontFaceObserver('Bariol');
      
      Bariol.load()
        .then(function() {
          window.fontFaceObserverDiagnostics.push('Show buttons b/c Bariol loaded');
          document.body.className = "";
        }, function() {
          window.fontFaceObserverDiagnostics.push('Show buttons b/c Bariol failed to load');
          document.body.className += "";
        });
      
    </script>
    <ww-cookie-acceptance></ww-cookie-acceptance>
    <div class="main-page">
      <div class="mobile-nav ng-cloak">
        <div class="nav-items">
          <h3 class="active"><a href="/" target="_self">Home</a></h3>
          <h3><a href="/dash" target="_self">Dash</a></h3>
          <h3><a href="/dot" target="_self">Dot</a></h3>
          <h3><a href="/apps" target="_self">Apps</a></h3>
          <ul>
            <li><a href="/apps/go">Go</a></li>
            <li><a href="/apps/blockly" class=" ">Blockly</a></li>
            <li><a href="/apps/path" class=" ">Path</a></li>
            <li><a href="/apps/xylo" class=" ">Xylo</a></li>
            <li><a href="/apps/wonder" class=" ">Wonder</a></li>
          </ul>
          <h3><a href="/start">Get Started</a></h3>
          <h3><a href="https://teachers.makewonder.com" target="_self">Teachers</a></h3>
          <h3><a href="https://store.makewonder.com">Store</a></h3>
        </div>
      </div>
      <div class="wrap">
        <noscript>
          <div class="notice-bar">
            <div class="container">Please note: This website requires JavaScript to be enabled in order to function properly. Please<a href="http://www.enable-javascript.com/" target="_blank">enable your JavaScript</a> and refresh the page.</div>
          </div>
        </noscript><!--[if lt IE 9 ]>
        <div ng-hide="dismissNotice" class="notice-bar">
          <div class="container">This website has limited support on Internet Explorer versions 9 and below. We recommend using the latest version of<a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Chrome</a>,<a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a>,<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">Internet Explorer</a>or<a href="http://support.apple.com/downloads/#safari" target="_blank">Safari</a>.<br><br>
            <button ng-click="dismissNotice = true" class="pill-button orange-button sm-button">Got it</button>
          </div>
        </div><![endif]-->
        <div scroll-off-top="scrolling" class="header-2 ng-scope active" id="header2"><a class="hamburger-icon"><i class="fa fa-bars"></i></a>
          <div class="container">
            <nav class="wat-header active">
              <ul>{/menu:getNav/}</ul>
            </nav>
          </div><img src="templates/frontend/makewonder_1.0/img/w_logo.png" srcset="templates/frontend/makewonder_1.0/img/w_logo.png 2x" alt="Wonder Workshop" class="mobile-logo">
        </div>
        
        <div class="main">
          <div ng-controller="home2Controller as homeCtrl" class="home-page">
            <section class="home-first-section">
              <section class="hero-section">
                <div class="hero-outer">
                  <div class="hero-inner">
                    <div class="container">
                      <div class="copy">
                        <h1>Poznaj roboty Dash i Dot</h1><h3>Z nimi programowanie to świetna zabawa.</h3><!--<a onClick="_gaq.push(['_trackEvent', 'Klikniecie', 'Sklep', 'Odwiedz']);goog_report_conversion ('http://wonderpolska.pl/');" id="buy-now-hero" role="button" href="http://wonderpolska.pl/" class="button pill-button orange-button shadow-button">Odwiedź nasz sklep</a>-->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="pageDown"></div>
              </section>
              <section class="banner">
                <div class="container">
                  <div class="retail-locations-links"> 
                    <h4>Dostępne również:</h4><span>
                    <a href="http://allegro.pl" target="_blank" rel="nofollower noreferrer"><img src="templates/frontend/makewonder_1.0/img/media/allegro.png" alt="Allegro"/></a>
                    <a id="Cortland" href="http://www.cortland.pl/" target="_blank" rel="nofollower noreferrer"><img src="templates/frontend/makewonder_1.0/img/media/cortland.png" alt="Cortland"/></a>
                    <a id="iSpot" href="http://ispot.pl/" target="_blank" rel="nofollower noreferrer"><img src="templates/frontend/makewonder_1.0/img/media/ispot.png" alt="iSpot"/></a></span>
                  </div>
                </div>
              </section>
            </section>
            <div class="scroll-section">
              <section class="section-1"><a name="section-1"></a>
                <div class="container">
                  <div class="content">
                  	{/articles:have_your_kids_learn_to_code_from_a_native_speaker/}
                    <br class="no-mobile">
                    <div class="row"><a role="button" click-full-screen-video video-src="d0jcw3C36Qk" class="button pill-button orange-button">Zobacz co potrafią</a><img src="templates/frontend/makewonder_1.0/img/robots_with_tablet.jpg" srcset="templates/frontend/makewonder_1.0/img/robots_with_tablet_2x.jpg 2x" alt="Dash and Dot with Tablet"/>
                    </div>
                  </div>
                </div>
                <div class="section-bg-image"></div>
              </section>
              <section class="section-2"><a name="section-2"></a>
                <div class="container">
                  <div class="content">
                    {/articles:coding_is_the_career_of_the_future/}<br class="no-mobile">
                    <div class="row"><a href="dash.html" class="button pill-button orange-button">Poznaj Dasha</a>&nbsp;&nbsp;&nbsp;<a href="dot.html" class="button pill-button orange-button">Poznaj Dota</a>
                    </div>
                  </div>
                </div>
                <div class="section-bg-image"></div>
              </section>
              <section class="section-3"><a name="section-3"></a>
                <div class="container">
                  <div class="content">
                    {/articles:dash__and__dot_would_never_brag/}                  
                    
										<div class="fading-slider">
											<ul>
												<li>
													<img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_ttpm-f720dfe0a33b52ac658b1b55c52aba45.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_ttpm_2x-f2908a686937a1c5c4b48b44485dc602.png 2x" alt="TTPM Award"/>
													<img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_toy_insider-9011af19d11246fc9ae58705521e716e.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_toy_insider_2x-0d2551b9a288418883be6fe8168f7827.png 2x" alt="Toy Insider"/>
	                        <img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_dr_toy-cff986c0e6c51d685ab0e5304a0b7a20.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_dr_toy_2x-5dfee547a1398a77d42d72bfa85d8985.png 2x" alt="Dr. Toy"/>
                        </li>
                        <li>
                        	<img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_examiner-b6677a3f6d51c9e8cf5db57dceb8fb39.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_examiner_2x-b2f2d7ce20085b1a5f734f769635ccce.png 2x" alt="Examiner"/>
	                        <img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_scholastic-49b35a9174f3d3f47a8ec2c6a0e8c47a.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_scholastic_2x-28108a480c64ca0844898581e1d5caca.png 2x" alt="Scholastic"/>
                        	<img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_nappa-53b7eaa99026ddb1c756c056624ecf6e.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_nappa_2x-8727c3630c7daab2ebe4f96fab5ac64d.png 2x" alt="NAPPA Gold Award"/>
                        </li>
											</ul>
										</div>
                  	
                  	<div class="my-slider">
											<ul>
												<li>"Myślenie algorytmami, umiejętności programowania urządzeń codziennego użytku za chwilę będą równie potrzebne w życiu jak dziś obsługa systemów operacyjnych. Polska powinna tego uczyć w podstawówkach."<br/><strong>Rafał Belke, Komputerświat.pl</strong><br/><a href="http://www.komputerswiat.pl/nowosci/sprzet/2015/49/roboty-firmy-wonder-workshop-ucza-dzieci-programowac.aspx" target="_blank">przejdź do artykułu &raquo;</a>
									    </li>
									    
						      		<li>"Jeśli połączymy dobrą zabawę z nauką logicznego myślenia i wstępem do programowania to myślę, że żaden rodzic nie będzie żałował takiej inwestycji."<br/>
									    <strong>Grzegorz Marczak, Antyweb.pl</strong><br/><a href="http://antyweb.pl/dash-i-dot-to-nowa-zabawka-uczaca-programowania-i-logicznego-myslenia/" target="_blank">przejdź do artykułu &raquo;</a>
								    	</li>
								    
						      		<li>"Świetna zabawka edukacyjna, która nauczy wasze dziecko programowania, które będzie kluczowe w momencie kiedy będzie ono wchodzić w dorosłe życie."<br/>
									    <strong>Marysia Górecka, mamygadżety.pl</strong><br/><a href="http://mamygadzety.pl/dash-i-dot/" target="_blank">przejdź do artykułu &raquo;</a>
									    </li>
								    
						      		<li>"Dzieciaki dostają solidną dawkę wiedzy i gimnastyki umysłu. Fakt, że mogą korzystać wtedy z telefonów i tabletów też nie jest dla nich bez znaczenia :-)"<br/>
									    <strong>Maciej Mazurek, Zuchpisze.pl</strong><br/><a href="http://zuchpisze.pl/jak-nauczyc-dzieci-programowania-podczas-swietnej-zabawy-roboty-dash-i-dot-przychodza-z-pomoca/" target="_blank">przejdź do artykułu &raquo;</a>
									    </li>
									    
						      		<li>"Na pierwszy rzut oka to świecące, jeżdżące i gadające zabawki. Ale jest coś jeszcze. Są idealne do tego, by nauczyć dzieci programowania."<br/>
									    <strong>Grzegorz Sadowski, Wprost</strong><br/><a href="http://www.wprost.pl/ar/527879/KNOW-HOW/" target="_blank">przejdź do artykułu &raquo;</a>
									    
								    </li>
								    </ul>
                  	</div>
                  
                  </div>
                </div>
                <div class="section-bg-image"></div>
              </section>
              <section class="section-4"><a name="section-4"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    {/articles:smart_kind_and_popular_at_school/}<img src="templates/frontend/makewonder_1.0/img/mapa_polski.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/map_pins_2x-fc670b369db41d83b42e8489af22766a.png 2x" class="map-pins" alt="Mapa"/>
                    <br class="no-mobile">
                    <div class="row"><a role="button" class="button pill-button orange-button animated-chevron-button" target="_blank" href="http://nauczyciele.makewonder.pl/">Portal dla nauczycieli <i class="fa fa-chevron-right"></i></a></div>
                  </div>
                </div>
              </section>
              <section class="section-5"><a name="section-5"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    {/articles:kids_get_bored_easily/}<br><a role="button" href="aplikacje.html" style="width: 260px;" class="button pill-button orange-button">Poznaj&nbsp;nasze&nbsp;aplikacje</a><br class="no-mobile"><br><a role="button" href="kompatybilne_urzadzenia.html" style="width: 260px;" class="button pill-button scroll-section-link orange-button">Kompatybilne&nbsp;urządzenia</a>
                  </div>
                </div>
              </section>
              <section class="section-6"><a name="section-6"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    {/articles:do_more_learn_more/}<a role="button" href="https://wonderpolska.pl" class="button pill-button orange-button">Zamów teraz</a><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/launcher-6f819271d56be25d2e19c1bda5980b84.jpg" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/launcher_2x-b540f6ce0e0fceb353d6b0b719f442b8.jpg 2x" alt="Launcher" class="launcher-img">
                  </div>
                </div>
              </section>   
              
              <ul class="scroller"><a scroll-to="section-1" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-2" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-3" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-4" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-5" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-6" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a>
              </ul>
            </div>
            <section class="no-scroll-section">
              
              <section class="packed-with-value-section">
                <div class="container">
                  <h1>W pakiecie</h1>
                  <div class="packed-items">
                    <div><img src="templates/frontend/makewonder_1.0/img/packed_dashbox.jpg" srcset="templates/frontend/makewonder_1.0/img/packed_dashbox_2x.jpg 2x" alt="Robots">
                      <h3>Roboty</h3>
                      <p>W tym łączniki do klocków LEGO<sup>®</sup>  i kabel do ładowania</p>
                    </div>
                    <div class="big-plus">+</div>
                    <div><img src="templates/frontend/makewonder_1.0/img/packed_apps.png" srcset="templates/frontend/makewonder_1.0/img/packed_apps_2x.png 2x" alt="All apps">
                      <h3>Wszystkie aplikacje</h3>
                      <p>5 darmowych aplikacji i zapowiedź kolejnych</p>
                    </div>
                    <div class="big-plus">+</div>
                    <div><img src="templates/frontend/makewonder_1.0/img/packed_guarantee.png" srcset="templates/frontend/makewonder_1.0/img/packed_guarantee_2x.png 2x" alt="Satisfaction Guarantee">
                      <h3>Gwarancja satysfakcji</h3>
                      <p>Aż 30 dni na zwrot bez podania przyczyny</p>
                    </div>
                  </div>
                  <hr>
                  <div class="price-wrap">
                    <h2>Ceny od 799 zł</h2><a onClick="_gaq.push(['_trackEvent', 'Klikniecie', 'Sklep', 'Zamow teraz']);goog_report_conversion ('http://wonderpolska.pl/');" id="buy-now-bottom" href="http://www.wonderpolska.pl/" role="button" class="buy-btn button pill-button orange-button animated-chevron-button">Zamów teraz&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                  </div>
                </div>
              </section>
            </section>
          </div>
        </div>
      </div>  
    </div>
    
    <div class="footer-container">
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="footer-link-section">
                {/menu:getFootMenu/}
              <div class="footer-newsletter">
                {/menu:getFormNewsletter/}
              </div>
            </div>
          </div>
          
          <div class="footer-bottom row">
            <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
            <div class="legal-stuff">
              
              <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
            </div>
          </div>
          <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
        </div>
      </div>
    </div>
    
    <script src="https://d2eawk67eat224.cloudfront.net/build/js/makeWonder-12b9af8c353683912d2ae7f99d66f5a4.js"></script>
    <script src="https://d2eawk67eat224.cloudfront.net/build/js/makeWonder.main-85e91970497270f123dd4a90cd03cb44.js"></script><!--[if lt IE 10]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.1.3/jquery.placeholder.min.js"></script>
    <script>
      $(function(){
        $('input, textarea').placeholder({ customClass: 'ie-placeholder' });
      });
    </script><![endif]-->
    <script type="text/ng-template" id="ww-mobile-nav.html">
      <div class="mobile-nav">
        <div class="the-part-with-the-logo"><img src="https://d1my81jgdt4emn.cloudfront.net/images/header/w_square_logo.png" srcset="https://d1my81jgdt4emn.cloudfront.net/images/header/w_square_logo_2x.png 2x" alt="W Logo"></div>
        <div class="nav-items">
          <ul>
            <li><a href="{{ HOME_URL }}/" target="_self" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.HOME }">Home</a></li>
            <li><a href="{{ STORE_URL }}" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.STORE }">Store</a></li>
            <li><a href="{{ CLUBS_URL }}" target="_self" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.CLUBS }">Clubs</a></li>
            <li><a href="{{ TEACHER_PORTAL_URL }}" target="_self" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.TEACHERS }">Teachers</a></li>
            <li ng-hide="hCtrl.user.isLoggedIn()"><a ng-click="mobileNavCtrl.close(); hCtrl.loginModal.openLoginModal()">Sign in</a></li>
            <li class="non-nav"><span>Robots</span></li>
            <li class="link-group">
              <ul>
                <li><a href="{{ HOME_URL }}/dash" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.DASH }"><span>Dash</span></a></li>
                <li><a href="{{ HOME_URL }}/dot" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.DOT }"><span>Dot</span></a></li>
              </ul>
            </li>
            <li class="non-nav"><span>Apps</span></li>
            <li class="link-group">
              <ul>
                <li><a href="{{ HOME_URL }}/apps/wonder" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.APPS$WONDER }"><span>Wonder</span></a></li>
                <li><a href="{{ HOME_URL }}/apps/blockly" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.APPS$BLOCKLY }"><span>Blockly</span></a></li>
                <li><a href="{{ HOME_URL }}/apps/xylo" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.APPS$XYLO }"><span>Xylo</span></a></li>
                <li><a href="{{ HOME_URL }}/apps/go" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.APPS$GO }"><span>Go</span></a></li>
                <li><a href="{{ HOME_URL }}/apps/path" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.APPS$PATH }"><span>Path</span></a></li>
              </ul>
            </li>
            <li class="non-nav"><span>Play</span></li>
            <li class="link-group">
              <ul>
                <li><a href="{{ HOME_URL }}/start" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.GET_STARTED }"><span>Get Started</span></a></li>
                <li><a href="{{ HOME_URL }}/play/setup" ng-class="{ 'active': currentActiveLink === hCtrl.NavItems.SETUP }"><span>Accessories Setup</span></a></li>
                <li><a href="http://play.makewonder.com"><span>Kid's Portal</span></a></li>
              </ul>
            </li>
          </ul>
        </div>
        <div ng-show="hCtrl.user.isLoggedIn()" class="user-logged-in">
          <div ng-style="{ 'background-image': 'url(' + hCtrl.user.avatarURL + ')' }" class="avatar"></div>
          <div class="deits">Signed in as {{ hCtrl.user.firstName }}<br><a ng-click="hCtrl.user.destroy()">Sign out</a></div>
        </div>
      </div>
    </script>
    <script type="text/ng-template" id="ww-login-modal.html">
      <div class="login-box"><a ng-click="close()" ng-hide="noCloseIcon" class="fa fa-times close-icon"></a>
        <div ng-hide="needEmail || forgotPassword" class="login-box-face login-box-front">
          <div class="login-box-logo"></div>
          <div ng-if="flash.content" class="flash-box ng-cloak">
            <div class="flash {{ flash.content.category }}">{{ flash.content.message }}</div>
          </div>
          <div id="fb-oauth" class="login-oauth">
            <button ng-disabled="loading || disabled" ng-click="lmCtrl.loginFacebook($event)" class="login-box-button login-facebook-button"><span class="facebook-icon"></span>Login with Facebook</button>
          </div>
          <div id="google-oauth" class="login-oauth">
            <button ng-disabled="loading || disabled" ng-click="lmCtrl.loginGoogle($event)" class="login-box-button login-google-button"><span class="google-icon"></span>Login with Google</button>
          </div>
          <div class="login-box-or">
            <hr>
          </div>
          <form name="loginForm" ng-submit="lmCtrl.login($event)" method="POST" class="login-form">
            <label for="ww-login-email" class="login-box-label">Email</label>
            <input id="ww-login-email" type="email" ng-model="email" ng-disabled="loading || disabled" autofocus placeholder="Email Address" required class="login-box-input">
            <label for="ww-login-password" class="login-box-label">Password</label>
            <input id="ww-login-password" type="password" ng-model="password" ng-disabled="loading || disabled" placeholder="Password" required class="login-box-input">
            <div class="login-form-bottom">
              <button ng-disabled="loading || disabled" class="login-box-button login-form-button">Log In</button>
              <div class="login-box-text"><a href="https://www.makewonder.com/account/new">Sign Up</a>&nbsp; | &nbsp;<a ng-click="lmCtrl.recoverPassword()">Forgot password?</a>
                <div class="login-box-legal">By clicking Log In, you agree to our&nbsp;<a href="https://www.makewonder.com/tos">Terms</a> and&nbsp;<a href="https://www.makewonder.com/privacy">Privacy Policy</a>.</div>
              </div>
            </div>
          </form>
        </div>
        <div ng-show="needEmail" class="login-box-face login-box-back ng-cloak">
          <h4>Sign up for an account</h4>
          <div ng-if="flash.content" class="flash-box">
            <div ng-class="flash.content.category" class="flash">{{ flash.content.message }}</div>
          </div>
          <div ng-if="emailSuccess" class="login-form-bottom">
            <button ng-click="closeSuccess($event)" class="login-box-button login-box-email-success">OK</button>
          </div>
          <form name="signupForm" ng-hide="emailSuccess" method="POST">
            <label for="ww-signup-email" class="login-box-label">Email</label>
            <input id="ww-signup-email" type="email" ng-model="email" ng-disabled="loading || disabled" class="login-box-input">
            <div class="login-form-bottom">
              <button ng-disabled="loading || disabled" ng-click="lmCtrl.updateEmail($event)" class="login-box-button login-box-email-button">Submit</button>
              <button ng-click="closeSuccess($event)" class="login-box-button login-box-email-button login-box-cancel-button">No thanks</button>
            </div>
          </form>
        </div>
        <div ng-show="forgotPassword" class="login-box-face login-box-back ng-cloak">
          <h4>Recover your password</h4>
          <form name="signupForm" ng-submit="lmCtrl.sendRecoveryEmail(email)" method="POST" class="login-form">
            <div ng-if="flash.content" class="flash-box ng-cloak">
              <div ng-class="flash.content.category" class="flash">{{ flash.content.message }}</div>
            </div>
            <label for="ww-recover-email" class="login-box-label">Email</label>
            <input id="ww-recover-email" type="email" ng-model="email" ng-disabled="loading || disabled" required class="login-box-input">
            <div class="login-form-bottom">
              <button ng-disabled="loading || disabled" class="login-box-button">Submit</button>
              <div class="login-box-text"><a href="#" ng-click="lmCtrl.viewLogin()">Login</a></div>
            </div>
          </form>
        </div>
      </div>
    </script>
    <script type="text/ng-template" id="ww-cookie-acceptance.html">
      <div ng-show="showCookieBanner" class="cookie-acceptance">
        <button ng-click="accept()" role="button">I Accept.&nbsp;<i class="fa fa-close"></i></button>
        <div>Wonder Workshop is using cookies in order to deliver you the best website experience. If you are browsing on our website, you are accepting our&nbsp;<a href="{{ HOME_URL }}/privacy" target="_blank">Cookie Policy</a>.&nbsp;</div>
      </div>
    </script>
    <script>
      $(function() {
        //- mobile devices are unlikely to support active scroll events, so just disable them
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          $('.scroll-section').addClass('mobile-scroll-section');
          return;
        }
      
        var sections = $('.scroll-section section');
        var currentActiveSection;
        var scrollSectionActive = false;
        var ticking = false;
        /**
         * Store a reference to each section's scroll icon.
         */
        _.each(sections, function(section, index){
          section.scrollIcon = $('.scroller a')[index];
        });
      
        var getSectionOffsets = function(){
          _.each(sections, function(section, index) {
            section.top = $(section).offset().top;
          });
        };
      
        getSectionOffsets();
      
        var sectionIsOffTop = function(section){
          var documentHeight = $(window).height();
          return section.top < ($(window).scrollTop() + documentHeight);
        };
        /**
         * Toggle sub-sections active/inactive.
         */
        var toggleSubSectionActive = function(){
          var oldActiveSection = currentActiveSection;
          var scrollTop = $(window).scrollTop();
          currentActiveSection = _.findLast(sections, sectionIsOffTop) || sections[0];
          if (oldActiveSection !== currentActiveSection) {
            removeActiveFromSection(oldActiveSection);
            addActiveToSection(currentActiveSection);
          }
          ticking = false;
        };
      
        var addActiveToSection = function(section){
          $(section).addClass('active');
          $(section.scrollIcon).addClass('active');
        };
      
        var removeActiveFromSection = function(section){
          $(section).removeClass('active');
          $(section && section.scrollIcon).removeClass('active');
        };
        /**
         * Once our page scroll is past the top, toggle it active so the background image becomes fixed.
         */
        var toggleScrollSectionActive = function(){
          var top = $('.scroll-section').offset().top;
          if ($(window).scrollTop() > top && !scrollSectionActive) {
            scrollSectionActive = true;
            $('.scroll-section').addClass('active');
          } else if ($(window).scrollTop() <= top && scrollSectionActive) {
            scrollSectionActive = false;
            $('.scroll-section').removeClass('active');
          }
        }
        /**
         * Master scroll handler. Place toggling subsections under requestAnimationFrame, but not the handler for toggling the parent scroll section.
         */
        var toggleScrollyGuy = function() {
          toggleScrollSectionActive();
          if (ticking) {
            return;
          }
          requestAnimationFrame(toggleSubSectionActive);
          ticking = true;
        };
      
        $(window).scroll(toggleScrollyGuy);
        $(window).on('resize', getSectionOffsets);
      
      });
      
    </script>
    
    <script type="text/javascript" src="templates/frontend/makewonder_1.0/js/unslider-min.js"></script>
    <script type="text/javascript" src="templates/frontend/makewonder_1.0/js/script.js"></script>
  </body>
</html>