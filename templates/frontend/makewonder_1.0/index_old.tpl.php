<!DOCTYPE html>
<html lang="pl" ng-app="makeWonder.main">
<head>
{/metadata:homepage/}
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="templates/frontend/makewonder_1.0/css/style.css" rel="stylesheet">
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">

<link href="http://www.makewonder.com/favicon.ico" rel="icon" type="image/x-icon">
<link href="http://www.makewonder.com/favicon.ico" rel="shortcun icon" type="image/x-icon">
<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<![endif]-->

<meta content="True" name="HandheldFriendly">
<!--<meta name="google-site-verification" content="jEGpiPWCRNsUBjNuOBsnVOhyaYYOutpQ8W7IRsML-NE">-->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0 user-scalable=no">
<!--<script type="text/javascript" async="" src="templates/frontend/makewonder_1.0/js/mixpanel-2.2.min.js"></script>-->
<script async="" type="text/javascript" src="templates/frontend/makewonder_1.0/js/roundtrip.js"></script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-70029657-1', 'auto');
ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
 <script>
 
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
 document,'script','//connect.facebook.net/en_US/fbevents.js');

 fbq('init', '1495186814109494');
 fbq('track', "PageView");</script>
 <noscript><img height="1" width="1" style="display:none"
 
src="https://www.facebook.com/tr?id=1495186814109494&ev=PageView&noscript=1"
 /></noscript>
 <!-- End Facebook Pixel Code --> 
 <!--google-->
 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-70029657-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 934334004;
    w.google_conversion_label = "75i4CLK7oWMQtJzDvQM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script> 
<style>
h3 {font-size: 2.3em;}
.header-2{background-color: rgba(0,0,0,.7);}
</style>
</head>
  <body>
    <div class="main-page">
      <div class="mobile-nav">
        <div class="nav-items">
          {/menu:getMobileNav/}
        </div>
      </div>
      <div class="wrap">
        <noscript>
          &lt;div class="notice-bar"&gt;
            &lt;div class="container"&gt;Please note: This website requires JavaScript to be enabled in order to function properly. Please &lt;a href="http://www.enable-javascript.com/" target="_blank"&gt;enable your JavaScript&lt;/a&gt; and refresh the page.&lt;/div&gt;
          &lt;/div&gt;
        </noscript><!--[if lt IE 10 ]>
        <div ng-hide="dismissNotice" class="notice-bar">
          <div class="container">This website has limited support on Internet Explorer versions 9 and below. We recommend using the latest version of <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Chrome</a>, <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a>, <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">Internet Explorer </a>or <a href="http://support.apple.com/downloads/#safari" target="_blank">Safari</a>.<br><br>
            <button ng-click="dismissNotice = true" class="pill-button orange-button sm-button">Got it</button>
          </div>
        </div><![endif]-->
        <div scroll-off-top="scrolling" class="header-2 ng-scope active"><a class="hamburger-icon"><i class="fa fa-bars"></i></a>
          <div class="container">
            <nav class="wat-header active">
              <ul>{/menu:getNav/}</ul>
            </nav>
          </div><img src="templates/frontend/makewonder_1.0/img/w_logo.png" srcset="templates/frontend/makewonder_1.0/img/w_logo.png 2x" alt="Wonder Workshop" class="mobile-logo">
        </div>
        <div class="main">
          <div class="home-page">
            <section class="home-first-section">
              <section class="hero-section">
                <div class="hero-outer">
                  <div class="hero-inner">
                    <div class="container">
                      <div class="copy">
                        <h1>Poznaj roboty Dash i Dot</h1><h3>Z nimi programowanie to świetna zabawa.</h3><!--<a onClick="_gaq.push(['_trackEvent', 'Klikniecie', 'Sklep', 'Odwiedz']);goog_report_conversion ('http://wonderpolska.pl/');" id="buy-now-hero" role="button" href="http://wonderpolska.pl/" class="button pill-button orange-button shadow-button">Odwiedź nasz sklep</a>-->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="pageDown"></div>
              </section>
            </section>
            <div class="scroll-section">
              <section class="first-section"><a name="section-1"></a>
                <div class="section-bg-image desktop-only"></div><img src="templates/frontend/makewonder_1.0/img/dash_and_dot.jpg" srcset="templates/frontend/makewonder_1.0/img/dash_and_dot2x.jpg 2x" alt="Dash and Dot" class="mobile-only">
                <div class="container">
                  <div class="content">
                  	{/articles:dash_i_dot/}
                    <a role="button" click-full-screen-video="" video-src="d0jcw3C36Qk" class="button pill-button dark-outline-button"><i style="position: relative; bottom: 2px;" class="fa fa-play"></i>&nbsp;&nbsp;Zobacz co potrafią</a><LA9py48X6_o>
                  </div>
                </div>
              </section>
              <section class="inverse light-and-sound"><a name="section-2"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    {/articles:kodowanie_to_wspolczesna_supermoc/}
                    <div class="awards desktop-only"><a href="http://www.nappaawards.com/#!Dash-Dot-Wonder-Pack-by-Wonder-Workshop/cpt1/554930280cf273133519147b" target="_blank"><img src="templates/frontend/makewonder_1.0/img/nappa_gold_award.png" srcset="templates/frontend/makewonder_1.0/img/nappa_gold_award_2x.png 2x" alt="Nappa Gold Award Winner"></a><a href="http://openingmindsusa.org/wonder-workshop-named-winner-of-opening-minds-usa-innovation-award-renee-bevis-rn-to-receive-the-service-to-young-children/" target="_blank"><img src="templates/frontend/makewonder_1.0/img/opening_minds_award_white.png" srcset="templates/frontend/makewonder_1.0/img/opening_minds_award_white_2x.png 2x" alt="Opening Minds Innovation Award"></a></div>
                    <div class="awards mobile-only"><a href="http://www.nappaawards.com/#!Dash-Dot-Wonder-Pack-by-Wonder-Workshop/cpt1/554930280cf273133519147b" target="_blank"><img src="templates/frontend/makewonder_1.0/img/nappa_gold_award_black.png" srcset="templates/frontend/makewonder_1.0/img/nappa_gold_award_black_2x.png 2x" alt="Nappa Gold Award Winner"></a><a href="http://openingmindsusa.org/wonder-workshop-named-winner-of-opening-minds-usa-innovation-award-renee-bevis-rn-to-receive-the-service-to-young-children/" target="_blank"><img src="templates/frontend/makewonder_1.0/opening_minds_award.png" srcset="templates/frontend/makewonder_1.0/img/opening_minds_award_2x.png 2x" alt="Opening Minds Innovation Award"></a></div>
                  </div>
                </div>
              </section>
              <section class="accessories inverse"><a name="section-3"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    {/articles:dzieki_robotom_programowanie_staje_sie_jezykiem_zabawy/}<a href="dash.html" class="button pill-button dark-outline-button">Poznaj Dasha</a><a href="dot.html" class="button pill-button dark-outline-button">Poznaj Dota</a>
                  </div>
                </div>
              </section>
              <section class="video-section inverse"><a name="section-4"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    {/articles:dzieki_naszym_aplikacjom_roboty_moga_byc_zabawa_dla_kazdego_dziecka/}<a href="aplikacje.html" role="button" class="button pill-button">Dowiedz się więcej o naszych aplikacjach</a>
                  </div>
                </div>
              </section>
              <section class="apps-devices"><a name="section-5"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    {/articles:wspolpracuje_z_urzadzeniami_apple_i_android/}<a role="button" href="kompatybilne_urzadzenia.html" target="_self" style="width: 255px;" class="button pill-button scroll-section-link">Sprawdź kompatybilne urządzenia</a>
                  </div>
                </div>
              </section>
              <ul class="scroller"><a scroll-to="section-1" scroll-to-clear=".header-2" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-2" scroll-to-clear=".header-2" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-3" scroll-to-clear=".header-2" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-4" scroll-to-clear=".header-2" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-5" scroll-to-clear=".header-2" class="outer-circle">
                  <div class="inner-circle"></div></a></ul>
            </div>
                          
            <section class="no-scroll-section"> 
              <section class="packed-quotes">
              	<div class="container">
						      <div class="cSlick">
						      	
						      	<div class="cSlide">
						      		<div class="sl70">"Myślenie algorytmami, umiejętności programowania urządzeń codziennego użytku za chwilę będą równie potrzebne w życiu jak dziś obsługa systemów operacyjnych. Polska powinna tego uczyć w podstawówkach."</div>
									    <div class="sl30"><strong>Rafał Belke, Komputerświat.pl</strong><br/><a href="http://www.komputerswiat.pl/nowosci/sprzet/2015/49/roboty-firmy-wonder-workshop-ucza-dzieci-programowac.aspx" target="_blank">przejdź do artykułu &raquo;</a>
									    </div>
								    </div>
								    
								    <div class="cSlide">
						      		<div class="sl70">"Jeśli połączymy dobrą zabawę z nauką logicznego myślenia i wstępem do programowania to myślę, że żaden rodzic nie będzie żałował takiej inwestycji."</div>
									    <div class="sl30"><strong>Grzegorz Marczak, Antyweb.pl</strong><br/><a href="http://antyweb.pl/dash-i-dot-to-nowa-zabawka-uczaca-programowania-i-logicznego-myslenia/" target="_blank">przejdź do artykułu &raquo;</a>
									    </div>
								    </div>
								    
								    <div class="cSlide">
						      		<div class="sl70">"Świetna zabawka edukacyjna, która nauczy wasze dziecko programowania, które będzie kluczowe w momencie kiedy będzie ono wchodzić w dorosłe życie."</div>
									    <div class="sl30"><strong>Marysia Górecka, mamygadżety.pl</strong><br/><a href="http://mamygadzety.pl/dash-i-dot/" target="_blank">przejdź do artykułu &raquo;</a>
									    </div>
								    </div>
								    
								    <div class="cSlide">
						      		<div class="sl70">"Dzieciaki dostają solidną dawkę wiedzy i gimnastyki umysłu. Fakt, że mogą korzystać wtedy z telefonów i tabletów też nie jest dla nich bez znaczenia :-) Dzieci rwą się do technologii, mogłyby bez umiaru siedzieć przy ekranach. Możemy wykorzystać ten naturalny pociąg, żeby zrobić coś fajnego i wartościowego."</div>
									    <div class="sl30"><strong>Maciej Mazurek, Zuchpisze.pl</strong><br/><a href="http://zuchpisze.pl/jak-nauczyc-dzieci-programowania-podczas-swietnej-zabawy-roboty-dash-i-dot-przychodza-z-pomoca/" target="_blank">przejdź do artykułu &raquo;</a>
									    </div>
								    </div>
								    
								    <div class="cSlide">
						      		<div class="sl70">"Na pierwszy rzut oka to świecące, jeżdżące i gadające zabawki. Ale jest coś jeszcze. Są idealne do tego, by nauczyć dzieci programowania."</div>
									    <div class="sl30"><strong>Grzegorz Sadowski, Wprost</strong><br/><a href="http://www.wprost.pl/ar/527879/KNOW-HOW/" target="_blank">przejdź do artykułu &raquo;</a>
									    </div>
								    </div>
								    
								  </div>
								</div>
            	</section>
              
              <section class="packed-with-value-section">
                <div class="container">
                  <h1>W pakiecie</h1>
                  <div class="packed-items">
                    <div><img src="templates/frontend/makewonder_1.0/img/packed_dashbox.jpg" srcset="templates/frontend/makewonder_1.0/img/packed_dashbox_2x.jpg 2x" alt="Robots">
                      <h3>Roboty</h3>
                      <p>W tym łączniki do klocków LEGO<sup>®</sup>  i kabel do ładowania</p>
                    </div>
                    <div class="big-plus">+</div>
                    <div><img src="templates/frontend/makewonder_1.0/img/packed_apps.png" srcset="templates/frontend/makewonder_1.0/img/packed_apps_2x.png 2x" alt="All apps">
                      <h3>Wszystkie aplikacje</h3>
                      <p>5 darmowych aplikacji i zapowiedź kolejnych</p>
                    </div>
                    <div class="big-plus">+</div>
                    <div><img src="templates/frontend/makewonder_1.0/img/packed_guarantee.png" srcset="templates/frontend/makewonder_1.0/img/packed_guarantee_2x.png 2x" alt="Satisfaction Guarantee">
                      <h3>Gwarancja satysfakcji</h3>
                      <p>Aż 30 dni na zwrot bez podania przyczyny</p>
                    </div>
                  </div>
                  <hr>
                  <div class="price-wrap">
                    <h2>Ceny od 799 zł</h2><a onClick="_gaq.push(['_trackEvent', 'Klikniecie', 'Sklep', 'Zamow teraz']);goog_report_conversion ('http://wonderpolska.pl/');" id="buy-now-bottom" href="http://www.wonderpolska.pl/" role="button" class="buy-btn button pill-button orange-button animated-chevron-button">Zamów teraz&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                  </div>
                </div>
              </section>
            </section>
          </div>
        </div>
      </div>
      <div class="footer-container">
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="footer-link-section">
                {/menu:getFootMenu/}
              <div class="footer-newsletter">
                {/menu:getFormNewsletter/}
              </div>
            </div>
          </div>
          
          <div class="footer-bottom row">
            <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
            <div class="legal-stuff">
              
              <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
            </div>
          </div>
          <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
        </div>
      </div>
    </div>
    <script src="templates/frontend/makewonder_1.0/js/libraries.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/thriftFile.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/application.js"></script><!--[if lt IE 10]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.1.3/jquery.placeholder.min.js"></script>
    <script>
      $(function(){
        $('input, textarea').placeholder({ customClass: 'ie-placeholder' });  
      });
    </script><![endif]-->
    <script>
      $(function() {
        //- mobile devices are unlikely to support active scroll events, so just disable them
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          $('.scroll-section').addClass('mobile-scroll-section');
          return;  
        }
      
        var sections = $('.scroll-section section');
        var currentActiveSection;
        var scrollSectionActive = false;
        var ticking = false;
        /**
         * Store a reference to each section's scroll icon.
         */
        _.each(sections, function(section, index){
          section.scrollIcon = $('.scroller a')[index];
        });
      
        var getSectionOffsets = function(){
          _.each(sections, function(section, index) {
            section.top = $(section).offset().top;
          });
        };
      
        getSectionOffsets();
      
        var sectionIsOffTop = function(section){
          var documentHeight = $(window).height();
          return section.top < ($(window).scrollTop() + documentHeight);
        };
        /**
         * Toggle sub-sections active/inactive.
         */
        var toggleSubSectionActive = function(){
          var oldActiveSection = currentActiveSection;
          var scrollTop = $(window).scrollTop();
          currentActiveSection = _.findLast(sections, sectionIsOffTop) || sections[0];
          if (oldActiveSection !== currentActiveSection) {
            removeActiveFromSection(oldActiveSection);
            addActiveToSection(currentActiveSection);
          }
          ticking = false;
        };
      
        var addActiveToSection = function(section){
          $(section).addClass('active');
          $(section.scrollIcon).addClass('active');
        };
      
        var removeActiveFromSection = function(section){
          $(section).removeClass('active');
          $(section && section.scrollIcon).removeClass('active');
        };
        /**
         * Once our page scroll is past the top, toggle it active so the background image becomes fixed.
         */
        var toggleScrollSectionActive = function(){
          var top = $('.scroll-section').offset().top;
          if ($(window).scrollTop() > top && !scrollSectionActive) {
            scrollSectionActive = true;
            $('.scroll-section').addClass('active');
          } else if ($(window).scrollTop() <= top && scrollSectionActive) {
            scrollSectionActive = false;
            $('.scroll-section').removeClass('active');
          }
        }
        /**
         * Master scroll handler. Place toggling subsections under requestAnimationFrame, but not the handler for toggling the parent scroll section.
         */
        var toggleScrollyGuy = function() {
          toggleScrollSectionActive();
          if (ticking) {
            return;
          }
          requestAnimationFrame(toggleSubSectionActive);
          ticking = true;
        };
      
        $(window).scroll(toggleScrollyGuy);
        $(window).on('resize', getSectionOffsets);
      
      });
    </script>
    <script type="text/javascript" >
			pi = {}
			pi.campaign = "a749e38f556d5eb1dc13b9221d1f994f";
			pi.type = "MakewonderSG";
			pi.sitegroup = "MakewonderSG";
			pi.generateIpk = false;
			</script>
		<script type="text/javascript" src="templates/frontend/makewonder_1.0/js/pi.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004542796;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="templates/frontend/makewonder_1.0/js/slick.min.js"></script>
<script type="text/javascript" src="templates/frontend/makewonder_1.0/js/script.js"></script>
<script>
	$(document).ready(function(){
  $('.cSlick').slick({
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  autoplay: true,
	  autoplaySpeed: 7000,
	  dots: true,
	});
});
</script>
<style>
.home-page .packed-quotes {color: #666;background-color: #fff;}
.slick-dots {display: block;}
.slick-prev, .slick-next { top: 40px;}
.sl70 {	width: 69%;	display: inline-block;font-size: 1.3em;padding: 20px 0 5px 0;}
.sl30 {	width: 29%;	display: inline-block;text-align:right;vertical-align: top; padding: 40px 0 0 0;}
</style>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004542796/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body></html>