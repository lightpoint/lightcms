<!DOCTYPE html>
<html ng-app="teacherPortal" lang="pl">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="icon" type="image/x-icon">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="shortcun icon" type="image/x-icon">
    <meta description="Wonder Workshop Teacher's Portal">
    <meta charset="utf-8">
    <title>Portal dla nauczycieli</title>
    <base href="">
    <link href="css/teachers.css" rel="stylesheet">
    <link href="css/materialize.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="js/tabs.js"></script>
    <script src="js/materialize.min.js"></script>
  </head>
  <body>
    <div class="fade-in active">
      <div class="header-spacer"></div>
      <header ng-controller="headerController as headerCtrl">
        <div class="container">
          <div class="logo-wrap"><a href="https://www.makewonder.com/" class="logo">Wonder Workshop</a></div>
          {/menu:getNavTeachersTop/}
          <div class="header-sign-in-spacer">
            
          </div>
        </div>
      </header>
      <!-- uiView: undefined --><div autoscroll="false" class="ui-view ng-scope">
<section class="hero">
  <div class="container">
    <div class="row">
      <div class="span-5">
        <div class="text">
          <h1>Wprowadź roboty do swojej szkoły.</h1>
          <p>Dash i Dot wprowadzają dzieci od 5. roku życia w świat robotyki i programowania!</p>
        </div>
      </div>
      
    </div>
  </div>
</section>
<section class="junebug ng-scope" style="display: none;">
  <div class="container">
    <div class="row">
      <div class="item"><img src="img/book_icon.png" srcset="/assets/images/home/book_icon_2x.png 2x" alt="Curriculum Included">
        <div class="text">
          <h2 class="header">Gotowe lekcje w pakiecie</h2>
          <p>Udostępniamy nauczycielom gotowe lekcje z wykorzystaniem Dasha i Dota.
Lekcje w j. polskim będą systematycznie dodawane do zakładki ‘Lekcje’ (już niedługo publikacja pierwszej lekcji).<br/>Ponadto, pracujemy nad własnym podręcznikiem przygotowywanym przez polskich nauczycieli, który będzie spełniał wymogi zmian proponowanych przez MEN.</p>
        </div>
      </div>
      <div class="item"><img src="img/crayon_icon.png" srcset="/assets/images/home/crayon_icon_2x.png 2x" alt="Fun and Engaging">
        <div class="text">
          <h2 class="header">Zabawne i wciągające</h2>
          <p>Dash i Dot to osobistości, które rozpalają w dzieciach ciekawość i pewność siebie w trakcie odkrywania możliwości zarówno robota, jak i swoich. Poprzez zabawę robotami uczniowie rozwijają umiejętność współpracy, komunikacji i obsługi urządzeń cyfrowych.</p>
        </div>
      </div>
      <div class="item"><img src="img/cogs_icon.png" srcset="/assets/images/home/cogs_icon_2x.png 2x" alt="Easy to Use">
        <div class="text">
          <h2 class="header">Szybki start</h2>
          <p>Nie potrzebujesz wiedzy z programowania, aby móc zacząć nauczać dzieci z pomocą Dasha i Dota! Wystarczy ściągnąć 1 z 5 dostępnych darmowych aplikacji na tableta (najłatwiejsza jest aplikaca Go), naładować roboty i start! Początkowo pozwól uczniom oswoić się z robotami, by się z nimi zaprzyjaźniły. Uwzględnij czas na swobodną zabawę z Dashem i Dotem, zanim zaczniesz prowadzić pierwszą z przygotowanych przez nas lekcji.</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="coding-curriculum">
	<div class="container">
    <div class="row">
    	<h1>Dla kogo?</h1>
      <p>Dash i Dot to gotowe rozwiązanie edukacyjne dla dzieci w wieku od 5 do 12 lat.<br/>
Nasze roboty sprawdzą się w przedszkolach, szkołach podstawowych, gimnazjach, szkołach języka angielskiego oraz szkołach specjalnych</p>
    </div>
  </div>
</section>



<section class="getting-started">
	<div class="container">
    <div class="row">
    	<h1>Szerokie zastosowanie</h1>
    	<h2>W trakcie zajęć dydaktycznych</h2>
      <p align="left">Dash i Dot doskonale sprawdzają się jako uzupełnienie i wzbogacenie zajęć lekcyjnych, np. matematyki, j. polskiego, fizyki, geografii, muzyki. Roboty dają możliwość wizualizacji i praktycznego zastosowania opracowywanych zagadnień, dzięki czemu silnie angażują i motywują uczniów do poznawania nowych rzeczy.</p>
      <p align="left">Nauczyciele korzystają z robotów m.in. do nauki rozpoznawania liczb, obliczanie przebytej drogi i czasu, poznawania figur geometrycznych, poznawania mapy i kierunków świata, opracowywania krótkich opowiadań, poznawania nut na pięciolinii, prowadzenia zajęć z grafomotoryki i innych.</p>
    </div>
  </div>
</section>

<section class="home-quotes ng-scope" style="display:none;">
  <div class="container">
    <div style="padding-top: 25px;" class="row">
      <div style="position: relative; min-height: 1px" class="span-8">
       
      </div>
      <div class="span-4"><img src="img/quotes_dash.png" srcset="/assets/images/home/quotes_dash_2x.png 2x" alt="Get Started with Dash & Dot" class="dash-globe"></div>
    </div>
  </div>
  <div height="50px" style="background-color: #fff; height: 60px" class="row"><a target="_blank" role="button" ui-sref="map" class="button grey-outline-button pill-button" href="https://teachers.makewonder.com/map" style="width: 18em;">Znajdź szkoły używające Dasha i Dota!</a></div>
</section>


<section class="coding-curriculum">
  <div class="container">
    <div class="row">
      <h2>Nauka programowania</h2>
      <p align="left">Dasha i Dota programuje się z poziomu dostępnych dedykowanych aplikacji. Uczniowie w prosty, intuicyjny sposób nauczą się tworzyć algorytmy, budować pętle, warunki oraz korzystać ze zmiennych. Wybierz i pobierz aplikacje, które najlepiej spełnią potrzeby Twoje i Twoich uczniów.</p>
      <p align="left">Podstawową aplikacją, z której warto korzystać i na której opieramy nasze scenariusze lekcji jest BLOCKLY (łączenie bloków z poleceniami, podobnie jak w Scratchu). Ciekawą aplikacją do nauki programowania i poznania możliwości robotów jest też WONDER, która dodatkowo wymaga znajomości j. angielskiego na poziomie średniozaawansowanym. Natomiast dla mniejszych dzieci została stworzona aplikacja PATH, w której w prosty sposób programujemy robota poprzez narysowanie ścieżki na ekranie i nakładanie na niej obrazków z poleceniem.</p>
    </div>
  </div>
</section>

<section class="getting-started">
	<div class="container">
    <div class="row">
    	<h2>Swobodna zabawa</h2>
      <p align="left">Dash i Dot to responsywne roboty o wyjątkowo przyjaznym wyglądzie, dzięki czemu zarówno chłopcy, jak i dziewczynki uwielbiają się nimi bawić i traktują je jak swoich przyjaciół. Pozwól uczniom pobawić się robotami indywidualnie, w czasie wolnym lub zorganizuj zabawę grupową (np. wyścig robotów albo mecz piłki nożnej robotów).</p>
      <p align="left">Aplikacja GO pozwala zdalnie sterować robotami, nagrywać dźwięki i zmieniać kolory światełek robotów (np. oko Dasha). W aplikacji XYLO można komponować własne melodie, które Dash odegra na dzwonkach. Dysponując dodatkowymi akcesoriami do robotów, możliwości zabawy są praktycznie nieograniczone.</p>
    </div>
  </div>
</section>

<section class="giving" style="display: none;">
  <div class="container">
      <div class="slider">
    <ul class="slides">
      <li>
        <div class="caption center-align">
          <h1>Joanna Apanasewicz, "Mała Szkoła" Niepubliczna Szkoła Podstawowa w Koninie Żagańskim</h1>
          <h2>„Dzięki zajęciom z robotami dzieciaki mogą realizować swoje pomysły, co z kolei buduje ich poczucie sprawstwa i wpływu na proces nauczania. Polecam tę wspaniałą naukę poprzez zabawę. Radość, aktywność i zaangażowanie ucznia na lekcji to największe atuty takich lekcji.”</h2>
        </div>
      </li>
      <li>
        <div class="caption center-align">
          <h1>Jolanta Majkowska, Zespół Szkół Specjalnych w Kowanówku, Specjalni.pl</h1>
          <h3>„Roboty Dash i Dot rewelacyjnie sprawdziły się również w edukacji dziecka ze spektrum autyzmu. Dostarczają mu mniej bodźców niż człowiek, dlatego komunikowanie z nimi jest łatwiejsze, bezpieczniejsze i bez wątpienia ma działanie terapeutycznie.”</h3>
        </div>
      </li>
      <li>
        <div class="caption center-align">
          <h1>Monika Walkowiak, Szkoła Podstawowa im. Bolesława Krzywoustego w Kamieńcu Wrocławskim</h1>
          <h3>„Kiedy przyniosłam do klasy Dasha i Dota dzieciaki momentalnie dokonały ich animizacji. Jeden z uczniów próbował nawet poczęstować Dasha drugim śniadaniem, dziewczynki przytulały Dota czule do niego przemawiając. Wszyscy traktowali roboty tak, jakby były żywymi istotami. Nic dziwnego, że każdy uczeń chciał by towarzyszyły mu podczas lekcji.”</h3>
        </div>
      </li>
      
       <li>
        <div class="caption center-align">
          <h1>Zyta Czechowska, Zespół Szkół Specjalnych w Kowanówku, Specjalni.pl</h1>
          <h3>„Uczniowie z pomocą Dasha i Dota uczą się współpracy w grupie, słuchania siebie wzajemnie i współpracują w zakresie ustalania aktywności robotów.”</h3>
        </div>
      </li>
      
      <li>
        <div class="caption center-align">
          <h1>Anna Świć, Przedszkole nr 83 w Lublinie:</h1>
          <h3>„Dzieci bardzo chętnie sięgały po roboty, chociaż zdecydowanie częściej po Dota. W zabawie wykazywały się kreatywnością, budowały dla niego domki, całe miasteczka, bardzo delikatnie się z nim obchodziły. Dot wyzwalał w nich pozytywne uczucia, chętnie bawiły się nim też dzieci nieśmiałe, kontynuując następnie zabawę w większym gronie.”</h3>
        </div>
      </li>
    </ul>
  </div>
  </div>
</section>

<section class="cta-bar red-cta-bar">
  <div class="container">
    <h1>Ponad <strong>1500 szkół podstawowych</strong> z całego świata używa robotów Dash i Dot w trakcie zajęć lekcyjnych.</h1>
  </div>
</section>

<section class="home-quotes ng-scope">
  <div class="container">
    <div style="padding-top: 25px;" class="row">
      <div style="position: relative; min-height: 1px" class="span-8">
 				<div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Joanna Apanasewicz" src="img/person/JoannaApanasewicz.png"></div>
          <div class="quote-body">
            <div class="quote-quote ng-binding">Dzięki zajęciom z robotami dzieciaki mogą realizować swoje pomysły, co z kolei buduje ich poczucie sprawstwa i wpływu na proces nauczania. Polecam tę wspaniałą naukę poprzez zabawę. Radość, aktywność i zaangażowanie ucznia na lekcji to największe atuty takich lekcji.</div>
            <div class="author"> <span class="author-name">Joanna Apanasewicz</span>, "Mała Szkoła" Niepubliczna Szkoła Podstawowa w Koninie Żagańskim</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Jolanta Majkowska" src="img/person/JolantaMajkowska.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Roboty Dash i Dot rewelacyjnie sprawdziły się również w edukacji dziecka ze spektrum autyzmu. Dostarczają mu mniej bodźców niż człowiek, dlatego komunikowanie z nimi jest łatwiejsze, bezpieczniejsze i bez wątpienia ma działanie terapeutycznie.</div>
            <div class="author"> <span class="author-name">Jolanta Majkowska</span>, Zespół Szkół Specjalnych w Kowanówku, Specjalni.pl</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Monika Walkowiak" src="img/person/MonikaWalkowiak.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Kiedy przyniosłam do klasy Dasha i Dota dzieciaki momentalnie dokonały ich animizacji. Jeden z uczniów próbował nawet poczęstować Dasha drugim śniadaniem, dziewczynki przytulały Dota czule do niego przemawiając. Wszyscy traktowali roboty tak, jakby były żywymi istotami. Nic dziwnego, że każdy uczeń chciał by towarzyszyły mu podczas lekcji.</div>
            <div class="author"> <span class="author-name">Monika Walkowiak</span>, Szkoła Podstawowa im. Bolesława Krzywoustego w Kamieńcu Wrocławskim</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Zyta Czechowska" src="img/person/ZytaCzechowska.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Uczniowie z pomocą Dasha i Dota uczą się współpracy w grupie, słuchania siebie wzajemnie i współpracują w zakresie ustalania aktywności robotów.</div>
            <div class="author"> <span class="author-name">Zyta Czechowska</span>, Zespół Szkół Specjalnych w Kowanówku, Specjalni.pl</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Anna Świć" src="img/person/AnnaSwic.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Dzieci bardzo chętnie sięgały po roboty, chociaż zdecydowanie częściej po Dota. W zabawie wykazywały się kreatywnością, budowały dla niego domki, całe miasteczka, bardzo delikatnie się z nim obchodziły. Dot wyzwalał w nich pozytywne uczucia, chętnie bawiły się nim też dzieci nieśmiałe, kontynuując następnie zabawę w większym gronie.</div>
            <div class="author"> <span class="author-name">Anna Świć</span>, Przedszkole nr 83 w Lublinie</div>
          </div>
        </div>
 					
      	<a target="_blank" role="button" ui-sref="map" class="button grey-outline-button pill-button" href="https://teachers.makewonder.com/map" style="width: 18em;">Znajdź szkoły używające Dasha i Dota!</a>
      	<p>&nbsp;</p>
      	<p>Prowadzisz zajęcia z Dashem i Dotem a nie ma Twojej szkoły na mapie?<br/>Zgłoś ten fakt do nas: <a href="mailto:edukacja@makewonder.pl">edukacja@makewonder.pl</a></p>
      </div>
      <div class="span-4"><img src="img/quotes_dash.png" alt="" class="dash-globe"></div>
    </div>
  </div>
  <div height="50px" style="background-color: #fff; height: 60px" class="row"></div>
</section>

<section class="coding-curriculum">
  <div class="container">
    <div class="row">
    	<h1>Gotowe lekcje w pakiecie</h1>
      <p align="left">Szkołom udostępniamy gotowe scenariusze lekcji z wykorzystaniem Dasha i Dota. Scenariusze są opracowane przez nauczycieli dla nauczycieli. Scenariusze składają się standardowo z 2 części: część wprowadzająca do konkretnego zagadnienia (np. ułamki na matematyce) oraz część z programowania robotów. Każdy scenariusz lekcji spełnia określone cele z Podstawy Programowej.</p>
    </div>
  </div>
</section>

<section class="giving" style="display: none;">
	<div class="container">
    
    	<h1 align="center"><strong>WYPRÓBUJ ZA DARMO</strong> (oferta tylko dla szkół)</h1>
      <p align="left">Wciąż się wahasz? Zapraszamy do BEZPŁATNEGO wypróbowania robotów Dash i Dot w Twojej klasie! Oferujemy wypożyczenie 1 zestawu robotów Dash i Dot na okres 14 dni</p>
      <p align="left">Zarejestruj się na Portalu dla Nauczycieli i zapytaj o pierwszy wolny termin, w którym dostępne są roboty</p>
    
  </div>
</section>

<section class="getting-started" style="display: none;">
  <div class="container">
    <h1>Masz pytania?</h1>
    <p align="left">Zapraszamy do kontaktu:<br/>mail: <a href="mailto:edukacja@makewonder.pl">edukacja@makewonder.pl</a><br/>tel: 534-222-422
    </p>
  </div>
</section>

<section class="signup-cta ng-scope" style="display: none;">
  <div class="container">
    <h1>Zaproś Dasha i Dota do swojej klasy.</h1>
    <button type="button" ng-click="signupCtrl.signUp()" class="orange-button pill-button">Zarejestruj się</button>
  </div>
</section>

</div>
<div class="container">
    <div class="footer-top">
        <div class="row">
          <div class="footer-link-section">
            {/menu:getFootMenu/}
          <div class="footer-newsletter">
            {/menu:getFormNewsletter/}
          </div>
        </div>
      </div>
      
      <div class="footer-bottom row">
        <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
        <div class="legal-stuff">
          
          <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
        </div>
      </div>
      <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
    </div>
	</div>
</div>
{/school:schoolForm/}  
<script src="js/slider.js"></script>
<script>
$(document).ready(function(){
	$('.modal-trigger').leanModal();
	$('ul.tabs').tabs();
	$('.slider').slider({height: '400px'});
});
</script>
</body></html>