<!DOCTYPE html>
<html lang="pl" ng-app="makeWonder.apps">
  <head>
  	{/metadata:aplikacje_path/}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="templates/frontend/makewonder_1.0/css/style2.css" rel="stylesheet">
    <link href="http://www.makewonder.com/favicon.ico" rel="icon" type="image/x-icon">
		<link href="http://www.makewonder.com/favicon.ico" rel="shortcun icon" type="image/x-icon"><!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
    <meta content="True" name="HandheldFriendly">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0 user-scalable=no">
    <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-70029657-1', 'auto');
ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
 <script>
 
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
 document,'script','//connect.facebook.net/en_US/fbevents.js');

 fbq('init', '1495186814109494');
 fbq('track', "PageView");</script>
 <noscript><img height="1" width="1" style="display:none"
 
src="https://www.facebook.com/tr?id=1495186814109494&ev=PageView&noscript=1"
 /></noscript>
 <!-- End Facebook Pixel Code --> 
 <!--google-->
 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-70029657-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 934334004;
    w.google_conversion_label = "75i4CLK7oWMQtJzDvQM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script> 
  </head>
  <body>
    <div class="main-page">
      <div class="mobile-nav ng-cloak">
        <div class="nav-items">
          {/menu:getMobileNav/}
        </div>
      </div>
      <div class="wrap">
        <noscript>
          <div class="notice-bar">
            <div class="container">Please note: This website requires JavaScript to be enabled in order to function properly. Please <a href="http://www.enable-javascript.com/" target="_blank">enable your JavaScript</a> and refresh the page.</div>
          </div>
        </noscript><!--[if lt IE 10 ]>
        <div ng-hide="dismissNotice" class="notice-bar">
          <div class="container">This website has limited support on Internet Explorer versions 9 and below. We recommend using the latest version of <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Chrome</a>, <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a>, <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">Internet Explorer </a>or <a href="http://support.apple.com/downloads/#safari" target="_blank">Safari</a>.<br><br>
            <button ng-click="dismissNotice = true" class="pill-button orange-button sm-button">Got it</button>
          </div>
        </div><![endif]-->
        <div ng-controller="headerController" scroll-off-top="scrolling" style="" ng-class="{ 'active': user }" header-2 class="header-2"><a ng-click="toggleMobileNav($event)" class="hamburger-icon"><i class="fa fa-bars"></i></a>
          <div class="container">
            <nav class="wat-header active">
              <ul>{/menu:getNav/}</ul>
            </nav>
          </div><img src="templates/frontend/makewonder_1.0/img/w_logo.png" srcset="templates/frontend/makewonder_1.0/img/w_logo_2x.png 2x" alt="Wonder Workshop" class="mobile-logo">
        </div>
        <div class="main">
          <section ng-controller="appPageController" ios-app-id="" class="app-page-section">
            <div class="top-bar path-top-bar"></div>
            <div class="masthead app-page-masthead path-masthead">
              <div class="container">
                <div class="masthead-text">
                  <div class="back-link"><a href="aplikacje.html"><i class="fa fa-chevron-left"></i> Powrót do aplikacji</a></div>
                  <div class="masthead-title"><img src="templates/frontend/makewonder_1.0/img/appicon_path.png" srcset="templates/frontend/makewonder_1.0/img/appicon_path_2x.png 2x" alt="path app icon" class="app-icon-image">
                    <h1>Path</h1>
                  </div>
                 {/articles:aplikacja_path/}
                </div>
              </div>
            </div>
            <div class="app-page-body path-body">
              <section class="details-section">
                <div class="container">
                  <div class="details-body">
                    <div class="row">
                      <div class="half"> 
                        <h3>Pobierz aplikację:</h3>
                        <div class="download-buttons">
                          <div class="download-button"><a href="https://itunes.apple.com/us/app/id928643204" target="_blank" role="button"><img src="templates/frontend/makewonder_1.0/img/appstore_apple.svg" alt="iOS App Store icon"></a>
                            <div class="ratings-section">
                              <div class="ratings-stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
                              </div><span class="rating-count">30 Ratings</span>
                            </div>
                          </div>
                          <div class="download-button"><a href="https://play.google.com/store/apps/details?id=com.makewonder.pathandroid" target="_blank" role="button"><img src="templates/frontend/makewonder_1.0/img/appstore_google.svg" alt="Google Play Store icon"></a>
                            <div class="ratings-section">
                              <div class="ratings-stars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>
                              </div><span class="rating-count">35 Ratings</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="half">
                        <h3>Współpracuje z</h3>
                        <div class="app-page-robot-icon"><a href="/dash"><img src="templates/frontend/makewonder_1.0/img/Dash.png" alt="Dash Icon"></a>
                          <div class="robot-name">Dash</div>
                        </div>
                      </div>
                    </div>
                    <hr>
                    {/articles:informacje_edukacyjne_path/}                   
                    <hr>
                  </div>
                </div>
              </section>
              <section class="gallery-section">
                <div class="container">
                  <h3>Galeria</h3>
                  <div class="row">
                    <div class="screenshots">
                      <carousel dots-container=".carousel-controls" arrows-container=".carousel-controls" after-change="changeCaption(currentSlide)" class="app-page-screenshots">
                        <div class="screenshot"><img src="templates/frontend/makewonder_1.0/img/1path.png"></div>
                        <div class="screenshot"><img src="templates/frontend/makewonder_1.0/img/2path.png" class="ng-cloak"></div>
                        <div class="screenshot"><img src="templates/frontend/makewonder_1.0/img/3path.png" class="ng-cloak"></div>
                        <div class="screenshot"><img src="templates/frontend/makewonder_1.0/img/4path.png" class="ng-cloak"></div>
                      </carousel>
                    </div>
                    <div class="captions">
                      <div ng-show="currentSlide === 0" class="caption">Narysuj ścieżkę, po której chcesz poprowadzić Dasha, używając pojedynczej 'linii' kodu.</div>
                      <div ng-show="currentSlide === 1" class="caption ng-cloak">Do ścieżki dodaj węzły, dzięki którym Dash zyska nowe umiejętności.</div>
                      <div ng-show="currentSlide === 2" class="caption ng-cloak">Wciśnij przycisk na głowie Dasha lub kliknij robota na ekranie, aby Dash ruszył w drogę!</div>
                      <div ng-show="currentSlide === 3" class="caption ng-cloak">Odblokuj 4 motywy i stwórz swoją własną przygodę!</div>
                      <div class="carousel-controls"></div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="other-apps">
                <div class="container">
                  <h3>Pozostałe aplikacje</h3>
                  <div class="row"><a href="aplikacke_wonder.html" class="tile span-3 wonder-tile">
                      <h2 style="margin: 0;">Wonder</h2></a><a href="aplikacje_go.html" class="tile span-3 go-tile">
                      <h2 style="margin: 0;">Go</h2></a><a href="aplikacje_xylo.html" class="tile span-3 xylo-tile">
                      <h2 style="margin: 0;">Xylo</h2></a><a href="aplikacje_blockly.html" class="tile span-3 blockly-tile">
                      <h2 style="margin: 0;">Blockly</h2></a>
                  </div>
                </div>
              </section>
            </div>
          </section>
        </div>
      </div>
      <div ng-controller="footerController" class="footer-container">
        <div class="container">
          <div class="footer-top">
            <div class="row">
               <div class="footer-link-section">
                {/menu:getFootMenu/}
              <div class="footer-newsletter">
                {/menu:getFormNewsletter/}
              </div>
            </div>
          </div>
          
          <div class="footer-bottom row">
            <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
            <div class="legal-stuff">
              
              <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
            </div>
          </div>
          <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
        </div>
      </div>
    </div>
    <script src="templates/frontend/makewonder_1.0/js/libraries.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/thriftFile.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/application.js"></script>
    <script type="text/javascript" src="templates/frontend/makewonder_1.0/js/script.js"></script>
    <!--[if lt IE 10]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.1.3/jquery.placeholder.min.js"></script>
    <script>
      $(function(){
        $('input, textarea').placeholder({ customClass: 'ie-placeholder' });  
      });
    </script><![endif]-->
    <div class="pixels">
      <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=776859882372153&amp;amp;amp;ev=PixelInitialized"></noscript>
      <script src="//platform.twitter.com/oct.js" type="text/javascript" async></script><img src="https://sp.analytics.yahoo.com/spp.pl?a=100057833829&amp;.yp=403878" height="1" width="1" alt="" style="display: none;">
    </div>
    <script>
      // localStorage shim
      (function(e){if(!e){var t={},n;window.localStorage={setItem:function(e,n){return t[e]=String(n)},getItem:function(e){return t.hasOwnProperty(e)?t[e]:n},removeItem:function(e){return delete t[e]},clear:function(){return t={}}}}})(function(){try{return"localStorage"in window&&window.localStorage!=null}catch(e){return false}}())
      window.stage = "prod";
    </script>
    <script>
      if (navigator.userAgent.match(/android/i)) {
        $('html').addClass('android');
      }
      
    </script>
    <script type="text/javascript" >
			pi = {}
			pi.campaign = "a749e38f556d5eb1dc13b9221d1f994f";
			pi.type = "MakewonderSG";
			pi.sitegroup = "MakewonderSG";
			pi.generateIpk = false;
			</script>
		<script type="text/javascript" src="templates/frontend/makewonder_1.0/js/pi.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004542796;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004542796/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>