$(document).ready(function() {
	$('.nav-dropdown').click(function(){
		$('.nav-dropdown').not(this).removeClass('active')
		$(this).toggleClass('active');
	});
	
	$('.my-slider').unslider({
		autoplay: true,
		delay: 12000
	});
	
	$('.fading-slider').unslider({
		animation: 'fade',
		autoplay: true,
		arrows: false,
		delay: 6000
	});	
										
});