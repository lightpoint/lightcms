angular.module("makeWonder", ["piToolbelt", "angular-carousel", "ngTouch", "ngSanitize", "ui.router"]).run(["config", "pacmanService", "facebook", "google", "$window", "user", "$rootScope", "$state",
    function(e, t, n, r, o, i, a, s) {
        var u, c;
        return a.$on("$stateChangeStart", function(e, t, n) {
            return t.redirectTo ? (e.preventDefault(), s.go(t.redirectTo, n)) : void 0
        }), "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.register({
            mw_version: Cookies.get("mw_version")
        }), u = _.reduce(e, function(e, t, n) {
            return /(ORDERS|PACMAN|STORE|PYRAMID|REFERRALS)/.test(n) && (e[t] = "/xdomain_proxy"), e
        }, {}), xdomain.slaves(u), n.setup(e.FACEBOOK_APP_ID), r.setup({
            clientId: e.GOOGLE_CLIENT_ID,
            apiKey: e.GOOGLE_API_KEY
        }), t.registerUrl(e.PACMAN_URL), "prod" === o.stage && console.log('__      __                   _                           __      __                 _              _                _ __           \n \\ \\    / / ___    _ _     __| |    ___      _ _     o O O\\ \\    / / ___      _ _   | |__    ___   | |_      ___    | \'_ \\    o O O \n  \\ \\/\\/ / / _ \\  | \' \\   / _` |   / -_)    | \'_|   o      \\ \\/\\/ / / _ \\    | \'_|  | / /   (_-<   | \' \\    / _ \\   | .__/   o      \n   \\_/\\_/  \\___/  |_||_|  \\__,_|   \\___|   _|_|_   TS__[O]  \\_/\\_/  \\___/   _|_|_   |_\\_\\   /__/_  |_||_|   \\___/   |_|__   TS__[O] \n _|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""| {======|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""|_|"""""| {======| \n "`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'./o--000\'"`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'"`-0-0-\'./o--000\' \n    \n Party on, Dot! Party on, Dash!\n\n    '), Cookies.get("mw_version") ? (c = "1" === Cookies.get("mw_version") ? "site 3.0" : "site 2.0", "undefined" != typeof mixpanel && null !== mixpanel ? mixpanel.register("site_version", c) : void 0) : void 0
    }
]), angular.module("makeWonder.apps", ["makeWonder"]), angular.module("makeWonder.main", ["makeWonder", "ngAnimate"]).run(["pixelMachine",
    function(e) {
        return e.track("pageView")
    }
]);
var slice = [].slice;
angular.module("makeWonder.orderDash", ["makeWonder"]).run(["$rootScope",
    function(e) {
        return e.$on("$stateChangeError", function() {
            var t, n, r;
            return t = 2 <= arguments.length ? slice.call(arguments, 0, r = arguments.length - 1) : (r = 0, []), n = arguments[r++], $exceptionHandler(n), e.routeLoaded = !0
        }), e.$on("$stateChangeSuccess", function() {
            return e.routeLoaded = !0
        })
    }
]);
var slice = [].slice;
angular.module("makeWonder.playIdeas", ["makeWonder"]).run(["pixelMachine", "$rootScope", "$exceptionHandler", "$state",
    function(e, t, n, r) {
        return e.track("pageView"), t.$on("$stateChangeError", function() {
            var e, r, o;
            return e = 2 <= arguments.length ? slice.call(arguments, 0, o = arguments.length - 1) : (o = 0, []), r = arguments[o++], n(r), t.routeLoaded = !0
        }), t.$on("$stateChangeSuccess", function() {
            return t.routeLoaded = !0
        }), t.$state = r
    }
]), angular.module("makeWonder.setup", ["makeWonder"]);
var slice = [].slice;
angular.module("makeWonder.userAccount", ["makeWonder"]).run(["$rootScope", "$window", "$exceptionHandler", "$state",
    function(e, t, n, r) {
        return e.$on("userLoggedOut", function() {
            return r.go("login")
        }), e.$on("userLoggedIn", function() {
            return r.go("account")
        }), e.$on("$stateChangeError", function() {
            var t, r, o;
            return t = 2 <= arguments.length ? slice.call(arguments, 0, o = arguments.length - 1) : (o = 0, []), r = arguments[o++], n(r), e.routeLoaded = !0
        }), e.$on("$stateChangeSuccess", function() {
            return e.routeLoaded = !0
        })
    }
]), angular.module("makeWonder").constant("config", {
    FACEBOOK_APP_ID: "315999128545112",
    DEV_PORTAL_URL: "https://developer.makewonder.com",
    TEACHER_PORTAL_URL: "https://teachers.makewonder.com",
    PACMAN_URL: "https://pacman.makewonder.com",
    ORDERS_URL: "https://orders.makewonder.com",
    STORE_URL: "https://store.makewonder.com",
    LOGIN_URL: "https://login.makewonder.com",
    CELERY_URL: "https://celery.makewonder.com",
    ADMIN_DASH_URL: "http://admin.makewonder.com",
    PYRAMID_URL: "https://pyramid.makewonder.com",
    REFERRALS_URL: "https://referrals.makewonder.com",
    MAIN_SITE_URL: "https://www.makewonder.com",
    MAILCHIMP_NEWSLETTER_LIST_ID: "508454a804",
    MAILCHIMP_MAGAZINE_LIST_ID: "219790b8c0",
    BLOG_URL: "http://blog.makewonder.com",
    HELP_URL: "http://help.makewonder.com",
    CLOUDFRONT_URL: "https://d2eawk67eat224.cloudfront.net",
    GOOGLE_CLIENT_ID: "763461670984-1dhdq42ih0dgqirttm198v0h8phmca92.apps.googleusercontent.com",
    GOOGLE_API_KEY: "0c119fb5d0ecafb7b2896cfb52dae04882ec0382",
    PORT: 7e3,
    DOMAIN: ".makewonder.com",
    BRAINTREE_TOKEN: "MIIBCgKCAQEA7QkOAi0wUMHurPs1HanH4TiLMbci2guG0nMukeE1xiohRHAmEQsca9GgToQ2eBpyWA4r/33l692OVRGqKaz93MSdeekRXeV0atZ9WI/HzQJu18QZ0q5NCFVvF1DkFSv+gUsbBGVMorj+4GIFxisUYOGigvXzx6RiBeecVcQOOirFSpriEYPDQyGLbdnfUXIzSlpE5y4PZzYLhbgE2/IrjLfhb80HFnYQ5z891MHZDA56Ra8zDfb5hj9swJS52BwaEc2Q1EyJA8z/WBIRJl05vjjYsxdbbbnCekqpfOT/Vv0vLOqj1ZZZM4XLmkQcJY9+D3VyU9ntUoX1PMjthMWJfwIDAQAB",
    TWITTER_API_KEY: "IoXekeuuuY5ik88PQUChOWBpi",
    MIXPANEL_TOKEN: "b43dbb2162c0c694c31559e80cb6aeca",
    MEDIAKIT_URL: "https://drive.google.com/folderview?id=0B0WfNM4WtK2oSG5uRVpRc2UzRnM&usp=sharing",
    PLAY_IDEAS_CACHE_EXPIRATION: 9e5
}), angular.module("makeWonder").config(["crossStorageProvider", "config", "$urlMatcherFactoryProvider",
    function(e, t, n) {
        return e.init(t.MAIN_SITE_URL + "/hub.html"), n.strictMode(!1)
    }
]), angular.module("makeWonder").controller("footerController", ["$scope", "config", "$http", "$window", "thisPersonIsOverAge",
    function(e, t, n, r, o) {
        var i;
        return e.storeUrl = t.STORE_URL, e.devPortalUrl = t.DEV_PORTAL_URL, e.newsletterSignedUp = r.Cookies.get("newsletterSignedUp"), i = function() {
            return Cookies.set("newsletterSignedUp", "1", {
                path: "/",
                domain: t.domain
            }), e.newsletterSignedUp = "1"
        }, r.localStorage.newsletterSignedUp && i(), e.notSignedUpMessage = "Stay in the know about new wonder products, events, and news.", e.signedUpMessage = "Thank you for signing up for our newsletter!", e.joinNewsletter = function() {
            return e.newsLetterBot ? void 0 : o(e.dob, 13) ? (e.loading = !0, n.post("/newsletter", {
                email: e.newsletterEmail,
                source: "footer"
            }).then(function() {
                return e.loading = !1, delete e.newsletterEmail, delete e.newsLetterError, i()
            }, function(t) {
                return 409 === t.status ? i() : e.newsLetterError = "There was an error subscribing your email."
            })["finally"](function() {
                return e.loading = !1, "undefined" != typeof mixpanel && null !== mixpanel ? mixpanel.track("signup newsletter", {
                    location: "footer"
                }) : void 0
            })) : (e.under13 = !0, void 0)
        }, r.mixpanel && mixpanel.track_links ? (mixpanel.track_links(".footer-link-section a", "Click Footer Link", function(e) {
            return {
                link: $(e).text()
            }
        }), mixpanel.track_links(".social-links a", "Click Footer Link", function(e) {
            return {
                link: $(e).attr("class")
            }
        }), mixpanel.track_links(".legal-stuff a", "Click Footer Link", function(e) {
            return {
                link: $(e).text()
            }
        }), mixpanel.track_links("#amazon a", "Click Amazon Link")) : void 0
    }
]), angular.module("makeWonder").controller("headerController", ["$scope", "shoppingCart", "$rootScope", "config", "user", "pyramidService", "pacmanService", "$q", "Modal", "$timeout", "$attrs",
    function(e, t, n, r, o, i, a, s, u, c) {
        var l, d, p, h, f;
        return e.toggleMobileNav = function() {
            return n.$broadcast("toggle nav")
        }, e.shoppingCart = t, e.storeUrl = r.STORE_URL, e.devPortalUrl = r.DEV_PORTAL_URL, p = void 0, e.openLoginModal = function(t, n) {
            return null == n && (n = {}), t.preventDefault(), p = new u({
                template: '<login-modal class="modal-body" on-success="login(token)" disable-signup ' + (n.resetPassword ? "reset-password" : "") + "></login-modal>",
                controller: e,
                center: !0,
                scope: e,
                darkenBackground: !0
            }), p.open()
        }, e.login = function() {
            return function(e) {
                return o.login(e), p.success(), n.$broadcast("userLoggedIn")
            }
        }(this), l = function(e) {
            var t;
            return t = $(e.target), t.is(".user-menu") || t.parents().is(".user-menu") ? void 0 : d()
        }, d = function() {
            return e.userMenuActive = !1, e.$$phase || e.$apply(), $("body").off("click", l), $("body").off("keydown", l)
        }, e.openUserMenu = function() {
            return e.userMenuActive = !0, c(function() {
                return $("body").on("click", l), $("body").on("keydown", l)
            })
        }, e.signOut = function() {
            return o.destroy(), d(), n.$broadcast("userLoggedOut")
        }, o.identify().then(function() {
            return e.user = o
        }).then(function() {
            return i.listAccounts(a.getToken(), o.accessKey, function() {})
        }).then(function(t) {
            return e.hasPyramidAccount = !! t.length
        })["finally"](function() {
            return e.user = o
        }), null != (h = window.mixpanel) && h.track_links("header a", "Click Header Link", function(e) {
            return {
                link: $(e).text()
            }
        }), null != (f = window.mixpanel) && f.track_links("a.nav-link", "Click Header Link", function(e) {
            return {
                link: $(e).text()
            }
        }), n.$on("open login", e.openLoginModal)
    }
]), angular.module("makeWonder").controller("mobileNavController", ["$scope", "$rootScope", "$window", "$timeout", "$q", "config",
    function(e, t, n, r, o, i) {
        var a;
        return t.navOpen = !1, a = function() {
            var e;
            return e = o.defer(), t.navOpen = !t.navOpen, r(function() {
                return e.resolve()
            }, 350), e.promise
        }, e.$on("toggle nav", a), e.goTo = function(e) {
            return a().then(function() {
                return n.location.assign(e)
            })
        }, e.storeUrl = i.STORE_URL, e.devPortalUrl = i.DEV_PORTAL_URL
    }
]), angular.module("makeWonder").controller("playHomeController", ["setupInfo", "$scope", "scrollToBottom",
    function(e, t, n) {
        return t.setupInfo = e, n.attach("play home"), t.$on("$destroy", n.cleanUp)
    }
]), angular.module("makeWonder").controller("signupController", ["$scope", "config", "$http", "$window", "thisPersonIsOverAge",
    function(e, t, n, r, o) {
        var i;
        return e.messages = {
            isSignedUp: "Thank you for joining the Wonder Workshop Newsletter!",
            isNotSignedUp: "Be the first to know about new products, events and promotions!"
        }, e.newsletterSignedUp = r.Cookies.get("newsletterSignedUp"), e.error = !1, i = function() {
            return Cookies.set("newsletterSignedUp", "1", {
                path: "/",
                domain: t.domain
            }), e.newsletterSignedUp = "1", e.error = !1
        }, r.localStorage.newsletterSignedUp && i(), e.signupNewsletter = function() {
            return e.bot ? void 0 : o(e.dob, 13) ? (e.loading = !0, n.post("/newsletter", {
                email: e.email,
                source: "hero"
            }).then(function() {
                return e.loading = !1, delete e.email, i()
            }, function(t) {
                return 409 === t.status ? i() : e.error = "There was an error subscribing your email."
            })["finally"](function() {
                return e.loading = !1, "undefined" != typeof mixpanel && null !== mixpanel ? mixpanel.track("signup newsletter", {
                    location: "hero"
                }) : void 0
            })) : (e.under13 = !0, void 0)
        }
    }
]), angular.module("makeWonder").directive("carousel", [
    function() {
        return {
            restrict: "E",
            transclude: !0,
            replace: !0,
            scope: {
                dots: "=",
                speed: "=",
                afterChange: "&"
            },
            template: function(e, t) {
                return t.dotsContainer ? '<div class="carousel-container">\n  <div class="carousel-slides" ng-transclude>\n  </div>\n</div>' : '<div class="carousel-container">\n  <div class="carousel-slides" ng-transclude>\n  </div>\n  <div class="control-container">\n    <div class="control-dots-container"><div>\n  </div>\n</div>'
            },
            link: function(e, t, n) {
                var r, o;
                return r = t.find(".control-dots-container"), o = {
                    dots: n.dots || !0,
                    speed: e.speed || 500,
                    appendDots: $(n.dotsContainer || r),
                    appendArrows: $(n.arrowsContainer || r),
                    onAfterChange: function(t, n) {
                        return e.afterChange({
                            slick: t,
                            currentSlide: n
                        }), e.$apply()
                    }
                }, null != n.adaptiveHeight && (o.adaptiveHeight = !0), null != n.noArrows && (o.arrows = !1), t.find(".carousel-slides").slick(o)
            }
        }
    }
]);
var isAndroid;
isAndroid = function() {
    return navigator.userAgent.toLowerCase().indexOf("android") > -1
}, angular.module("makeWonder").directive("clickFullScreenVideo", ["FullScreenVideo", "$window", "pixelMachine",
    function(e, t, n) {
        return {
            restrict: "A",
            link: function(r, o, i) {
                return o.click(function() {
                    var r;
                    return n.track("play youtube video", {
                        video: i.videoSrc
                    }), t.innerWidth >= 768 || !isAndroid() ? (r = new e(i.videoSrc), r.open()) : t.location.assign("https://m.youtube.com/watch?v=" + i.videoSrc)
                })
            }
        }
    }
]), angular.module("makeWonder").directive("dobMask", function() {
    return {
        restrict: "A",
        require: "ngModel",
        link: function(e, t, n, r) {
            var o;
            return o = function(e) {
                var t, n, r, o;
                return r = e.split("/").map(function(e) {
                    return~~ e
                }), n = r[0], t = r[1], o = r[2], n && t && o
            }, t.inputmask("99/99/9999", {
                placeholder: "MM/DD/YYYY"
            }).attr("placeholder", "MM/DD/YYYY"), e.$watch(n.ngModel, function(e) {
                return e ? r.$setValidity("date", !! o(e)) : r.$setValidity("date", !1)
            })
        }
    }
}), angular.module("makeWonder").directive("markdown", ["$compile",
    function() {
        return {
            restrict: "A",
            scope: {
                markdown: "&"
            },
            link: function(e, t) {
                var n, r;
                return (r = e.markdown()) ? (n = marked(r), t.html(n)) : void 0
            }
        }
    }
]), angular.module("makeWonder").directive("maskCurrency", [
    function() {
        return {
            restrict: "A",
            require: "^ngModel",
            link: function(e, t) {
                return t.inputmask("99-999-999")
            }
        }
    }
]);
var bind = function(e, t) {
    return function() {
        return e.apply(t, arguments)
    }
};
angular.module("makeWonder").directive("mobileNav", ["$rootScope", "config", "$timeout", "$window", "$q",
    function(e, t, n, r, o) {
        var i;
        return i = function() {
            function t(t) {
                this.$element = t, this.escapeListener = bind(this.escapeListener, this), this.mask = $("<div class='mobile-nav-mask'></div>"), this.mask.css({
                    width: "100%",
                    height: "100%",
                    "background-color": "rgba(0,0,0,0.5)",
                    position: "fixed",
                    top: "0",
                    left: "0",
                    "z-index": "500"
                }), e.$on("toggle nav", this.open.bind(this))
            }
            return t.prototype.open = function() {
                return this.$element.css({
                    transform: "translateX(0)"
                }), $("body").append(this.mask), n(function(e) {
                    return function() {
                        return $("body").on("touchstart click", e.escapeListener)
                    }
                }(this), 0)
            }, t.prototype.close = function() {
                var e;
                return this.$element.css({
                    transform: "translateX(-100%)"
                }), this.mask.remove(), $("body").off("touchstart click", this.escapeListener), e = o.defer(), n(function() {
                    return e.resolve()
                }, 350), e.promise
            }, t.prototype.escapeListener = function(e) {
                return $(e.target).is(this.$element) || $(e.target).parents().is(this.$element) ? void 0 : $(e.target).is(this.mask) ? (e.preventDefault(), this.close()) : void 0
            }, t
        }(), {
            restrict: "C",
            link: function(e, n) {
                var o;
                return o = new i(n), e.goTo = function(e) {
                    return o.close().then(function() {
                        return r.location.assign(e)
                    })
                }, e.storeUrl = t.STORE_URL, e.homeUrl = t.DEV_PORTAL_URL
            }
        }
    }
]), angular.module("makeWonder").directive("referralBox", [
    function() {
        return {
            restrict: "E",
            replace: !0,
            templateUrl: "/templates/shared/referral_box.html",
            controller: "ReferralBoxController as ctrl"
        }
    }
]), angular.module("makeWonder").directive("scrollFixTop", ["$window", "$timeout",
    function(e, t) {
        var n, r, o, i, a, s, u;
        return n = 0, u = !1, i = !1, o = function(e, t) {
            return i ? void 0 : (t.css({
                display: "block"
            }), e.css({
                position: "fixed",
                top: 0,
                left: 0,
                right: 0
            }), e.addClass("fixed-top"), i = !0)
        }, s = function(e, t) {
            return i ? (t.css({
                display: "none"
            }), e.css({
                position: "relative"
            }), e.removeClass("fixed-top"), i = !1) : void 0
        }, r = function(e) {
            var t;
            return t = $('<div class="placeholder"></div>'), t.css({
                height: e.outerHeight(),
                position: "relative",
                width: "100%",
                display: "none"
            }), t.insertAfter(e), e.css({
                "z-index": 100
            }), t
        }, a = function(t, n, r) {
            var i, a;
            return u = !1, a = $(e.document).scrollTop(), i = r - a, 0 >= i && window.innerWidth > 768 ? o(t, n) : s(t, n)
        }, {
            restrict: "A",
            link: function(o, s) {
                var c, l, d;
                return n++, i = !1, u = !1, l = s.offset().top, c = r(s), t(function() {
                    return a(s, c, l)
                }, 15), d = function() {
                    return u ? void 0 : (requestAnimationFrame(a.bind(null, s, c, l)), u = !0)
                }, e.requestAnimationFrame && e.innerWidth > 768 && $(e.document).scroll(d), o.$on("$destroy", function() {
                    return e.requestAnimationFrame ? $(e.document).off("scroll", d) : void 0
                })
            }
        }
    }
]), angular.module("makeWonder").directive("scrollOffTop", ["$window",
    function(e) {
        return {
            restrict: "A",
            link: function(t, n, r) {
                var o, i, a;
                return i = !1, a = function() {
                    return e.pageYOffset > (r.scrollOffTopOffset || 0) ? n.addClass(r.scrollOffTop) : n.removeClass(r.scrollOffTop), i = !1
                }, a(), o = function() {
                    return i ? void 0 : (e.requestAnimationFrame(a), i = !0)
                }, $(e).scroll(o), t.$on("$destroy", function() {
                    return $(e).off("scroll", o)
                })
            }
        }
    }
]), angular.module("makeWonder").directive("scrollRight", function() {
    return {
        restrict: "A",
        link: function(e, t, n) {
            var r, o;
            return o = t.find(n.target), r = t.find(n.control), r.click(function() {
                return o.parent().animate({
                    scrollLeft: Math.abs(o.offset().left - n.scrollAmount)
                }, n.scrollDuration)
            })
        }
    }
}), angular.module("makeWonder").directive("scrollToAnchor", ["$location", "$window",
    function(e, t) {
        return {
            restrict: "A",
            link: function(n, r, o) {
                var i;
                return i = $("a[name=" + o.scrollToAnchor + "]"), i.length ? r.click(function() {
                    return e.hash(o.scrollToAnchor), t.scroll(i.offset().left, i.offset().top)
                }) : void 0
            }
        }
    }
]), angular.module("makeWonder").directive("showAfterElement", ["$window",
    function(e) {
        return {
            restrict: "A",
            compile: function(t, n) {
                var r, o, i, a;
                return r = !1, i = $(n.showAfterElement), i.length ? (a = i.offset().top, o = function() {
                    var n;
                    return n = $(e.document).scrollTop(), n > a && !r ? (t.css({
                        opacity: 1
                    }), r = !0) : a > n && r ? (t.css({
                        opacity: 0
                    }), r = !1) : void 0
                }, o(), $(window).scroll(o)) : void 0
            }
        }
    }
]), angular.module("makeWonder").directive("validateCreditCard", ["$parse",
    function(e) {
        var t, n, r, o, i, a;
        return t = [{
            name: "amex",
            longName: "American Express",
            pattern: /^3[47]/,
            validLength: [15]
        }, {
            name: "diners_club_carte_blanche",
            longName: "Diner's Club Carte Blanche",
            pattern: /^30[0-5]/,
            validLength: [14]
        }, {
            name: "diners_club_international",
            longName: "Diner's Club International",
            pattern: /^36/,
            validLength: [14]
        }, {
            name: "jcb",
            longName: "JCB",
            pattern: /^35(2[89]|[3-8][0-9])/,
            validLength: [16]
        }, {
            name: "laser",
            longName: "Laser",
            pattern: /^(6304|670[69]|6771)/,
            validLength: [16, 17, 18, 19]
        }, {
            name: "visa_electron",
            longName: "Visa Electron",
            pattern: /^(4026|417500|4508|4844|491(3|7))/,
            validLength: [16]
        }, {
            name: "visa",
            longName: "VISA",
            pattern: /^4/,
            validLength: [16]
        }, {
            name: "mastercard",
            longName: "MasterCard",
            pattern: /^5[1-5]/,
            validLength: [16]
        }, {
            name: "maestro",
            longName: "Maestro",
            pattern: /^(5018|5020|5038|6304|6759|676[1-3])/,
            validLength: [12, 13, 14, 15, 16, 17, 18, 19]
        }, {
            name: "discover",
            longName: "Discover",
            pattern: /^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)/,
            validLength: [16]
        }], a = {
            placeholder: " ",
            onBeforeMask: function(e) {
                return e.trim()
            }
        }, o = function(e) {
            var n, r;
            for (n = 0; n < t.length;) {
                if (r = t[n], r.pattern.test(e)) return r;
                n++
            }
            return null
        }, i = function(e, t) {
            return e ? _.contains(e.validLength, t.toString().length) : !1
        }, r = void 0, n = function(e, t) {
            e !== r && ("amex" === e ? t.inputmask("9999 999999 99999", a) : t.inputmask("9999 9999 9999 9999", a), r = e)
        }, {
            restrict: "A",
            require: "ngModel",
            scope: !0,
            link: function(t, i, s) {
                var u;
                return u = e(s.validateCreditCard)(t), r = null, u.type ? n(u.type.name, i) : i.inputmask("9999 9999 9999 9999", a), t.$watch(s.ngModel, function(e) {
                    var t, r;
                    if (e) return t = e.split(" ").join(""), r = o(t), r ? (u.type = r, n(r.name, i)) : void 0
                })
            }
        }
    }
]), angular.module("makeWonder").directive("wwSrc", function() {
    return {
        restrict: "A",
        link: function(e, t, n) {
            return t.attr("src", n.wwSrc)
        }
    }
}), angular.module("makeWonder").directive("wwSrcset", function() {
    return {
        restrict: "A",
        link: function(e, t, n) {
            return t.attr("srcset", n.wwSrcset)
        }
    }
}), angular.module("makeWonder").filter("partition", [
    function() {
        var e;
        return e = function(e, t) {
            var n;
            for (e = _.clone(e), n = []; e.length;) n.push(e.splice(0, t));
            return n
        }, _.memoize(e)
    }
]), angular.module("makeWonder").filter("startFrom", function() {
    return function(e, t) {
        return e ? (t = +t, e.slice(t)) : []
    }
}), angular.module("makeWonder").filter("uniqueTrackingNumber", function() {
    return function(e) {
        return _.uniq(e, function(e) {
            return e.trackingNumber
        })
    }
}), angular.module("makeWonder").factory("Cache", ["$q", "$timeout",
    function(e, t) {
        var n;
        return n = function() {
            function n() {
                this._cache = Object.create(null)
            }
            return n.prototype.get = function(e) {
                return this._cache[e]
            }, n.prototype.getOrElse = function(t, n, r) {
                var o;
                return null == r && (r = n), this.get(t) ? e.when(this.get(t)) : (o = e.defer(), o.resolve(r()), o.promise.then(function(e) {
                    return function(r) {
                        return e.set(t, r, n)
                    }
                }(this)))
            }, n.prototype.set = function(e, n, r) {
                return this._cache[e] = n, _.isNumber(r) && t(function(t) {
                    return function() {
                        return t.remove(e)
                    }
                }(this), r), n
            }, n.prototype.remove = function(e) {
                return delete this._cache[e]
            }, n
        }()
    }
]);
var bind = function(e, t) {
    return function() {
        return e.apply(t, arguments)
    }
};
angular.module("makeWonder").factory("GeneralUser", ["$http", "pacmanService", "$q", "config",
    function(e, t, n, r) {
        var o;
        return o = function() {
            function o(e) {
                this.getDetailedProfile = bind(this.getDetailedProfile, this), null == e.teacher && (e.teacher = {
                    school: {
                        district: void 0,
                        name: void 0
                    }
                }), _.extend(this, e), this.endpoint = r.PACMAN_URL + "/account/" + this.accessKey
            }
            return o.prototype.getDetailedProfile = function() {
                return e.get(this.endpoint + "?detailedProfile=true", {
                    headers: {
                        authorization: t.getToken()
                    }
                }).then(function(e) {
                    return function(t) {
                        var n;
                        return n = t.data, _.extend(e, n)
                    }
                }(this))
            }, o.prototype.update = function() {
                return n.all([this.updateGeneralDetails(), this.updateDeveloperStatus(), this.updateTeacherStatus()]).then(this.getDetailedProfile)
            }, o.prototype.DTO = function() {
                return {
                    firstName: this.firstName,
                    lastName: this.lastName,
                    email: this.email,
                    avatarUrl: this.avatarUrl,
                    password: this.password
                }
            }, o.prototype.updateGeneralDetails = function() {
                return e.put(this.endpoint, this.DTO(), {
                    headers: {
                        Authorization: t.getToken()
                    }
                })
            }, o.prototype.resendVerificationEmail = function() {
                return e.put(this.endpoint + "/emailToken", {}, {
                    headers: {
                        Authorization: "Bearer: " + t.getToken()
                    }
                })
            }, o.prototype.updateTeacherStatus = function() {
                return e.put(this.endpoint + "/teacher", this.teacher, {
                    headers: {
                        Authorization: t.getToken()
                    },
                    params: {
                        status: this.teacherCurriculum
                    }
                })
            }, o.prototype.updateDeveloperStatus = function() {
                return e.put(this.endpoint + "/developer", {}, {
                    headers: {
                        Authorization: t.getToken()
                    },
                    params: {
                        status: this.isDeveloper
                    }
                })
            }, o
        }()
    }
]), angular.module("makeWonder").factory("LineItems", ["products", "$q", "offerings", "$rootScope",
    function(e, t, n) {
        var r;
        return r = function() {
            function r(e) {
                this.getAllProducts(), this.getAllOfferings(), this.resolve = this.bootstrapItems(e)
            }
            var o;
            return r.prototype.bootstrapItems = function(r) {
                return t.all(_.map(r, function(r) {
                    return "product" === r.typ ? e.find(r.itemId).then(function(e) {
                        return r.product = e, r
                    }) : "offering" === r.typ || "return" === r.typ ? n.find(r.itemId).then(function(e) {
                        return r.offering = e, r
                    }) : t.when(r)
                })).then(function(e) {
                    return function(t) {
                        return e.items = t, e.items
                    }
                }(this))
            }, r.prototype.getAllProducts = function() {
                return e.all().then(function(e) {
                    return function(t) {
                        return e.products = t
                    }
                }(this))
            }, r.prototype.getAllOfferings = function() {
                return n.all().then(function(e) {
                    return function(t) {
                        return e.offerings = t
                    }
                }(this))
            }, r.prototype.add = function(e) {
                return this.items.push(e)
            }, r.prototype._setItemId = function(e) {
                return e.product && _.isNumber(e.product.id) ? e.product.id : e.offering && _.isNumber(e.offering.id) ? e.offering.id : _.isNumber(e.itemId) ? e.itemId : -1
            }, o = function(e) {
                return e.product ? null != e.product.listPrice ? e.product.listPrice : e.listPrice : e.offering ? null != e.offering.listPrice ? e.offering.listPrice : e.listPrice : 0
            }, r.prototype.format = function() {
                return _.map(this.items, function(e) {
                    return function(t) {
                        if (null == t.netPrice) throw new Error("unable to set proper net price for item");
                        return {
                            itemId: e._setItemId(t),
                            netPrice: t.netPrice,
                            listPrice: o(t),
                            typ: t.typ,
                            data: null,
                            sku: t.product && t.product.sku ? t.product.sku : t.sku,
                            quantity: t.quantity,
                            description: t.description || null
                        }
                    }
                }(this))
            }, r.prototype.products = function() {
                return _.filter(this.items, function(e) {
                    return "product" === e.typ
                })
            }, r.prototype.offerings = function() {
                return _.filter(this.items, function(e) {
                    return "offering" === e.typ
                })
            }, r.prototype.labels = function() {
                return _.filter(this.items, function(e) {
                    return "label" === e.typ
                })
            }, r.prototype.hasLabels = function() {
                return !!this.labels().length
            }, r.prototype.adjustments = function() {
                return _.filter(this.items, function(e) {
                    return "adjustment" === e.typ
                })
            }, r.prototype.returns = function() {
                return _.filter(this.items, function(e) {
                    return "return" === e.typ
                })
            }, r
        }()
    }
]), angular.module("makeWonder").factory("Note", ["$http", "config", "pacmanService",
    function(e, t, n) {
        var r;
        return r = function() {
            function r(e, t, n) {
                this.orderId = n, _.extend(this, e), t || this.createEditable()
            }
            return r.prototype.save = function(e) {
                return this.editing = !1, this.id ? this.update(e) : this.create(e), this.message = this.editable.message
            }, r.prototype.createEditable = function() {
                return this.editable = new r(_.cloneDeep(this), !0)
            }, r.prototype.deleteable = function() {
                return !this.id
            }, r.prototype.edit = function() {
                return this.createEditable(), this.editing = !0
            }, r.prototype.update = function() {
                return e.put(t.ORDERS_URL + "/v2/orders/" + this.orderId + "/notes/" + this.id, {
                    message: this.editable.message
                }, {
                    headers: {
                        Authorization: n.getToken()
                    }
                })
            }, r.prototype.create = function() {
                return e.post(t.ORDERS_URL + "/v2/orders/" + this.orderId + "/notes", {
                    message: this.editable.message
                }, {
                    headers: {
                        Authorization: n.getToken()
                    }
                }).then(function(e) {
                    return function(t) {
                        return e.id = t.data.id, e.createdDate = new Date
                    }
                }(this))
            }, r
        }()
    }
]), angular.module("makeWonder").factory("Notes", ["$http", "config", "pacmanService", "Note", "$q",
    function(e, t, n, r, o) {
        var i;
        return i = function() {
            function i(e, t) {
                this.orderId = e, this.notes = null != t ? t : [], this.notes = _.map(this.notes, function(e) {
                    return function(t) {
                        return new r(t, !1, e.orderId)
                    }
                }(this))
            }
            return i.prototype.fetchNotes = function() {
                return e.get(t.ORDERS_URL + "/v2/orders/" + this.orderId + "/notes", {
                    headers: {
                        Authorization: n.getToken()
                    }
                }).then(function(e) {
                    return function(t) {
                        return _.reduce(t.data, function(t, n) {
                            return e.find(n.id) || t.push(new r(n, !1, e.orderId)), t
                        }, e.notes)
                    }
                }(this))
            }, i.prototype["new"] = function() {
                var e;
                return e = new r({
                    message: ""
                }, !1, this.orderId), e.editing = !0, this.notes.push(e), e
            }, i.prototype.find = function(e) {
                return _.find(this.notes, function(t) {
                    return t.id === e
                })
            }, i.prototype.save = function() {
                return o.all(_.map(this.notes, function(e) {
                    return function(t) {
                        return t.save(e.orderId)
                    }
                }(this)))
            }, i.prototype["delete"] = function(e) {
                return this.notes.splice(_.indexOf(this.notes, e), 1)
            }, i
        }()
    }
]), angular.module("makeWonder").factory("Offering", ["$window",
    function(e) {
        var t;
        return t = function() {
            function t(e) {
                _.extend(this, e)
            }
            return t.prototype.isDiscounted = function() {
                return this.listPrice - this.netPrice > 0
            }, t.prototype.image = function() {
                var t;
                return t = e.devicePixelRatio && e.devicePixelRatio >= 2 ? 600 : 300, "https://d1my81jgdt4emn.cloudfront.net/images/products/" + this.imageId + "_" + t + ".png"
            }, t.prototype.isRobot = function() {
                return _.contains(["Bo & Yana Pack", "Wonder Pack", "Bo", "Yana", "Dash", "Dot", "Dash & Dot Pack"], this.displayName)
            }, t.prototype.soldOut = function() {
                return "Developer's Pack" === this.displayName
            }, t
        }()
    }
]), angular.module("makeWonder").factory("Order", ["$window", "Product", "LineItems", "Notes", "$http", "config", "pacmanService", "braintree", "products", "offerings", "users", "shippingTables",
    function(e, t, n, r, o, i, a, s, u, c, l, d) {
        var p;
        return p = function() {
            function e(e, t) {
                _.extend(this, e), this.items = new n(this.lineItems), this.notes = new r(this.id, this.notes), this.creditCard = this.creditCard || {
                    type: null,
                    encrypt: {
                        number: null,
                        fullName: null,
                        month: null,
                        year: null,
                        postalCode: null,
                        cvv: null
                    }
                }, this.editing = !1, t || this.createEditable(), this.shippingAddress = this.shippingAddress || {
                    firstName: null,
                    lastName: null,
                    street1: null,
                    street2: null,
                    country: null,
                    postalCode: null,
                    city: null,
                    state: null
                }, this.taxExemption = this.taxExemption || {
                    exempt: null,
                    number: null
                }, this.endpoint = i.ORDERS_URL + "/v2/orders/" + this.id, this.shippingAddress.country && (!this.shippingAddress.country.countryCode || t) && this.setShipping()
            }
            return e.prototype.edit = function() {
                return this.createEditable(), this.editing = !0
            }, e.prototype.isEditable = function() {
                return _.contains(["PaymentCompleted", "PaymentPending"], this.status)
            }, e.prototype.getSubtotal = function() {
                var e;
                return e = function(e) {
                    return "offering" === e.typ ? e.offering.netPrice : "product" === e.typ ? e.product.netPrice : e.netPrice || 0
                }, _.reduce(this.items.items, function(t, n) {
                    var r;
                    return "tax" === n.typ || "shipping" === n.typ ? t : (r = e(n), t + r * n.quantity)
                }, 0)
            }, e.prototype.getTax = function() {
                var e;
                return null != this.tax ? this.tax : (e = _.find(this.lineItems, function(e) {
                    return "tax" === e.typ
                }), e.netPrice)
            }, e.prototype.getShipping = function() {
                return this.shippingAddress.country ? this.shippingAddress.country.cost : 0
            }, e.prototype.getTotal = function() {
                return this.getSubtotal() + this.getTax() + this.getShipping()
            }, e.prototype.cancelEdit = function() {
                return this.createEditable(), this.editing = !1
            }, e.prototype.createEditable = function() {
                return this.editable = new e(_.cloneDeep(this), !0)
            }, e.prototype.addAdjustment = function() {
                return this.items.add({
                    typ: "adjustment",
                    description: "",
                    netPrice: 0,
                    quantity: 1
                })
            }, e.prototype.addReturn = function() {
                return this.items.add({
                    typ: "return",
                    quantity: -1,
                    offering: c.find(1)
                })
            }, e.prototype.addProduct = function() {
                return this.items.add({
                    typ: "product",
                    quantity: 1,
                    product: u.find(23)
                })
            }, e.prototype.addOffering = function() {
                return this.items.add({
                    typ: "offering",
                    quantity: 1,
                    offering: c.find(23)
                })
            }, e.prototype.addHold = function(e) {
                var t;
                return t = e.reason, o.post(this.endpoint + "/holds", {
                    reason: t
                }, {
                    headers: {
                        Authorization: a.getToken()
                    }
                })
            }, e.prototype.removeHold = function(e, t) {
                return o.put(this.endpoint + "/holds/" + e.id, {
                    reason: t
                }, {
                    headers: {
                        Authorization: a.getToken()
                    }
                })
            }, e.prototype.needCharge = function() {
                return !!(this.balance - this.pendingPayments + this.pendingRefunds)
            }, e.prototype.DTO = function() {
                return {
                    email: this.email,
                    shippingAddress: this.formattedShipping(),
                    total: this.getTotal(),
                    lineItems: this.items.format(),
                    payments: this.payments(),
                    taxExemption: this.taxExemption,
                    warranty: "warranty" === this.paymentType ? this.warrantyDTO() : void 0
                }
            }, e.prototype.payments = function() {
                return {
                    creditCard: this.creditCardsPayments(),
                    check: this.checkPayments(),
                    purchaseOrder: this.purchaseOrderPayments(),
                    customTxn: this.customTxnPayments(),
                    referrals: []
                }
            }, e.prototype.creditCardsPayments = function() {
                return "CC" !== this.paymentType ? [] : [{
                    creditCard: this.encryptedCard(),
                    ccAmount: this.getTotal()
                }]
            }, e.prototype.checkPayments = function() {
                return "check" !== this.paymentType ? [] : [{
                    check: this.check,
                    checkAmount: this.getTotal()
                }]
            }, e.prototype.purchaseOrderPayments = function() {
                return "purchaseOrder" !== this.paymentType ? [] : [{
                    poAmount: this.getTotal()
                }]
            }, e.prototype.customTxnPayments = function() {
                return "custom" !== this.paymentType ? [] : [{
                    customAmount: this.customTxn.amount,
                    customTxn: {
                        txnStatus: this.customTxn.txnStatus,
                        description: this.customTxn.description,
                        amount: this.customTxn.amount
                    }
                }]
            }, e.prototype.warrantyDTO = function() {
                return {
                    creditCard: this.encryptedCard(),
                    amazon: this.amazon,
                    srcOrderId: this.srcOrderId,
                    srcAmazonOrderId: this.srcAmazonOrderId
                }
            }, e.prototype.setShipping = function() {
                return d().then(function(e) {
                    return function(t) {
                        return e.shippingAddress.country = _.find(t, function(t) {
                            return t.countryCode === e.shippingAddress.country || t.countryCode === e.shippingAddress.country.countryCode
                        })
                    }
                }(this))
            }, e.prototype.submit = function(e) {
                var t;
                return null == e && (e = {
                    addPromotions: !1
                }), t = this.DTO(), t.notes = this.notes.notes[0].message, o.post(i.ORDERS_URL + "/v2/orders?promotions=" + e.addPromotions, t, {
                    headers: {
                        Authorization: a.getToken()
                    }
                })
            }, e.prototype.updateShipping = function() {
                return o.put(this.endpoint + "/shipping", this.editable.formattedShipping(), {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        return e.shippingAddress = e.editable.shippingAddress
                    }
                }(this))
            }, e.prototype.updateEmail = function() {
                var e;
                return e = {
                    email: this.editable.email
                }, o.put(this.endpoint + "/email", e, {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        return e.email = e.editable.email
                    }
                }(this))
            }, e.prototype.updateLineItems = function() {
                var e;
                return e = {
                    lineItems: this.editable.items.format(),
                    accessKey: this.accessKey,
                    orderId: this.id,
                    email: this.email
                }, o.put(this.endpoint + "/updateLineItems", e, {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        return e.refresh()
                    }
                }(this))
            }, e.prototype.updateStatus = function() {
                var e;
                return e = {
                    status: this.editable.status
                }, o.put(this.endpoint + "/overrideStatus", e, {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        return e.status = e.editable.status
                    }
                }(this))
            }, e.prototype.updateCheckTransaction = function(e, t) {
                var n;
                return n = _.cloneDeep(e), n.status.data.type = t, o.put(this.endpoint + "/txns", n, {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function() {
                    return e.status.data.type = t
                })
            }, e.prototype.submitTransaction = function(e) {
                return e.txnId || (e.txnId = {
                    value: parseInt(1e15 * Math.random()).toString(16)
                }), o.put(this.endpoint + "/txns", e, {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        return e.refresh()
                    }
                }(this))
            }, e.prototype.encryptedCard = function() {
                return this.creditCard.encrypt.expDate = this.creditCard.encrypt.month + "/" + this.creditCard.encrypt.year, _.transform(this.creditCard.encrypt, function(e, t, n) {
                    return "month" !== n && "year" !== n ? ("number" === n && (t = t.split(" ").join("")), e[n] = s.encrypt(t)) : void 0
                })
            }, e.prototype.formattedShipping = function() {
                return {
                    firstName: this.shippingAddress.firstName,
                    lastName: this.shippingAddress.lastName,
                    street1: this.shippingAddress.street1,
                    street2: this.shippingAddress.street2,
                    country: this.shippingAddress.country.countryCode,
                    postalCode: this.shippingAddress.postalCode,
                    city: this.shippingAddress.city,
                    state: this.shippingAddress.state,
                    phoneNumber: this.shippingAddress.phoneNumber
                }
            }, e.prototype.shipped = function() {
                return this.trackingDetails && this.trackingDetails.length || "Shipped" === this.status
            }, e.prototype.cancelled = function() {
                return "Canceled" === this.status
            }, e.prototype.refresh = function() {
                return o.get(this.endpoint, {
                    headers: {
                        Authorization: "Bearer: " + a.getToken()
                    }
                }).then(function(e) {
                    return function(t) {
                        return _.extend(e, t.data), e.setShipping(), e.items = new n(e.lineItems), e.notes.fetchNotes()
                    }
                }(this))
            }, e.prototype.cancel = function(e) {
                return null == e && (e = {}), o.put(this.endpoint + "/cancel", {
                    message: e.message
                }, {
                    headers: {
                        Authorization: "Bearer: " + a.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        return e.refresh()
                    }
                }(this))
            }, e.prototype.hasReturns = function() {
                return this.items.returns().length
            }, e.prototype.hasAdjustments = function() {
                return this.items.adjustments().length
            }, e.prototype.createTrackingLabel = function(e) {
                var t;
                return t = {
                    trackingNumbers: [e]
                }, o.put(this.endpoint + "/trackingNumbers", t, {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        return e.refresh()
                    }
                }(this))
            }, e.prototype.resendReceipt = function() {
                return o.put(this.endpoint + "/resendReceipt", {}, {
                    headers: {
                        Authorization: a.getToken()
                    }
                })
            }, e.find = function(t) {
                return o.get(i.ORDERS_URL + "/v2/orders/" + t, {
                    headers: {
                        Authorization: a.getToken()
                    }
                }).then(function(t) {
                    var n;
                    return n = t.data, new e(n)
                })
            }, e
        }()
    }
]), angular.module("makeWonder").factory("Product", ["$window",
    function(e) {
        var t;
        return t = function() {
            function t(e) {
                _.extend(this, e)
            }
            return t.prototype.isDiscounted = function() {
                return this.listPrice - this.netPrice > 0
            }, t.prototype.image = function() {
                var t;
                return t = e.devicePixelRatio && e.devicePixelRatio >= 2 ? 600 : 300, "https://d1my81jgdt4emn.cloudfront.net/images/products/" + this.imageId + "_" + t + ".png"
            }, t.prototype.isRobot = function() {
                return _.contains(["Bo & Yana Pack", "Wonder Pack", "Bo", "Yana", "Dash", "Dot", "Dash & Dot Pack"], this.displayName)
            }, t.prototype.soldOut = function() {
                return "Developer's Pack" === this.displayName
            }, t.prototype.mostPopular = function() {
                return "Dash & Dot Pack" === this.displayName
            }, t
        }()
    }
]), angular.module("makeWonder").factory("ReferralAccount", ["$http", "pacmanService", "config", "user", "$q", "$timeout",
    function(e, t, n, r, o, i) {
        var a;
        return a = function() {
            function a(e, t) {
                this.referralTerms = t, _.extend(this, e), this.formattedCredits = this.formatCredits()
            }
            return a.endpoint = function() {
                return n.REFERRALS_URL + "/accts/" + r.accessKey
            }, a.findAccount = function(r) {
                var o;
                return o = void 0, e.get(n.REFERRALS_URL + "/accts/" + r, {
                    headers: {
                        Authorization: t.getToken()
                    }
                }).then(function(e) {
                    return o = e.data.acct
                }).then(this.getDiscountTerms).then(function(e) {
                    return new a(o, e)
                })
            }, a._rawGetAccount = function() {
                return e.get(this.endpoint(), {
                    headers: {
                        Authorization: t.getToken()
                    }
                })
            }, a.getAccount = function() {
                var e;
                return e = void 0, a._rawGetAccount()["catch"](function(e) {
                    return 404 !== e.status ? o.reject(e) : a.createAccount().then(function() {
                        return i(function() {
                            return void 0
                        }, 1e3)
                    }).then(function() {
                        return a._rawGetAccount()
                    })
                }).then(function(t) {
                    return e = t.data.acct
                }).then(this.getDiscountTerms).then(function(t) {
                    return new a(e, t)
                })
            }, a.validateToken = function(t) {
                return r.isLoggedIn() ? o.all([a.getAccount(), e.get(n.REFERRALS_URL + "/token/" + encodeURIComponent(t))]).then(function(e) {
                    var n;
                    return n = e[0], n.token === t ? o.reject(t) : void 0
                }) : e.get(n.REFERRALS_URL + "/token/" + encodeURIComponent(t))
            }, a.getDiscountTerms = function() {
                return e.get(n.REFERRALS_URL + "/terms").then(function(e) {
                    var t;
                    return t = e.data
                })
            }, a.createAccount = function(o) {
                return e.post(n.REFERRALS_URL + "/accts", {
                    refId: o || r.accessKey
                }, {
                    headers: {
                        Authorization: t.getToken()
                    }
                })
            }, a.prototype.shareEmails = function(n, r) {
                return e.post("/referrals/email", {
                    emails: n,
                    message: r,
                    referId: this.token
                }, {
                    headers: {
                        Authorization: t.getToken()
                    }
                })
            }, a.prototype.sharingUrl = function() {
                return n.MAIN_SITE_URL + "/?refer=" + this.token
            }, a.prototype.formatCredits = function() {
                var e;
                return e = _.reduce(this.orders, function(e) {
                    return function(t, n) {
                        return "Complete" === n.orderState && n.eligible !== !1 && t.push({
                            status: "Pending",
                            firstName: n.firstName,
                            lastName: n.lastName,
                            amntCents: n.creditAmntCents,
                            createdDate: n.createdDate,
                            availableBy: n.createdDate + 864e5 * e.referralTerms.referralPeriodDays
                        }), t
                    }
                }(this), []), _.reduce(this.credits, function(e) {
                    return function(t, n) {
                        var r, o;
                        return t.push(_.merge(n, {
                            status: "Available",
                            firstName: null != (r = e.orders[n.orderId]) ? r.firstName : void 0,
                            lastName: null != (o = e.orders[n.orderId]) ? o.lastName : void 0
                        })), t
                    }
                }(this), e)
            }, a
        }()
    }
]);
var extend = function(e, t) {
    function n() {
        this.constructor = e
    }
    for (var r in t) hasProp.call(t, r) && (e[r] = t[r]);
    return n.prototype = t.prototype, e.prototype = new n, e.__super__ = t.prototype, e
}, hasProp = {}.hasOwnProperty;
angular.module("makeWonder").factory("WWModal", ["$document", "$controller", "$rootScope", "$compile", "Modal",
    function(e, t, n, r, o) {
        var i;
        return i = function(e) {
            function t() {
                return t.__super__.constructor.apply(this, arguments)
            }
            return extend(t, e), t.prototype.styleInner = function(e) {
                return e.css({
                    display: "table-cell",
                    "vertical-align": "middle",
                    margin: "0 auto",
                    height: "100%"
                }), e
            }, t
        }(o)
    }
]), angular.module("makeWonder").factory("braintree", ["config", "$window",
    function(e, t) {
        return t.Braintree ? Braintree.create(e.BRAINTREE_TOKEN) : void 0
    }
]), angular.module("makeWonder").factory("createAccountService", ["$http", "pacmanService", "google", "facebook", "user",
    function(e, t, n, r, o) {
        var i, a, s;
        return a = function(e) {
            var t;
            return t = 6e5, (new Date).getTime() - e < t
        }, i = {
            email: function(e) {
                var n, r, i, a;
                return r = e.firstName, i = e.lastName, n = e.email, a = e.password, t.signUp({
                    firstName: r,
                    lastName: i,
                    email: n,
                    password: a
                }).then(function() {
                    return function() {
                        return t.login({
                            email: n,
                            password: a
                        })
                    }
                }(this)).then(function() {
                    return function(e) {
                        var t;
                        return t = e.data.token, o.login(t)
                    }
                }(this))
            },
            facebook: function() {
                return r.login().then(t.loginFacebook).then(function() {
                    return function(e) {
                        var t, n, r;
                        return n = e.data, r = n.token, t = n.profile, o.login(r), a(t.created)
                    }
                }(this))
            },
            google: function() {
                return n.login().then(t.loginGoogle).then(function() {
                    return function(e) {
                        var t, n, r;
                        return n = e.data, r = n.token, t = n.profile, o.login(r), a(t.created)
                    }
                }(this))
            }
        }, s = function(e) {
            var t;
            return t = {
                409: "This email address is already in use. Please try another one.",
                500: "Something went wrong with creating your account. Is your email address valid?",
                "default": "Oh no! Something went wrong with creating your account."
            }, {
                error: t[e.status] || t["default"]
            }
        }, {
            create: function(e, t) {
                return null == t && (t = {}), i[e](t)
            }
        }
    }
]), angular.module("makeWonder").factory("detectLocation", ["$http", "config",
    function(e, t) {
        return {
            country: function() {
                return e.get(t.STORE_URL + "/geoip/country")
            },
            zip: function(n) {
                return "CA" === n.country && (n.zip = n.zip.split(" ")[0]), e.get(t.STORE_URL + "/geoip/zip", {
                    params: n
                })
            }
        }
    }
]), angular.module("makeWonder").factory("$exceptionHandler", ["$log", "$window",
    function(e, t) {
        var n;
        return n = _.throttle(function(e) {
            return $.ajax({
                url: "/client_error_log",
                type: "POST",
                data: JSON.stringify(e),
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            })
        }, 3e4),
        function(r, o) {
            var i, a;
            e.error(r, o);
            try {
                return n({
                    cause: o,
                    message: r.message,
                    stack: r.stack,
                    userAgent: navigator.userAgent,
                    url: t.location.href
                })
            } catch (a) {
                return i = a, e.warn(i)
            }
        }
    }
]);
var bind = function(e, t) {
    return function() {
        return e.apply(t, arguments)
    }
};
angular.module("makeWonder").factory("FullScreenVideo", ["pixelMachine", "$window",
    function(e, t) {
        var n;
        return n = function() {
            function n(e) {
                var n, r;
                this.src = e, this.removeVideoListener = bind(this.removeVideoListener, this), r = Math.min(720, t.innerWidth), n = r / 1.6, this.modalContainer = $('<div class="outer absolute-outer dark-outer"><div class="inner"></div></div>'), this.youtubeBody = $('<iframe class="youtube-hero" id="MainVideo" src="//www.youtube.com/embed/' + this.src + '?rel=0&autohide=1&showinfo=0&autoplay=1" frameborder="0" width="' + r + '" height="' + n + '" allowfullscreen></iframe>'), this.modalContainer.find(".inner").append(this.youtubeBody), this.video = $("#background-video").get(0)
            }
            return n.prototype.open = function() {
                return e.track("playVideo", {
                    src: this.src
                }), this.video && this.video.pause(), $("body").keydown(this.removeVideoListener), $("body").prepend(this.modalContainer), this.modalContainer.click(this.removeVideoListener)
            }, n.prototype.removeVideoListener = function(e) {
                var t;
                if (!("mousedown" === e.type && (t = $(e.target), t.is("#youtube-hero") || t.parents().is("#youtube-hero")) || "keydown" === e.type && 27 !== e.keyCode && 91 !== e.keyCode)) return this.close()
            }, n.prototype.close = function() {
                return this.modalContainer.off("click", this.removeVideoListener), $("body").off("keydown", this.removeVideoListener), this.modalContainer.remove(), this.video ? this.video.play() : void 0
            }, n
        }()
    }
]), angular.module("makeWonder").factory("offerings", ["$window", "$q", "Offering", "$http", "config", "pacmanService", "Cache",
    function(e, t, n, r, o, i, a) {
        var s, u;
        return u = new a, s = "offerings", {
            all: function() {
                return u.getOrElse(s, function(e) {
                    return function() {
                        return e.fetchOfferings()
                    }
                }(this))
            },
            fetchOfferings: _.once(function() {
                return r.get(o.ORDERS_URL + "/offerings?promotions=false", {
                    params: {
                        active: !1
                    }
                }).then(function(e) {
                    return e.data.sort(this.offeringComparator)
                }).then(function(e) {
                    return _.map(e, function(e) {
                        return new n(e)
                    })
                })
            }),
            fetchAdminOfferings: function() {
                return r.get(o.ORDERS_URL + "/offerings?promotions=false", {
                    params: {
                        active: !1
                    },
                    headers: {
                        Authorization: i.getToken()
                    }
                }).then(function(e) {
                    return e.data.sort(this.offeringComparator)
                }).then(function(e) {
                    return e = _.map(e, function(e) {
                        return new n(e)
                    })
                })
            },
            offeringComparator: function(e, t) {
                return e.id > t.id ? 1 : t.id > e.id ? -1 : 0
            },
            find: function(e) {
                return this.all().then(function(t) {
                    return _.findWhere(t, {
                        id: e
                    })
                })
            }
        }
    }
]), angular.module("makeWonder").factory("orders", ["Order", "$window", "$q", "$http", "config", "pacmanService", "user", "Cache",
    function(e, t, n, r, o, i, a, s) {
        var u, c;
        return c = new s, u = "orders", {
            all: function(t) {
                return null == t && (t = !1), t && c.remove(u), c.getOrElse(u, function() {
                    return r.get(o.ORDERS_URL + "/v2/orders?accessKey=" + a.accessKey, {
                        headers: {
                            Authorization: "Bearer: " + i.getToken()
                        }
                    }).then(function(t) {
                        return _.map(t.data.data, function(t) {
                            return new e(t)
                        })
                    })
                })
            },
            get: function(t, n) {
                var a;
                return null == n && (n = {}), a = u + "." + t, n.force && c.remove(a), c.getOrElse(a, function() {
                    return r.get(o.ORDERS_URL + "/v2/orders/" + t, {
                        headers: {
                            Authorization: i.getToken()
                        }
                    }).then(function(t) {
                        var n;
                        return n = t.data, new e(n)
                    })
                })
            },
            query: function(t) {
                return r.get(o.ORDERS_URL + "/v2/orders", {
                    params: t,
                    headers: {
                        Authorization: "Bearer: " + i.getToken()
                    }
                }).then(function(t) {
                    var n;
                    return n = _.map(t.data.data, function(t) {
                        return new e(t)
                    })
                })
            },
            clean: function() {
                return orders.length = 0
            }
        }
    }
]), angular.module("makeWonder").factory("pixelMachine", ["$window", "$q", "$timeout", "$location",
    function(e, t) {
        var n, r, o, i, a, s;
        return n = function() {
            function t() {
                this.facebookId = "6015401431383", this.init()
            }
            return t.prototype.init = function() {
                var t, n, r;
                return t = e._fbq || (e._fbq = []), t.loaded || (n = document.createElement("script"), n.async = !0, n.src = "//connect.facebook.net/en_US/fbds.js", r = document.getElementsByTagName("script")[0], r.parentNode.insertBefore(n, r), t.loaded = !0), t.push(["addPixelId", this.facebookId])
            }, t.prototype.clickTeacherSignup = function() {
                return e._fbq.push(["track", "6022104659336", {
                    value: "0.01",
                    currency: "USD"
                }])
            }, t
        }(), o = function() {
            function n() {}
            return n.prototype.trackAsync = function(e, n) {
                var r;
                return r = t.defer(), mixpanel.track(e, n, r.resolve), setTimeout(r.resolve, 500), r.promise
            }, n.prototype.pageView = function() {
                return this.trackAsync("page view", {
                    url: e.location.href
                })
            }, n.prototype.ABTest = function(e) {
                return this.trackAsync(e.name, e.data)
            }, n.prototype.playVideo = function(e) {
                return this.trackAsync("playing video", {
                    video: "crowdfunding video",
                    src: e.src
                })
            }, n.prototype.clickBuyNow = function(e) {
                return this.trackAsync("click buy now", e)
            }, n.prototype.scrollToBottom = function(e) {
                return this.trackAsync("scrollToBottom", e)
            }, n.prototype.trySignUp = function(e) {
                return this.trackAsync("user attempted sign up", e)
            }, n.prototype.signedUp = function(e) {
                return this.trackAsync("user signed up", e)
            }, n.prototype.emailVerified = function(e) {
                return this.trackAsync("user verified email", e)
            }, n.prototype.resendVerification = function(e) {
                return this.trackAsync("user resent verification email", e)
            }, n.prototype.clickTeacherSignup = function() {
                return this.trackAsync("click teacher button", {
                    button: "Teacher Signup"
                })
            }, n.prototype.referralsShare = function(e) {
                return this.trackAsync("share referral", {
                    type: e
                })
            }, n
        }(), r = function() {
            function t() {}
            return t.prototype.pageView = function() {
                return "prod" === e.stage ? ga("send", "pageview", {
                    page: e.location.href
                }) : void 0
            }, t.prototype.magazinePage = function(e) {
                return ga("send", {
                    hitType: "event",
                    eventCategory: "button",
                    eventAction: "click",
                    eventLabel: e
                })
            }, t
        }(), i = function() {
            function t() {
                this.endpoint = "//api.nanigans.com/event.php?"
            }
            return t.prototype.createPixel = function(e) {
                return $('<img src="' + e + '" style="display: none;"></img>')
            }, t.prototype.pageView = function() {
                var t, n;
                if ("prod" === e.stage) return t = {
                    app_id: "71030",
                    type: "visit",
                    name: "landing"
                }, n = this.endpoint + $.param(t), $(".pixels").append(this.createPixel(n))
            }, t
        }(), a = function() {
            function e() {}
            return e.prototype.clickTeacherSignup = function() {
                return twttr.conversion.trackPid("l5q7z", {
                    tw_sale_amount: 0,
                    tw_order_quantity: 0
                })
            }, e
        }(), s = {
            facebook: new n,
            google: new r,
            nanigans: new i,
            mPanel: new o,
            twitter: new a
        }, {
            track: function(e, n) {
                var r;
                return r = [], _.each(s, function(o) {
                    var i;
                    if ("function" == typeof o[e]) try {
                        return i = t.when(o[e](n)), r.push(i)
                    } catch (a) {}
                }), t.all(r)
            }
        }
    }
]), angular.module("makeWonder").factory("products", ["$window", "$q", "Product", "$http", "config",
    function(e, t, n, r, o) {
        var i;
        return i = [], {
            all: function() {
                return i && i.length ? t.when(i) : this.fetchProducts()
            },
            fetchProducts: _.once(function() {
                return r.get(o.ORDERS_URL + "/products?promotions=false", {
                    params: {
                        active: !1
                    }
                }).then(function(e) {
                    return e.data.sort(function(e, t) {
                        return e.id > t.id ? 1 : t.id > e.id ? -1 : 0
                    })
                }).then(function(e) {
                    return e = _.map(e, function(e) {
                        return new n(e)
                    })
                })
            }),
            find: function(e) {
                return this.all().then(function(t) {
                    return _.find(t, function(t) {
                        return t.id === e
                    })
                })
            },
            priceForProductWithId: function(e) {
                var t;
                return t = _.find(i, function(t) {
                    return t.id === e
                }), t ? t.netPrice : void 0
            }
        }
    }
]), angular.module("makeWonder").factory("promotions", ["$window", "$q", "$http", "config",
    function(e, t, n, r) {
        return {
            allAdmin: function() {
                return n.get(r.ORDERS_URL + "/promotions", {
                    params: {
                        active: !1
                    }
                }).then(function(e) {
                    return e.data
                })
            }
        }
    }
]), angular.module("makeWonder").factory("pyramidService", ["config",
    function(e) {
        var t, n;
        return n = new Thrift.Transport(e.PYRAMID_URL, {
            useCORS: !0
        }), t = new Thrift.Protocol(n), new pyramid.PyramidServiceClient(t)
    }
]), angular.module("makeWonder").factory("scrollToBottom", ["$window", "pixelMachine",
    function(e, t) {
        var n, r, o, i, a;
        return o = {}, a = _.throttle(function() {
            return t.track("scrollToBottom", o)
        }, 3e5), i = function() {
            return $(e).scrollTop() + $(e).height() === $(document).height() ? a() : void 0
        }, n = function(t) {
            return o.page = t, $(e).scroll(i)
        }, r = function() {
            return $(e).off("scroll", i)
        }, {
            attach: n,
            cleanUp: r
        }
    }
]), angular.module("makeWonder").constant("setupInfo", {
    bunny_ears_tail: {
        title: "Bunny Ears & Tail",
        description: "Intro: Dash & Dot love to dress up, pretend, and play! Your robots can hop around, and be as quick as a bunny with these fun accessories.",
        mainImage: {
            dash: "/assets/images/setup/bunny/dash_bunny.jpg",
            dot: "/assets/images/setup/bunny/dot_bunny.jpg"
        },
        steps: {
            dash: [{
                image: "/assets/images/setup/bunny/steps_bunny_dash_1.jpg",
                description: "Dash\u2019s bunny ears can connect to the head or the body. To connect, line up the rings on the ears to the round connection points on your robot."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dash_2.jpg",
                description: "Press firmly until the ear snaps into place."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dash_3.jpg",
                description: "The bunny ears can be rotated up and down while connected. You will hear a click as they snap into place."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dash_4.jpg",
                description: "Line up the bunny tail\u2019s connector ring with a round connection point on your robot. The two connectors on the back of Dash make a great place to connect the bunny tail."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dash_5.jpg",
                description: "Press firmly until the tail snaps into place."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dash_6.jpg",
                description: "You can raise or lower the tail, which will click as it turns. If you are using an app that may require the rear sensor (like Blockly or Path), raise the tail about two or three clicks from the center level position to allow Dash to sense objects using the rear distance sensor."
            }],
            dot: [{
                image: "/assets/images/setup/bunny/steps_bunny_dot_1.jpg",
                description: "Bunny ears can connect to either side of Dot\u2019s head, or on the bottom. To connect, line up the rings on the ears to the round connection point on your robot. To use the bottom connector, remove Dot from the stand. Press firmly until each ear snaps into place."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dot_2.jpg",
                description: "Line up the bunny tail\u2019s connector ring with a round connection point on your robot. The bottom connector is a great place to place the tail. To attach the tail to Dot\u2019s bottom connector, remove the base."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dot_3.jpg",
                description: "Press firmly until the tail snaps into place."
            }, {
                image: "/assets/images/setup/bunny/steps_bunny_dot_4.jpg",
                description: "The bunny ears can be rotated up and down while connected. You will hear a click as they snap into place."
            }]
        }
    },
    tow_hook: {
        title: "Tow Hook",
        description: "Whether delivering a special message with Dash, or hanging around with Dot, the tow hook introduces many new possibilities for robot adventures!",
        mainImage: {
            dash: "/assets/images/setup/towhook/dash_towhook.jpg",
            dot: "/assets/images/setup/towhook/dot_towhook.jpg"
        },
        steps: {
            dash: [{
                image: "/assets/images/setup/towhook/steps_towhook_dash_1.jpg",
                description: "Pull out the connector rings outward and line the tow hook up to the connection points on Dash\u2019s back (or head, if you prefer). The tip of the hook should be pointing upward (or forward, if used on Dash\u2019s head)."
            }, {
                image: "/assets/images/setup/towhook/steps_towhook_dash_2.jpg",
                description: "Press firmly to snap the tow hook into place."
            }, {
                image: "/assets/images/setup/towhook/steps_towhook_dash_3.jpg",
                description: "Connectors lock at 90 and 45 degrees (straight up and down, forward, back, or diagonal). You can change the tow hook\u2019s position by disconnecting and snapping back in at another angle. Once it is attached, Dash is ready to tow!"
            }],
            dot: [{
                image: "/assets/images/setup/towhook/steps_towhook_dot_1.jpg",
                description: "Pull out the connector rings outward and line the tow hook up to the connection points around Dot."
            }, {
                image: "/assets/images/setup/towhook/steps_towhook_dot_2.jpg",
                description: "Press firmly to snap the tow hook into place."
            }, {
                image: "/assets/images/setup/towhook/steps_towhook_dot_3.jpg",
                description: "Connectors lock at 90 and 45 degrees (straight up and down, forward, back, or diagonal). You can change the tow hook\u2019s position by disconnecting and snapping back in at another angle. Once it is attached, Dot is ready to hang!"
            }]
        }
    },
    building_brick_extensions: {
        title: "Building Brick Extensions",
        description: "The play possibilities are endless! Transform Dash & Dot into anything you can imagine using your favorite building bricks, including LEGO\xae. Simply snap on and start building!",
        mainImage: {
            dash: "/assets/images/setup/buildingbrick/dash_buildingbrick.jpg",
            dot: "/assets/images/setup/buildingbrick/dot_buildingbrick.jpg"
        },
        steps: {
            dash: [{
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dash_1.jpg",
                description: "Building Brick Extensions can be snapped on to any of the 6 connection points on Dash\u2019s head or body. First, line up your extension\u2019s connector ring to the round connection point on your robot."
            }, {
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dash_2.jpg",
                description: "Press firmly until you hear a click to snap the extension into place. Building Brick Extensions can be rotated for additional building possibilities."
            }, {
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dash_3.jpg",
                description: "To rotate your Building Brick Extension, pull it straight out until it is disconnected. Then rotate as desired, and repeat Steps 1 and 2 to reconnect."
            }, {
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dash_4.jpg",
                description: "Building Brick Extensions can be connected at 90 or 45 degrees (i.e., straight up and down, forward, backwards, or diagonal). Rotate and snap them in to build and play!"
            }],
            dot: [{
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dot_1.jpg",
                description: "Building Brick Extensions can be snapped either side of Dot, or on the bottom. First, line up your extension\u2019s connector ring to the round connection point on your robot. To use the bottom connector, remove Dot from stand."
            }, {
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dot_2.jpg",
                description: "Press firmly until you hear a click to snap the extension into place. Building Brick Extensions can be rotated for more building possibilities."
            }, {
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dot_3.jpg",
                description: "To rotate your Building Brick Extension, pull it straight out until it is disconnected. Then rotate as desired, and repeat Steps 1 and 2 to reconnect."
            }, {
                image: "/assets/images/setup/buildingbrick/steps_buildingbrick_dot_4.jpg",
                description: "Building Brick Extensions can be connected at 90 or 45 degrees (i.e., straight up and down, forward, backwards, or diagonal). Rotate and snap them in to build and play!"
            }]
        }
    },
    smartphone_mount: {
        title: "Smartphone Mount",
        description: "Take photos and videos from a robot\u2019s point of view!",
        mainImage: {
            dash: "/assets/images/setup/phonemount/smartphonemount_dash.jpg"
        },
        steps: {
            dash: [{
                image: "/assets/images/setup/phonemount/steps_phonemount_dash_1.jpg",
                description: "Pull the connector rings of the smartphone mount outward and line them up around Dash\u2019s head."
            }, {
                image: "/assets/images/setup/phonemount/steps_phonemount_dash_2.jpg",
                description: "Snap the rings into place to connect smartphone mount to Dash\u2019s head. The mount can rotate and lock at 90 and 45 degrees (straight up and down, forward, backward, or diagonal), but we recommend keeping it upright to keep your smartphone balanced on top of Dash. "
            }, {
                image: "/assets/images/setup/phonemount/steps_phonemount_dash_3.jpg",
                description: "Grab your smartphone, and adjust the mount so your phone fits snugly. The screen of your smartphone should be facing the back of the robot. Take a test photo to check your smartphone camera\u2019s position!"
            }, {
                image: "/assets/images/setup/phonemount/steps_phonemount_dash_4.jpg",
                description: "Once your smartphone is firmly secured and positioned correctly within the mount, and then Dash is ready to roam!"
            }]
        }
    },
    bulldozer_bar: {
        title: "Bulldozer Bar",
        description: "Messy room? No problem! Clean up your blocks and toys with Dash\u2019s bulldozer bar.",
        mainImage: {
            dash: "/assets/images/setup/bulldozerbar/acc_bulldozer_small.jpg"
        },
        steps: {
            dash: [{
                image: "/assets/images/setup/bulldozerbar/steps_bulldozerbar_dash_1.jpg",
                description: "The bulldozer bar snaps on to the connectors on the sides of Dash\u2019s front two wheels. Line up the rings of the bulldozer bar to the connection points on Dash."
            }, {
                image: "/assets/images/setup/bulldozerbar/steps_bulldozerbar_dash_2.jpg",
                description: "Press firmly until you hear a click to connect. The bulldozer bar should lie flat on the ground, and moves along with Dash as clean-up duty is completed!"
            }]
        }
    },
    xylophone: {
        title: "Xylophone",
        description: "Compose and loop some original tunes or play some old favorites as Dash drives around!",
        mainImage: {
            dash: "/assets/images/setup/xylo/dash_xylophone.jpg"
        },
        steps: {
            dash: [{
                image: "/assets/images/setup/xylo/steps_xylo_dash_1.jpg",
                description: "Place Dash on a flat surface. Pull the connector rings of the xylophone outward and line them up to the connection points on the sides of Dash\u2019s front wheels.Press firmly to snap the xylophone in place. To make sure the xylophone is  properly aligned, check that its wheel is resting on the ground in front of Dash. "
            }, {
                image: "/assets/images/setup/xylo/steps_xylo_dash_2.jpg",
                description: "Line up the mallet\u2019s connector ring with the connection point on the left side of Dash\u2019s head. (If you are facing the robot, its left is to your right.)"
            }, {
                image: "/assets/images/setup/xylo/steps_xylo_dash_3.jpg",
                description: "Press firmly to snap the mallet into place."
            }, {
                image: "/assets/images/setup/xylo/steps_xylo_dash_4.jpg",
                description: "To make sure the mallet is properly aligned, check that it is level (pointing straight forward) when Dash\u2019s head and eye are facing straight ahead. Once the xylophone is connected, Dash is ready to play!"
            }]
        }
    },
    launcher: {
        title: "Launcher",
        description: "Turn Dash into a projectile firing machine!",
        mainImage: {
            dash: "/assets/images/setup/launcher/ball_launcher_main.jpg"
        },
        steps: {
            dash: [{
                image: "/assets/images/setup/launcher/steps_launcher_1.jpg",
                description: "The Launcher (blue) snaps on to the orange Connector. Line up the rings of the Launcher to the connection points on Connector, and snap into place."
            }, {
                image: "/assets/images/setup/launcher/steps_launcher_2.jpg",
                description: "With the Launcher now affixed, press the Connector firmly on to the connection points on the sides of one of Dash\u2019s wheels. Press\xa0 firmly until you hear a click. \xa0Now slide the small black Ball Holder to the Launcher, with the slender end affixed to the top of the Connector."
            }, {
                image: "/assets/images/setup/launcher/steps_launcher_3.jpg",
                description: "Now click the remaining orange Connector to Dash\u2019s head, so that the curved piece is in the back. Line up the rings to the buttons on either side of the head and snap into place."
            }, {
                image: "/assets/images/setup/launcher/steps_launcher_4.jpg",
                description: "Add one of the three colored balls to each of the ball compartments of the Launcher. The Launcher is now ready to use with the Blockly or Wonder apps. Use the apps to vary the power on the Launcher and try to aim for targets. Ready\u2026 Aim\u2026 Launch!"
            }]
        }
    }
}), angular.module("makeWonder").factory("shippingTables", ["$http", "config", "$q", "$window",
    function(e, t, n, r) {
        var o;
        return o = function(e) {
            return _(e).map(function(e) {
                return e.countryName = e.countryName || e.name, e
            }).sort(function(e) {
                return "North America" === e.region ? -1 : 1
            }).uniq(function(e) {
                return e.countryName
            }).value()
        }, r.shippingTables ? function() {
            return n.when(r.shippingTables).then(o)
        } : _.once(function() {
            return e.get(t.ORDERS_URL + "/shippingTables").then(function(e) {
                return o(e.data)
            })
        })
    }
]), angular.module("makeWonder").factory("shoppingCart", ["pixelMachine",
    function() {
        var e;
        return e = function() {
            function e() {
                this.storage = {}, this.getStorage()
            }
            return e.prototype.emptyStorage = function() {
                return Cookies.expire("shoppingCartItems")
            }, e.prototype.getStorage = function() {
                var e, t;
                try {
                    return this.storage = JSON.parse(Cookies.get("shoppingCartItems"))
                } catch (t) {
                    return e = t, this.emptyStorage()
                }
            }, e.prototype.count = function() {
                return _.reduce(this.storage, function(e, t) {
                    return e + t.quantity
                }, 0)
            }, e
        }(), new e
    }
]);
var thisPersonIsOverAge;
thisPersonIsOverAge = function(e, t) {
    var n, r, o, i, a, s, u;
    return s = e.split("/").map(function(e) {
        return~~ e
    }), a = s[0], o = s[1], u = s[2], a && o && u ? (n = new Date(u, a - 1, o), r = new Date, i = new Date(r.getFullYear() - t, r.getMonth(), r.getDay()), i.getTime() >= n.getTime()) : !1
}, angular.module("makeWonder").factory("thisPersonIsOverAge", function() {
    return thisPersonIsOverAge
}), angular.module("makeWonder").factory("users", ["$http", "$q", "pacmanService", "GeneralUser", "config",
    function(e, t, n, r, o) {
        return {
            query: function(t) {
                return e.get(o.PACMAN_URL + "/account/", {
                    params: t,
                    headers: {
                        Authorization: "Bearer: " + n.getToken()
                    }
                }).then(function(e) {
                    return _.map(e.data, function(e) {
                        return new r(e)
                    })
                })
            },
            find: function(t) {
                return e.get(o.PACMAN_URL + "/account/" + t, {
                    headers: {
                        Authorization: "Bearer: " + n.getToken()
                    }
                }).then(function(e) {
                    return new r(e.data)
                })
            },
            findByLegacyId: function(t) {
                return e.get(o.PACMAN_URL + "/account_by_legacy_id", {
                    params: {
                        legacyId: t
                    },
                    headers: {
                        Authorization: "Bearer: " + n.getToken()
                    }
                }).then(function(e) {
                    return new r(e.data)
                })
            }
        }
    }
]), angular.module("makeWonder.apps").controller("appPageController", ["$scope", "$attrs", "appReviewService", "pixelMachine", "scrollToBottom",
    function(e, t, n, r, o) {
        var i;
        return e.ratings = !1, e.currentTab = "details", e.currentSlide = 0, (t.iosAppId || t.androidAppId) && (i = _.reduce(["iosAppId", "androidAppId"], function(e, n) {
            return t[n] && (e[n] = t[n]), e
        }, {}), n.getReviews(i).then(function(t) {
            return e.ratings = t
        })), e.changeCaption = function(t) {
            return e.currentSlide = t
        }, o.attach("apps"), e.$on("$destroy", o.cleanUp)
    }
]), angular.module("makeWonder.apps").factory("appReviewService", ["$http",
    function(e) {
        return {
            getReviews: function(t) {
                return e.get("/apps/ratings", {
                    params: t
                }).then(function(e) {
                    return e.data
                })
            }
        }
    }
]);
var ReferralBoxController, bind = function(e, t) {
        return function() {
            return e.apply(t, arguments)
        }
    };
ReferralBoxController = function() {
    function e(e, t, n, r, o, i, a, s, u, c, l, d, p, h, f, g) {
        this.$scope = e, this.user = t, this.pacmanService = n, this.google = r, this.facebook = o, this.$http = a, this.config = s, this.$window = u, this.pixelMachine = c, this.qs = l, this.$q = d, this.ReferralAccount = p, this.$exceptionHandler = h, this.createAccountService = f, this.$rootScope = g, this.getReferralAccount = bind(this.getReferralAccount, this), this.$scope.curState = "signin", this.$scope.message = !1, this.setDefaultEmail(), this.$scope.$watch("[loginData, newAccount, user.isLoggedIn()]", function(e) {
            return function(t) {
                return t && (t[0] || t[1] || t[2]) ? e.$scope.notification = {} : void 0
            }
        }(this), !0), this.user.identify().then(function(e) {
            return function() {
                return e.getReferralAccount()["finally"](function() {
                    return e.$scope.checkedUserIdentity = !0
                })
            }
        }(this), function(e) {
            return function() {
                return i(function() {
                    return e.$scope.checkedUserIdentity = !0
                })
            }
        }(this)), this.$scope.personalMessage = "I thought your child would love Dash & Dot, captivating robots that teach kids to how to code. Designed for children ages 5 & up, Dash & Dot can do anything your child programs them to do: clean a room, make music, bark like a dog, or anything else imaginable!", this.$scope.user = this.user, this.$scope.sentEmail = !1, this.$rootScope.$on("userLoggedOut", function(e) {
            return function() {
                return e.referId = void 0
            }
        }(this)), this.$rootScope.$on("userLoggedIn", this.getReferralAccount)
    }
    return e.$inject = ["$scope", "user", "pacmanService", "google", "facebook", "$timeout", "$http", "config", "$window", "pixelMachine", "queryParserService", "$q", "ReferralAccount", "$exceptionHandler", "createAccountService", "$rootScope"], e.prototype.referralLink = function() {
        return this.config.MAIN_SITE_URL + "/?refer=" + this.referId
    }, e.prototype.goToState = function(e) {
        return this.$scope.notification = {}, this.$scope.curState = e
    }, e.prototype.checkState = function(e) {
        return this.$scope.curState === e
    }, e.prototype.login = function(e, t) {
        return event && event.preventDefault(), this.$scope.loading = !0, this.pacmanService.login({
            email: e,
            password: t
        }).then(function(e) {
            return function(t) {
                return e.user.login(t.data.token)
            }
        }(this)).then(this.getReferralAccount)["catch"](function(e) {
            return function() {
                return e.$scope.notification = {
                    error: "Email and password do not match any users"
                }
            }
        }(this))["finally"](function(e) {
            return function() {
                return e.$scope.loading = !1
            }
        }(this))
    }, e.prototype.resetPassword = function() {
        return this.$scope.$emit("open login", {
            resetPassword: !0
        })
    }, e.prototype.loginGoogle = function() {
        return this.google.login().then(this.pacmanService.loginGoogle).then(function(e) {
            return function(t) {
                var n;
                return n = t.data.token, e.user.login(n)
            }
        }(this)).then(this.getReferralAccount)
    }, e.prototype.loginFacebook = function() {
        return this.facebook.login().then(this.pacmanService.loginFacebook).then(function(e) {
            return function(t) {
                var n;
                return n = t.data.token, e.user.login(n)
            }
        }(this)).then(this.getReferralAccount)
    }, e.prototype.shareFacebook = function() {
        return this.$window.FB.ui({
            method: "share",
            href: this.referralLink() + "&source=fb"
        }, function(e) {
            return e && e.post_id ? this.pixelMachine.track("referralsShare", "facebook") : void 0
        })
    }, e.prototype.shareTwitter = function(e) {
        var t;
        return t = {
            text: "Get a FREE $20 credit towards Dash & Dot, smart robots that teach kids how to code, from @WonderWorkshop",
            url: this.referralLink() + "&source=tw"
        }, e = "https://www.twitter.com/share?" + this.qs.toQueryStr(t), this.$window.open(e, "_blank", "left=20,top=20,width=1024,height=257,toolbar=1,resizable=0"), this.pixelMachine.track("referralsShare", "twitter")
    }, e.prototype.shareEmail = function(e, t) {
        var n;
        return this.$scope.notification = {}, this.$scope.loading = !0, e = _.filter(e, function(e) {
            return null != e.email && "" !== e.email
        }), n = e.length ? angular.copy(e) : [{
            email: ""
        }], this.validateEmails(n).then(function(e) {
            return function() {
                return e.referralAccount.shareEmails(n, t)
            }
        }(this)).then(function(e) {
            return function() {
                return e.pixelMachine.track("referralsShare", "email"), e.$scope.sentEmail = !0, e.setDefaultEmail()
            }
        }(this))["catch"](function(e) {
            return function(t) {
                return "invalid" === t.reason ? e.$scope.notification = {
                    error: '"' + t.email + '" is not a valid email address'
                } : void 0
            }
        }(this))["finally"](function(e) {
            return function() {
                return e.$scope.loading = !1
            }
        }(this))
    }, e.prototype.createAccount = function(e, t) {
        return null == t && (t = {}), this.$scope.loading = !0, this.createAccountService.create(e, t).then(this.getReferralAccount)["finally"](function(e) {
            return function() {
                return e.$scope.loading = !1
            }
        }(this))
    }, e.prototype.shareMore = function() {
        return this.$scope.sentEmail = !1, this.setDefaultEmail()
    }, e.prototype.setDefaultEmail = function() {
        return this.$scope.emails = [{
            email: ""
        }]
    }, e.prototype.addEmail = function() {
        return this.$scope.emails.push({
            email: ""
        })
    }, e.prototype.getReferralAccount = function() {
        return this.ReferralAccount.getAccount().then(function(e) {
            return function(t) {
                return e.referId = t.token, e.referralAccount = t
            }
        }(this))["catch"](function(e) {
            return function(t) {
                return e.$exceptionHandler(new Error("Referrals Service responded with status " + t.statusCode + ". Details: " + JSON.stringify(t.error))), e.$scope.getReferralAccountError = !0
            }
        }(this))
    }, e.prototype.validateEmails = function(e) {
        return this.$q.all(_.map(e, function(e) {
            return function(t) {
                return e.validateEmail(t.email)
            }
        }(this)))
    }, e.prototype.validateEmail = function(e) {
        return this.$http.get(this.config.ORDERS_URL + "/validateEmail", {
            params: {
                email: e
            }
        })["catch"](function(t) {
            return function() {
                return t.$q.reject({
                    reason: "invalid",
                    email: e
                })
            }
        }(this))
    }, e
}(), angular.module("makeWonder.main").controller("aboutUsController", ["$scope", "$anchorScroll", "$state", "$window", "scrollToBottom",
    function(e, t, n, r, o) {
        var i;
        return e.galleryImages = [{
            imageUrl: "/assets/images/history_gallery/history_1.jpg",
            description: "In 2012, the founders Vikas, Saurabh, and Mikal got together with a simple yet big mission - to make coding meaningful and fun for kids. At that time, there was no Dash & Dot or even robots in the picture. One of the first prototypes was a modular robotics kit that you could program by adding different types of blocks to it.",
            title: "Dec 2012"
        }, {
            imageUrl: "/assets/images/history_gallery/history_2.jpg",
            description: "We decided that adding physical objects to change the program was too limiting. You could only add a few things before your car became unwieldy. We wanted our product to have a low floor but a high ceiling. We knew that concepts are more concrete to kids when they are expressed in a tangible, real-world way. But what if the physical object was a platform that you could use with a touch device? That was when the robot was born.",
            title: "May 2013"
        }, {
            imageUrl: "/assets/images/history_gallery/history_3.jpg",
            description: "The robot you see looks really different from Dash & Dot. We spent a lot of time in kids\u2019 homes user testing to see what form factors would appeal to them. At first, girls were uninterested in our designs. They compared the robot to a \u201cboy\u2019s toy\u201d and said that it looked \u201clike a car.\u201d After we made the decision to hide the wheels, the robot suddenly became engaging to girls in addition to boys!",
            title: "July 2013"
        }, {
            imageUrl: "/assets/images/history_gallery/history_4.jpg",
            description: "As we geared up for our crowdfunding campaign, we had to make a quality prototype of Dash & Dot. The entire team went to the unveiling of the robots from a local factory that helped us print them! Check out Dash & Dot before they had their coats painted on. True story: we made big last minute changes to Dot right before the crowdfunding campaign. Originally, Dot didn\u2019t even have an eye!",
            title: "Sept 2013"
        }, {
            imageUrl: "/assets/images/history_gallery/history_5.jpg",
            description: "November 2013 was a whirlwind month for us. The entire team helped out with the crowdfunding effort on everything from responding to support emails to doing demos of Dash & Dot at various companies in the Bay Area. After many long days, we wrapped up the month with over $1.4 million of robots sold. We were humbled and inspired that so many people were excited about our mission!",
            title: "Nov 2013"
        }, {
            imageUrl: "/assets/images/history_gallery/history_6.jpg",
            description: "As we entered 2014, the pressure was on. We had to design for manufacture, do countless rounds of tests at each stage of production, and build a software team to work on our applications all in less than a year. Here is one of the first engineering test units that our partner manufacturer made.",
            title: "Jan 2014"
        }, {
            imageUrl: "/assets/images/history_gallery/history_7.jpg",
            description: "During Summer 2014, we had our first engineering production robots come down the line. We could finally hold the Dash & Dot we spent so long working on in our hands. We see this product launch as phase one of delivering our commitment to you, our customers. We will continue to improve our play and learning experiences in the coming months and years. This is just the beginning of our story.",
            title: "Aug 2014"
        }], i = 0, e.currentImage = e.galleryImages[i], e.nextImage = function() {
            return e.currentImage = e.galleryImages[++i % e.galleryImages.length]
        }, e.previousImage = function() {
            return 0 === i && (i = 7), e.currentImage = e.galleryImages[--i % e.galleryImages.length]
        }, e.selectImage = function(t) {
            return i = t, e.currentImage = e.galleryImages[i]
        }, e.goToCareers = function() {
            return r.location.replace("/careers")
        }, o.attach("about us"), e.$on("$destroy", o.cleanUp)
    }
]), angular.module("makeWonder").controller("bluetoothModalController", ["$scope",
    function(e) {
        return e.currentTab = "ios"
    }
]);
var Home2Controller, bind = function(e, t) {
        return function() {
            return e.apply(t, arguments)
        }
    };
Home2Controller = function() {
    function e(e, t, n, r, o, i, a, s, u, c, l) {
        this.$scope = e, this.$window = t, this.config = n, this.pixelMachine = r, this.$timeout = i, this.shoppingCart = a, this.queryParserService = s, this.pyramidService = u, this.$q = c, this.ReferralAccount = l, this._escapeListener = bind(this._escapeListener, this), this.queries = this.queryParserService.parseURLQuery(), this.setAffiliate(), this.setReferrer(), this.setPromoCode(), this.setTrackers(), o.attach("home"), this.$scope.$on("$destroy", o.cleanUp), this.$scope.navOpen = !1
    }
    return e.$inject = ["$scope", "$window", "config", "pixelMachine", "scrollToBottom", "$timeout", "shoppingCart", "queryParserService", "pyramidService", "$q", "ReferralAccount"], e.prototype.noVideoSupport = function() {
        return !this.$window.document.createElement("video").canPlayType || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)
    }, e.prototype.setPromoCode = function() {
        return this.queries.promo_code ? Cookies.set("promoCode", this.queries.promo_code, {
            domain: this.config.DOMAIN,
            expires: 86400,
            path: "/"
        }) : void 0
    }, e.prototype.setAffiliate = function() {
        return this.queries.af && ("undefined" != typeof mixpanel && null !== mixpanel && mixpanel.unregister("referrer"), "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.unregister("source"), "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.register({
            ambassador: this.queries.af
        }, 7), Cookies.set("af", this.queries.af, {
            domain: this.config.DOMAIN,
            expires: 604800,
            path: "/"
        })), Cookies.get("af") ? this.$q.jqWhen(this.pyramidService.getAffiliateDTO(Cookies.get("af"), function() {})).then(function(e) {
            return function(t) {
                return e.$scope.ambassador = t
            }
        }(this)) : void 0
    }, e.prototype.setReferrer = function() {
        var e;
        return this.queries.refer && "vTyvc" !== this.queries.refer && (e = this.queries.refer.split("/?utm_source")[0], "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.unregister("ambassador"), "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.register({
            referrer: e
        }), this.queries.source && "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.register({
            referralSource: this.queries.source
        }), Cookies.set("referrer", e, {
            domain: this.config.DOMAIN,
            expires: 604800,
            path: "/"
        }), this.queries.source && Cookies.set("referralSource", this.queries.referralSource, {
            domain: this.config.DOMAIN,
            expires: 604800,
            path: "/"
        })), "vTyvc" === Cookies.get("referrer") && Cookies.expire("referrer"), Cookies.get("referrer") ? this.ReferralAccount.validateToken(Cookies.get("referrer")).then(function(e) {
            return function() {
                return e.$scope.referrer = !0
            }
        }(this))["catch"](function(e) {
            return function() {
                return "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.unregister("referrer"), "undefined" != typeof mixpanel && null !== mixpanel && mixpanel.unregister("referralSource"), Cookies.expire("referrer"), Cookies.expire("referralSource"), e.$scope.referrer = !1
            }
        }(this)) : this.$scope.referrer = !1
    }, e.prototype.setTrackers = function() {
        return this.$window.mixpanel && mixpanel.track_links ? (mixpanel.track_links("a.media-square", "Click Media Tile", function(e) {
            return {
                tile: e.id
            }
        }), mixpanel.track_links(".portal-links a", "Click Header Link", function(e) {
            return {
                link: $(e).text()
            }
        }), mixpanel.track_links("nav li a", "Click Header Link", function(e) {
            return {
                link: $(e).text()
            }
        }), mixpanel.track_links(".cta", "Click Referrals CTA", function(e) {
            return {
                link: $(e).text()
            }
        }), mixpanel.track_links(".brand-item", "Click Media Tile", function(e) {
            return {
                tile: e.id
            }
        }), mixpanel.track_links("#buy-now-hero", "click buy now", {
            location: "hero"
        }), mixpanel.track_links("#buy-now-bottom", "click buy now", {
            location: "bottom"
        }), mixpanel.track_links(".scroll-section-link", "Click Home Page Link", function(e) {
            return {
                link: $(e).text()
            }
        }), $(".video-tile").click(function(e) {
            return e.preventDefault(), mixpanel.track("Watch Community Video", {
                video: $(this).find("h4").text()
            })
        }), mixpanel.track_links(".see-more-link", "Click Home Page Link", function(e) {
            return {
                link: $(e).text()
            }
        })) : void 0
    }, e.prototype.shop = function(e) {
        return this.pixelMachine.track("clickBuyNow", {
            location: e
        }).then(function(e) {
            return function() {
                return e.$window.location.assign(e.config.STORE_URL)
            }
        }(this))
    }, e.prototype.goDashAndDot = function() {
        return this.$window.location.assign("/robots/dashanddot")
    }, e.prototype.toggleNav = function() {
        return this.$scope.$emit("toggle nav"), this.$scope.navOpen = !this.$scope.navOpen, this.$timeout(function(e) {
            return function() {
                return e.$scope.navOpen ? $("body").click(e._escapeListener) : $("body").off("click", e._escapeListener)
            }
        }(this))
    }, e.prototype._escapeListener = function(e) {
        var t;
        return t = $(e.target), "A" === t.prop("tagName") && mixpanel.track("Click Header Link", {
            link: t.text()
        }, function() {
            return window.location.assign(t.attr("href"))
        }), t.is("nav") || t.parents().is("nav") ? void 0 : this.$scope.$apply(function(e) {
            return function() {
                return e.toggleNav()
            }
        }(this))
    }, e.prototype.buy = function(e) {
        return this.pixelMachine.track("clickBuyNow", {
            location: "merch section",
            id: e
        }).then(function(t) {
            return function() {
                return t.$window.location.assign(t.config.STORE_URL + "?add_item=" + e)
            }
        }(this))
    }, e
}(), angular.module("makeWonder.main").controller("home2Controller", Home2Controller), angular.module("makeWonder.main").controller("magazineController", ["$http", "$scope", "$window", "queryParserService", "$timeout", "pixelMachine", "thisPersonIsOverAge",
    function(e, t, n, r, o, i, a) {
        return t.magazineSignedUp = localStorage.magazineSignedUp, $(".dob").inputmask("99/99/9999", {
            placeholder: "MM/DD/YYYY"
        }), t.shareFacebook = function(e) {
            return n.FB.ui({
                method: "share",
                href: e
            }), i.track("magazinePage", "share facebook")
        }, t.shareTwitter = function(e) {
            var t;
            return t = {
                via: "WonderWorkshop",
                text: "Wonder Magazine: Getting Started with Dash & Dot",
                url: e
            }, e = "https://www.twitter.com/share?" + r.toQueryStr(t), n.open(e, "_blank", "left=20,top=20,width=1024,height=257,toolbar=1,resizable=0"), i.track("magazinePage", "share twitter"), !0
        }, t.shareEmail = function(e) {
            var t;
            return t = {
                subject: "Check out Wonder Magazine",
                body: "Hi, \n\nI wanted to share Wonder Magazine with you. It has some activity ideas for what to do with Dash & Dot, robot toys that help kids learn to code, from Wonder Workshop. \n\nRead it here:\n" + e
            }, e = "mailto:?" + r.toQueryStr(t), n.open(e, "_blank"), i.track("magazinePage", "share email"), !0
        }, t.handleEvent = function(e) {
            return i.track("magazinePage", e)
        }, t.signup = function() {
            return a(t.dob, 13) ? (i.track("magazinePage", "email signup"), t.loading = !0, e.post("/magazine", {
                email: t.email
            }).then(function() {
                return t.flash = {
                    success: "You have signed up for our magazine!"
                }, t.magazineSignedUp = localStorage.magazineSignedUp = !0
            })["catch"](function(e) {
                return t.flash = 409 === e.status ? {
                    success: "You have signed up for our magazine!"
                } : {
                    error: e.data
                }
            })["finally"](function() {
                return t.loading = !1
            })) : (t.flash = {
                error: "Sorry! You must be 13 or over to sign up"
            }, void 0)
        }
    }
]);
var ReferralsLandingController;
ReferralsLandingController = function() {
    function e(e, t) {
        this.$scope = e, this.Modal = t
    }
    return e.$inject = ["$scope", "Modal"], e
}(), angular.module("makeWonder.main").controller("referralsLandingController", ReferralsLandingController), angular.module("makeWonder").controller("startController", ["$scope", "Modal", "thisPersonIsOverAge", "pixelMachine", "$http",
    function(e, t, n, r, o) {
        var i;
        return i = new t({
            templateUrl: "templates/modals/bluetooth_modal.html",
            controller: "bluetoothModalController",
            extendScope: !0,
            center: !0,
            darkenBackground: !0
        }), e.openBluetoothModal = function() {
            return i.open()
        }, e.signUpForNewsletter = function(t, i) {
            return console.log("signing up for newsletter"), e.bot ? void 0 : n(i, 13) ? (r.track("magazinePage", "email signup"), e.loading = !0, o.post("/magazine", {
                email: t
            }).then(function() {
                return e.flash = {
                    success: "You have signed up for our magazine!"
                }, e.magazineSignedUp = localStorage.magazineSignedUp = !0
            })["catch"](function(t) {
                return e.flash = 409 === t.status ? {
                    success: "You have signed up for our magazine!"
                } : {
                    error: t.data
                }
            })["finally"](function() {
                return e.loading = !1
            })) : (e.flash = {
                error: "Sorry! You must be 13 or over to sign up"
            }, void 0)
        }
    }
]), angular.module("makeWonder.main").controller("storyController", ["$scope", "$timeout", "$window",
    function(e, t, n) {
        var r;
        return e.currentStory = 0, e.noHtml5Video = !n.document.createElement("video").canPlayType || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent), r = function() {
            return $("video").each(function(e, t) {
                return t.paused ? void 0 : t.pause && t.pause()
            })
        }, e.switchStory = function(t) {
            return e.currentStory = t, e.$apply(), r(), n.innerWidth < 768 ? void 0 : $("#story-video-" + (t + 1))[0].play && $("#story-video-" + (t + 1))[0].play()
        }
    }
]), angular.module("makeWonder").directive("footerAccordion", ["$window",
    function(e) {
        return {
            restrict: "A",
            link: function(t, n, r) {
                return angular.isDefined(r.hideDefault) && e.innerWidth <= 760 && n.siblings().slideUp(), n.click(function() {
                    return e.innerWidth > 760 ? void 0 : n.siblings().slideToggle()
                })
            }
        }
    }
]), angular.module("makeWonder.main").directive("specsAccordion", ["$window",
    function(e) {
        return {
            restrict: "A",
            link: function(t, n, r) {
                var o;
                return angular.isDefined(r.hideDefault) && e.innerWidth <= 760 && n.siblings().css({
                    display: "none"
                }), o = !1, n.click(function() {
                    var t;
                    if (!(e.innerWidth > 760)) return o = !o, t = o ? "" : "none", n.siblings().css({
                        display: t
                    }), n.find(".specs-toggle").text(o ? "-" : "+")
                })
            }
        }
    }
]), angular.module("makeWonder.main").directive("startAccordion", ["$window",
    function() {
        return {
            restrict: "A",
            scope: "=",
            transclude: !0,
            template: "<div ng-transclude></div>",
            link: function(e, t) {
                var n;
                return t.siblings().slideUp(), n = !0, t.click(function() {
                    var e, r;
                    return t.siblings().slideToggle(), n = !n, n ? (r = "fa-caret-down", e = "fa-caret-right") : (r = "fa-caret-right", e = "fa-caret-down"), t.find(".toggle-icon").removeClass(r).addClass(e)
                })
            }
        }
    }
]), angular.module("makeWonder.playIdeas").directive("ideaSearchBarAccordion", ["$window",
    function(e) {
        return {
            restrict: "A",
            link: function(t, n) {
                return e.innerWidth > 760 || $(".search-bar").slideUp(), n.click(function() {
                    return e.innerWidth >= 760 ? void 0 : ($(".search-bar").slideToggle(), n.find(".chevron").toggleClass("active"))
                })
            }
        }
    }
]), angular.module("makeWonder.main").directive("blocklyScroll", ["$window",
    function(e) {
        var t;
        return t = function() {
            function t() {
                this.robot = $(".maze-dash"), this.ipad = $(".maze-ipad"), this.robotStartingTop = this.robot.offset().top, this.currentTranslate = "translate3d(0, 0, 0)", this.anchors = [0, 3, 6, 9, 19, 24, 28.8, 33.8, 44, 49, 61.2, 66.2, 90], this.currentAnchor = 0, this.speed = 10, this.multiplier = 12.4
            }
            return t.prototype.step = function() {
                return this.ticking = !1, this.currentPos = this.getPos(), this.currentStep = _.findIndex(this.anchors, function(e) {
                    return function(t, n, r) {
                        return e.currentPos >= t && e.currentPos < r[n + 1]
                    }
                }(this)), this.currentStep < 0 ? void 0 : this["step" + this.currentStep](this.currentPos - this.anchors[this.currentStep])
            }, t.prototype.getPos = function() {
                var t, n;
                return t = $(document).scrollTop() + e.innerHeight, n = t - this.robotStartingTop, n / this.speed - 10
            }, t.prototype.move = function() {
                return this.robot.css({
                    transform: this.currentTranslate + " " + this.currentRotate
                })
            }, t.prototype.earColor = function(e) {
                return this.currentColor !== e ? (this.currentColor = e, this.robot.attr("src", "/assets/images/maze/dash" + e + ".png")) : void 0
            }, t.prototype.step0 = function() {
                return this.earColor(""), this.currentRotate = "rotateZ(0deg)", this.currentTranslate = "translate3d(0, 0, 0)"
            }, t.prototype.step1 = function() {
                return this.earColor("_green"), this.setBlock(1), this.currentRotate = "rotateZ(0deg)", this.currentTranslate = "translate3d(0, 0, 0)"
            }, t.prototype.step2 = function() {
                return this.earColor("_green_red"), this.setBlock(2), this.currentRotate = "rotateZ(0deg)", this.currentTranslate = "translate3d(0, 0, 0)"
            }, t.prototype.step3 = function(e) {
                return this.setBlock(3), this.currentRotate = "rotateZ(0deg)", this.currentTranslate = "translate3d(" + e * this.multiplier + "%, 0, 0)", this.move()
            }, t.prototype.step4 = function(e) {
                return this.setBlock(4), this.currentTranslate = "translate3d(124%, 0, 0)", this.currentRotate = "rotateZ(" + -(18 * e) + "deg)", this.move()
            }, t.prototype.step5 = function(e) {
                return this.setBlock(5), this.currentRotate = "rotateZ(-90deg)", this.currentTranslate = "translate3d(124%, " + -e * this.multiplier + "%, 0em)", this.move()
            }, t.prototype.step6 = function(e) {
                return this.setBlock(6), this.currentTranslate = "translate3d(124%, -60%, 0)", this.currentRotate = "rotateZ(" + (18 * e - 90) + "deg)", this.move()
            }, t.prototype.step7 = function(e) {
                return this.currentRotate = "rotateZ(0deg)", this.currentTranslate = "translate3d(" + (e * this.multiplier + 124) + "%, -60%, 0)", this.move()
            }, t.prototype.step8 = function(e) {
                return this.currentTranslate = "translate3d(250%, -60%, 0)", this.currentRotate = "rotateZ(" + 18 * e + "deg)", this.move()
            }, t.prototype.step9 = function(e) {
                return this.currentRotate = "rotateZ(90deg)", this.currentTranslate = "translate3d(250%, " + (e * this.multiplier - 60) + "%, 0)", this.move()
            }, t.prototype.step10 = function(e) {
                return this.currentTranslate = "translate3d(250%, 91%, 0)", this.currentRotate = "rotateZ(" + (90 - 18 * e) + "deg)", this.move()
            }, t.prototype.step11 = function(e) {
                return this.currentRotate = "rotateZ(0deg)", this.currentTranslate = "translate3d(" + (250 + e * this.multiplier) + "%, 91%, 0)", this.move()
            }, t.prototype.requestTick = function() {
                return this.ticking ? void 0 : (requestAnimationFrame(function(e) {
                    return function() {
                        return e.step()
                    }
                }(this)), this.ticking = !0)
            }, t.prototype.setBlock = function(e) {
                return this.currentBlock !== e ? (this.currentBlock = e, this.ipad.attr("src", "/assets/images/maze/ipad_" + e + ".png")) : void 0
            }, t.prototype.ticking = !1, t
        }(), {
            restrict: "A",
            link: function(n) {
                var r, o;
                return r = new t, o = function() {
                    return r.requestTick()
                }, e.requestAnimationFrame && e.innerWidth > 768 && $(e.document).scroll(o), n.$on("$destroy", function() {
                    return e.requestAnimationFrame ? $(e.document).off("scroll", o) : void 0
                })
            }
        }
    }
]), angular.module("makeWonder.main").directive("doodle", ["$window",
    function(e) {
        var t, n, r, o;
        return t = 1, o = !1, r = function() {
            var t, r;
            return r = $(e.document).scrollTop(), t = r + window.innerHeight, n(r), o = !1
        }, n = function(e) {
            var n, r;
            return n = $(".future-doodle-large-circle"), r = $(".future-doodle-small-circle"), n.css({
                transform: "rotateZ(" + e * t * (5 / 8) + "deg)"
            }), r.css({
                transform: "rotateZ(" + e * t + "deg)"
            })
        }, {
            restrict: "A",
            link: function(t) {
                var n, i, a;
                return n = $(".future-doodle"), i = n.offset().top, a = function() {
                    return o ? void 0 : (requestAnimationFrame(function() {
                        return r(n, i)
                    }), o = !0)
                }, e.requestAnimationFrame && window.innerWidth > 768 && $(e.document).scroll(a), t.$on("$destroy", function() {
                    return e.requestAnimationFrame ? $(e.document).off("scroll", a) : void 0
                })
            }
        }
    }
]), angular.module("makeWonder").directive("scrollTo", ["$location", "$anchorScroll", "$window",
    function(e) {
        return {
            restrict: "A",
            link: function(t, n, r) {
                var o, i;
                return i = $("a[name=" + r.scrollTo + "], a[id=" + r.scrollTo + "]"), o = r.scrollToClear ? $(r.scrollToClear) : $("*[scroll-fix-top]"), n.click(function() {
                    var t;
                    return t = i.offset().top, o.length && (t -= o.height()), e.hash(r.scrollTo), $("html, body").animate({
                        scrollTop: t
                    })
                })
            }
        }
    }
]), angular.module("makeWonder").directive("showBeforeVideo", [
    function() {
        return {
            restrict: "A",
            compile: function(e, t) {
                var n;
                return n = $(t.showBeforeVideo)[0], _.isFunction(n.addEventListener) ? n.addEventListener("playing", function() {
                    return e.css({
                        display: "none"
                    })
                }) : void 0
            }
        }
    }
]), angular.module("makeWonder.main").directive("videoCarousel", [
    function() {
        return {
            restrict: "E",
            scope: {
                onBeforeChange: "&",
                onInit: "&"
            },
            link: function(e, t) {
                var n, r;
                return n = '<button class="video-arrow-button right-arrow-button" name="next-story">\n  <i class="fa fa-chevron-right"></i>\n</button>', r = '<button class="video-arrow-button left-arrow-button" name="previous-story">\n  <i class="fa fa-chevron-left"></i>\n</button>', t.slick({
                    arrows: !0,
                    nextArrow: n,
                    prevArrow: r,
                    appendArrows: [$(".story-text-section .container"), $(".video-screen")],
                    onBeforeChange: function(t, n, r) {
                        return e.onBeforeChange({
                            index: r
                        })
                    },
                    onInit: function() {
                        return $(".story-text-section .right-arrow-button").click(function() {
                            return t.slickNext()
                        }), $(".story-text-section .left-arrow-button").click(function() {
                            return t.slickPrev()
                        })
                    }
                })
            }
        }
    }
]), angular.module("makeWonder.orderDash").config(["$locationProvider",
    function(e) {
        return e.html5Mode(!0).hashPrefix("#")
    }
]);
var slice = [].slice;
angular.module("makeWonder.orderDash").config(["$stateProvider", "$urlRouterProvider",
    function(e) {
        return e.state("overview", {
            url: "/",
            templateUrl: "/templates/orders_dashboard/partials/orders_overview.html",
            controller: "orderOverviewController",
            resolve: {
                userOrders: ["orders", "user",
                    function(e, t) {
                        return t.identify().then(function() {
                            return e.all()
                        }, function() {
                            return []
                        }).then(function(e) {
                            return _.filter(e, function(e) {
                                return "PaymentFailed" !== e.status || "Combined" !== e.status
                            })
                        })
                    }
                ]
            }
        }).state("details", {
            url: "/:oid",
            views: {
                "": {
                    controller: "orderDetailController",
                    resolve: {
                        order: ["orders", "$stateParams",
                            function(e, t) {
                                return e.get(t.oid)
                            }
                        ],
                        authorization: ["authentication",
                            function(e) {
                                return e.authorize()
                            }
                        ]
                    },
                    templateUrl: "/templates/orders_dashboard/partials/order_details.html"
                },
                "detailsMainTab@details": {
                    templateUrl: "/templates/orders_dashboard/order_details_main_tab.html"
                }
            }
        })
    }
]).run(["$rootScope", "$exceptionHandler", "$state",
    function(e, t, n) {
        return e.$on("$stateChangeError", function() {
            var e, n, r;
            return e = 2 <= arguments.length ? slice.call(arguments, 0, r = arguments.length - 1) : (r = 0, []), n = arguments[r++], console.log("got error " + n), t(n)
        }), e.$on("userLoggedOut", function() {
            return n.go("overview")
        })
    }
]), angular.module("makeWonder.orderDash").controller("orderCancelController", ["$scope", "order", "$state",
    function(e, t, n) {
        return e.goBack = function() {
            return n.go("details", {
                oid: t.id
            })
        }
    }
]), angular.module("makeWonder.orderDash").controller("orderDetailController", ["$scope", "$stateParams", "order", "$http", "$state", "$window",
    function(e, t, n, r, o, i) {
        return e.order = n, e.flash || (e.flash = {}), e.resendReceipt = function() {
            return n.resendReceipt().then(function() {
                return e.flash.success = "Receipt has been resent to this user"
            }, function(t) {
                return e.flash.error = t.data
            })
        }, n.isOnHold && (e.flash.info = 'This order is currently on hold. If you have any questions about this, contact us at <a href="mailto:support@makewonder.com" target="_blank">our support line</a>.'), e.$on("userLoggedOut", function() {
            return o.go("overview")
        }), e.getNarvarCarrierId = function(e) {
            var t;
            return i.shippingTables ? (t = _.find(i.shippingTables, function(t) {
                return t.roCode === e
            }), t ? t.narvarCode : !1) : !1
        }
    }
]), angular.module("makeWonder.orderDash").controller("orderEditController", ["order", "$scope", "$state", "shippingTables",
    function(e, t, n, r) {
        var o, i;
        return e.edit(), t.order = e, r.then(function(e) {
            return t.countries = e
        }), t.quantities = function() {
            for (i = [], o = 0; 100 >= o; o++) i.push(o);
            return i
        }.apply(this), t.goBack = function() {
            return e.cancelEdit(), n.go("details", {
                oid: e.id
            })
        }
    }
]), angular.module("makeWonder.orderDash").controller("orderOverviewController", ["$scope", "$state", "orders", "user", "userOrders",
    function(e, t, n, r, o) {
        var i;
        return i = function() {
            return n.all().then(function(t) {
                return e.orders = _.filter(t, function(e) {
                    return "PaymentFailed" !== e.status && "Combined" !== e.status
                })
            })
        }, e.orders = _.filter(o, function(e) {
            return "PaymentFailed" !== e.status && "Combined" !== e.status
        }), e.user = r, e.$on("userLoggedIn", i), e.$on("userLoggedOut", function() {
            return n.clean(), e.orders = []
        }), e.goTo = function(e, n) {
            return t.go(n, {
                oid: e.id
            })
        }, e.openLoginModal = function() {
            return e.$emit("open login")
        }
    }
]), angular.module("makeWonder.orderDash").factory("authentication", ["user", "$state", "$location",
    function(e, t) {
        return {
            authorize: function() {
                return e.identify()["catch"](function() {
                    return t.go("overview")
                })
            }
        }
    }
]), angular.module("makeWonder.playIdeas").config(["$locationProvider", "$uiViewScrollProvider",
    function(e, t) {
        return e.html5Mode(!0).hashPrefix("#"), t.useAnchorScroll()
    }
]);
var slice = [].slice;
angular.module("makeWonder.playIdeas").config(["$stateProvider", "$urlRouterProvider",
    function(e, t) {
        return e.state("overview", {
            url: "/",
            templateUrl: "/templates/play/partials/play_ideas_overview.html",
            controller: "overviewController",
            resolve: {
                ideas: ["PlayIdea",
                    function(e) {
                        return e.all({
                            active: !0
                        })
                    }
                ],
                foo: ["playIdeaHighlights",
                    function(e) {
                        return e.fetch()
                    }
                ]
            }
        }).state("admin", {
            url: "/admin",
            "abstract": !0,
            template: "<div ui-view></div>",
            resolve: {
                blah: ["authentication",
                    function(e) {
                        return e.authorize()
                    }
                ]
            }
        }).state("admin.overview", {
            url: "/overview",
            templateUrl: "/templates/play/partials/admin_overview.html",
            controller: "adminOverviewController",
            resolve: {
                ideas: ["PlayIdea",
                    function(e) {
                        return e.all()
                    }
                ],
                foo: ["playIdeaHighlights",
                    function(e) {
                        return e.fetch()
                    }
                ]
            }
        }).state("admin.newIdea", {
            url: "/new",
            templateUrl: "/templates/play/partials/admin_edit_idea.html",
            controller: "adminEditController",
            resolve: {
                idea: ["newPlayIdea",
                    function(e) {
                        return e.idea
                    }
                ]
            }
        }).state("admin.highlight", {
            url: "/highlight",
            template: "<div ui-view></div>",
            "abstract": !0,
            resolve: {
                ideas: ["PlayIdea",
                    function(e) {
                        return e.all()
                    }
                ]
            }
        }).state("admin.highlight.new", {
            url: "/new",
            templateUrl: "/templates/play/partials/admin_edit_highlight.html",
            controller: "adminEditHighlightController",
            resolve: {
                highlight: [
                    function() {
                        return {
                            title: "",
                            ideas: []
                        }
                    }
                ]
            }
        }).state("admin.highlight.edit", {
            url: "/:title",
            templateUrl: "/templates/play/partials/admin_edit_highlight.html",
            controller: "adminEditHighlightController",
            resolve: {
                highlight: ["$stateParams", "playIdeaHighlights",
                    function(e, t) {
                        return t.findByTitle(e.title)
                    }
                ]
            }
        }).state("admin.idea", {
            url: "/:idea",
            "abstract": !0,
            template: "<div ui-view></div>",
            resolve: {
                idea: ["PlayIdea", "$stateParams", "newPlayIdea",
                    function(e, t, n) {
                        return t.idea ? e.find(t.idea) : n.idea
                    }
                ]
            }
        }).state("admin.idea.edit", {
            url: "/edit",
            templateUrl: "/templates/play/partials/admin_edit_idea.html",
            controller: "adminEditController"
        }).state("admin.idea.preview", {
            url: "/preview",
            views: {
                "": {
                    templateUrl: "/templates/play/partials/play_idea_preview.html",
                    controller: "adminPreviewController"
                },
                "playIdeaBody@admin.idea.preview": {
                    templateProvider: ["$stateParams", "PlayIdea", "$templateFactory",
                        function(e, t, n) {
                            return t.find(e.idea).then(function(e) {
                                return "normal" === e.editable.type ? n.fromUrl("/templates/play/partials/play_idea_details.html") : n.fromUrl("/templates/play/partials/play_idea_lego.html")
                            })
                        }
                    ],
                    controller: "adminPreviewController"
                }
            }
        }).state("admin.idea.preview.educational", {
            url: "/educational",
            views: {
                "playIdeaBody@admin.idea.preview": {
                    templateUrl: "/templates/play/partials/play_idea_educational.html"
                }
            }
        }).state("admin.idea.preview.details", {
            url: "/details",
            views: {
                "playIdeaBody@admin.idea.preview": {
                    templateUrl: "/templates/play/partials/play_idea_educational.html"
                }
            }
        }).state("singleIdea", {
            url: "/:idea",
            resolve: {
                idea: ["PlayIdea", "$stateParams", "$state",
                    function(e, t) {
                        return e.find(t.idea)
                    }
                ]
            },
            views: {
                "": {
                    controller: "singleIdeaController",
                    templateUrl: "/templates/play/partials/play_idea.html"
                },
                "playIdeaBody@singleIdea": {
                    templateProvider: ["$stateParams", "PlayIdea", "$templateFactory",
                        function(e, t, n) {
                            return t.find(e.idea).then(function(e) {
                                return "normal" === e.type ? n.fromUrl("/templates/play/partials/play_idea_details.html") : n.fromUrl("/templates/play/partials/play_idea_lego.html")
                            })
                        }
                    ],
                    controller: "singleIdeaController"
                }
            }
        }).state("singleIdea.educational", {
            url: "/educational",
            views: {
                "playIdeaBody@singleIdea": {
                    templateUrl: "/templates/play/partials/play_idea_educational.html"
                }
            },
            resolve: {
                blah: ["idea", "$state",
                    function(e, t) {
                        return "normal" !== e.type ? t.go("singleIdea.details", {
                            idea: e.id
                        }) : void 0
                    }
                ]
            }
        }).state("singleIdea.details", {
            url: "/details",
            views: {
                "playIdeaBody@singleIdea": {
                    templateUrl: "/templates/play/partials/play_idea_educational.html"
                }
            },
            resolve: {
                blah: ["idea", "$state",
                    function(e, t) {
                        return "lego" !== e.type ? t.go("singleIdea.educational", {
                            idea: e.id
                        }) : void 0
                    }
                ]
            }
        }), t.otherwise("/")
    }
]).run(["$rootScope", "$state", "pixelMachine",
    function(e, t, n) {
        return e.$on("$stateChangeStart", function(e, r, o) {
            return n.track("pageView"), "admin" === o.idea ? (e.preventDefault(), t.go("admin.overview")) : void 0
        }), e.$on("$stateChangeError", function() {
            var e, n, r;
            return e = 2 <= arguments.length ? slice.call(arguments, 0, r = arguments.length - 1) : (r = 0, []), n = arguments[r++], t.go("overview")
        })
    }
]), angular.module("makeWonder.playIdeas").controller("adminEditController", ["$scope", "ideaCategories", "idea", "$state", "uploader",
    function(e, t, n, r, o) {
        return e.legoNameIndex = 0, e.legoNames = ["my eggo", "barello", "Nelly Furtado", "my pants", "to the zoo", "las"], e.categories = t, e.idea = n, e.flash = {}, e.removeSupply = function(e) {
            return n.editable.supplies.splice(_.indexOf(n.editable.supplies, e), 1)
        }, e.addSupply = function() {
            return n.editable.supplies.push({
                value: ""
            })
        }, e.addEducationalLeftColumn = function() {
            return n.editable.educationalInfo.leftColumn.push({
                value: ""
            })
        }, e.addEducationalRightColumn = function() {
            return n.editable.educationalInfo.rightColumn.push({
                value: ""
            })
        }, e.removeRightColumn = function(e) {
            return n.editable.educationalInfo.rightColumn.splice(_.indexOf(n.editable.educationalInfo.rightColumn, e), 1)
        }, e.removeLeftColumn = function(e) {
            return n.editable.educationalInfo.leftColumn.splice(_.indexOf(n.editable.educationalInfo.leftColumn, e), 1)
        }, e.addStep = function() {
            return n.editable.steps.push({
                description: ""
            })
        }, e.saveIdea = function() {
            return n.save().then(function() {
                return r.go("admin.overview")
            }, function(t) {
                return e.flash.error = t
            })
        }, e.preview = function(e) {
            return e.preventDefault(), r.go("admin.idea.preview", {
                idea: n.id
            })
        }, e.uploadDownloadable = function(t) {
            return o.uploadFile(t, "/api/play/downloadable").then(function(e) {
                return n.editable.downloadable = e.data.url
            }, function(t) {
                return e.flash.error = t
            })
        }, e.uploadMainImage = function() {
            return o.uploadFile(e.mainFileImage, "/api/play/images").then(function(t) {
                return n.editable.mainImage = t.data.url, e.editMainImage = !1
            }, function(t) {
                return e.flash.error = t
            })
        }, e.uploadImageForStep = function(t) {
            return o.uploadFile(t.imageFile, "/api/play/images").then(function(e) {
                return t.image = e.data.url, delete t.editImage, delete t.imageFile
            }, function(t) {
                return e.flash.error = t
            })
        }, e.uploadLegoImages = function() {
            return o.uploadMultipleFiles(e.legoStepImages, "/api/play/lego_images").then(function(e) {
                return n.editable.legoImages = e.data.urls
            }, function(t) {
                return e.flash.error = t
            })
        }, e.deleteStep = function(e) {
            var t;
            return t = _.indexOf(n.editable.steps, e), n.editable.steps.splice(t, 1)
        }
    }
]), angular.module("makeWonder.playIdeas").controller("adminEditHighlightController", ["$scope", "ideas", "highlight", "$state", "playIdeaHighlights",
    function(e, t, n, r, o) {
        return e.flash = {}, e.ideas = function() {
            return _.difference(t, e.highlightIdeas)
        }, e.highlight = n, e.highlightIdeas = _.filter(t, function(e) {
            return _.some(n.ideas, function(t) {
                return t.id === e.id
            })
        }), e.removeIdea = function(t) {
            var n;
            return n = _.indexOf(e.highlightIdeas, t), e.highlightIdeas.splice(n, 1)
        }, e.addIdea = function(t) {
            return e.highlightIdeas.push(t)
        }, e.save = function() {
            return n.ideas = _.reduce(e.highlightIdeas, function(e, t) {
                return e.push({
                    id: t.id
                }), e
            }, []), _.contains(o.highlights, n) || o.highlights.push(n), o.save().then(function() {
                return r.go("admin.overview")
            }, function(t) {
                return e.flash.error = t
            })
        }
    }
]), angular.module("makeWonder.playIdeas").controller("adminOverviewController", ["ideas", "$scope", "playIdeaHighlights",
    function(e, t, n) {
        return t.ideas = e, t.playIdeaHighlights = n, t.highlightFlash = {}, t.weekFlash = {}, t.flash = {}, t.ideaOfTheWeek = function() {
            return _.find(e, function(e) {
                return e.id === n.ideaOfTheWeek.id
            })
        }, t.highlights = n.highlights, t.ideaForId = function(t) {
            return _.find(e, function(e) {
                return e.id === t
            })
        }, t.deleteHighlight = function(e) {
            var r;
            if (confirm("are you sure?")) return r = _.indexOf(n.highlights, e), n.highlights.splice(r, 1), n.save()["catch"](function(e) {
                return t.highlightFlash.error = e
            })
        }, t.setNewWeekIdea = function() {
            var e;
            return e = n.ideaOfTheWeek.id, n.ideaOfTheWeek.id = t.newIdeaOfTheWeek.id, n.save().then(function() {
                return t.weekFlash.success = "New idea is set"
            }, function(r) {
                return n.ideaOfTheWeek.id = e, t.weekFlassh.error = r
            })
        }, t["delete"] = function(e) {
            return confirm("are you sure?") ? e["delete"]()["catch"](function(e) {
                return t.flash.error = e
            }) : void 0
        }
    }
]), angular.module("makeWonder.playIdeas").controller("adminPreviewController", ["idea", "$scope", "$controller",
    function(e, t, n) {
        return n("singleIdeaController", {
            $scope: t,
            idea: e.editable
        })
    }
]), angular.module("makeWonder.playIdeas").controller("overviewController", ["$scope", "ideaCategories", "ideas", "$filter", "playIdeaHighlights",
    function(e, t, n, r, o) {
        return e.categories = t, e.search = {
            category: {}
        }, e.searching = function() {
            return e.search.category.Difficulties || e.search.category.Robots || e.search.category.Apps || e.search.category.Accessories || e.search.title
        }, e.ideas = function() {
            var t;
            return t = r("playIdeaFilter")(n, e.search), e.resultCount = t.length, t
        }, e.deselect = function(t) {
            return delete e.search.category[t]
        }, e.playIdeaHighlights = o, e.ideaForId = function(e) {
            return _.find(n, function(t) {
                return t.id === e
            })
        }, e.loaded = !0
    }
]), angular.module("makeWonder.playIdeas").controller("singleIdeaController", ["$scope", "idea", "ideaCategories", "$window",
    function(e, t, n, r) {
        var o;
        return e.idea = t, e.products = [], e.downloadPDF = function() {
            return r.open(t.pdf, "_blank")
        }, e.currentPageIndex = 0, e.setCurrentPage = function(t) {
            return e.currentPageIndex = t
        }, e.perPage = 9, e.pages = _(t.legoImages).sortBy(function(e) {
            return e
        }).groupBy(function(t, n) {
            return Math.floor(n / e.perPage)
        }).toArray().value(), o = function(e, t) {
            return _.reduce(e, function(e, n, r) {
                return n && _.contains(t, r) && (e[r] = !0), e
            }, {})
        }, _.each(o(t.category.Robots, n.Robots), function(t, n) {
            return t ? e.products.push({
                image: "/assets/images/product_icons/" + n + ".png",
                name: n
            }) : void 0
        }), _.each(o(t.category.Accessories, n.Accessories), function(t, n) {
            return t ? e.products.push({
                image: "/assets/images/product_icons/" + n + ".png",
                name: n
            }) : void 0
        })
    }
]), angular.module("makeWonder.playIdeas").directive("fileModel", [
    function() {
        return {
            restrict: "A",
            scope: {
                fileModel: "="
            },
            link: function(e, t) {
                return t.bind("change", function(t) {
                    return e.$apply(function() {
                        return e.fileModel = t.target.files[0]
                    })
                })
            }
        }
    }
]), angular.module("makeWonder.playIdeas").directive("fileModelMultiple", [
    function() {
        return {
            restrict: "A",
            scope: {
                fileModelMultiple: "="
            },
            link: function(e, t) {
                return t.bind("change", function(t) {
                    return e.$apply(function() {
                        return e.fileModelMultiple = t.target.files
                    })
                })
            }
        }
    }
]), angular.module("makeWonder.playIdeas").directive("ideaSearchQuery", function() {
    return {
        restrict: "A",
        scope: {
            ideaSearchQuery: "&"
        },
        link: function(e, t) {
            return t.find("input").focus(function() {
                return t.addClass("active")
            }), t.find("input").blur(function() {
                return e.ideaSearchQuery() ? void 0 : t.removeClass("active")
            }), t.click(function() {
                return t.find("input").focus()
            })
        }
    }
}), angular.module("makeWonder.playIdeas").filter("playIdeaFilter", function() {
    return function(e, t) {
        return t ? _.filter(e, function(e) {
            return e.hasTitle(t.title) && e.hasCategories(t.category)
        }) : e
    }
}), angular.module("makeWonder.playIdeas").factory("PlayIdea", ["$http", "$q", "pacmanService", "$filter",
    function(e, t, n) {
        var r;
        return r = function() {
            function r(e, t, n) {
                this.exists = null != t ? t : !1, null == n && (n = !1), this.type = "normal", this.category = {
                    Difficulties: {},
                    Products: {},
                    Accessories: {}
                }, this.supplies = [], this.steps = [], this.educationalInfo = {
                    leftColumn: [],
                    rightColumn: []
                }, this.playOptions = {}, _.extend(this, e), n || (this.editable = this.createEditable())
            }
            return r.prototype.save = function() {
                return this.exists ? e.put("/api/play/ideas/" + this.id, this.editable.payload(), {
                    headers: {
                        Authorization: n.getToken()
                    }
                }).then(function(e) {
                    return function(t) {
                        return _.extend(e, t.data)
                    }
                }(this)) : e.post("/api/play/ideas", this.editable.payload(), {
                    headers: {
                        Authorization: n.getToken()
                    }
                }).then(function(e) {
                    return function(t) {
                        return e.exists = !0, _.extend(e, t.data), r._ideas ? r._ideas.push(e) : void 0
                    }
                }(this))
            }, r.prototype["delete"] = function() {
                return e["delete"]("/api/play/ideas/" + this.id, {
                    headers: {
                        Authorization: n.getToken()
                    }
                }).then(function(e) {
                    return function() {
                        var t;
                        return t = _.indexOf(r._ideas, e), r._ideas.splice(t, 1)
                    }
                }(this))
            }, r.prototype.hasTitle = function(e) {
                return new RegExp(e, "i").test(this.title)
            }, r.prototype.hasCategories = function(e) {
                return _.reduce(e, function(e) {
                    return function(t, n, r) {
                        return e.category && e.category[r] && e.category[r][n] && t
                    }
                }(this), !0)
            }, r.prototype.createEditable = function() {
                return new r(_.cloneDeep(this), this.exists, !0)
            }, r.prototype.payload = function() {
                var e;
                return e = _.cloneDeep(this), delete e.exists, e.playOptions = _.reduce(e.playOptions, function(e, t, n) {
                    return t && (e[n] = t), e
                }, {}), e
            }, r._ideas = void 0, r._hasAll = !1, r.all = function() {
                return r._hasAll ? t.when(r._ideas) : e.get("/api/play/ideas").then(function(e) {
                    return e.data
                }).then(function(e) {
                    return r._ideas = _.map(e, function(e) {
                        return new r(e, !0)
                    }), r._ideas
                })
            }, r.find = function(n) {
                var o;
                return r._ideas || (r._ideas = []), o = _.find(r._ideas, function(e) {
                    return e.id === n
                }), null != o ? t.when(o) : e.get("/api/play/ideas/" + n).then(function(e) {
                    return o = new r(e.data, !0), r._ideas.push(o), o
                })
            }, r
        }()
    }
]), angular.module("makeWonder.playIdeas").factory("playIdeaHighlights", ["$http", "$q", "pacmanService",
    function(e, t, n) {
        return {
            fetch: function() {
                return e.get("/api/play/highlights").then(function(e) {
                    return function(t) {
                        return e.ideaOfTheWeek = t.data.ideaOfTheWeek || {}, e.highlights = t.data.highlights || []
                    }
                }(this))
            },
            save: function() {
                return e.put("/api/play/highlights", this.payload(), {
                    headers: {
                        Authorization: n.getToken()
                    }
                }).then(function(e) {
                    return this.ideaOfTheWeek = e.data.ideaOfTheWeek, this.highlights = e.data.highlights
                })
            },
            payload: function() {
                return {
                    ideaOfTheWeek: this.ideaOfTheWeek,
                    highlights: this.highlights
                }
            },
            ideaOfTheWeek: void 0,
            highlights: void 0,
            findByTitle: function(e) {
                var n;
                return null != this.highlights ? (n = _.find(this.highlights, function(t) {
                    return t.title === e
                }), t.when(n)) : this.fetch().then(function(t) {
                    return function() {
                        return _.find(t.highlights, function(t) {
                            return t.title === e
                        })
                    }
                }(this))
            }
        }
    }
]), angular.module("makeWonder.playIdeas").factory("authentication", ["user", "$state", "$location",
    function(e, t) {
        return {
            authorize: function() {
                return e.identify().then(function() {
                    return /(@makewonder.com|@play-i.com)$/.test(e.email) ? e : t.go("overview")
                }, function() {
                    return t.go("overview")
                })
            }
        }
    }
]), angular.module("makeWonder.playIdeas").constant("ideaCategories", {
    Difficulties: ["Beginner", "Intermediate", "Advanced"],
    Robots: ["Dash", "Dot", "Dash & Dot"],
    Apps: ["Blockly", "Go", "Path", "Xylo"],
    Accessories: ["Bulldozer Bar", "Bunny Ears & Tail", "Tow Hook", "Smartphone Mount", "Xylophone", "Building Brick Extensions"]
}), angular.module("makeWonder.playIdeas").factory("newPlayIdea", ["PlayIdea",
    function(e) {
        return {
            idea: new e,
            createNewIdea: function() {
                return this.idea = new e
            }
        }
    }
]), angular.module("makeWonder.playIdeas").factory("uploader", ["$http", "$q", "pacmanService",
    function(e, t, n) {
        return {
            uploadFile: function(r, o) {
                var i, a;
                return i = t.defer(), a = new FormData, a.append("file", r), e.post(o, a, {
                    transformRequest: angular.identity,
                    headers: {
                        "Content-Type": void 0,
                        Authorization: n.getToken()
                    }
                })
            },
            uploadMultipleFiles: function(t, r) {
                var o;
                return o = new FormData, _.each(t, function(e) {
                    return o.append("file", e)
                }), e.post(r, o, {
                    transformRequest: angular.identity,
                    headers: {
                        "Content-Type": void 0,
                        Authorization: n.getToken()
                    }
                })
            }
        }
    }
]), angular.module("makeWonder.setup").config(["$locationProvider",
    function(e) {
        return e.html5Mode(!0).hashPrefix("#")
    }
]);
var slice = [].slice;
angular.module("makeWonder.setup").config(["$stateProvider", "$urlRouterProvider",
    function(e, t) {
        return e.state("setup", {
            url: "/:accessory?robot",
            templateUrl: "/templates/setup/partials/setup_main.html",
            controller: "setupController",
            resolve: {
                directions: ["setupInfo", "$stateParams", "$q", "$state",
                    function(e, t, n, r) {
                        return n.when().then(function() {
                            return e[t.accessory] || r.go("setup", {
                                accessory: "building_brick_extensions"
                            })
                        })
                    }
                ]
            }
        }), t.otherwise("/building_brick_extensions")
    }
]).run(["$rootScope", "$exceptionHandler", "$state", "pixelMachine",
    function(e, t, n, r) {
        return e.$on("$stateChangeError", function() {
            var e, n, r;
            return e = 2 <= arguments.length ? slice.call(arguments, 0, r = arguments.length - 1) : (r = 0, []), n = arguments[r++], t(n)
        }), e.$on("$stateChangeStart", function(e, t, r) {
            return "" === r.accessory ? (e.preventDefault(), n.go("setup", {
                accessory: "building_brick_extensions"
            })) : void 0
        }), e.$on("$stateChangeSuccess", function() {
            return r.track("pageView")
        })
    }
]), angular.module("makeWonder.setup").controller("setupController", ["$stateParams", "$scope", "directions", "$rootScope",
    function(e, t, n, r) {
        return t.directions = n, t.title = e.accessory, t.current = "dot" === e.robot ? "dot" : "dash", r.title = "Wonder Workshop | Setup - " + n.title
    }
]), angular.module("makeWonder.setup").directive("emitRepeatDone", ["$timeout",
    function(e) {
        return {
            restrict: "A",
            link: function(t, n) {
                return t.$last === !0 ? e(function() {
                    return t.$emit("repeatDone", n)
                }) : void 0
            }
        }
    }
]), angular.module("makeWonder.setup").directive("setupCarousel", [
    function() {
        return {
            restrict: "A",
            link: function(e, t) {
                return e.$on("repeatDone", function() {
                    return t.slick({
                        dots: !1,
                        speed: 500,
                        slidesToScroll: 4,
                        slidesToShow: 8,
                        arrows: !0,
                        responsive: [{
                            breakpoint: 1150,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 1
                            }
                        }, {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 1
                            }
                        }, {
                            breakpoint: 870,
                            settings: {
                                slidesToShow: 6,
                                slidesToScroll: 1
                            }
                        }, {
                            breakpoint: 760,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3
                            }
                        }, {
                            breakpoint: 440,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        }]
                    })
                }), e.$on("$destroy", function() {
                    return t.unslick()
                })
            }
        }
    }
]), angular.module("makeWonder.setup").directive("setupPicker", ["setupInfo",
    function(e) {
        return {
            restrict: "E",
            template: '<div setup-carousel>\n  <div class="setup-picker-item" ng-repeat="(accessoryName, directions) in setupInfo", emit-repeat-done>\n    <a class="setup-picker-thumb" ui-sref="setup({ accessory: accessoryName })" ui-sref-active="active">\n      <img ng-src="/assets/images/product_icons/{{ directions.title }}.png" />\n      <div>{{ directions.title }}</div>\n    </a>\n  </div>\n</div>',
            link: function(t) {
                return t.setupInfo = e
            }
        }
    }
]), angular.module("makeWonder.userAccount").config(["$locationProvider",
    function(e) {
        return e.html5Mode(!0).hashPrefix("#")
    }
]), angular.module("makeWonder.userAccount").config(["$stateProvider", "$urlRouterProvider",
    function(e, t) {
        return e.state("account", {
            url: "/",
            resolve: {
                foo: ["user", "$state",
                    function(e, t) {
                        return e.identify()["catch"](function() {
                            return t.go("login")
                        })
                    }
                ],
                referralAccount: ["ReferralAccount", "user", "$exceptionHandler",
                    function(e, t, n) {
                        return t.identify().then(function() {
                            return e.getAccount()["catch"](function(e) {
                                var t, r;
                                return r = e.status, t = e.data, n(new Error("Received status code " + r + " to fetch account from Referrals service. Details: " + JSON.stringify(t))), t
                            })
                        })["catch"](function() {
                            return {}
                        })
                    }
                ],
                referralTerms: ["ReferralAccount",
                    function(e) {
                        return e.getDiscountTerms()
                    }
                ]
            },
            templateUrl: "/templates/users_dashboard/partials/account_details.html",
            controller: "accountDetailsController as detailCtrl"
        }).state("edit", {
            url: "/edit",
            resolve: {
                editUser: ["user",
                    function(e) {
                        return _.clone(e)
                    }
                ]
            },
            controller: "editAccountController as editCtrl",
            templateUrl: "/templates/users_dashboard/partials/edit_account.html"
        }).state("new", {
            url: "/new?redirect",
            controller: "newAccountController as newCtrl",
            templateUrl: "/templates/users_dashboard/partials/new_account.html"
        }).state("login", {
            url: "/login",
            templateUrl: "/templates/users_dashboard/partials/login.html",
            resolve: {
                loggedIn: ["user", "$state",
                    function(e, t) {
                        return e.identify().then(function() {
                            return t.go("account")
                        })["catch"](function() {
                            return {}
                        })
                    }
                ]
            }
        }), t.otherwise("/")
    }
]);
var AccountController;
AccountController = function() {
    function e(e, t, n, r, o) {
        this.$scope = e, this.user = t, this.$state = n, r.$on("userLoggedOut", function() {
            return o.location.assign("/")
        }), this.$scope.resendVerification = function(e) {
            return function() {
                return e.user.resendVerificationEmail().then(function() {
                    return e.$scope.verificationSent = !0
                })
            }
        }(this)
    }
    return e.$inject = ["$scope", "user", "$state", "$rootScope", "$window", "ReferralAccount"], e
}(), angular.module("makeWonder").controller("accountController", AccountController);
var AccountDetailsController;
AccountDetailsController = function() {
    function e(e, t, n, r, o) {
        this.$scope = e, this.$state = t, this.referralAccount = n, this.referralTerms = o, this.$scope.config = r
    }
    return e.$inject = ["$scope", "$state", "referralAccount", "config", "referralTerms"], e
}(), angular.module("makeWonder.userAccount").controller("accountDetailsController", AccountDetailsController);
var EditAccountController;
EditAccountController = function() {
    function e(e, t, n, r, o, i, a, s) {
        this.editUser = e, this.user = t, this.$scope = n, this.pacmanService = r, this.$q = o, this.$state = i, this.$timeout = s, this.forgotPasswordModal = new a({
            template: '<login-modal class="modal-body" disable-signup reset-password"></login-modal>',
            controller: this.$scope,
            center: !0,
            scope: this.$scope,
            darkenBackground: !0
        })
    }
    return e.$inject = ["editUser", "user", "$scope", "pacmanService", "$q", "$state", "Modal", "$timeout"], e.prototype.handleForgotPassword = function() {
        return this.forgotPasswordModal.open()
    }, e.prototype.editAccount = function(e) {
        return this.$scope.loading = !0, this.$timeout(function(t) {
            return function() {
                return t.verfiyCurrentPassword(e).then(function() {
                    return t.pacmanService.updateAccount({
                        firstName: e.firstName,
                        lastName: e.lastName,
                        email: e.email,
                        password: e.newPassword
                    })
                }).then(function() {
                    return t.user.update()
                }).then(function() {
                    return t.$state.go("account"), t.$scope.$parent.flash = {
                        success: "Your account details have been updated!"
                    }
                })["catch"](function(e) {
                    var n;
                    return null == e && (e = {}), n = {
                        badPassword: function() {
                            return t.$scope.badPassword = !0
                        },
                        unknown: function() {
                            return t.$scope.error = !0
                        }
                    }, e.reason || (e.reason = "unknown"), n[e.reason]()
                })["finally"](function() {
                    return t.$scope.loading = !1
                })
            }
        }(this), 3e3)
    }, e.prototype.verfiyCurrentPassword = function(e) {
        var t, n;
        return t = e.currentPassword, n = e.newPassword, n ? this.pacmanService.login({
            email: this.editUser.email,
            password: t
        })["catch"](function(e) {
            return function() {
                return e.$q.reject({
                    reason: "badPassword"
                })
            }
        }(this)) : this.$q.when()
    }, e
}(), angular.module("makeWonder.userAccount").controller("editAccountController", EditAccountController);
var NewAccountController, slice = [].slice;
NewAccountController = function() {
    function e(e, t, n, r, o, i, a, s, u) {
        this.$scope = e, this.pacmanService = t, this.$state = n, this.user = r, this.facebook = o, this.google = i, this.$exceptionHandler = a, this.$stateParams = s, this.$window = u
    }
    return e.prototype.createAccount = function() {
        var e, t;
        return t = arguments[0], e = 2 <= arguments.length ? slice.call(arguments, 1) : [], this._createBy[t].apply(this, e).then(function(e) {
            return function() {
                return e.$stateParams.redirect ? e.$window.location.assign(e.$stateParams.redirect) : e.$state.go("account")
            }
        }(this))["catch"](function(e) {
            return function(t) {
                return e._handleAccountError(t)
            }
        }(this))
    }, e.prototype._handleAccountError = function(e) {
        var t;
        return console.error(e), t = {
            409: "This email address is already in use. Please try another one.",
            500: "Something went wrong with creating your account. Is your email address valid?",
            "default": "Oh no! Something went wrong with creating your account."
        }, this.$scope.flash = {
            error: t[e.status] || t["default"]
        }
    }, e.prototype._createBy = {
        email: function(e, t, n, r) {
            return this.pacmanService.signUp({
                firstName: e,
                lastName: t,
                email: n,
                password: r
            }).then(function(e) {
                return function() {
                    return e.pacmanService.login({
                        email: n,
                        password: r
                    })
                }
            }(this)).then(function(e) {
                return function(t) {
                    var n;
                    return n = t.data.token, e.user.login(n)
                }
            }(this)).then(function(e) {
                return function() {
                    return e.$scope.$parent.flash = {
                        success: "Your account has been created! Please check your email to verify your email address."
                    }
                }
            }(this))
        },
        facebook: function() {
            return this.facebook.login().then(this.pacmanService.loginFacebook).then(function(e) {
                return function(t) {
                    var n, r, o;
                    return r = t.data, o = r.token, n = r.profile, e.user.login(o), e._determineNewlyCreated(n.created)
                }
            }(this)).then(function(e) {
                return function(t) {
                    return t ? e.$scope.$parent.flash = {
                        success: "Your account has been created!"
                    } : void 0
                }
            }(this))
        },
        google: function() {
            return this.google.login().then(this.pacmanService.loginGoogle).then(function(e) {
                return function(t) {
                    var n, r, o;
                    return r = t.data, o = r.token, n = r.profile, e.user.login(o), e._determineNewlyCreated(n.created)
                }
            }(this)).then(function(e) {
                return function(t) {
                    return t ? e.$scope.$parent.flash = {
                        success: "Your account has been created!"
                    } : void 0
                }
            }(this))
        }
    }, e.prototype._determineNewlyCreated = function(e) {
        var t;
        return t = 6e5, (new Date).getTime() - e < t
    }, e.$inject = ["$scope", "pacmanService", "$state", "user", "facebook", "google", "$exceptionHandler", "$stateParams", "$window"], e
}(), angular.module("makeWonder.userAccount").controller("newAccountController", NewAccountController);
