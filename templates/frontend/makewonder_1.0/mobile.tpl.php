<!DOCTYPE html>
<html lang="pl" class="no-js">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>JURAJSKA</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" href="templates/frontend/jurajska_1.0/css/materialize.css" type="text/css" media="all">
<link href="templates/frontend/jurajska_1.0/css/style.css" type="text/css" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Alegreya+Sans:400,300,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.pack.js"></script>
<script type="text/javascript" src="templates/frontend/jurajska_1.0/js/materialize.js"></script>
<script type="text/javascript" src="templates/frontend/jurajska_1.0/js/script.mobile.js"></script>
</head>
<body>
<div id="loader"><div id="loaderWrap"><div class="preloader-wrapper big active">
    <div class="spinner-layer spinner-blue-only">
      <div class="circle-clipper left">
        <div class="circle"></div>
      </div><div class="gap-patch">
        <div class="circle"></div>
      </div><div class="circle-clipper right">
        <div class="circle"></div>
      </div>
    </div>
  </div></div>
 </div>
{/menu:getTop/}
<div id="nav-spacer"></div>
<div id="pageContainer" class="pages-mobile">{/content/}</div>

</body>
</script>
</html>