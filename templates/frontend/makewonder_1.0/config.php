<?php
$config_template = array(
	'index' => 'index.tpl.php',
	'nauczyciele' => 'nauczyciele.tpl.php',
	'dash' => 'dash.tpl.php',
	'dot' => 'dot.tpl.php',
	'aplikacje' => 'aplikacje.tpl.php',
	'jak_zaczac' => 'jak_zaczac.tpl.php',
	'aplikacje_wonder' => 'wonder_app.tpl.php',
	'aplikacje_go' => 'go_app.tpl.php',
	'aplikacje_path' => 'path_app.tpl.php',
	'aplikacje_xylo' => 'xylo_app.tpl.php',
	'aplikacje_blockly' => 'blockly_app.tpl.php',
	'przewodnik_po_akcesoriach' => 'przewodnik_po_akcesoriach.tpl.php',
	'historia' => 'o_nas.tpl.php',
	'gwarancja_satysfakcji' => 'gwarancja_satysfakcji.tpl.php',
	'kontakt' => 'kontakt.tpl.php',
	'kompatybilne_urzadzenia' => 'kompatybilne.tpl.php',
	'pomoc' => 'pomoc.tpl.php',
	'gwarancja_producenta' => 'gwarancja_producenta.tpl.php',
	'polityka_prywatnosci' => 'polityka_prywatnosci.tpl.php',
	'inspiracje' => 'polityka_prywatnosci.tpl.php',
);

$config_file = array(
	'alias'=>'makewonder_1.0',
	'name'=>'MakeWonder 1.0',
	'description'=>'Podstawowa skórka',
	'author'=>'Expansja',
	'date_add'=>'2015-10-26',
	'thumbnail'=>'templates/frontend/makewonder_1.0/img/thumbnail.png',
);