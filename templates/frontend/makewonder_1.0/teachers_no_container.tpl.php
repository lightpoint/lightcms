<!DOCTYPE html>
<html ng-app="teacherPortal" lang="pl">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="icon" type="image/x-icon">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="shortcun icon" type="image/x-icon">
    <meta description="Wonder Workshop Teacher's Portal">
    <meta charset="utf-8">
    <title></title>
    <base href="">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <link href="css/teachers.css" rel="stylesheet">
    <link href="css/materialize.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="js/tabs.js"></script>
    <script src="js/materialize.min.js"></script>
  </head>
  <body>
    <div ng-class="{ 'active': routeLoaded }" class="fade-in active">
      <div class="header-spacer"></div>
      <header ng-controller="headerController as headerCtrl">
        <div class="container">
          <div class="logo-wrap"><a href="http://www.makewonder.pl/" class="logo">Wonder Workshop</a></div>
          {/menu:getNavTeachersTop/}
          <div class="header-sign-in-spacer">
            
          </div>
        </div>
      </header>
    </div>
<div autoscroll="false" class="ui-view">
 {/content/}

<section style="display: none;" class="signup-cta ng-scope">
  <div class="container">
    <h1>Zaproś Dasha i Dota do swojej klasy</h1>
    <button type="button" ng-click="signupCtrl.signUp()" class="orange-button pill-button">Zarejestruj się</button>
  </div>
</section></div>
<div class="container">
    <div class="footer-top">
        <div class="row">
          <div class="footer-link-section">
            {/menu:getFootMenu/}
          <div class="footer-newsletter">
            {/menu:getFormNewsletter/}
          </div>
        </div>
      </div>
      
      <div class="footer-bottom row">
        <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
        <div class="legal-stuff">
          
          <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
        </div>
      </div>
      <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
    </div>
</div>
  {/school:schoolForm/}  
<script>
$(document).ready(function(){
	$('.modal-trigger').leanModal();
	$('ul.tabs').tabs();
});
</script>
</body></html>