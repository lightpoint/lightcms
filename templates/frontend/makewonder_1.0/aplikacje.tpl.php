<!DOCTYPE html>
<html lang="pl">
<head>
{/metadata:aplikacje/}
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="templates/frontend/makewonder_1.0/js/client.js"></script>
<link href="templates/frontend/makewonder_1.0/css/style.css" rel="stylesheet">
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
<link href="http://www.makewonder.com/favicon.ico" rel="icon" type="image/x-icon">
<link href="http://www.makewonder.com/favicon.ico" rel="shortcun icon" type="image/x-icon"><!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0 user-scalable=no">
 <link rel="stylesheet" type="text/css" href="templates/frontend/makewonder_1.0/css/slick.css">
 <script async="" type="text/javascript" src="templates/frontend/makewonder_1.0/js/roundtrip.js"></script>
 <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-70029657-1', 'auto');
ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
 <script>
 
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
 document,'script','//connect.facebook.net/en_US/fbevents.js');

 fbq('init', '1495186814109494');
 fbq('track', "PageView");</script>
 <noscript><img height="1" width="1" style="display:none"
 
src="https://www.facebook.com/tr?id=1495186814109494&ev=PageView&noscript=1"
 /></noscript>
 <!-- End Facebook Pixel Code --> 
 <!--google-->
 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-70029657-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 934334004;
    w.google_conversion_label = "75i4CLK7oWMQtJzDvQM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script> 
 </head>
  <body>
    <div class="main-page">
      <div class="mobile-nav">
        <div class="nav-items">
          {/menu:getMobileNav/}
        </div>
      </div>
      <div class="wrap">
        <noscript>
          &lt;div class="notice-bar"&gt;
            &lt;div class="container"&gt;Please note: This website requires JavaScript to be enabled in order to function properly. Please &lt;a href="http://www.enable-javascript.com/" target="_blank"&gt;enable your JavaScript&lt;/a&gt; and refresh the page.&lt;/div&gt;
          &lt;/div&gt;
        </noscript><!--[if lt IE 10 ]>
        <div ng-hide="dismissNotice" class="notice-bar">
          <div class="container">This website has limited support on Internet Explorer versions 9 and below. We recommend using the latest version of <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Chrome</a>, <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a>, <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">Internet Explorer </a>or <a href="http://support.apple.com/downloads/#safari" target="_blank">Safari</a>.<br><br>
            <button ng-click="dismissNotice = true" class="pill-button orange-button sm-button">Got it</button>
          </div>
        </div><![endif]-->
        <div scroll-off-top="scrolling"  class="header-2 active"><a class="hamburger-icon"><i class="fa fa-bars"></i></a>
          <div class="container">
            <nav class="wat-header active">
              <ul>{/menu:getNav/}</ul>
            </nav>
          </div><img src="templates/frontend/makewonder_1.0/img/w_logo.png" srcset="templates/frontend/makewonder_1.0/img/w_logo_2x.png 2x" alt="Wonder Workshop" class="mobile-logo">
        </div>
        <div class="main">
          <section class="hero-2 apps-hero">
            <div class="container">
              <div class="copy">{/articles:robotyka_na_wyciagniecie_reki_aplikacje/}</div>
            </div>
          </section>
          <section class="apps-section">
            <div class="container">
              <div class="row"><a href="aplikacje_wonder.html" class="tile wonder-tile span-12">
                  <div class="tile-copy"><img src="templates/frontend/makewonder_1.0/img/appicon_wonder.png" srcset="templates/frontend/makewonder_1.0/img/appicon_wonder_2x.png 2x" alt="aplikacja wonder" class="app-icon-image">
                    {/articles:wonder_aplikacje_short/}
                  </div></a></div>
              <div class="row"><a href="aplikacje_go.html" class="tile go-tile span-6">
                  <div class="tile-copy"><img src="templates/frontend/makewonder_1.0/img/appicon_go.png" srcset="templates/frontend/makewonder_1.0/img/appicon_go_2x.png 2x" alt="aplikacja go" class="app-icon-image">
                    {/articles:go_aplikacje_short/}
                  </div></a><a href="aplikacje_path.html" class="tile path-tile span-6">
                  <div class="tile-copy"><img src="templates/frontend/makewonder_1.0/img/appicon_path.png" srcset="templates/frontend/makewonder_1.0/img/appicon_path_2x.png 2x" alt="aplikacje path" class="app-icon-image">
                    {/articles:path_aplikacje_short/}
                  </div></a></div>
              <div class="row"><a href="aplikacje_blockly.html" class="tile blockly-tile span-6">
                  <div class="tile-copy"><img src="templates/frontend/makewonder_1.0/img/appicon_blockly.png" srcset="templates/frontend/makewonder_1.0/img/appicon_blockly_2x.png 2x" alt="aplikacja blockly" class="app-icon-image">
                    {/articles:blockly_aplikacje_short/}
                  </div></a><a href="aplikacje_xylo.html" class="tile xylo-tile span-6">
                  <div class="tile-copy"><img src="templates/frontend/makewonder_1.0/img/appicon_xylo.png" srcset="templates/frontend/makewonder_1.0/img/appicon_xylo_2x.png 2x" alt="xylo app icon" class="app-icon-image">
                    {/articles:xylo_aplikacje_short/}
                  </div></a></div>
            </div>
          </section>
        </div>
      </div>
      <div class="footer-container">
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="footer-link-section">
                {/menu:getFootMenu/}
              <div class="footer-newsletter">
                {/menu:getFormNewsletter/}
              </div>
            </div>
          </div>
          
          <div class="footer-bottom row">
            <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
            <div class="legal-stuff">
              
              <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
            </div>
          </div>
          <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
        </div>
      </div>
    </div>
    <script src="templates/frontend/makewonder_1.0/js/libraries.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/thriftFile.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/application.js"></script><!--[if lt IE 10]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.1.3/jquery.placeholder.min.js"></script>
    <script>
      $(function(){
        $('input, textarea').placeholder({ customClass: 'ie-placeholder' });  
      });
    </script><![endif]-->  
    <script type="text/javascript" src="templates/frontend/makewonder_1.0/js/script.js"></script>
    <script type="text/javascript" >
			pi = {}
			pi.campaign = "a749e38f556d5eb1dc13b9221d1f994f";
			pi.type = "MakewonderSG";
			pi.sitegroup = "MakewonderSG";
			pi.generateIpk = false;
			</script>
		<script type="text/javascript" src="templates/frontend/makewonder_1.0/js/pi.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004542796;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004542796/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body></html>