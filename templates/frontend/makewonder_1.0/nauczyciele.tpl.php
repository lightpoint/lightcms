<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="icon" type="image/x-icon">
    <link href="https://d1my81jgdt4emn.cloudfront.net/images/brand/favicon.ico" rel="shortcun icon" type="image/x-icon">
    <meta description="Wonder Workshop Teacher's Portal">
    <meta charset="utf-8">
    <title>Portal dla nauczycieli</title>
    <base href="">
    <link href="css/teachers.css" rel="stylesheet">
    <link href="css/materialize.css" rel="stylesheet">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="js/tabs.js"></script>
    <script src="js/materialize.min.js"></script>
  </head>
  <body>
    <div class="fade-in active">
      <div class="header-spacer"></div>
      <header>
        <div class="container">
          <div class="logo-wrap"><a href="http://www.makewonder.pl/" class="logo">Wonder Workshop</a></div>
          {/menu:getNavTeachersTop/}
          <div class="header-sign-in-spacer">
            
          </div>
        </div>
      </header>
      <!-- uiView: undefined --><div autoscroll="false" class="ui-view ng-scope">
<section class="hero">
  <div class="container">
    <div class="row">
      <div class="span-5">
        <div class="text">
          <h1>Zaproś roboty do swojej szkoły!</h1>
          <p>Dash i Dot wprowadzają uczniów w świat programowania i robotyki.</p>
          <p><br/><a href="https://www.youtube.com/embed/heN4eLd0qh0" target="_blank" class="button orange-button pill-button">Obejrzyj Webinar poświęcony Dash i Dot</a></p>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="cta-bar red-cta-bar">
  <div class="container">
    <h1>Dash i Dot to gotowe rozwiązanie edukacyjne dla dzieci w wieku od 5 do 12 lat.</h1>
  </div>
</section>

<section class="coding-teachers">
	<div class="container">
    <div class="row">
    	<img src="img/icons/nauka.png" align="left" width="240px" hspace="90">
    	<h2 align="left">Nauka Programowania</h2>
      <p align="left">Programując roboty Dash i Dot, uczniowie uczą się logicznego myślenia, kreatywnego podejścia do rozwiązywania problemów oraz pracy w grupie, czyli kluczowych kompetencji XXI wieku. Do nauki programowania w szkołach szczególnie polecamy aplikację BLOCKLY.</p>
      <a href="http://makewonder.pl/aplikacje.html" class="button orange-button pill-button">Więcej o aplikacjach</a>
    </div>
  </div>
</section>

<section class="coding-teachers">
  <div class="container">
    <div class="row">
    	<img src="img/icons/wsparcie.png" align="left" width="240px" hspace="90">
      <h2 align="left">Wsparcie zajęć dydaktycznych</h2>
      <p align="left">Roboty Dash i Dot mogą stanowić dodatkowe narzędzie do prowadzenia zajęć lekcyjnych z matematyki, geografii, j.  polskiego,  muzyki  i  innych.  Roboty  dają  możliwość wizualizacji  i  praktycznego  zastosowania  opracowywanych zagadnień, przez co dzieciom łatwiej przyswoić wiedzę.</p>
    </div>
  </div>
</section>

<section class="coding-teachers">
	<div class="container">
    <div class="row">
    	<img src="img/icons/scenariusze.png" align="left" width="240px" hspace="90">
    	<h2 align="left">Gotowe scenariusze lekcji</h2>
      <p align="left">Nauczycielom udostępniamy scenariusze lekcji z wykorzystaniem  Dasha  i  Dota. Scenariusze  spełniają  określone  cele z  Podstawy  Programowej  Wychowania  Przedszkolnego i Kształcenia Ogólnego dla Szkół Podstawowych. Każdy ze scenariuszy  został  opracowany  i  sprawdzony  w  praktyce przez nauczycieli.</p>
      <a href="http://nauczyciele.makewonder.pl/scenariusze-lekcji.html" class="button orange-button pill-button">Przejrzyj scenariusze</a>
    </div>
  </div>
</section>

<section class="coding-teachers">
  <div class="container">
    <div class="row">
    	<img src="img/icons/zabawa.png" align="left" width="240px" hspace="90">
      <h2 align="left">Świetna zabawa</h2>
      <p align="left">Dash i Dot to interaktywne roboty o przyjaznym wyglądzie. Zarówno chłopcy jak i dziewczynki szybko się z nimi zaprzyjaźniają i wprost nie mogą się doczekać „pracy” z robotami na  lekcjach.  Jako  formę  edukacyjnej  zabawy  grupowej można  zorganizować  np.  wyścig  albo  mecz  piłki  „nożnej” robotów.</p>
      <a href="http://nauczyciele.makewonder.pl/inspiracje.html" class="button orange-button pill-button">Inspiracje nauczycieli</a>
    </div>
  </div>
</section>

<section class="coding-teachers">
	<div class="container">
    <div class="row">
    	<img src="img/icons/podstawy.png" align="left" width="240px" hspace="90">
    	<h2 align="left">Podstawy języka angielskiego – nauka przez zabawę!</h2>
      <p align="left">Aplikacje  wykorzystują  proste  słownictwo  a  każdą komendę można „przetłumaczyć” obserwując zachowanie robota(aplikacje Blockly i Wonder). Poprzez systematyczne korzystanie z aplikacji dzieci mimowolnie uczą się komend po angielsku.</p>
    </div>
  </div>
</section>

<section class="coding-teachers">
  <div class="container">
    <div class="row">
    	<img src="img/icons/start.png" align="left" width="240px" hspace="90">
      <h2 align="left">Łatwy start</h2>
      <p align="left">Nie potrzeba wiedzy z programowania ani informatyki, aby móc zacząć prowadzić lekcje z Dashem i Dotem! Wystarczy pobrać  aplikacje  Go  oraz  Blockly  na  tableta,  doładować roboty i można zaczynać.</p>
      <a href="http://makewonder.pl/kompatybilne_urzadzenia.html" class="button orange-button pill-button">Kompatybilne urządzenia</a>
      <a href="http://makewonder.pl/jak_zaczac.html" class="button orange-button pill-button" style="display: none;">Jak zacząć?</a>
      <a href="http://makewonder.pl/files/attachments/Przewodnik_na_start_dla_nauczycieli.pdf" target="_blank" class="button orange-button pill-button"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> Jak zacząć?</a>
    </div>
  </div>
</section>

<section class="coding-teachers">
	<div class="container">
    <div class="row">
    	<img src="img/icons/oferta.png" align="left" width="240px" hspace="90">
    	<h2 align="left">Specjalna oferta dla edukacji</h2>
      <p align="left">Oferujemy zarówno roboty w cenach detalicznych, jak również w  atrakcyjnych  pakietach  edukacyjnych. Uzupełnieniem robotów jest aż 5 darmowych aplikacji (iOS oraz Android) o  różnym  stopniu  trudności,  dostosowanym  do  wieku i umiejętności dzieci.</p>
      <a href="http://www.wonderpolska.pl/OFERTA-DLA-SZKOL-cinfo-pol-35.html" class="button orange-button pill-button">Sprawdź pakiety dla szkół</a>
    </div>
  </div>
</section>

<section class="cta-bar red-cta-bar">
  <div class="container">
    <h1>Ponad <strong>1500 szkół podstawowych</strong> z całego świata używa robotów Dash i Dot w trakcie zajęć lekcyjnych.</h1>
  </div>
</section>

<section class="home-quotes ng-scope">
  <div class="container">
    <div style="padding-top: 1px;" class="row">
      <div style="position: relative; min-height: 1px" class="span-8">
 				<div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Joanna Apanasewicz" src="img/person/JoannaApanasewicz.png"></div>
          <div class="quote-body">
            <div class="quote-quote ng-binding">Zaprogramowanie robota, który na żywo wykona nasze polecenia jest czymś niesamowitym. Roboty wniosły wiele radości do klasy ale jednocześnie podniosły zaangażowanie uczniów oraz ich zainteresowanie problematyką programowania.</div>
            <div class="author"> <span class="author-name">Joanna Apanasewicz</span>, "Mała Szkoła" Niepubliczna Szkoła Podstawowa w Koninie Żagańskim</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Jolanta Majkowska" src="img/person/JolantaMajkowska.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Roboty Dash i Dot rewelacyjnie sprawdziły się również w edukacji dziecka ze spektrum autyzmu. Dostarczają mu mniej bodźców niż człowiek, dlatego komunikowanie z nimi jest łatwiejsze, bezpieczniejsze i bez wątpienia ma działanie terapeutycznie.</div>
            <div class="author"> <span class="author-name">Jolanta Majkowska</span>, Zespół Szkół Specjalnych w Kowanówku, Specjalni.pl</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Monika Walkowiak" src="img/person/MonikaWalkowiak.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Kiedy przyniosłam do klasy Dasha i Dota dzieciaki momentalnie dokonały ich animizacji. Wszyscy traktowali roboty tak, jakby były żywymi istotami. Nic dziwnego, że każdy uczeń chciał by towarzyszyły mu podczas lekcji.</div>
            <div class="author"> <span class="author-name">Monika Walkowiak</span>, Szkoła Podstawowa im. Bolesława Krzywoustego w Kamieńcu Wrocławskim</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Zyta Czechowska" src="img/person/ZytaCzechowska.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Roboty Dash i Dot na pewno można uznać za innowację w procesie edukacji dzieci, w tym również niepełnosprawnych intelektualnie. Zapewniają one naukę poprzez wartościową, kreatywną i edukacyjną zabawę.</div>
            <div class="author"> <span class="author-name">Zyta Czechowska</span>, Zespół Szkół Specjalnych w Kowanówku, Specjalni.pl</div>
          </div>
        </div>
        
        <div class="quote" style="transition-property: none;">
          <div class="profile-pic"><img alt="Anna Świć" src="img/person/AnnaSwic.png"></div>
          <div class="quote-body">
            <div class="quote-quote">Mnogość aplikacji, rozbudowane możliwości aplikacji Blockly pozwalają na szeroki wachlarz wykorzystania robotów: zarówno w zabawie dowolnej, zajęciach dydaktycznych, jak i indywidualnych.</div>
            <div class="author"> <span class="author-name">Anna Świć</span>, Przedszkole nr 83 w Lublinie</div>
          </div>
        </div>
 					
      	<a target="_blank" class="button orange-button pill-button" href="https://teachers.makewonder.com/map" style="width: 18em; border: 1px solid #000;">Znajdź szkoły używające Dasha i Dota!</a>
      </div>
      <div class="span-4"><img src="img/quotes_dash.png" alt="" class="dash-globe"></div>
    </div>
  </div>
  <div height="50px" style="background-color: #fff; height: 60px;display:none;" class="row"></div>
</section>

<section class="giving" style="display: none;">
	<div class="container">
    
    	<h1 align="center"><strong>WYPRÓBUJ ZA DARMO</strong> (oferta tylko dla szkół)</h1>
      <p align="left">Wciąż się wahasz? Zapraszamy do BEZPŁATNEGO wypróbowania robotów Dash i Dot w Twojej klasie! Oferujemy wypożyczenie 1 zestawu robotów Dash i Dot na okres 14 dni</p>
      <p align="left">Zarejestruj się na Portalu dla Nauczycieli i zapytaj o pierwszy wolny termin, w którym dostępne są roboty</p>
    
  </div>
</section>

<section class="getting-started" style="display: none;">
  <div class="container">
    <h1>Masz pytania?</h1>
    <p align="left">Zapraszamy do kontaktu:<br/>mail: <a href="mailto:edukacja@wonderpolska.pl">edukacja@wonderpolska.pl</a><br/>tel: 534-222-422
    </p>
  </div>
</section>

<section class="signup-cta ng-scope" style="display: none;">
  <div class="container">
    <h1>Zaproś Dasha i Dota do swojej klasy.</h1>
    <button type="button" class="orange-button pill-button">Zarejestruj się</button>
  </div>
</section>

</div>
<div class="container">
    <div class="footer-top">
        <div class="row">
          <div class="footer-link-section">
            {/menu:getFootMenu/}
          <div class="footer-newsletter">
            {/menu:getFormNewsletter/}
          </div>
        </div>
      </div>
      
      <div class="footer-bottom row">
        <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
        <div class="legal-stuff">
          
          <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
        </div>
      </div>
      <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
    </div>
	</div>
</div>
{/school:schoolForm/}  
<script src="js/slider.js"></script>
<script>
$(document).ready(function(){
	$('.modal-trigger').leanModal();
	$('ul.tabs').tabs();
	$('.slider').slider({height: '400px'});
});
</script>
</body></html>