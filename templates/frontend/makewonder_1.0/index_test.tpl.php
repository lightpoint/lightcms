<!DOCTYPE html>
<html lang="pl" ng-app="makeWonder.main" class="ng-scope">
<head>
{/metadata:dash/}
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="templates/frontend/makewonder_1.0/css/style.css" rel="stylesheet">
<link href="templates/frontend/makewonder_1.0/css/homepage.css" rel="stylesheet">
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
<script src="templates/frontend/makewonder_1.0/js/client.js"></script>
<link href="http://www.makewonder.com/favicon.ico" rel="icon" type="image/x-icon">
<link href="http://www.makewonder.com/favicon.ico" rel="shortcun icon" type="image/x-icon">
<!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<![endif]-->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0 user-scalable=no">
<script type="text/javascript" async="" src="templates/frontend/makewonder_1.0/js/mixpanel-2.2.min.js"></script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-70029657-1', 'auto');
ga('send', 'pageview');

</script>  
  <!-- Facebook Pixel Code -->
 <script>
 
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
 document,'script','//connect.facebook.net/en_US/fbevents.js');

 fbq('init', '1495186814109494');
 fbq('track', "PageView");</script>
 <noscript><img height="1" width="1" style="display:none"
 
src="https://www.facebook.com/tr?id=1495186814109494&ev=PageView&noscript=1"
 /></noscript>
 <!-- End Facebook Pixel Code -->   
 <!--google-->
 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-70029657-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 934334004;
    w.google_conversion_label = "75i4CLK7oWMQtJzDvQM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script> 
</head>
  <body data-pinterest-extension-installed="cr1.39.1">
    <div class="main-page">
      <div class="mobile-nav">
        <div class="nav-items">
          {/menu:getMobileNav/}
        </div>
      </div>
      <div class="wrap">
      <!--[if lt IE 10 ]>
        <div ng-hide="dismissNotice" class="notice-bar">
          <div class="container">This website has limited support on Internet Explorer versions 9 and below. We recommend using the latest version of <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Chrome</a>, <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a>, <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">Internet Explorer </a>or <a href="http://support.apple.com/downloads/#safari" target="_blank">Safari</a>.<br><br>
            <button ng-click="dismissNotice = true" class="pill-button orange-button sm-button">Got it</button>
          </div>
        </div><![endif]-->
        <div ng-controller="headerController" scroll-off-top="scrolling" style="" ng-class="{ &#39;active&#39;: user }" header-2="" class="header-2 ng-scope active"><a ng-click="toggleMobileNav($event)" class="hamburger-icon"><i class="fa fa-bars"></i></a>
          <div class="container">
            <nav class="wat-header active">
              <ul>{/menu:getNav/}</ul>
            </nav>
          </div><img src="templates/frontend/makewonder_1.0/img/w_logo.png" srcset="templates/frontend/makewonder_1.0/img/w_logo_2x.png 2x" alt="Wonder Workshop" class="mobile-logo">
        </div>
        <div class="main">
            
<!-- -->

					<div ng-controller="home2Controller as homeCtrl" class="home-page">
            <section class="home-first-section">
              <section class="ad-hero-section">
                <div lazy-load class="hero-section-bg">
                  <div class="banner">
                    <h1>Buy Dash and get a <strong>Free </strong>Launcher!</h1>
                  </div>
                  <div class="content">
                    <div class="image phone"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/Mobile_ROBOT_&amp;_LAUNCHER-6de37823e5bf4ef33de7253b5f295c1b.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/Mobile_ROBOT_&amp;_LAUNCHER_2x-829ba5f0203c708909dc644870826558.png 2x"/>
                    </div>
                    <div class="image tablet"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/Tablet_ROBOT_&amp;_LAUNCHER-561f39ede2cb25be3ee1abdcee14088a.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/Tablet_ROBOT_&amp;_LAUNCHER_2x-02ee7ac95fa1a49420863d211f006d76.png 2x"/>
                    </div>
                    <div class="image desktop"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/Desktop_ROBOT_&amp;_LAUNCHER-1c7a836428768954e0cd8be90631ca55.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/Desktop_ROBOT_&amp;_LAUNCHER_2x-4665b7f6f120f7d71f71ad9baf9bb32f.png 2x"/>
                    </div>
                    <div class="text">
                      <h2>Purchase the Holiday Pack and save <strong>$29.95</strong></h2>
                      <p class="medium">Use promo code:</p>
                      <p class="large">HOLIDAY</p><a href="https://store.makewonder.com" class="button pill-button">BUY NOW</a>
                      <p class="small">Valid through 12/17/16</p>
                    </div>
                  </div>
                </div>
              </section>
              <section class="banner">
                <div class="container">
                  <div class="retail-locations-links"> 
                    <h4>Also available at:</h4><span><a href="http://www.apple.com/shop/product/HJYC2VC/A/wonder-workshop-dash-robot" target="_blank" rel="nofollower noreferrer"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/apple-3bdc17d58fbf959b4e0ba925949f492c.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/apple_2x-460e36ff19c8364672698f172661b48e.png 2x" alt="Apple Retail Store"/></a><a id="amazon" href="http://www.amazon.com/gp/product/B00SKURVKY/" target="_blank" rel="nofollower noreferrer"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/amazon-d70fb515556aae5dc8a962070fcc8a50.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/amazon_2x-9329e41d17ba06ef239dfdfd7a54ff4f.png 2x" alt="Amazon.com"/></a><a id="target" href="http://www.target.com/p/wonder-workshop-dash-the-robot/-/A-50689589" target="_blank" rel="nofollower noreferrer"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/target-41724a01a17c64cb0eb66459f0291dd0.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/target_2x-871112587cd8cd0f9f2aacdd2b80e992.png 2x" alt="Target"/></a><br class="no-desktop"><a id="barnes-and-noble" href="http://www.barnesandnoble.com/w/toys-games-dash-robot/29550066" target="_blank" rel="nofollower noreferrer"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/bn-fef2c380dfaa8facb66aa41d680c6127.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/bn_2x-3587b72481d5ee9bb8e7bd50e15a3312.png 2x" alt="Barnes &amp; Noble"/></a><a id="best-buy" href="http://www.bestbuy.com/site/wonder-workshop-dash-robot-turquoise/5253402.p?id=bb5253402&amp;skuId=5253402" target="_blank" rel="nofollower noreferrer"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/best_buy-5d0fb4b9ebc435d52420ad1edb68b2a9.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/best_buy_2x-e4d43d978df6a7db86ab72240f7ed2c1.png 2x" alt="Best Buy"/></a><a id="toys-r-us" href="http://www.toysrus.com/search/index.jsp?kwCatId=&amp;kw=wonder%20workshop%20dash&amp;keywords=wonder%20workshop%20dash&amp;origkw=wonder+workshop+dash&amp;sr=1" target="_blank" rel="nofollower noreferrer"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/toys_r_us-cfedf765f54e69ff03ea679b99695bd8.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/retail_brands/toys_r_us_2x-b80ede885cac73fbef4a1235a80a85c2.png 2x" alt="Toys R Us"/></a></span>
                  </div>
                </div>
              </section>
            </section>
            <div class="scroll-section">
              <section class="section-1"><a name="section-1"></a>
                <div class="container">
                  <div class="content">
                    <h1>Have your kids learn to code from a native speaker.</h1>
                    <p class="text">Dash &amp; Dot are real robots that teach your kids to code while they play. Using our free apps and a compatible tablet or smartphone, kids learn to code while they make Dash sing, dance and navigate all around the house. Sensors on the robot mean they react to the environment around them, including your kids.</p><br class="no-mobile">
                    <div class="row"><a role="button" click-full-screen-video video-src="LA9py48X6_o" class="button pill-button orange-button">Learn More</a><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/robots_with_tablet-a6f5b585c9d0ffae3e2d1078758f3b9e.jpg" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/robots_with_tablet_2x-6502854911e4c40d8307dad945bd3799.jpg 2x" alt="Dash and Dot with Tablet"/>
                    </div>
                  </div>
                </div>
                <div class="section-bg-image"></div>
              </section>
              <section class="section-2"><a name="section-2"></a>
                <div class="container">
                  <div class="content">
                    <h1>Coding is the career of the future. Naturally, it's best learned from robots.</h1>
                    <p class="text">Today, coding is a revered job skill. But tomorrow, the ability to code will be expected of nearly everyone. And like any language, coding is best learned while young.</p><br class="no-mobile">
                    <div class="row"><a href="/dash" class="button pill-button orange-button">Meet Dash</a>&nbsp;&nbsp;&nbsp;<a href="/dot" class="button pill-button orange-button">Meet Dot</a>
                    </div>
                  </div>
                </div>
                <div class="section-bg-image"></div>
              </section>
              <section class="section-3"><a name="section-3"></a>
                <div class="container">
                  <div class="content">
                    <h1>Dash &amp; Dot would never brag. But we will.</h1>
                    <p class="text">True fun combined with actual learning is no small feat, and the world has taken note. Dash won Good Housekeeping's Toy of the Year award, and was Melinda &amp; Bill Gates' favorite STEM gift for kids. Not bad for a fun robot.</p><br><br>
                    <div class="awards">
                      <div class="row">
                        <li class="award-ttpm"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_ttpm-f720dfe0a33b52ac658b1b55c52aba45.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_ttpm_2x-f2908a686937a1c5c4b48b44485dc602.png 2x" alt="TTPM Award"/>
                        </li>
                        <li class="award-toy-insider"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_toy_insider-9011af19d11246fc9ae58705521e716e.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_toy_insider_2x-0d2551b9a288418883be6fe8168f7827.png 2x" alt="Toy Insider"/>
                        </li>
                        <li class="award-dr-toy"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_dr_toy-cff986c0e6c51d685ab0e5304a0b7a20.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_dr_toy_2x-5dfee547a1398a77d42d72bfa85d8985.png 2x" alt="Dr. Toy"/>
                        </li>
                      </div>
                      <div class="row">
                        <li class="award-examiner"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_examiner-b6677a3f6d51c9e8cf5db57dceb8fb39.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_examiner_2x-b2f2d7ce20085b1a5f734f769635ccce.png 2x" alt="Examiner"/>
                        </li>
                        <li class="award-scholastic"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_scholastic-49b35a9174f3d3f47a8ec2c6a0e8c47a.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_scholastic_2x-28108a480c64ca0844898581e1d5caca.png 2x" alt="Scholastic"/>
                        </li>
                        <li class="award-nappa"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_nappa-53b7eaa99026ddb1c756c056624ecf6e.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/awards/badge_nappa_2x-8727c3630c7daab2ebe4f96fab5ac64d.png 2x" alt="NAPPA Gold Award"/>
                        </li>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="section-bg-image"></div>
              </section>
              <section class="section-4"><a name="section-4"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    <h1>Smart, kind and popular at school.</h1>
                    <p class="text">When something works, it catches on; which is why over 8,500 elementary schools worldwide have purchased Dash &amp; Dot to make computer science education fun and effective. Many of these schools also participate in our annual robotics competition. <a href="https://clubs.makewonder.com" target="_blank">Learn more</a>.</p><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/map_pins-244281cf816f1f7081e08574cc7503db.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/map_pins_2x-fc670b369db41d83b42e8489af22766a.png 2x" class="map-pins" alt="Map pins"/>
                  </div>
                </div>
              </section>
              <section class="section-5"><a name="section-5"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    <h1>Kids get bored easily, right? Not with Dash.</h1>
                    <p class="text">Dash and Dot present your kids with hundreds of projects, challenges, and puzzles as well as endless possibilities for freeform play.</p><br><a role="button" href="/apps" style="width: 260px;" class="button pill-button orange-button">Learn About Our Apps</a><br class="no-mobile"><br><a role="button" href="/compatibility" style="width: 260px;" class="button pill-button scroll-section-link orange-button">See Compatible Devices</a>
                  </div>
                </div>
              </section>
              <section class="section-6"><a name="section-6"></a>
                <div class="section-bg-image"></div>
                <div class="container">
                  <div class="content">
                    <h1>Do more. Learn more.</h1>
                    <p class="text">Check out the accessories that make Dash and Dot even more engaging. Transform Dash into a projectile-launching machine or build your own accessories with our Lego &reg; compatible brick connectors.</p><a role="button" href="https://store.makewonder.com" class="button pill-button orange-button">Buy Now</a><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/launcher-6f819271d56be25d2e19c1bda5980b84.jpg" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/launcher_2x-b540f6ce0e0fceb353d6b0b719f442b8.jpg 2x" alt="Launcher" class="launcher-img">
                  </div>
                </div>
              </section>
              <section class="section-7"><a name="section-7"></a>
                <div class="container">
                  <div class="content">
                    <h1>Let's geek out, shall we?</h1>
                    <p class="text">Your kids will see a super fun robot. But if your inner-geek wants to know a little more, take a closer look at all the sophisticated technology Dash comes equipped with.</p>
                  </div>
                </div>
                <div class="section-bg-image"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/backgrounds/7-959fd4a099f841a17773a385e1313a1f.png" alt="Dash diagram" class="wide"><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/backgrounds/7_m-4bcc03afe431a7f5cca0287a9746e238.jpg" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/backgrounds/7_m_2x-bcc850fe22d654be88de602c0739386e.jpg" alt="Dash diagram" class="tall"></div>
              </section>
              <ul class="scroller"><a scroll-to="section-1" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-2" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-3" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-4" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-5" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-6" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a><a scroll-to="section-7" scroll-to-clear=".wat-header" class="outer-circle">
                  <div class="inner-circle"></div></a>
              </ul>
            </div>
            <section class="no-scroll-section">
              <section ng-controller="signupController" class="signup-section">
                <div class="container">
                  <p ng-show="signedUp">Thank you for signing up for the newsletter!</p>
                  <p ng-hide="signedUp">Stay in the know about new products, events, and more.</p>
                  <form ng-submit="signupNewsletter(email, 'Home Page')" name="heroSignupForm" class="home-signup-wrapper ng-cloak">
                    <div style="position: absolute; left: -5000px;">
                      <input type="text" name="b_6c1539b72e697a4dc1503b46b_e5770b11b0" tabindex="-1" value="" ng-model="bot">
                    </div>
                    <input type="email" placeholder="Your Email:" ng-model="email" ng-disabled="signedUp">
                    <button ng-disabled="heroSignupForm.$invalid || signedUp"><span ng-hide="loading">Sign Me Up!</span><span ng-show="loading" class="ng-cloak"><i class="fa fa-spinner fa-spin"></i></span></button>
                  </form>
                </div>
              </section>
              <section class="packed-with-value-section">
                <div class="container">
                  <h1>Packed with Value</h1>
                  <div class="packed-items">
                    <div><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/packed_dashbox-3ef24d4627cd720bfa7708518b737ee0.jpg" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/packed_dashbox_2x-3cba74a18857e0962d8d2a8c36f86555.jpg 2x" alt="Robots"/>
                      <h3>Robots</h3>
                      <p>Includes LEGO<sup>&reg;</sup> connectors &amp; charging cord</p>
                    </div>
                    <div class="big-plus">+</div>
                    <div><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/all_apps-3b729a905ae85a57382b9eea59e8ed78.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/all_apps_2x-4ada80062eaa7036ed32a5c7594f563c.png 2x" alt="All apps"/>
                      <h3>All Apps</h3>
                      <p>5 free apps with more to come</p>
                    </div>
                    <div class="big-plus">+</div>
                    <div><img src="https://d2eawk67eat224.cloudfront.net/assets/images/home/packed_guarantee-7c60e8266f78ad112f6e2bcba1ebe87d.png" srcset="https://d2eawk67eat224.cloudfront.net/assets/images/home/packed_guarantee_2x-75d69ca82a1f7314989a4d0c1ed50db5.png 2x" alt="Satisfaction Guarantee"/>
                      <h3>Satisfaction Guarantee</h3>
                      <p>30 day returns, no questions asked</p>
                    </div>
                  </div>
                  <hr>
                  <div class="price-wrap">
                    <h2>Starting at $49.99</h2><a id="buy-now-bottom" href="https://store.makewonder.com" role="button" class="buy-btn button pill-button orange-button animated-chevron-button">Buy Now&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
                  </div>
                </div>
              </section>
            </section>
          </div>
          
<!-- -->
          
        </div>
      </div>
      
      
      <div ng-controller="footerController" class="footer-container ng-scope">
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="footer-link-section">
                {/menu:getFootMenu/}
              <div class="footer-newsletter">
                {/menu:getFormNewsletter/}
              </div>
            </div>
          </div>
          
          <div class="footer-bottom row">
            <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
            <div class="legal-stuff">
              
              <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
            </div>
          </div>
          <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
        </div>
      </div>
    </div>
    </div>
    <script src="templates/frontend/makewonder_1.0/js/libraries.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/thriftFile.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/application.js"></script>
    <script type="text/javascript" src="templates/frontend/makewonder_1.0/js/script.js"></script>
    <!--[if lt IE 10]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.1.3/jquery.placeholder.min.js"></script>
    <script>
      $(function(){
        $('input, textarea').placeholder({ customClass: 'ie-placeholder' });  
      });
    </script><![endif]-->
    <script type="text/javascript" >
			pi = {}
			pi.campaign = "a749e38f556d5eb1dc13b9221d1f994f";
			pi.type = "MakewonderSG";
			pi.sitegroup = "MakewonderSG";
			pi.generateIpk = false;
			</script>
			<script type="text/javascript" src="templates/frontend/makewonder_1.0/js/pi.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004542796;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004542796/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
 </body></html>