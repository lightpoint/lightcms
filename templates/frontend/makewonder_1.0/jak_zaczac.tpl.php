<!DOCTYPE html>
<html lang="pl" ng-app="makeWonder.main" class="ng-scope">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="templates/frontend/makewonder_1.0/js/client.js"></script>
<link href="templates/frontend/makewonder_1.0/css/style.css" rel="stylesheet">
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">
<link href="http://www.makewonder.com/favicon.ico" rel="icon" type="image/x-icon">
<link href="http://www.makewonder.com/favicon.ico" rel="shortcun icon" type="image/x-icon"><!--[if lt IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script><![endif]-->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0 user-scalable=no">
{/metadata:jak_zaczac/}
<style>
  .header-right-top {
    display: none;
  }
  
  .header-right-bottom {
    display: none;
  }
  
</style>
 <script async="" type="text/javascript" src="templates/frontend/makewonder_1.0/js/roundtrip.js"></script>
 <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-70029657-1', 'auto');
ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
 <script>
 
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
 
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
 
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
 document,'script','//connect.facebook.net/en_US/fbevents.js');

 fbq('init', '1495186814109494');
 fbq('track', "PageView");</script>
 <noscript><img height="1" width="1" style="display:none"
 
src="https://www.facebook.com/tr?id=1495186814109494&ev=PageView&noscript=1"
 /></noscript>
 <!-- End Facebook Pixel Code --> 
 <!--google-->
 <script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-70029657-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 934334004;
    w.google_conversion_label = "75i4CLK7oWMQtJzDvQM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script> 
 </head>
  <body>
    <div class="main-page">
      <div class="mobile-nav">
        <div class="nav-items">
          {/menu:getMobileNav/}
        </div>
      </div>
      <div class="wrap">
        <noscript>
          &lt;div class="notice-bar"&gt;
            &lt;div class="container"&gt;Please note: This website requires JavaScript to be enabled in order to function properly. Please &lt;a href="http://www.enable-javascript.com/" target="_blank"&gt;enable your JavaScript&lt;/a&gt; and refresh the page.&lt;/div&gt;
          &lt;/div&gt;
        </noscript><!--[if lt IE 10 ]>
        <div ng-hide="dismissNotice" class="notice-bar">
          <div class="container">This website has limited support on Internet Explorer versions 9 and below. We recommend using the latest version of <a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank">Chrome</a>, <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">Firefox</a>, <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie" target="_blank">Internet Explorer </a>or <a href="http://support.apple.com/downloads/#safari" target="_blank">Safari</a>.<br><br>
            <button ng-click="dismissNotice = true" class="pill-button orange-button sm-button">Got it</button>
          </div>
        </div><![endif]-->
        <div scroll-off-top="scrolling" class="header-2 ng-scope active" id="header2"><a class="hamburger-icon"><i class="fa fa-bars"></i></a>
          <div class="container">
            <nav class="wat-header active">
              <ul>{/menu:getNav/}</ul>
            </nav>
          </div><img src="templates/frontend/makewonder_1.0/img/w_logo.png" srcset="templates/frontend/makewonder_1.0/img/w_logo.png 2x" alt="Wonder Workshop" class="mobile-logo">
        </div>
        <div class="main">
          <div class="start-2">
            <div class="masthead start-masthead main-masthead hero-2">
              <div class="hero-video desktop-only" style="overflow: hidden;">
                <video id="masthead-video" autoplay="" muted="" loop="" style="display: block; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); height: auto; width: 1263px;" class="video">
                  <source src="http://d2eawk67eat224.cloudfront.net/assets/video/getting_started-41f04e466beb6f73900bdb871ed26ba7.webm" type="video/webm">
                  <source src="http://d2eawk67eat224.cloudfront.net/assets/video/getting_started-4eb5ac665656da9f90908cb3a77c3c20.mp4" type="video/mp4">
                </video>
              </div>
              <div class="container">
                {/articles:zaczynajmy/}
              </div>
            </div>
            <div ng-controller="startController" class="wrapper ng-scope">
              <div class="start-instructions">
                <div class="container">
                  <section class="start-section start-download-section"><a name="download"></a>
                    <div class="half">
                      <div class="outer">
                        <div class="inner">
                          <div class="instructions-image download-image"></div>
                        </div>
                      </div>
                    </div>
                    <div class="half instructions-text">
                      <div class="instructions-text-text">
                        <div class="row"><img src="templates/frontend/makewonder_1.0/img/number1.png" srcset="templates/frontend/makewonder_1.0/img/number1_2x.png 2x" alt="Step number 1" class="number">
                          {/articles:sciagnij_aplikacje_wonder/}
                        </div><strong>Pobierz Go</strong>
                        <div class="download-buttons"><a href="https://itunes.apple.com/us/app/id928519463" target="_blank" role="button"><img src="templates/frontend/makewonder_1.0/img/appStore_Apple.png" srcset="templates/frontend/makewonder_1.0/img/appStore_Apple_2x.png 2x"></a><a href="https://play.google.com/store/apps/details?id=com.w2.apps.go" target="_blank" role="button"><img src="templates/frontend/makewonder_1.0/img/appStore_Google.png" srcset="templates/frontend/makewonder_1.0/img/appStore_Google_2x.png 2x"></a></div>
                      </div>
                    </div>
                  </section>
                  <hr>
                  <section class="start-section start-connect-section"><a name="connect"></a>
                    <div class="half half-mobile">
                      <div class="outer">
                        <div class="inner">
                          <div class="instructions-image connect-image"></div>
                        </div>
                      </div>
                    </div>
                    <div class="half instructions-text"><img src="templates/frontend/makewonder_1.0/img/number2.png" srcset="templates/frontend/makewonder_1.0/img/number2_2x.png 2x" alt="Step number 1" class="number">
                      <div class="instructions-text-text">
                        {/articles:polacz_sie_z_dashem_i_dotem/}
                      </div>
                    </div>
                    <div class="half half-tablet">
                      <div class="outer">
                        <div class="inner">
                          <div class="instructions-image connect-image"></div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <div id="not_visible" style="display:none;"><hr>
                  <section class="start-section sign-up-section"><a name="signup"></a>
                    <div class="half">
                      <div class="outer">
                        <div class="inner"><a click-full-screen-video="" video-src="9nM3WsevvfM" class="dash-dot-show"><img src="templates/frontend/makewonder_1.0/img/dashdot_show.jpg" srcset="templates/frontend/makewonder_1.0/img/dashdot_show_2x.jpg 2x"></a></div>
                      </div>
                    </div>
                    <div class="half instructions-text join-wonder-league">
                      <div class="instructions-text-text">
                        <div class="row"><img src="templates/frontend/makewonder_1.0/img/number3.png" srcset="templates/frontend/makewonder_1.0/img/number3_2x.png 2x" alt="Step number 3" class="number">
                          <h2 class="download-instruction">Join the Wonder League!</h2>
                        </div>
                        <p>Kids, sign up to get the <a href="https://www.youtube.com/watch?v=C1v8L1Jzl24&list=PLXSgvv3NnVuSYP9aZJOWPnkbVkxIAuoTX" target="_blank">Dash &amp; Dot Show</a>, play ideas, and learning challenges in your inbox weekly.</p>
                        <p>Sign up here to get the most out of Dash &amp; Dot.</p>
                        <div class="row"><a role="button" target="_blank" href="https://privohub.privo.com/ag/leadaccount?featureIdName=WONDER-NEWSLETTER#/ag/leadaccount/register?featureIdName=WONDER-NEWSLETTER&status=200" class="button pill-button animated-chevron-button">Sign Up Now <i class="fa fa-chevron-right"></i></a><a role="button" target="_blank" href="http://help.makewonder.com/customer/portal/topics/797150-privacy/articles" class="button pill-button">FAQ for Parents</a></div>
                      </div>
                    </div>
                  </section></div>
                  <hr>
                  <section class="start-section start-play-section"><a name="play"></a>
                    <div class="half mobile-only">
                      <div class="outer">
                        <div class="inner">
                          <div class="instructions-image play-image"></div>
                        </div>
                      </div>
                    </div>
                    <div class="half instructions-text">
                      <div class="instructions-text-text">
                        {/articles:zacznij_zabawe/}
                        <div style="padding: 1.5em 0" class="row"><a href="przewodnik_po_akcesoriach.html" role="button" class="button pill-button animated-chevron-button">Zamontuj akcesoria <i class="fa fa-chevron-right"></i></a><a href="aplikacje.html" class="button pill-button animated-chevron-button">Poznaj aplikacje <i class="fa fa-chevron-right"></i></a>
                        </div>
                        <p> <strong>Pobierz aplikacje:</strong></p>
                        <div class="download-buttons">
                          <div class="button-group"><a href="https://itunes.apple.com/us/artist/wonder-workshop-inc./id906750150" target="_blank" role="button"><img src="templates/frontend/makewonder_1.0/img/appStore_Apple.png" srcset="templates/frontend/makewonder_1.0/img/appStore_Apple_2x.png 2x"></a><a href="https://play.google.com/store/apps/developer?id=Wonder+Workshop" target="_blank" role="button"><img src="templates/frontend/makewonder_1.0/img/appStore_Google.png" srcset="templates/frontend/makewonder_1.0/img/appStore_Google_2x.png 2x"></a></div>
                        </div>
                      </div>
                    </div>
                    <div class="half desktop-only">
                      <div class="outer">
                        <div class="inner">
                          <div class="instructions-image play-image"></div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
              <section class="video-list-section">
                <div class="container">
                  <h1>Sprawdź wszystkie niesamowite rzeczy, jakie mogą robić dzieci, rodzice i nauczyciele.</h1>
                  <div class="video-list"><a click-full-screen-video="" video-src="znURM93rQOI" class="video-tile">
                      <div class="video-tile-preview"><i class="fa fa-play"></i></div>
                      <div class="video-tile-bottom">
                        <h4>Robot Fashion Show</h4>
                        <p>Amelia</p>
                        <p>7 lat</p>
                      </div></a><a click-full-screen-video="" video-src="dEpVsT4i3DA" class="video-tile">
                      <div class="video-tile-preview"><i class="fa fa-play"></i></div>
                      <div class="video-tile-bottom">
                        <h4>Godzina kodowania</h4>
                        <p>1. klasa Szkoły Podstawowej w Punahou</p>
                        <p>Hawaje, USA</p>
                      </div></a><a click-full-screen-video="" video-src="9nM3WsevvfM" class="video-tile">
                      <div style="background-image: url(templates/frontend/makewonder_1.0/img/watch_show_img.jpg);" class="video-tile-preview"><i class="fa fa-play"></i></div>
                      <div class="video-tile-bottom">
                        <h4>The Dash and Dot Show</h4>
                        <p>Wally i Mimi</p>
                        <p>11 i 12 lat</p>
                      </div></a></div><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ" target="_blank" class="button pill-button see-more-link">Zobacz więcej inspiracji</a>
                  <hr>
                  <h1>Napisali o nas:</h1>
                  <div class="brand-list row"><a id="antyweb" href="http://antyweb.pl/dash-i-dot-to-nowa-zabawka-uczaca-programowania-i-logicznego-myslenia/" target="_blank" class="brand-item">
                      <div><img src="templates/frontend/makewonder_1.0/img/media/antyweb.png" alt="AntyWeb"></div></a>
                      <a id="komputerswiat" href="http://www.komputerswiat.pl/nowosci/sprzet/2015/49/roboty-firmy-wonder-workshop-ucza-dzieci-programowac.aspx" target="_blank" class="brand-item">
                      <div><img src="templates/frontend/makewonder_1.0/img/media/komputerswiat.png" alt="Digital Trends"></div></a>
                      <a id="wprost" href="http://www.wprost.pl/ar/527879/KNOW-HOW/" target="_blank" class="brand-item">
                      <div><img src="templates/frontend/makewonder_1.0/img/media/wprost.png" alt="Wprost"></div></a>
                      <a id="mamygadzety" href="http://mamygadzety.pl/dash-i-dot/" target="_blank" class="brand-item">
                      <div><img src="templates/frontend/makewonder_1.0/img/media/mamygadzety.png" alt="Mamy gadżety"></div></a>
                      <a id="wptv" href="http://zuchpisze.pl/jak-nauczyc-dzieci-programowania-podczas-swietnej-zabawy-roboty-dash-i-dot-przychodza-z-pomoca/" target="_blank" class="brand-item">
                      <div><img src="templates/frontend/makewonder_1.0/img/media/zuchpisze.png" alt="Zuch Pisze"></div></a>
                  </div>
                </div>
              </section>
              <div class="start-faq">
                <div class="container">
                  <div class="row">
                    {/faq:ladowanie_i_uzytkowanie/}
                    {/faq:rozwiazywanie_problemow/}
                  </div>
                  <div style="padding: 1.5em 0; text-align: center;" class="row"><a href="pomoc.html" role="button" class="button pill-button animated-chevron-button">Przejrzyj wszystkie pytania <i class="fa fa-chevron-right"></i></a>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div ng-controller="footerController" class="footer-container ng-scope">
        <div class="container">
          <div class="footer-top">
            <div class="row">
              <div class="footer-link-section">
                {/menu:getFootMenu/}
              <div class="footer-newsletter">
                {/menu:getFormNewsletter/}
              </div>
            </div>
          </div>
          <div class="footer-bottom row">
            <div class="social-links"><a href="https://www.facebook.com/makewonderpl" target="_blank" role="button" class="social-link social-link-facebook sweep-btn"><i class="fa fa-facebook"></i></a><a href="https://www.youtube.com/channel/UCZgluWgYIZ5k5EVHNUziTeQ/featured" target="_blank" role="button" class="social-link social-link-youtube sweep-btn"><i class="fa fa-youtube"></i></a><a href="https://instagram.com/_wonderworkshop" target="_blank" role="button" class="social-link social-link-instagram sweep-btn"><i class="fa fa-instagram"></i></a><a href="https://www.pinterest.com/makewonder1" target="_blank" role="button" class="social-link social-link-pinterest sweep-btn"><i class="fa fa-pinterest"></i></a></div>
            <div class="legal-stuff">
              
              <div class="copyright">Copyright © Wonder Workshop, Inc.</div>
            </div>
          </div>
          <div class="apple-tm-attribution">Apple, the Apple logo, and iPad are trademarks of Apple Inc., registered in the U.S. and other countries.</div>
        </div>
      </div>
    </div>
    <script src="templates/frontend/makewonder_1.0/js/libraries.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/thriftFile.js"></script>
    <script src="templates/frontend/makewonder_1.0/js/application.js"></script>
    <script type="text/javascript" src="templates/frontend/makewonder_1.0/js/script.js"></script>
    <!--[if lt IE 10]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.1.3/jquery.placeholder.min.js"></script>
    <script>
      $(function(){
        $('input, textarea').placeholder({ customClass: 'ie-placeholder' });  
      });
    </script><![endif]-->
    <script>
      $('html:not(.android) #masthead-video').coverVid(1920, 1080);
      
      $(function(){
        $('html:not(.android) #masthead-video').css({
          display: "block"
        });
      });
      
    </script>
    <script type="text/javascript" >
			pi = {}
			pi.campaign = "a749e38f556d5eb1dc13b9221d1f994f";
			pi.type = "MakewonderSG";
			pi.sitegroup = "MakewonderSG";
			pi.generateIpk = false;
			</script>
		<script type="text/javascript" src="templates/frontend/makewonder_1.0/js/pi.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1004542796;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1004542796/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body></html>