var imgRegExp = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i,txtRegExp = /\.(txt|html)(.*)?$/i, type, wrapContent, className;
var wrapBox = '<div id="lightPanel"><div id="lightContainer"><div id="lightHeader"><div id="lightTitle"></div><div id="lightControls"><button id="lightClose"></button></div></div><div id="lightContent"></div></div></div>';
var wrapMessage = '<div id="lightPanel" class="lightPanelInfo"><div id="lightContainer"><div id="lightHeader"><div id="lightControls"><button id="lightClose"></button></div></div><div id="lightContent" class="lightContentInfo"></div><div class="lightContentInfo"><span class="diamond dButton"><button class="lightClose">zamknij</button></span></div></div></div>';
var wrapInfoModal = '<div id="lightMasterModal"></div>';
var wrapInfo = '<div id="lightMaster"></div>';
var wrapFooter = '';
var responseFalse = 'brak danych';

$(document).ready(function() {
	$(document).on("click",'.light' ,function(event) {
		event.preventDefault();
		
		var href = $(this).attr('href');
		var classInfo = $(this).attr('class');
		if(classInfo=='light')
			className = '';
		else className = classInfo;
		
		if(href.match(imgRegExp)) 
			type = 'image';
    else if(href.match(txtRegExp)) 
			type = 'txt';
		else type = 'ajax';
		
		if($(this).attr('title')=='' || $(this).attr('title')==undefined)
			var title = '';
		else var title = $(this).attr('title');
		
		//$(wrapInfo+wrapBox).hide().appendTo("body").fadeIn(800);	
		$(wrapInfo+wrapBox).appendTo("body");
		
		if(type=='image') {
			var myImage = document.createElement('img');
			myImage.src = href;
			myImage.title = title;
			myImage.id = 'lightImage';
			
  		$('#lightContent').append(myImage);
  		$("#lightImage").load(function() {
  			sizeWindowImg(myImage.width, myImage.height, title, className);
  		});
		} else if(type=='ajax') {
			$.ajax({
	  		url: href,
	  		cache: false
			}).done(function(response) {
	  		//$('body').append(wrapBox+wrapFooter);
	  		$('#lightContent').append(response);
	  		sizeWindow();
			}).fail(function(html) {
	  		//$('body').append(wrapBox+wrapFalse+wrapFooter);
	  		sizeWindow();
			});
		} else if(type=='txt') {
      $.get(href, function(data) {
        $( ".result" ).html( data ); 
      });
    }
		
	});
	
	$(document).on("click",'#lightClose, #lightMaster, .lightClose' ,function() {
		$(this).lightClose();
	});
	
	$(document).keydown(function(event) {
  	if(event.which==27)
  		$(this).lightClose();
	});
	
	function sizeWindow() {
		var widthRatio;
		var windowHeight = window.innerHeight;
		var windowWidth = window.innerWidth;
		
		//resolutions
		if(windowWidth>1000)
			widthRatio = 0.56;
			
		if(windowWidth<1000)
			widthRatio = 0.7;
			
		if(windowWidth<767)
			widthRatio = 0.98;
		
		var minWindowWidth = Math.floor(windowWidth*widthRatio);
		
		$('#lightPanel').css('width', minWindowWidth+'px');
		
		var lightPanelWidth = Math.floor($('#lightPanel').innerWidth()/2);
		var lightPanelHeight = Math.floor($('#lightPanel').innerHeight()/2);
		
		var styles = {
    	marginLeft: '-'+lightPanelWidth+'px',
    	marginTop: '-'+lightPanelHeight+'px',
    	left: '50%',
			top: '50%',
  	};
		$('#lightPanel').css(styles);

	  $("#lightPanel, #lightPanel img, #lightPanel.zoom").show( "scale", 
       {percent: 100, direction: "both"}, 500 );
	   

		var sh = window.innerHeight;
		var lh = $('#lightPanel').innerHeight();
		//console.log(sh+ ' -- '+lh);
		if(sh<lh) {
			sh = sh - 100;	
			$('#lightContent').css('height',sh+'px');
		}
	}
	
	function sizeWindowImg(imgWidth, imgHeight, title, className) {
		var windowHeight = window.innerHeight;
		var windowWidth = window.innerWidth;
		
		if(className!='')
    	$('#lightPanel').addClass(className);
		
		if(imgWidth>windowWidth) {
			imgWidth = Math.floor(windowWidth-(windowWidth/10));
			$('#lightPanel img').css('width', imgWidth+'px');
			imgHeight = 	$('#lightPanel img').height();
		}
		
		if(imgHeight>windowHeight) {
			imgHeight = Math.floor(windowHeight-(windowHeight/10));
			$('#lightPanel img').css('height', imgHeight+'px');
			imgWidth = 	$('#lightPanel img').width();
		}
		
		var headerHeight = $('#lightHeader').height();
		imgHeight = imgHeight+headerHeight;
		
		$('#lightPanel').css({'width': imgWidth+'px','height': imgHeight+'px'});
		
		var lightPanelWidth = Math.floor($('#lightPanel').innerWidth()/2);
		var lightPanelHeight = Math.floor($('#lightPanel').innerHeight()/2);
	
		var styles = {
			left: '50%',
			top: '50%',
    	marginLeft: '-'+lightPanelWidth+'px',
    	marginTop: '-'+lightPanelHeight+'px',
  	};
		$('#lightPanel').css(styles);

	  $("#lightPanel, #lightPanel img, #lightPanel.zoom").show( "scale", 
       {percent: 100, direction: "both"}, 2000 );
    $('#lightTitle').html(title);
    
	}
	
});

(function($) {
	$.fn.lightClose = function() {
		$('#lightPanel').remove();
		$('#lightMaster, #lightMasterModal').fadeOut(200, function() {
	    $('#lightMaster, #lightMasterModal').remove();
  	});
	}
	
	$.fn.lightInfo = function(info) {
		$(wrapInfoModal+wrapMessage).hide().appendTo("body").fadeIn(800);
		$('#lightContent').append(info);
	}
})(jQuery);