<?php
error_reporting(0);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Podgląd</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="templates/default/css/view.css" type="text/css" media="screen" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript" src="http://jqueryrotate.googlecode.com/svn/trunk/jQueryRotate.js"></script>

</head>
<body onload='startup();'>
<div id="topMenu">menu <button id="rotateLeft"><img src="templates/default/img/left.png" width="16" title="obróć w lewo"></button> <button id="rotateRight"><img src="templates/default/img/right.png" width="16" title="obróć w prawo"></button> | <button id="zoomOut"><img src="templates/default/img/zoomout.png" width="16" title="pomniejsz"></button> <button id="zoomIn"><img src="templates/default/img/zoomin.png" width="16" title="powiększ"></button> <button id="toWidth"><img src="templates/default/img/width.png" width="16" title="dopasuj do szerokości ekranu"></button> <button id="toHeight"><img src="templates/default/img/height.png" width="16" title="dopasuj do wysokości ekranu"></button></div>

<img src="<?php echo $image; ?>" id="image" align="center">

	<script type="text/javascript">
		$(document).ready(function(){ 
			var angle = 0;
 			//var screenWidthS = screen.availWidth;
 			//var screenHeightS = screen.availHeight;
			var screenWidthS = document.body.clientWidth-30;
 			var screenHeightS = window.innerHeight-70;
			console.log(screenWidthS+' + '+screenHeightS);
			$("#rotateLeft").click(function(){
				angle -= 90;
				$("#image").rotate(angle);
			});
			
			$("#rotateRight").click(function(){
				angle += 90;
				$("#image").rotate(angle);
			});
		
		
		$("#zoomIn").click(function(){
			var sImage = document.getElementById('image'); ;
			var sWidth = sImage.width;
			var sHeight = sImage.height;
			sWidth = sWidth+100;
			
			$('#image').css('width',sWidth+'px').css('height','');
		});
		
		$("#zoomOut").click(function(){
			var sImage = document.getElementById('image'); ;
			var sWidth = sImage.width;
			var sHeight = sImage.height;
			sWidth = sWidth-100;
			
			$('#image').css('width',sWidth+'px').css('height','');
		});
		
		$("#toWidth").click(function(){
			$('#image').css('width',screenWidthS+'px').css('height','');
		});
		
		$("#toHeight").click(function(){
			$('#image').css('height',screenHeightS+'px').css('width','');
			
		});
	});
	</script>
</body>
</html>