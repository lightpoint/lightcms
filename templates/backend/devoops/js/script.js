$(document).ready(function() {
	$(".fancybox").fancybox();
	
	$(document).on("click",'.btn-request', function(event){
		event.preventDefault();
		var href = $(this).attr('href');
		
		$.post(href)
	  .done(function(json) {
	    var obj = jQuery.parseJSON(json);
	    	var btnInactive = '<a href="request.php?action=instagram&option=activate&id='+obj.id+'" class="btn btn-success btn-label-left btn-request"><span><i class="fa fa-instagram"></i></span>uaktywnij</a><br/><a href="request.php?action=instagram&option=delete&id='+obj.id+'" class="btn btn-danger btn-label-left btn-request"><span><i class="fa fa-minus-square"></i></span>usuń</a>';
	    	
	    	var btnActive = '<a href="request.php?action=instagram&option=deactivate&id='+obj.id+'" class="btn btn-warning btn-label-left btn-request btn-request"><span><i class="fa fa-instagram"></i></span>dezaktywuj</a><br/><a href="request.php?action=instagram&option=delete&id='+obj.id+'" class="btn btn-danger btn-label-left btn-request"><span><i class="fa fa-minus-square"></i></span>usuń</a>';
				if(obj.status=='active') {
					$('.'+obj.id).removeClass('inactive').addClass('active');
					$('.'+obj.id+' .ins-buttons').html(btnActive);
				}
				
				if(obj.status=='inactive') {
					$('.'+obj.id).removeClass('active').addClass('inactive');
					$('.'+obj.id+' .ins-buttons').html(btnInactive);
				}
				
				if(obj.status=='delete') {
					$('.'+obj.id).remove();
				}
	  });
	});
});