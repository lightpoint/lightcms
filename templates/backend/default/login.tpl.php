<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>ZALOGUJ SIĘ</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<link rel="stylesheet" href="/templates/backend/default/css/login.css" type="text/css" media="screen" />
	<script>
		function setFocus() {
			document.getElementById('l_login').focus(); 
		}
	</script>
</head>
<body onload="setFocus();">
	<div id="header">
			<div id="siteInfo">LIGHTCMS<br/><span> PANEL&nbsp;ADMINISTRACYJNY</span></div>
		</div>
	<div id="panel">
		<div id="loginInfo"><span class="__CLASS">Zaloguj się</span></div>
		<form name="login" method="post" action="login.php">
			<p align="right"><label>login</label><input type="text" name="l_login" id="l_login"></p>
			<p align="right"><label>hasło</label><input type="password" name="l_pass"></p>
			<p align="right"><input type="submit" name="l_submit" id="l_submit" value="zaloguj"></p>
		</form>
	</div>
	<div id="footer">
		<div id="foot1">LightCMS</div>
		<div id="foot2"><a href="http://www.lightpoint.pl" target="_blank">Lightpont 2013</a></div>
	</div>
</body>
</html>