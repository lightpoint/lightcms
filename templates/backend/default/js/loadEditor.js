tinyMCE.init({
	mode : "textareas",
  language : "pl",
	elements : "ajaxfilemanager",
	theme : "advanced",
	plugins : "advimage,advlink,media,contextmenu,fullscreen",
  /*toolbar: "fullscreen",*/
	/*theme_advanced_buttons1_add_before : "newdocument,separator",*/
	theme_advanced_buttons1_add : "fontselect,fontsizeselect",
	theme_advanced_buttons2_add : "separator,forecolor,backcolor,liststyle",
	theme_advanced_buttons2_add_before: "cut,copy,separator,",
	theme_advanced_buttons3_add_before : "",
	theme_advanced_buttons3_add : "media,separator,fullscreen",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	extended_valid_elements : "hr[class|width|size|noshade]",
	/*file_browser_callback : "ajaxfilemanager",*/
	file_browser_callback : "airfilemanager",
	paste_use_dialog : false,
	theme_advanced_resizing : true,
	theme_advanced_resize_horizontal : true,
	apply_source_formatting : true,
	force_br_newlines : true,
	force_p_newlines : false,
	relative_urls : true,
	convert_urls : false,
	remove_script_host : false,
	apply_source_formatting : false,
	valid_elements : "*[*]",
	extended_valid_elements : "a[*]",
});

function airfilemanager(field_name, url, type, win)
{
 switch(type) {
 	case 'media':
	case 'image':
	case 'flash':
	case 'file':
		break;
	default:
		return !1;
 }
 tinyMCE.activeEditor.windowManager.open({
	url: "../../../../js/tiny_mce/plugins/airfilemanager/afm.php"+"?type="+type,
	width: 780, height: 500, inline: "yes", close_previous: "no"
  },{
	window: win, input: field_name
  });
}