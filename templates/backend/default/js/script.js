$(document).ready(function(){
    var lesson_att_line = '<p><input type="file" name="attachment[]" class="lesson_attachment"></p>';
    $("#cancel").click(function(event) {
        event.preventDefault();
        history.back(1);
    });
	
    $(".cancel").click(function(event) {
        event.preventDefault();
        history.back(1);
    });
	
    $(document).on("click",'#filterCatButton', function(event){
        var sCatId = $('#ART_CAT_ID').val();
	document.location.href = '?action=articles&option=filter&sCatId='+sCatId;
    });
	
    $(document).on("click",'#filterFCatButton', function(event){
        var sCatId = $('#FCA_ID').val();
	document.location.href = '?action=faq&option=filter&sFCatId='+sCatId;
    });
    
    $(document).on('change','.lesson_attachment', function() {
       $('#attachments_container').append(lesson_att_line);
       console.log('file add');
    });
    
    /*$('.lesson_attachment.add').on('change', function() {
       $('#attachments_container').html(lesson_att_line);
       console.log('file add');
    });*/
    
    $(".add-subject").click(function(event) {
        event.preventDefault();
        $('#subject-container').append('<div class="col-sm-12"><input type="text" class="form-control" name="lesson_objective[]"></div>');
    });
    
    $(".add-helper").click(function(event) {
        event.preventDefault();
        $('#helper-container').append('<div class="col-sm-12"><input type="text" class="form-control" name="lesson_helpers[]"></div>');
    }); 
    
    $(".del-input").click(function(event) {
        event.preventDefault();
        $(this).parent().parent().remove();
    });
    
    $(".del-inspiration").click(function(event) {
			event.preventDefault();
			$(this).parent().parent().remove();
			$.post("request.php",{action: 'inspiracje', option: 'delFile', file: $(this).attr('file')})
				.done(function(response) {
					if(response==='ok') {
						//alert($(this));
			    	
			    }
			});
		});
		
		$(".del-attachment").click(function(event) {
			event.preventDefault();
			if (confirm("Czy na pewno chcesz usunąć tę pozycję?")) {
				$(this).parent().parent().remove();
				$.post("request.php",{action: 'lekcje', option: 'delAttachment', file: $(this).attr('file')})
					.done(function(response) {
						if(response==='ok') {
							//alert($(this));
				    	$(this).parent().parent().remove();
				    }
				});
			}
		});
});

function dialog_del(plik) {
  if (!confirm("Czy na pewno chcesz usunąć tę pozycję?"))
  return ;
  window.location=plik;
}

function dialog_save(plik) {
  if (!confirm("Zapisać dane?"))
  return ;
  window.location=plik;
}