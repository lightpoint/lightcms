<?php
	/**
	 * language pack
	 * @author Logan Cai (cailongqun [at] yahoo [dot] com [dot] cn)
	 * @link www.phpletter.com
	 * @since 22/April/2007
	 * pl by lukawar
	 */
	define('DATE_TIME_FORMAT', 'd/M/Y H:i:s');
	//Common
	//Menu
	
	
	
	
	define('MENU_SELECT', 'Wybierz');
	define('MENU_DOWNLOAD', 'Pobierz');
	define('MENU_PREVIEW', 'Podgląd');
	define('MENU_RENAME', 'Zmiana nazwy');
	define('MENU_EDIT', 'Eycja');
	define('MENU_CUT', 'Wytnij');
	define('MENU_COPY', 'Kopiuj');
	define('MENU_DELETE', 'Usuń');
	define('MENU_PLAY', 'Odtwórz');
	define('MENU_PASTE', 'Wklej');

	//Label
		//Top Action
		define('LBL_ACTION_REFRESH', 'Odśwież');
		define('LBL_ACTION_DELETE', 'Usuń');
		define('LBL_ACTION_CUT', 'Wytnij');
		define('LBL_ACTION_COPY', 'Kopiuj');
		define('LBL_ACTION_PASTE', 'Wklej');
		define('LBL_ACTION_CLOSE', 'Zamknij');
		define('LBL_ACTION_SELECT_ALL', 'Zaznacz wszytko');
		//File Listing
	define('LBL_NAME', 'Nazwa');
	define('LBL_SIZE', 'Rozmiar');
	define('LBL_MODIFIED', 'Zmodyfikowano');
		//File Information
	define('LBL_FILE_INFO', 'Informacje o pliku:');
	define('LBL_FILE_NAME', 'Nazwa:');
	define('LBL_FILE_CREATED', 'Utworzono:');
	define('LBL_FILE_MODIFIED', 'Zmodyfikowano:');
	define('LBL_FILE_SIZE', 'Rozmiar pliku:');
	define('LBL_FILE_TYPE', 'Typ pliku:');
	define('LBL_FILE_WRITABLE', 'Do zapisu');
	define('LBL_FILE_READABLE', 'Do odczytu');
		//Folder Information
	define('LBL_FOLDER_INFO', 'Informacje o folderze');
	define('LBL_FOLDER_PATH', 'Folder:');
	define('LBL_CURRENT_FOLDER_PATH', 'Lokalizaja foldera:');
	define('LBL_FOLDER_CREATED', 'Utworzono:');
	define('LBL_FOLDER_MODIFIED', 'Zmodyfikowano:');
	define('LBL_FOLDER_SUDDIR', 'Podfoldery:');
	define('LBL_FOLDER_FIELS', 'Pliki:');
	define('LBL_FOLDER_WRITABLE', 'Do zapisu');
	define('LBL_FOLDER_READABLE', 'Do odczytu');
	define('LBL_FOLDER_ROOT', 'Folder główny');
		//Preview
	define('LBL_PREVIEW', 'Podgląd');
	define('LBL_CLICK_PREVIEW', 'Kliknij, by zobaczyć podgląd.');
	//Buttons
	define('LBL_BTN_SELECT', 'Wybierz');
	define('LBL_BTN_CANCEL', 'Anuluj');
	define('LBL_BTN_UPLOAD', '<b>Wyślij na serwer</b>');
	define('LBL_BTN_CREATE', 'Utwórz');
	define('LBL_BTN_CLOSE', 'Zamknij');
	define('LBL_BTN_NEW_FOLDER', 'Nowy folder');
	define('LBL_BTN_NEW_FILE', 'Nowy plik');
	define('LBL_BTN_EDIT_IMAGE', 'Edycja');
	define('LBL_BTN_VIEW', 'Wybierz widok');
	define('LBL_BTN_VIEW_TEXT', 'Tekst');
	define('LBL_BTN_VIEW_DETAILS', 'Szczegóły');
	define('LBL_BTN_VIEW_THUMBNAIL', 'Miniaturki');
	define('LBL_BTN_VIEW_OPTIONS', 'Zobacz w:');
	//pagination
	define('PAGINATION_NEXT', 'Następny');
	define('PAGINATION_PREVIOUS', 'Poprzedni');
	define('PAGINATION_LAST', 'Ostatni');
	define('PAGINATION_FIRST', 'Pierwszy');
	define('PAGINATION_ITEMS_PER_PAGE', 'Wyświetl %s elementów na stronie');
	define('PAGINATION_GO_PARENT', 'Do katalogu nadrzędnego');
	//System
	define('SYS_DISABLED', 'Brak dostępu: The system is disabled.');

	//Cut
	define('ERR_NOT_DOC_SELECTED_FOR_CUT', 'Nie wybrano dokumentu do przeniesienia.');
	//Copy
	define('ERR_NOT_DOC_SELECTED_FOR_COPY', 'Nie wybrano dokumentu do kopiowania.');
	//Paste
	define('ERR_NOT_DOC_SELECTED_FOR_PASTE', 'Nie wybrano dokumentu do kopiowania.');
	define('WARNING_CUT_PASTE', 'Czy jesteś pewny, że chcesz przenieść wybrany dokument do obecnego folderu?');
	define('WARNING_COPY_PASTE', 'Czy jesteś pewny, że chcesz przekopiowć wybrany dokument do obecnego folderu?');
	define('ERR_NOT_DEST_FOLDER_SPECIFIED', 'Brak adresu folderu docelowego.');
	define('ERR_DEST_FOLDER_NOT_FOUND', 'Nie znaleziono adresu folderu docelowego.');
	define('ERR_DEST_FOLDER_NOT_ALLOWED', 'Brak pozwolenia na przeniesienie plików');
	define('ERR_UNABLE_TO_MOVE_TO_SAME_DEST', 'Błąd podczas przenoszenia pliku (%s): podana ścieżka jest taka sama, jak foldera docelowego.');
	define('ERR_UNABLE_TO_MOVE_NOT_FOUND', 'Błąd podczas przenoszenia pliku (%s): ścieżka nie istnieje.');
	define('ERR_UNABLE_TO_MOVE_NOT_ALLOWED', 'Błąd podczas przenoszenia pliku (%s): brak dostępu do pliku.');

	define('ERR_NOT_FILES_PASTED', 'Nie wklejono żadnych plików.');

	//Search
	define('LBL_SEARCH', 'Szukaj');
	define('LBL_SEARCH_NAME', 'Pełna/Częściowa nazwa:');
	define('LBL_SEARCH_FOLDER', 'Szukaj w:');
	define('LBL_SEARCH_QUICK', 'Szybkie szukanie');
	define('LBL_SEARCH_MTIME', 'Czas modyfikacji pliku(Zakres):');
	define('LBL_SEARCH_SIZE', 'Rozmiar pliku:');
	define('LBL_SEARCH_ADV_OPTIONS', 'Opcje zaawansowane');
	define('LBL_SEARCH_FILE_TYPES', 'Typy plików:');
	define('SEARCH_TYPE_EXE', 'Aplikacje');

	define('SEARCH_TYPE_IMG', 'Obraz');
	define('SEARCH_TYPE_ARCHIVE', 'Archiwum');
	define('SEARCH_TYPE_HTML', 'HTML');
	define('SEARCH_TYPE_VIDEO', 'Video');
	define('SEARCH_TYPE_MOVIE', 'Film');
	define('SEARCH_TYPE_MUSIC', 'Muzyka');
	define('SEARCH_TYPE_FLASH', 'Flash');
	define('SEARCH_TYPE_PPT', 'PowerPoint');
	define('SEARCH_TYPE_DOC', 'Dokument');
	define('SEARCH_TYPE_WORD', 'Word');
	define('SEARCH_TYPE_PDF', 'PDF');
	define('SEARCH_TYPE_EXCEL', 'Excel');
	define('SEARCH_TYPE_TEXT', 'Tekst');
	define('SEARCH_TYPE_UNKNOWN', 'Nieznany');
	define('SEARCH_TYPE_XML', 'XML');
	define('SEARCH_ALL_FILE_TYPES', 'Wszystkie typy plików');
	define('LBL_SEARCH_RECURSIVELY', 'Szukanie rekurencyjne:');
	define('LBL_RECURSIVELY_YES', 'Tak');
	define('LBL_RECURSIVELY_NO', 'Nie');
	define('BTN_SEARCH', 'Szukaj');
	//thickbox
	define('THICKBOX_NEXT', 'Następny&gt;');
	define('THICKBOX_PREVIOUS', '&lt;Poprzedni');
	define('THICKBOX_CLOSE', 'Zamknij');
	//Calendar
	define('CALENDAR_CLOSE', 'Zamknij');
	define('CALENDAR_CLEAR', 'Wyczyść');
	define('CALENDAR_PREVIOUS', '&lt;Poprzedni');
	define('CALENDAR_NEXT', 'Następny&gt;');
	define('CALENDAR_CURRENT', 'Dzisiaj');
	define('CALENDAR_MON', 'Pn');
	define('CALENDAR_TUE', 'Wt');
	define('CALENDAR_WED', 'Śr');
	define('CALENDAR_THU', 'Cz');
	define('CALENDAR_FRI', 'Pi');
	define('CALENDAR_SAT', 'So');
	define('CALENDAR_SUN', 'Ni');
	define('CALENDAR_JAN', 'Sty');
	define('CALENDAR_FEB', 'Lut');
	define('CALENDAR_MAR', 'Mar');
	define('CALENDAR_APR', 'Kwi');
	define('CALENDAR_MAY', 'Maj');
	define('CALENDAR_JUN', 'Cze');
	define('CALENDAR_JUL', 'Lip');
	define('CALENDAR_AUG', 'Sie');
	define('CALENDAR_SEP', 'Wrz');
	define('CALENDAR_OCT', 'Paz');
	define('CALENDAR_NOV', 'Lis');
	define('CALENDAR_DEC', 'Gru');
	//ERROR MESSAGES
		//deletion
	define('ERR_NOT_FILE_SELECTED', 'Wybierz plik.');
	define('ERR_NOT_DOC_SELECTED', 'Nie wybrano dokumentu do usunięcia.');
	define('ERR_DELTED_FAILED', 'Nie można usunąć wybranego dokumentu.');
	define('ERR_FOLDER_PATH_NOT_ALLOWED', 'Podana ścieżka do dokumentu jest nieprawidłowa.');
		//class manager
	define('ERR_FOLDER_NOT_FOUND', 'Nie można zlokalizować podanego folderu: ');
		//rename
	define('ERR_RENAME_FORMAT', 'Podaj nazwę zawierającą wyłącznie litery, cyfry, spacje, łączniki i podkreślenia.');
	define('ERR_RENAME_EXISTS', 'Wpisz nazwę unikalną w obecnym folderze.');
	define('ERR_RENAME_FILE_NOT_EXISTS', 'Plik/folder nie istnieje.');
	define('ERR_RENAME_FAILED', 'Nie można zmienić nazwy, spróbuj ponownie.');
	define('ERR_RENAME_EMPTY', 'Wpisz nazwę.');
	define('ERR_NO_CHANGES_MADE', 'Nie wykryto zmian.');
	define('ERR_RENAME_FILE_TYPE_NOT_PERMITED', 'Brak pozwolenia na zmianę rozszerzenia.');
		//folder creation
	define('ERR_FOLDER_FORMAT', 'Podaj nazwę zawierającą wyłącznie litery, cyfry, spacje, łączniki i podkreślenia.');
	define('ERR_FOLDER_EXISTS', 'Wpisz nazwę unikalną w obecnym folderze.');
	define('ERR_FOLDER_CREATION_FAILED', 'Nie można utworzyć folderu, sppóbuj ponownie.');
	define('ERR_FOLDER_NAME_EMPTY', 'Wpisz nazwę.');
	define('FOLDER_FORM_TITLE', 'Nowy folder');
	define('FOLDER_LBL_TITLE', 'Nazwa fodera:');
	define('FOLDER_LBL_CREATE', 'Utwórz folder');
	//New File
	define('NEW_FILE_FORM_TITLE', 'Nowy plik');
	define('NEW_FILE_LBL_TITLE', 'Nazwa pliku:');
	define('NEW_FILE_CREATE', 'Utwórz plik');
		//file upload
	define('ERR_FILE_NAME_FORMAT', 'Podaj nazwę zawierającą wyłącznie litery, cyfry, spacje, łączniki i podkreślenia.');
	define('ERR_FILE_NOT_UPLOADED', 'Nie wybrano plików do wysłania na serwer.');
	define('ERR_FILE_TYPE_NOT_ALLOWED', 'Brak uprawnień, do wysłania tego typu plików.');
	define('ERR_FILE_MOVE_FAILED', 'Błąd podczas przenoszenia plików.');
	define('ERR_FILE_NOT_AVAILABLE', 'Plik jest niedostępny.');
	define('ERROR_FILE_TOO_BID', 'Wybrany plik ma zbyt duży rozmiar. (max: %s)');
	define('FILE_FORM_TITLE', 'Wysyłanie plików na serwer');
	define('FILE_LABEL_SELECT', 'Wybierz plik');
	define('FILE_LBL_MORE', 'Dodaj plik do wysłania');
	define('FILE_CANCEL_UPLOAD', 'Anuluj wysyłanie');
	define('FILE_LBL_UPLOAD', 'Wyślij pliki');
	//file download
	define('ERR_DOWNLOAD_FILE_NOT_FOUND', 'Nie wybrano plików do wyslania.');
	//Rename
	define('RENAME_FORM_TITLE', 'Zmiana nazwy');
	define('RENAME_NEW_NAME', 'Nowa nazwa');
	define('RENAME_LBL_RENAME', 'Zmień nazwę');

	//Tips
	define('TIP_FOLDER_GO_DOWN', 'Kliknij, by otworzyć folder...');
	define('TIP_DOC_RENAME', 'Podwójne kliknięcie - edycja...');
	define('TIP_FOLDER_GO_UP', 'Kliknij by wrócić do foderu nadrzędnego...');
	define('TIP_SELECT_ALL', 'Wybierz wszystko');
	define('TIP_UNSELECT_ALL', 'Odznacz wszystko');
	//WARNING
	define('WARNING_DELETE', 'Jesteś pewien, że chcesz usunąć wybrany dokument?');
	define('WARNING_IMAGE_EDIT', 'Wybierz obraz do edycji.');
	define('WARNING_NOT_FILE_EDIT', 'Wybierz plik do edycji.');
	define('WARING_WINDOW_CLOSE', 'Zamknąć okno?');
	//Preview
	define('PREVIEW_NOT_PREVIEW', 'Podgląd niedostępny.');
	define('PREVIEW_OPEN_FAILED', 'Nie można otworzyć pliku.');
	define('PREVIEW_IMAGE_LOAD_FAILED', 'Nie można załadować obrazka');

	//Login
	define('LOGIN_PAGE_TITLE', 'Ajax File Manager Login Form');
	define('LOGIN_FORM_TITLE', 'Login Form');
	define('LOGIN_USERNAME', 'użytkownik:');
	define('LOGIN_PASSWORD', 'hasło:');
	define('LOGIN_FAILED', 'błędne hasło/użytkownik.');
	
	
	//88888888888   Below for Image Editor   888888888888888888888
		//Warning 
		define('IMG_WARNING_NO_CHANGE_BEFORE_SAVE', 'Nie wykryto zmian w obrazku.');
		
		//General
		define('IMG_GEN_IMG_NOT_EXISTS', 'Obraz nie istnieje');
		define('IMG_WARNING_LOST_CHANAGES', 'Wszystkie niezapisane zmiany przepadną, kontynuować?');
		define('IMG_WARNING_REST', 'Wszystkie niezapisane zmiany przepadną, wykonać reset?');
		define('IMG_WARNING_EMPTY_RESET', 'Nie wykonano żadnych zmian');
		define('IMG_WARING_WIN_CLOSE', 'Zamknąć okno?');
		define('IMG_WARNING_UNDO', 'Przywrócić poprzedni stan obrzka?');
		define('IMG_WARING_FLIP_H', 'Odwrócić obraz w poziomie?');
		define('IMG_WARING_FLIP_V', 'Odwrócić obraz w pionie?');
		define('IMG_INFO', 'Informacje obrazu');
		
		//Mode
			define('IMG_MODE_RESIZE', 'Zmień rozmiar:');
			define('IMG_MODE_CROP', 'Przytnij:');
			define('IMG_MODE_ROTATE', 'Obróć:');
			define('IMG_MODE_FLIP', 'Odwróć:');		
		//Button
		
			define('IMG_BTN_ROTATE_LEFT', '90&deg; w lewo');
			define('IMG_BTN_ROTATE_RIGHT', '90&deg; w prawo');
			define('IMG_BTN_FLIP_H', 'Odwróć w poziomie');
			define('IMG_BTN_FLIP_V', 'Odwróć w pionie');
			define('IMG_BTN_RESET', 'Resetuj');
			define('IMG_BTN_UNDO', 'Cofnij');
			define('IMG_BTN_SAVE', 'Zapisz');
			define('IMG_BTN_CLOSE', 'Zamknij');
			define('IMG_BTN_SAVE_AS', 'Zapisz jako');
			define('IMG_BTN_CANCEL', 'Anuluj');
		//Checkbox
			define('IMG_CHECKBOX_CONSTRAINT', 'Wymuś?');
		//Label
			define('IMG_LBL_WIDTH', 'Szerokość:');
			define('IMG_LBL_HEIGHT', 'Wysokość:');
			define('IMG_LBL_X', 'X:');
			define('IMG_LBL_Y', 'Y:');
			define('IMG_LBL_RATIO', 'Proporcja:');
			define('IMG_LBL_ANGLE', 'Kąt:');
			define('IMG_LBL_NEW_NAME', 'Nowa nazwa:');
			define('IMG_LBL_SAVE_AS', 'Zapisz jako');
			define('IMG_LBL_SAVE_TO', 'Zapisz w:');
			define('IMG_LBL_ROOT_FOLDER', 'Folder główny');
		//Editor
		//Save as 
		define('IMG_NEW_NAME_COMMENTS', 'Nazwa orazka bez rozszerzenia.');
		define('IMG_SAVE_AS_ERR_NAME_INVALID', 'Podaj nazwę zawierającą wyłącznie litery, cyfry, spacje, łączniki i podkreślenia.');
		define('IMG_SAVE_AS_NOT_FOLDER_SELECTED', 'Nie podano folderu docelowego.');	
		define('IMG_SAVE_AS_FOLDER_NOT_FOUND', 'Folder docelowy nie istnieje.');
		define('IMG_SAVE_AS_NEW_IMAGE_EXISTS', 'Istnieje już plik o takiej nazwie.');

		//Save
		define('IMG_SAVE_EMPTY_PATH', 'Brak ścieżki.');
		define('IMG_SAVE_NOT_EXISTS', 'Obraznie istnieje.');
		define('IMG_SAVE_PATH_DISALLOWED', 'Brak dostępu do pliku.');
		define('IMG_SAVE_UNKNOWN_MODE', 'Nieznana operacja na pliku');
		define('IMG_SAVE_RESIZE_FAILED', 'Wystąpił błąd podczas zmiany rozmiaru.');
		define('IMG_SAVE_CROP_FAILED', 'Wystąpił błąd podczas przycinania obrazu.');
		define('IMG_SAVE_FAILED', 'Wystąpił błąd podczas zapisywania.');
		define('IMG_SAVE_BACKUP_FAILED', 'Brak dostępu do wcześniejszej wersji obrazu.');
		define('IMG_SAVE_ROTATE_FAILED', 'Nie można obrócić obrazu.');
		define('IMG_SAVE_FLIP_FAILED', 'Nie można odwrócić obrazu.');
		define('IMG_SAVE_SESSION_IMG_OPEN_FAILED', 'Brak dostępu do obrazka w sesji.');
		define('IMG_SAVE_IMG_OPEN_FAILED', 'Nie można otworzyć obrazu');
		
		
		//UNDO
		define('IMG_UNDO_NO_HISTORY_AVAIALBE', 'Nie można wykonać operacji.');
		define('IMG_UNDO_COPY_FAILED', 'Brak możliwości przywrócenia obrazu.');
		define('IMG_UNDO_DEL_FAILED', 'Brak możliwości usunięcia obrazu');
	
	//88888888888   Above for Image Editor   888888888888888888888
	
	//88888888888   Session   888888888888888888888
		define('SESSION_PERSONAL_DIR_NOT_FOUND', 'Unable to find the dedicated folder which should have been created under session folder');
		define('SESSION_COUNTER_FILE_CREATE_FAILED', 'Unable to open the session counter file.');
		define('SESSION_COUNTER_FILE_WRITE_FAILED', 'Unable to write the session counter file.');
	//88888888888   Session   888888888888888888888
	
	//88888888888   Below for Text Editor   888888888888888888888
		define('TXT_FILE_NOT_FOUND', 'Nie znaleziono pliku.');
		define('TXT_EXT_NOT_SELECTED', 'Wybierz rozszerzenie');
		define('TXT_DEST_FOLDER_NOT_SELECTED', 'Wybierz folder docelowy');
		define('TXT_UNKNOWN_REQUEST', 'Nieznana operacja.');
		define('TXT_DISALLOWED_EXT', 'Brak uprawnień doedycji/dodawania plików tego typu.');
		define('TXT_FILE_EXIST', 'Taki plik już istnieje.');
		define('TXT_FILE_NOT_EXIST', 'Nie znaleziono.');
		define('TXT_CREATE_FAILED', 'Błąd podczas tworzenia pliku.');
		define('TXT_CONTENT_WRITE_FAILED', 'Błąd podczas zapisu treści do pliku.');
		define('TXT_FILE_OPEN_FAILED', 'Błąd podczas otwierania pliku.');
		define('TXT_CONTENT_UPDATE_FAILED', 'Błąd podczas aktualizacji pliku.');
		define('TXT_SAVE_AS_ERR_NAME_INVALID', 'Podaj nazwę zawierającą wyłącznie litery, cyfry, spacje, łączniki i podkreślenia.');
	//88888888888   Above for Text Editor   888888888888888888888
	
	
?>