<?php
 # file encoding: UTF-8
 # © 2010 airmedia.pl

 $LANG['%Select%'] = 'Wybierz';
 $LANG['%Cancel%'] = 'Anuluj';
 $LANG['%Refresh%'] = 'Odśwież';
 $LANG['%Upload%'] = 'Wczytaj plik';
 $LANG['%About%'] = 'Autor';
 $LANG['%Delete%'] = 'Usuń';
 $LANG['%MakeDir%'] = 'Dodaj folder';
 $LANG['%ViewAsIcons%'] = 'Pokaż jako miniatury';
 $LANG['%ViewAsList%'] = 'Pokaż jako listę';
 $LANG['%CurrentFile%'] = 'Wybrany plik';
 $LANG['%none%'] = 'brak';
 $LANG['%DirSize%'] = 'Rozmiar plików w katalogu';
 $LANG['%ViewAllFiles%'] = 'Pokaż wszystkie pliki';
 $LANG['%Download%'] = 'Pobierz';
 $LANG['%MyFiles%'] = 'Moje pliki';
 $LANG['%Close%'] = 'Zamknij';
 $LANG['%ClosePanel%'] = 'Zamknij panel';
 $LANG['%SelectAll%'] = 'Zaznacz wszystkie';
 $LANG['%DeSeleAll%'] = 'Odznacz wszystkie';
 $LANG['%RevSelect%'] = 'Odwróć zaznaczenie';

 # Delete
 $LANG['%NoSelectedFiles%'] = 'Proszę zaznaczyć pliki/foldery do usunięcia.';
 $LANG['%ConfirmDelete%'] = 'Na pewno skasować zaznaczone pliki/foldery?';
 $LANG['%Err'.AFMRC_DELETEOK.'%'] = '<span class="msgok">Wybrane pliki / foldery zostały usunięte.</span>';
 $LANG['%Err'.AFMRC_DELETEERR.'%'] = 'Błąd: nie wszystkie zaznaczone pliki zostały usunięte!';

 # MkDir
 $LANG['%NewDirInfo%'] = 'Utwórz nowy folder w ';
 $LANG['%NewDirName%'] = 'Nazwa nowego folderu';
 $LANG['%NewDirChmod%'] = 'Nadaj pełne prawa publiczne - <br>odczyt / zapis / wykonanie dla wszystkich';
 $LANG['%Create%'] = 'Utwórz';
 $LANG['%Err'.AFMRC_MKDIROK.'%'] = '<span class="msgok">Nowy folder został utworzony.</span>';
 $LANG['%Err'.AFMRC_MKDIRERR.'%'] = 'Błąd: nie udało się utworzyć nowego folderu!';

 # Upload
 $LANG['%Upload%'] = 'Wczytaj';
 $LANG['%FilesToUpload%'] = 'Wybierz plik(i) do wczytania';
 $LANG['%CreateThumb%'] = 'Twórz miniaturę';
 $LANG['%Width%'] = 'Szerokość';
 $LANG['%Height%'] = 'Wysokość';
 $LANG['%CutThb%'] = 'Przytnij';
 $LANG['%Err'.AFMRC_UPLOADOK.'%'] = '<span class="msgok">Pliki zostały poprawnie wczytane.</span>';
 $LANG['%Err'.AFMRC_UPERR_TYPE.'%'] = 'Jeden z plików był niedozwolonego typu i został zablokowany.';
 $LANG['%Err'.AFMRC_UPERR_THMB.'%'] = 'Nie udało się utworzyć miniatury do jednego lub więcej plików.';

 # About
 $LANG['%Version%'] = 'Wersja';
 $LANG['%CopyrightNotice%'] = 'Skrypt jest dostępny dla każdego na licencji Creative Commons.<br>Proszę nie usuwać informacji o autorze ani o licencji.';
?>