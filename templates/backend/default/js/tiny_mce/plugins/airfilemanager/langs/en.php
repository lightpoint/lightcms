<?php
 # file encoding: UTF-8
 # © 2010 airmedia.pl

 $LANG['%Select%'] = 'Select';
 $LANG['%Cancel%'] = 'Cancel';
 $LANG['%Refresh%'] = 'Refresh';
 $LANG['%Upload%'] = 'Upload file';
 $LANG['%About%'] = 'About';
 $LANG['%Delete%'] = 'Delete';
 $LANG['%MakeDir%'] = 'Create directory';
 $LANG['%ViewAsIcons%'] = 'Show as thumbnails';
 $LANG['%ViewAsList%'] = 'Show as list';
 $LANG['%CurrentFile%'] = 'Current file';
 $LANG['%none%'] = 'none';
 $LANG['%DirSize%'] = 'Total size of files in current folder';
 $LANG['%ViewAllFiles%'] = 'Show all files';
 $LANG['%Download%'] = 'Download';
 $LANG['%MyFiles%'] = 'Root dir';
 $LANG['%Close%'] = 'Close';
 $LANG['%ClosePanel%'] = 'Close panel';
 $LANG['%SelectAll%'] = 'Select all';
 $LANG['%DeSeleAll%'] = 'Deselect all';
 $LANG['%RevSelect%'] = 'Invert selection';

 # Delete
 $LANG['%NoSelectedFiles%'] = 'Please select the files/folders to be deleted.';
 $LANG['%ConfirmDelete%'] = 'Are you sure to delete the selected files/folders?';
 $LANG['%Err'.AFMRC_DELETEOK.'%'] = '<span class="msgok">The selected files/folders have been removed.</span>';
 $LANG['%Err'.AFMRC_DELETEERR.'%'] = 'Error: not all selected files have been removed!';

 # MkDir
 $LANG['%NewDirInfo%'] = 'Create in';
 $LANG['%NewDirName%'] = 'Enter new directory name';
 $LANG['%NewDirChmod%'] = 'Change the  permissions on a new directory - Read/Write/Execute (777)';
 $LANG['%Create%'] = 'Create';
 $LANG['%Err'.AFMRC_MKDIROK.'%'] = '<span class="msgok">New directory was created successfully.</span>';
 $LANG['%Err'.AFMRC_MKDIRERR.'%'] = 'Error: failed to create a new folder!';

 # Upload
 $LANG['%Upload%'] = 'Upload';
 $LANG['%FilesToUpload%'] = 'Select file(s) to upload';
 $LANG['%CreateThumb%'] = 'Create thumbnail(s)';
 $LANG['%Width%'] = 'Width';
 $LANG['%Height%'] = 'Height';
 $LANG['%CutThb%'] = 'Crop thumbnail';
 $LANG['%Err'.AFMRC_UPLOADOK.'%'] = '<span class="msgok">Files have been loaded successfully.</span>';
 $LANG['%Err'.AFMRC_UPERR_TYPE.'%'] = 'One type of files was illegal type and has been blocked.';
 $LANG['%Err'.AFMRC_UPERR_THMB.'%'] = 'Failed to create thumbnails in one or more files.';

 # About
 $LANG['%Version%'] = 'Version';
 $LANG['%CopyrightNotice%'] = 'This script is available to everyone licensed under a Creative Commons license.<br>Please do not remove information about the author and/or license.';
?>