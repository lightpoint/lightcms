<?php
 # file encoding: UTF-8
 # © 2010 airmedia.pl
 
 $LANG['%Select%'] = 'Выбор';
 $LANG['%Cancel%'] = 'Отмена';
 $LANG['%Refresh%'] = 'Обновить';
 $LANG['%Upload%'] = 'Загрузить файл';
 $LANG['%About%'] = 'О плагине';
 $LANG['%Delete%'] = 'Удалить';
 $LANG['%MakeDir%'] = 'Создать директорию';
 $LANG['%ViewAsIcons%'] = 'Отобразить плиткой';
 $LANG['%ViewAsList%'] = 'Отобразить спиком';
 $LANG['%CurrentFile%'] = 'Текущий файл';
 $LANG['%none%'] = 'не выбран';
 $LANG['%DirSize%'] = 'Общий размер файлов в текущей директории';
 $LANG['%ViewAllFiles%'] = 'Показать все файлы';
 $LANG['%Download%'] = 'Скачать';
 $LANG['%MyFiles%'] = 'Корневая директория';
 $LANG['%Close%'] = 'Закрыть';
 $LANG['%ClosePanel%'] = 'Закрыть панель';
 $LANG['%SelectAll%'] = 'Выбрать все';
 $LANG['%DeSeleAll%'] = 'Очистить выбор';
 $LANG['%RevSelect%'] = 'Инвертировать выбор';

 # Delete
 $LANG['%NoSelectedFiles%'] = 'Пожалуйста выберите файлы/папки для удаления.';
 $LANG['%ConfirmDelete%'] = 'Вы уверенны в удалении выбраных файлов/папок?';
 $LANG['%Err'.AFMRC_DELETEOK.'%'] = '<span class="msgok">Выбраные файлы/папки были удалены.</span>';
 $LANG['%Err'.AFMRC_DELETEERR.'%'] = 'Ошибка: не все выбраные файлы были удалены!';

 # MkDir
 $LANG['%NewDirInfo%'] = 'Создать в';
 $LANG['%NewDirName%'] = 'Введите название директории';
 $LANG['%NewDirChmod%'] = 'Изменить права доступа - Чтение/Запись/Исполнение (777)';
 $LANG['%Create%'] = 'Создать';
 $LANG['%Err'.AFMRC_MKDIROK.'%'] = '<span class="msgok">Новая папка создана успешно.</span>';
 $LANG['%Err'.AFMRC_MKDIRERR.'%'] = 'Ошибка: не удалось создать папку!';

 # Upload
 $LANG['%Upload%'] = 'Загрузить';
 $LANG['%FilesToUpload%'] = 'Выбор файлов для загрузки';
 $LANG['%CreateThumb%'] = 'Создать превью';
 $LANG['%Width%'] = 'Ширина';
 $LANG['%Height%'] = 'Высота';
 $LANG['%CutThb%'] = 'Обрезать привью';
 $LANG['%Err'.AFMRC_UPLOADOK.'%'] = '<span class="msgok">Файлы загружены успешно.</span>';
 $LANG['%Err'.AFMRC_UPERR_TYPE.'%'] = 'Запрещенный тип файла.';
 $LANG['%Err'.AFMRC_UPERR_THMB.'%'] = 'Не удалось создать превью для некоторых файлов.';

 # About
 $LANG['%Version%'] = 'Версия';
 $LANG['%CopyrightNotice%'] = 'Этот скрипт доступен каждому и распространяется по лицензии Creative Commons.<br>Пожалуйста не удаляйте информацию об авторе и/или лицензии.';
?>