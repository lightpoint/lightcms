$(document).ready(function() {	
	$('.light').click(function(event) {
		event.preventDefault();
		//var boxWidth = 0;
		var href = $(this).attr('href');
		var classInfo = $(this).attr('class');
		
		var wrapContent;
		
		if($(this).attr('title')=='' || $(this).attr('title')==undefined)
			var title = '';
		else var title = $(this).attr('title');
		
		//wraps
		var wrapContainer = '<div id="infoMaster"></div>';
		var wrapHeader = '<div id="infoPanel" class="'+classInfo+'"><div id="infoContainer"><div id="infoHeader"><div id="infoTitle">'+title+'</div><div id="infoControls"><button id="infoClose" class="infoClose"></button></div></div><div id="infoContent">';
		var wrapFooter = '</div></div></div>';
		var wrapFalse = 'brak danych';
		
		$('body').append(wrapContainer);
		
		showLoad();
		
		$.ajax({
  		url: href,
  		cache: false
		}).done(function(html) {
  		wrapContent = html;
			closeLoad();
  		$('body').append(wrapHeader+wrapContent+wrapFooter);
		}).fail(function(html) {
  		$('body').append(wrapHeader+wrapFalse+wrapFooter);
		});
	});
	
	$('.infoClose').live("click",function() {
		$('#infoPanel').remove();
		$('#infoMaster').fadeOut(200, function() {
	    $('#infoMaster').remove();
  	});
		return false;
	});
	
	function showWindow() {
		
	}
	
	function showLoad() {
		var wrapLoad = "<div id='lightLoad'><img src='/templates/img/load.gif'></div>";
		$('body').append(wrapLoad);
	}
	
	function closeLoad() {
		$('#lightLoad').remove();
	}
});