<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>LightCMS - panel administracyjny</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="/templates/backend/default/css/admin.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="/templates/backend/default/css/homepage.css" type="text/css" media="screen" />
		<link href="/templates/backend/default/css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script type="text/javascript" src="/templates/backend/default/js/jquery.tablesorter.min.js"></script>
		<script type="text/javascript" src="/templates/backend/default/js/jquery.tablesorter.paging.js"></script>
		<script type="text/javascript" src="/templates/backend/default/js/jquery.fancybox.pack.js"></script>
		<script type="text/javascript" src="/templates/backend/default/js/script.js"></script>
	</head>
	<body>
		<div id="header">
			<div id="siteInfo">LIGHTCMS<br/><span>PANEL&nbsp;ADMINISTRACYJNY</span></div>
			<div id="userInfo"><br/><br/><a href="logout.php">wyloguj</a></div>
		</div>
		<div id="contentContainer">
			<div id="sideMenu">{/admin_menu/}</div>
			<div id="content">{/content/}</div>	
		</div>
		<div id="footer">
			<div id="foot1">LightCMS</div>
			<div id="foot2"><a href="http://www.lightpoint.pl" target="_blank">Lightpoint 2013</a></div>
		</div>
		<script>			
		  $(document).ready(function() {
 		  	$.urlParam = function(name){
					var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
					if (results==null){
						return null;
					}else{
						return results[1] || 0;
					}	
				}
		  	
		  	var action = $.urlParam('action');
		  	$('#'+action).addClass('current');
		  });
		</script>
	</body>
</html>