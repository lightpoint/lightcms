<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>LightCMS - panel administracyjny</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="stylesheet" href="/templates/backend/default/css/admin.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="/templates/backend/default/css/metro.css" type="text/css" media="screen" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<script src="/templates/backend/default/metro.js"></script>
	</head>
	<body>
		<div id="header">
			<div id="siteInfo">LIGHTCMS<br/><span>PANEL&nbsp;ADMINISTRACYJNY</span></div>
			<div id="userInfo"><br/><br/><a href="logout.php">wyloguj</a></div>
		</div>
		<div id="contentContainer">
			<div id="content" class="fullPage">
				<div id="container">
				<div id="info"></div>
					<div id="metroContainer">{/content/}</div>
				</div>
		</div>	
		<div id="footer">
			<div id="foot1">LightCMS</div>
			<div id="foot2"><a href="http://www.lightpoint.pl" target="_blank">Lightpoint 2013</a></div>
		</div>
	</body>
</html>