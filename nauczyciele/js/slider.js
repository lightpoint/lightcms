var delayTime = 7000;
var play = true;
var slides_count = $('.quote').size();	
var slide_current = 1;
var next = true;

$(document).ready(function(){	
	$('.quote').hide();	
	$('.quote').first().show();
	
	function slide_text(id) {
		//console.log(slides_count+' - '+id);
		$('.quote').hide();	
		$('.quote').eq(id-1).show();
	}
	
	setInterval(function() {
		slide_current++;
		if(slide_current>slides_count)
			slide_current = 1;
	  if (next == true) slide_text(slide_current);
	      else next = true;
	  }, delayTime);
});