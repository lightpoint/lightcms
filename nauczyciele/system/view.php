<?php
class View {
	public $var;
	public $db;
	public $fb;
	public $agent;
	public $layout_path;

	public function __construct() {
		global $db, $agent;

		//base links
		$this->db = $db;

		//facebook api
		//$this->fb = $fb;
		
		/*$this->fbParams = array(
	    'scope' => 'email',
	    'redirect_uri' => SC_ROOT_URL,
		);*/

		//mobile detect
		$this->agent = $agent;

		//get all data from site
		$controller = new Controller;
		$this->var = $controller->getData();
    
    if($this->var->option=='lang' or $this->var->cat=='lang')
	    $_SESSION['s_lang'] = $this->var->s_lang = $this->var->id;  
	    
	  if($this->var->s_lang==null or $this->var->s_lang=='' or $this->var->s_lang=='pl')			
			$this->var->lang_sufix = null;
		else $this->var->lang_sufix = '_'.$this->var->s_lang;
	}

	//
	public function setLayout($layout_path) {
		$this->action = $this->var->action;

		//mobile config switch
		if($this->agent=='mobile') {
			if(!isset($this->var->action) or empty($this->var->action))
				if($_SESSION['type']=='admin_')
					$_GET['action'] = $this->var->action = 'index';
				else
					$_GET['action'] = $this->var->action = 'homepage';
			
      //if(!file_exists('modules/'.$this->var->action.'/'.$this->var->action.'.php')) {	
			/*if(!file_exists('modules/'.$this->var->action.'/'.$this->var->type.$this->var->action.'.php') and $this->var->action!='index') {
				$_GET['id'] = $this->var->action;
				$_GET['action'] = $this->var->action = 'simple';
			}*/

			//include $layout_path."config.mobile.php";
			include $layout_path."config.php";
			if(array_key_exists($this->var->action,$config_template))
				return $layout_path.$config_template[$this->var->action];
			else return $layout_path.'mobile.tpl.php';
		} else {
			if(!isset($this->var->action) or empty($this->var->action))
				if($_SESSION['type']=='admin_')
					$_GET['action'] = $this->var->action = 'index';
				else
					$_GET['action'] = $this->var->action = 'homepage';
				
			//if($this->var->action>0) {
      /*if(!file_exists('modules/'.$this->var->action.'/'.$this->var->type.$this->var->action.'.php') and $this->var->action!='index') {
				$_GET['id'] = $this->var->action;
				$_GET['action'] = $this->var->action = 'simple';
			}*/
				
			include "system/template.php";
			if(array_key_exists($this->var->option,$config_template))
				return $layout_path.$config_template[$this->var->option];
			else return $layout_path.'nauczyciele.tpl.php';
		}
	}

	//search module from template
	public function getModule($option) {
		global $headLinks;
		if(method_exists($this, $option))
			return $this->$option();
		//else return $this->moduleError($this, $option);
		else return $this->pageError($option);
	}

	public function pageError($text=null) {
	//	header("HTTP/1.1 404 Not Found");
		return "<div class='warningInfo'>brak strony do wyświetlenia - ".$text."</div>";
	}

	public function moduleError($module, $option) {
		return "<div class='errorInfo'>Can't load module - ".get_class($module)." / ".$option."</div>";
	}

	public function noProduct() {
		header("HTTP/1.1 404 Not Found");
		return "<div class='warningInfo'>brak produktów</div>";
	}
	
	public function noCategory() {
		header("HTTP/1.1 404 Not Found");
		return "<div class='warningInfo'>brak kategorii</div>";
	}

	public function userNotLogged() {
		return "<div class='warningInfo'>zaloguj się, by uzyskać dostęp do strony</div>";
	}

	public function noData() {
		return "<p align='center'> - brak danych - </p>";
	}

	public function goAway() {
		return "<div class='warningInfo'>brak uprawnień</div>";
	}

	public function error() {
		return "<div class='warningInfo'>wystąpił błąd</div>";
	}

	//check admin privileges
	public function checkAdminRole($id, $action, $option) {
		if($this->var->admin_role=='admin')
			return true;
		else {
			//$this->db->queryString('select CAR_ADM_ID from centra_admin_roles where CAR_ADM_ID='.$id.' and CAR_ROLE_ID="'.$action.'" and CAR_ROLE_OPTION="'.$option.'" limit 1');
			$this->db->queryString('select ADR_ADM_ID from '.__BP.'admin_roles where ADR_ADM_ID='.$id.' and ADR_ROLE_ID="'.$action.'"');
			//$this->db->query();
			if($this->db->getRow())
				return true;
			else return false;
		}
	}

	public function checkRequired($list) {
		$result = true;
		foreach($list as $element) {
			if($element==null)
				$result = false;
		}

		return $result;
	}
	
	public function limitAgency($field) {
		if($this->var->admin_agency=='A') 
			return null;
		else return " and ".$field." like '".$this->var->admin_agency."%' ";
	}
}

class Skin extends View {
	public function getTemplatePath() {
		if($_GET['siteTemplateLayoutPath_hs7fn2a']) 
			$_SESSION['siteTemplateLayoutPath_hs7fn2a'] = $_GET['siteTemplateLayoutPath_hs7fn2a'];

		if(isset($_SESSION['siteTemplateLayoutPath_hs7fn2a'])) {
			$_SESSION['selectedTemplate']	= $_SESSION['siteTemplateLayoutPath_hs7fn2a'];
			return V_PATH_TEMPLATE.$_SESSION['siteTemplateLayoutPath_hs7fn2a'].'/';
			
		} else {
			$defaultTemplate = $this->getDefaultTemplate();
			$_SESSION['selectedTemplate']	= $defaultTemplate;
			return V_PATH_TEMPLATE.$defaultTemplate;
		}
	}

	private function getDefaultTemplate() {
		if(U_TYPE=='file') {
			return V_TEMPLATE;	
		} else {
			$skin = $this->db->dict('SKIN_NAME');
			return $skin['DIC_VALUE'].'/';
		}
	}
}

//user_id - user or admin ID
//event - event string
//from - admin/user
class eventSaver extends View {
	public function add($user_id, $event, $from='admin')	{
		$insert = array(
			'LOE_USER' => $user_id,
			'LOE_EVENT' => $event,
			'LOE_FROM' => $from,
			'LOE_IP' => clientIP::get(),
			);
		$this->db->queryString(__BP.'log_events');
		$this->db->insertQuery($insert);
	}
}