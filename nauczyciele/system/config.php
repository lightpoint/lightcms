<?php
class config_options_teachers {
	public $mainentance = false;
	public $errorSite = false;
	public $testingSite = false;
	public $mobileAgentDetect = false;
}

//views
define('V_TEMPLATE','makewonder_1.0/');
define('V_PATH_TEMPLATE','../templates/frontend/');
define('V_COMPILE','/templates/_c'); //compiled views for OPT


//admin paths
define('V_A_TEMPLATE','/devoops/');
define('V_A_TEMPLATE_PATH','../templates/backend'.V_A_TEMPLATE);