<?php
session_start();
error_reporting(0);
//error_reporting(E_ALL);

require_once "../system/config.php";
require_once "system/config.php";
//require_once "//system/lang.php";
require_once "../system/connector.php";
require_once "system/system.php";
require_once "system/view.php";
require_once "../system/components.php";
require_once "../system/controller.php";
//require_once "//system/mobile_detect.php";
//require_once "//system/mailer.php";

$controller = new Controller;
$var = $controller->GetData();

$db = new Connector;
$db->BaseConnection();

if($_SESSION['s_lang']=='' or $_SESSION['s_lang']==null)
	$_SESSION['s_lang'] = 'pl';

$_SESSION['type'] = '';

/*if($_POST['action'] == null)
	$_POST['action'] = 'nauczyciele';*/

$data_module = new Module();
die($data_module->load($var->action,$var->option));