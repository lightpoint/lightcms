<?php
class head extends Layout {
	public function __construct() {
		global $headLinks;
	}
	
	public function addLink($path, $type) {
		global $headLinks;
		$headLinks.=$this->$type($path);
		
	}
	
	protected function css($path) {
		return "<link rel='stylesheet' href='".$path."'>\n";
	}
	
	protected function js($path) {
		return "<script src='".$path."'></script>\n";
	}
	
	public function getLinks() {
		global $headLinks;
		//var_dump($this->type);
		return $headLinks;
	}
}

function head() {
	$header = new head;
	return $header->getLinks();
}
?>