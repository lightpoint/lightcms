<?php
class Admin_modules extends View{
	public $path = 'modules/admin_menu/templates/';
	
	public function prepareMenu() {
		$role = $this->var->admin_role;
		return $this->$role();
	}
	
	//menu structure for ph
	private function user() {
		if($this->var->admin_id) {
			$this->db->queryString("select * from ".__BP."admin_roles where ADR_ADM_ID=".$this->var->admin_id);
			$admin_roles = $this->db->getDataFetch();
			
			if($admin_roles) {
				foreach($admin_roles as $ar) {
					$list = array(
							'__LINK' => 'admin.php?action='.$ar['ADR_ROLE_ID'],
							'__CLASS_LINK' => 'active',
							'__CLASS_ICON' => 'fa fa-arrow-circle-right',
							'__TEXT' => $ar['ADR_NAME'],
						);
					
					 $result.= Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 unset($list);
				}
				return $result;
			} else return null;
		}
	}
	
	//menu structure for admin
	private function admin() {
		//add admin options 
		$list = array(
			'__LINK' => 'admin.php?action=options&option=system',
			'__CLASS_LINK' => 'active',
			'__CLASS_ICON' => 'fa fa-cog',
			'__TEXT' => 'Konfiguracja',
		);
	 
	 $result[] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
	 unset($list);
		
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
	
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if(isset($menu)) {
		    	$list = array(
							'__LINK' => 'admin.php?action='.$menu['action'][0],
							'__CLASS_LINK' => 'active',
							'__CLASS_ICON' => $menu['icon'][0],
							'__TEXT' => $menu['title'][0],
						);
					 
					 //$result[$menu['order'][0]] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 $result[] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 unset($list);
		    }
		  }
		}
		
		ksort($result);
		foreach($result as $key=>$value)
			$line.=$value;
		return $line;
	}
	
	public function getForRole() {
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
		
		$result = "<div class='list-group'>\n";
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if(isset($menu)) {
		    	$result.="<a href='?action=".$menu['action'][0]."' id='".$menu['action'][0]."' class='list-group-item'>".$menu['title'][0]."</a>\n";
		    }
		  }
		}
		$result.= "</div>";
		return $result;
	}
}

function admin_menu() {
	$Admin_module = new Admin_modules;
	return $Admin_module->prepareMenu();
}
?>