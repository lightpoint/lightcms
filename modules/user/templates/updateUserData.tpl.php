<div id="userContainer">
	<h2>Aktualizacja danych</h2>
	<form name="userUpdate" id="userUpdate" method="post" action="index.php">
		<div id="userData">
			
			<p><label>Nazwa firmy*:&nbsp;</label><input type="text" name="firma" required="" class="validated" value="__FIRMA"></p>
			<p><label>NIP*:&nbsp;</label><input type="text" name="nip" required="" class="validated" value="__NIP"></p>
			<p><label>Imię*:&nbsp;</label><input type="text" name="usr_name" required="" class="validated" value="__IMIE"></p>
			<p><label>Nazwisko*:&nbsp;</label><input type="text" name="usr_surname" required="" class="validated" value="__NAZWISKO"></p>
			<p><label>Ulica*:&nbsp;</label><input type="text" name="ul" required="" class="validated" value="__UL"></p>
			<p><label>Nr domu*:&nbsp;</label><input type="text" name="ndom" required="" class="validated inputShort" value="__NDOM"></p>
			<p><label>Nr lokalu:&nbsp;</label><input type="text" name="nlok" class="inputShort" value="__NLOK"></p>
			<p><label>Kod pocztowy*:&nbsp;</label><input type="text" name="kodp" required="" class="validated inputShort" maxlength="6" value="__KODP"></p>
			<p><label>Miejscowość*:&nbsp;</label><input type="text" name="miasto" required="" class="validated" value="__MIASTO"></p>
			<hr>
			<p><label id="tel_group">przynajmniej jeden telefon*</label></p>
			<p><label>Telefon stacjonarny nr 1:&nbsp;</label><input type="text" name="tel_1" class="t_validated" value="__TEL1"></p>
			<p><label>Telefon stacjonarny nr 2:&nbsp;</label><input type="text" name="tel_2" class="t_validated" value="__TEL2"></p>
			<p><label>Telefon komórkowy:&nbsp;</label><input type="text" name="tel_kom" class="t_validated" value="__TEL_KOM"></p>
			<p><label>Fax:&nbsp;</label><input type="text" name="tel_fax" class="t_validated" value="__TEL_FAX"></p>
			<hr>
			
			<p><label>Email*:&nbsp;</label><input type="text" name="usr_email1" id="usr_email1" required="" class="validate" value="__EMAIL"></p>
			<p><label>Firmowa strona www:&nbsp;</label><input type="text" name="web" id="web" value="__WEB"></p>
		</div>
		<div id="valid_info"></div>
		<div id="buttonContainer"><input type="submit" value="Zapisz"></div>
		<input type='hidden' name='action' value='user'>
		<input type='hidden' name='option' value='updateSave'>
	</form>
	<hr>
	<h2>Zmiana hasła</h2>
	<form name="passwordChange" id="passwordChange" method="post" action="index.php">
		<p><label>Hasło (min. 6 znaków)*:&nbsp;</label><input type="password" name="usr_password1" id="usr_password1" required="" class="p_validated"></p>
		<p><label>Powtórz hasło*:&nbsp;</label><input type="password" name="usr_password2" id="usr_password2" required="" class="p_validated"></p>
		<div id="valid_info_p"></div>
		
		<div class="buttonContainer"><input type="submit" value="Zapisz"></div>
		<input type='hidden' name='action' value='user'>
		<input type='hidden' name='option' value='passwordChange'>
	</form>
</div>

<script>
$(document).ready(function(){
	$('#userRegister').submit(function() {
		var valid = true;
		
		$('.validate').each(function(index){
			$(this).removeClass('invalid');
			if($(this).val()=='') {
				$(this).addClass('invalid');
				valid = false	;
				$('#valid_info').html('Uzupełnij brakujące dane w zaznaczonych polach');
			}
		});
		
		$('.t_validate').each(function(index){
			if(count($(this))<3) {
				$('#tel_group').addClass('invalid');
				valid = false	;
				$('#valid_info').html('Uzupełnij brakujące dane w zaznaczonych polach');
			}
		});
		return valid;
	});
	
	$('#passwordChange').submit(function() {
		$('#valid_info_p').html('');
		var valid = true;
		
		if($('#usr_password1').val().length<6) {
			valid = false;
			$('#usr_password1').addClass('invalid');
			$('#valid_info_p').append('Hasło jest zbyt krótkie - przynajmniej 6 znaków.<br/>');
		}
		
		if($('#usr_password1').val()!=$('#usr_password2').val()) {
			$('#usr_password1').addClass('invalid');
			$('#usr_password2').addClass('invalid');
			valid = false;
			$('#valid_info_p').html('Hasła nie pasują do siebie.');
		}
		return valid;
	});
});
</script>