<form name="userData" id="userData" method="post" action="admin.php">
	<div id="userData">
		<h3>Szczegóły konta</h3>
		<div class="dataContainer">
			<p><label class="label-long">firma:&nbsp;</label><input type="text" name="USR_FIRM" value="__USR_FIRM"></p>
			<p><label class="label-long">imię:&nbsp;</label><input type="text" name="USR_NAME" value="__USR_NAME"></p>
			<p><label class="label-long">nazwisko:&nbsp;</label><input type="text" name="USR_SURNAME" value="__USR_SURNAME"></p>
		</div>
		<div class="dataContainer">
			<p><label class="label-long">ulica:&nbsp;</label><input type="text" name="USR_STREET" value="__USR_STREET"></p>
			<p><label class="label-long">numer domu:&nbsp;</label><input type="text" name="USR_NR_D" value="__USR_NR_D"></p>
			<p><label class="label-long">nr mieszkania:&nbsp;</label><input type="text" name="USR_NR_M" value="__USR_NR_M"></p>
			<p><label class="label-long">kod pocztowy:&nbsp;</label><input type="text" name="USR_POSTCODE" value="__USR_POSTCODE"></p>
			<p><label class="label-long">miejscowość:&nbsp;</label><input type="text" name="USR_CITY" value="__USR_CITY"></p>
		</div>
		
		<div class="dataContainer">
			<p><label class="label-long">potwierdzenie:&nbsp;</label><input type="text" name="USR_CONFIRMED" value="__USR_CONFIRMED"></p>
			<p><label class="label-long">status:&nbsp;</label><select name="USR_ACTIVE" id="USR_ACTIVE"><option value="nowy">nowy</option><option value="aktywny">aktywny</option><option value="zbanowany">zbanowany</option><option value="usunięty">usunięty</option></select></p>
			<p><label class="label-long">zmiana hasła:&nbsp;</label> <input type="checkbox" name="resetShow" id="resetShow"><div class="inputHidden" id="inputHidden"><p><input type="text" name="newUserPassword" id="newUserPassword">&nbsp;<input type="button" name="changeUserPassword" id="changeUserPassword" value="zmień"></p><p><input type="button" name="resetUserPassword" id="resetUserPassword" value="wyslij link resetujący hasło"></p></div></p>
	</div>
	<div id="button_container">
		<button type="button" class="btn btn-primary" id="userSave">Zapisz</button>
		<button type="button" class="btn btn-secondary lightClose">Zamknij</button>
	</div>
	<input type="hidden" name="action" value="user">
	<input type="hidden" name="option" value="saveUserData">
	<input type="hidden" name="USR_ID" id="USR_ID" value="__USR_ID">
</form>

<script>
$('#USR_ACTIVE').val('__USR_ACTIVE');
</script>