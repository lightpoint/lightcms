<div id="userContainer">
	<h2>Zarejestruj się</h2>
	<form name="userRegister" id="userRegister" method="post" action="index.php">
		<div id="userData" class="labelContainer">
			<p><label>Nazwa firmy*:&nbsp;</label><input type="text" name="firma" required="" class="validated min_3"></p>
			<p><label>NIP*:&nbsp;</label><input type="text" name="nip" required="" class="validated min_13" maxlength="13"></p>
			<p><label>Imię*:&nbsp;</label><input type="text" name="usr_name" required="" class="validated min_3"></p>
			<p><label>Nazwisko*:&nbsp;</label><input type="text" name="usr_surname" required="" class="validated min_3"></p>
			<p><label>Ulica*:&nbsp;</label><input type="text" name="ul" required="" class="validated min_3"></p>
			<p><label>Nr domu*:&nbsp;</label><input type="text" name="ndom" required="" class="validated inputShort"></p>
			<p><label>Nr lokalu:&nbsp;</label><input type="text" name="nlok" class="inputShort"></p>
			<p><label>Kod pocztowy*:&nbsp;</label><input type="text" name="kodp" required="" class="validated inputShort min_6" maxlength="6"></p>
			<p><label>Miejscowość*:&nbsp;</label><input type="text" name="miasto" required="" class="validated min_3"></p>
			<p><label>Firmowa strona www:&nbsp;</label><input type="text" name="web" id="web"></p>
			<hr>
			<p><label>Telefon stacjonarny nr 1**:&nbsp;</label><input type="text" name="tel_1" class="t_validated"></p>
			<p><label>Telefon stacjonarny nr 2**:&nbsp;</label><input type="text" name="tel_2" class="t_validated"></p>
			<p><label>Telefon komórkowy**:&nbsp;</label><input type="text" name="tel_kom" class="t_validated komkom"></p>
			<p><label>Fax:&nbsp;</label><input type="text" name="tel_fax" class="f_validated"></p>
			<p>** wymagany przynajmniej jeden numer telefonu</p>
			<hr>

			<p><label>Email*:&nbsp;</label><input type="text" name="usr_email1" id="usr_email1" required="" class="validated"></p>
			<p><label>Powtórz email*:&nbsp;</label><input type="text" name="usr_email2" id="usr_email2" required="" class="validated"></p>
			<p><label>Hasło (min. 6 znaków)*:&nbsp;</label><input type="password" name="usr_password1" id="usr_password1" required="" class="p_validated"></p>
			<p><label>Powtórz hasło*:&nbsp;</label><input type="password" name="usr_password2" id="usr_password2" required="" class="p_validated"></p>
			<p class="chContainer"><input type="checkbox" required="" name="prompt1" id="prompt1"> Dobrowolnie przekazuję oraz wyrażam zgodę na przetwarzanie moich danych osobowych, zgodnie z ustawą o ochronie danych osobowych (Dz.U. z 2002 r. Nr 101, poz. 926 z późn.zm.).*</p>
		</div>
		<p>&nbsp;</p>
		<div class="valid_info"></div>
		<p>* - pole obowiązkowe</p>
		<div id="buttonContainer"><span class="diamond dButton"><button type="button" id="formSave">Zapisz</button></span></div>
		<input type='hidden' name='action' value='user'>
		<input type='hidden' name='option' value='registerSave'>
	</form>
	<p>&nbsp;</p>
</div>

<script>
function validatePhone(phone_field_val) {
    var a = phone_field_val;
    var filter = /^([0-9\+ ]{0,4})?([0-9\(\) ]{0,5})?(?:[0-9]{0,3})(?:[ \-]{1})?(?:[0-9]{0,4})(?:[ \-]{1})?(?:[0-9]{0,4})?$/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}


$(document).ready(function(){


	$('#formSave').click(function() {
		var valid = true;
		var telefon = 0;
		var phonecounter = 0;
		$('.valid_info').html('');

		$('.p_validated, .t_validated, .chContainer').removeClass('invalid');

		$('.validated').each(function(index){
			$(this).removeClass('invalid');
			if($(this).val()=='') {
				$(this).addClass('invalid');
				valid = false	;
				$('.valid_info').html('Uzupełnij brakujące dane w zaznaczonych polach.<br/>');
			}
		});

		$('.t_validate').each(function(index){
			if(count($(this))<3) {
				$('#tel_group').addClass('invalid');
				valid = false;
				$('.valid_info').html('Uzupełnij brakujące dane w zaznaczonych polach.<br/>');
			}
		});
		
		if(!$('#prompt1').is(':checked')) {
			valid = false;
			$('.chContainer').addClass('invalid');
			$('.valid_info').append('Zaznacz zgodę.<br/>');
		}
		
		if($('#usr_password1').val()!=$('#usr_password2').val()) {
			$('#usr_password1').addClass('invalid');
			$('#usr_password2').addClass('invalid');
			valid = false	;
			$('.valid_info').append('Hasła nie pasują do siebie.<br/>');
		}

		if($('#usr_password1').val().length<6) {
			valid = false;
			$('#usr_password1').addClass('invalid');
			$('.valid_info').append('Hasło jest zbyt krótkie – powinno mieć przynajmniej 6 znaków.<br/>');
		}

		if($('.min_6').val().length<6) {
			valid = false;
			$('.min_6').addClass('invalid');
			$('.valid_info').append('Zły format.<br/>');
		}

		if($('.min_13').val().length<13) {
			valid = false;
			$('.min_13').addClass('invalid');
			$('.valid_info').append('Zły format.<br/>');
		}

		$('.min_3').each(function(index){
			if($(this).val().length<3) {
				$(this).addClass('invalid');
				valid = false	;
				$('.valid_info').append('Minimum 3 znaki.<br/>');
			}
		});


		//walidacja nr telefonu
		$('.t_validated').each(function(index){
			if($(this).val().length>=9){

				if (validatePhone($(this).val())) {
						//console.log($(this).val() + " jest prawidlowy <br>");
					}else{

						$('.valid_info').html('Podany numer ma niewłaściwy format (wzór: XYZ XYZ XYZ)<br/>');
						$(this).addClass('invalid');
						valid = false;
					}

				phonecounter += 1;
			}
		});

		if(phonecounter < 1) {
			$('.t_validated').addClass('invalid');
			$('.valid_info').append('Należy podać przynajmniej jeden prawidowy numer telefonu<br/>');
			valid = false;
		}

		if(!validateEmail($('#usr_email1').val())) {
			valid = false;
			$('#usr_email1').addClass('invalid');
			$('.valid_info').append('Błędny format maila.<br/>');
		}

		if($('#usr_email1').val()!=$('#usr_email2').val()) {
			$('#usr_email1').addClass('invalid');
			$('#usr_email2').addClass('invalid');
			valid = false;
			$('.valid_info').append('Wartości w polu email są różne.<br/>');
		}

		if(valid) {
			valid = false;
			$.post("ajax.php?action=user&option=checkMail&id="+$('#usr_email1').val(), function(html) {
				if(html=='exist') {
					$('#usr_email1').addClass('invalid');
					$('#usr_email2').addClass('invalid');
					valid = false;
					$('.valid_info').append('W systemie istnieje już taki e-mail!');
				} else $('#userRegister').submit();
			});
		}

		function validateEmail($email) {
		  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		  if(!emailReg.test($email)) {
		    return false;
		  } else {
		    return true;
			}
		}
	});
});
</script>