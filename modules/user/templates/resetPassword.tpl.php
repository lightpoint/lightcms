<div id="infoMaster"></div>
<div id="infoPanel" class="noDynamicWindow"><div id="infoContainer"><div id="infoHeader"><div id="infoTitle"><div id="infoControls"><button id="infoClose" class="infoClose"></button></div></div></div><div id="infoContent"><div>
	<h2>Reset hasła</h2>
	<form name="resetPassword" id="resetPassword" method="post" action="index.php">
		<div class="form-input-container" style="height: 200px;">
			<p><label>e-mail:&nbsp;</label><input type="text" name="mail" id="mail" required="required" class="fitie-input"></p>
			<p><label>nowe hasło*</label><input type="password" name="user_pass1" id="user_pass1" required="required" class="fitie-input"></p>
			<p><label>hasło ponownie*</label><input type="password" name="user_pass2" id="user_pass2" required="required" class="fitie-input"></p>
			
			<p>&nbsp;</p>
			 </div>
			 <div class="form-submit-container">
					<input type="button" class="fitie-submit-mid" value="">
				</div>
			<div id="valid_info" class="valid_info"><span><br/>*hasło min 8 znaków, małe i duże litery oraz przynajmniej 1 cyfra</span></div>
		<input type='hidden' name='action' value='user'>
		<input type='hidden' name='option' value='resetEnd'>
		<input type='hidden' name='id' value='__LINE'>
	</form>
</div></div></div></div>

<script>
$(document).ready(function(){
	$('.fitie-submit-mid').click(function() {
		var valid = true;
		$('#valid_info').html('');
		$('#mail, #user_pass1, #user_pass2').removeClass('invalid');

		if($('#mail').val()=='') {
			$('#mail').addClass('invalid');
			valid = false	;
			$('#valid_info').html('Pole nie może być puste.<br/>');
		}

		if(!validateEmail($('#mail').val())) {
			valid = false;
			$('#mail').addClass('invalid');
			$('#valid_info').append('Błędny format maila.<br/>');
		}

		if(!checkPassword($('#user_pass1').val())) {
		console.log('klikniete');
			valid = false;
			$('#user_pass1').addClass('invalid');
			$('#valid_info').append('hasło powinno składać się przynajmniej z 8 znaków (małych i dużych liter oraz cyfr).<br/>');
		}

		if($('#user_pass1').val()!=$('#user_pass2').val()) {
			$('#user_pass1').addClass('invalid');
			$('#user_pass2').addClass('invalid');
			valid = false	;
			$('#valid_info').append('Hasła nie pasują do siebie.<br/>');
		}

		if(valid) {
			$.post("ajax.php?action=user&option=checkMail&id="+$('#mail').val(), function(html) {
				if(html=='exist') {
					$('#resetPassword').submit();
				} else {
					valid = false;
					$('#mail').addClass('invalid');
					$('#valid_info').append('W systemie nie istnieje taki e-mail!');
				}
			});
		}
	});

	function validateEmail($email) {
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  if(!emailReg.test($email)) {
	    return false;
	  } else {
	    return true;
		}
	}

	function checkPassword(str) { // at least one number, one lowercase and one uppercase letter
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{8,}/;
		return re.test(str);
	}
	//todu usunać powtórzenia kodu


});
</script>