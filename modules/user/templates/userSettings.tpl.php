<div id="tabSettings" class="settingsCont">
	<h1 class="settingsHeader">Moje konto</h1>

		<ul class="settingsMenu etabs">
			<li class="tab"><a href="#moje">moje filmy</a></li>
			<li class="tab movie_"><a href="#polubione">polubione</a></li>
			<li class="tab movie_"><a href="#polecone">polecone</a></li>
			<li class="tab movie_ustawienia"> <a href="#ustawienia">ustawienia konta</a></li>
		</ul>


	<div class="settingsTabContents">

		<!-- moje tab -->
		<div id="moje">
			<div class="movieThumbs">
				__MOVIES_MY
			</div>
			<div class="settingsTools">
				<span align="center">Edycja<br>zaznaczonych<br>filmów:</span>
				<ul class="settingsList">
					<li class="movie_public"><img src="templates/frontend/fitie_1.1/img/img_panel_do_publicznych.gif" alt="dodaj do publiczych" title="dodaj do publiczych" class="pcursor"></li>
					<li class=" movie_private"><img src="templates/frontend/fitie_1.1/img/img_panel_do_prywatnych.gif" alt="dodaj do prywatnych" title="dodaj do prywatnych" class="pcursor"></li>
					<li class="movie_trash"><img src="templates/frontend/fitie_1.1/img/img_panel_do_kosza	.gif" alt="usuń" title="usuń" ></li>
				</ul>
				<span class="changedMovies"></span>
				<span class="opis_nawigacji center">przenieś i upuść w wybranej strefie</span>
			</div>
			<div class="settingsFooter"> Masz __MOVIES_COUNT filmów fitie.
			<!-- Teraz przeglądasz film nr 3. -->
		</div>
		</div>

		<!-- polubione tab -->
		<div id="polubione">
			<div class="movieThumbs">
				__MOVIES_LIKED
			</div>
			<div class="settingsTools">
				<span align="center">Usuń<br>z ulubionych:</span>
				<ul class="settingsList">
					<li class="liked_trash"><img src="templates/frontend/fitie_1.1/img/img_panel_do_kosza	.gif" alt="usuń" title="usuń" ></li>
				</ul>
				<span class="changedMovies"></span>

			</div>

		</div>

		<!-- polecanee tab -->
		<div id="polecone">
			<div class="movieThumbs">
				__MOVIES_SHARED
			</div>
      <div class="settingsTools">
				<span align="center">Usuń<br>z polecanych:</span>
				<ul class="settingsList">
					<li class="shared_trash"><img src="templates/frontend/fitie_1.1/img/img_panel_do_kosza	.gif" alt="usuń" title="usuń" ></li>
				</ul>
				<span class="changedMovies"></span>

			</div>
		</div>

		<!-- polubione tab -->
		<div id="ustawienia">
			<p><strong>Zmień hasło dostępu do swojego konta</strong> (min 8 znaków, małe i duże litery oraz przynajmniej 1 cyfra):</p>
			<form name="changePassword" id="changePassword" method="post" action="index.php">
			<div class="form-input-container settingsContainer">
				<p><input placeholder="stare hasło" type="password" name="USR_PASS_OLD" id="USR_PASS_OLD" required class="fitie-input"></p>
				<p><input placeholder="hasło" type="password" name="USR_PASS_1" id="USR_PASS_1" required class="fitie-input"></p>
				<p><input placeholder="ponownie hasło" type="password" name="USR_PASS_2" id="USR_PASS_2" required class="fitie-input"></p>
				<p class="valid_info"></p>
		</div>
		<div class="form-submit-container">
			<input type="submit" class="fitie-submit-mid" value="">
		</div>

		<input type='hidden' name='action' value='user'>
		<input type='hidden' name='option' value='changePassword'>
		</form>
		
		<form name="changeData" id="changeData" method="post" action="index.php">
		<div class="form-input-container" style="height: 180px; margin-top: 15px;">

			<p><strong>Zmiana danych konta</strong></p>
			<div class="fitie-input-div">telefon: <input placeholder="nr telefonu" type="text" name="telefon" id="telefon" required class="fitie-input-small" value="__USR_TEL"></div>
			<div class="fitie-input-div">e-mail: <input placeholder="e-mail" type="text" name="email" id="email" required class="fitie-input-small" value="__USR_MAIL"></div>
			<p><input type="button" value="zapisz" name="changeSave" id="changeSave" class="fitie-button f-b-normal"> <input type="button" value="anuluj" name="changeCancel" id="changeCancel" class="infoClose fitie-button f-b-normal"> <input type="button" value="usuń konto" name="delAccount" id="delAccount" class="fitie-button f-b-del"></p>
		</div>
		<input type='hidden' name='action' value='user'>
		<input type='hidden' name='option' value='changeData'>
		
		</form>
	</div>
	</div>
</div>

<script>
$(document).ready(function() {
  if($('#tabSettings').easytabs()) {
  
  }

	$("#changePassword").submit(function(event) {
		event.preventDefault();
		$('input').removeClass('invalid');
		var user_pass_1_value = $('#USR_PASS_1').val();
		var user_pass_2_value = $('#USR_PASS_2').val();
		var user_pass_old_value = $('#USR_PASS_OLD').val();

		$.post("ajax.php?action=user&option=checkPass&id="+user_pass_old_value, function(html) {
			if( html === 'match') {
				if(user_pass_1_value === user_pass_2_value) {
					if(user_pass_1_value.length >= 8) {
						$.post("ajax.php", $("#changePassword").serialize())
							.done(function(html) {
								$('input').removeClass('invalid');
								$(this).lightClose();
								$('body').append(html);
							});
					} else {
						$('#USR_PASS_1, #USR_PASS_2').addClass('invalid');
						$('.valid_info').html('hasło za krótkie — min. 8 znaków');
					}
				} else {
					$('#USR_PASS_1, #USR_PASS_2').addClass('invalid');
					$('.valid_info').html('hasła się różnią');
				}
			} else {
				$('#USR_PASS_OLD').addClass('invalid');
				$('.valid_info').html(html);
			}
		});

	});
});
</script>