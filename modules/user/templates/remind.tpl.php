<div id="userContainer">
	<h2>Przypomnienie hasła</h2>
	<div id="userData">
		<p>Wpisz mail, który podałeś podczas rejestracji.<br/></p>
		<p><label>e-mail*:&nbsp;</label><input type="text" name="mail" id="mail" required="" > &nbsp;&nbsp;&nbsp;<span class="diamond dButton"><button type="button" id="formPasswordRemind">Wyślij</button></span></p>
		<div class="valid_info"></div>
	</div>
</div>

<script>
$(document).on("click", "#formPasswordRemind", function(event){
	var valid = true;
	$('.valid_info').html('');
	$('#mail').removeClass('invalid');

	if($('#mail').val()=='') {
		$('#mail').addClass('invalid');
		valid = false	;
		$('.valid_info').html('Pole nie może być puste.<br/>');
	}

	if(!validateEmail($('#mail').val())) {
		valid = false;
		$('#mail').addClass('invalid');
		$('.valid_info').append('Błędny format maila.<br/>');
	}
	
	if(valid) {
		valid = false;
		$.post("ajax.php?action=user&option=checkMail&id="+$('#mail').val(), function(html) {
			if(html=='exist') {
				$.post("ajax.php",{action: 'user', option: 'remindSave', mail: $('#mail').val()})
				.done(function(response) {
					$(this).lightInfo(response);
				});
				$('#userReminder').submit();
			} else {
				valid = false;
				$('#mail').addClass('invalid');
				$('.valid_info').append('W systemie nie istnieje taki e-mail!');
			}
		});
	}
});

function validateEmail($email) {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if(!emailReg.test($email)) {
    return false;
  } else {
    return true;
	}
}
</script>