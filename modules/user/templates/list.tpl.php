<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=user">Użytkownicy</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-users"></i>
					<span>Lista zarejestrowanych użytkowników</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-container">
				<div class="col-sm-6">rekordów na stronie: <select name="paginator_value" id="paginator_value"><option value="25">25</option><option value="50">50</option><option value="100">100</option><option value="200">200</option><option value="500">500</option><option value="1000">1000</option></select> <button type="button" class="btn btn-primary" id="paginator_value_button">ok</button></div>
				<div class="col-sm-6"><input type="text" placeholder="szukaj" name="search_string" id="search_string"> <button type="button" class="btn btn-warning btn-label-right" id="search_button">szukaj<span><i class="fa fa-search"></i></span></button></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">__TABLE</div>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#paginator_value').val('__PAGINATOR_VALUE');
	
	$(document).on("click",'#paginator_value_button' ,function() {
		$.post("request.php?action=user&option=getUserTable", { paginator_value: $('#paginator_value').val()})
	  .done(function(data) {
	    $('#contentPlace').html(data);
	  });
	});
	
	$(document).on("click",'#search_button' ,function() {
		$.post("request.php?action=user&option=search&id="+$('#search_string').val(), { paginator_value: $('#paginator_value').val()})
	  .done(function(data) {
	    $('#contentPlace').html(data);
	  });
	});
	
	$(document).on("click",'#userSave' ,function() {
		$.post("request.php", $("#userData").serialize())
		  .done(function(data) {
		    location.reload();
		 });
	});
	
	$(document).on("change",'#resetShow' ,function() {
		$('#inputHidden').toggle();
	});
	
	$(document).on("click",'#resetUserPassword' ,function() {
		$.post('request.php', {action: 'user', option: 'resetUserPassword', id: $('#USR_ID').val()})
		.done(function(html) {
			$(this).lightClose();
			$(this).messageText(html);
		});
	});
	
	$(document).on("click",'#changeUserPassword' ,function() {
		if($('#newUserPassword').val()!='') {
			if(confirm('Zapisac nowe haslo uzytkownika?')) {
				$.post('request.php', {action: 'user', option: 'changeUserPassword', id: $('#USR_ID').val(), pass: $('#newUserPassword').val()})
				.done(function(html) {
					$(this).lightClose();
					$(this).messageText(html);
				});
			}
		}
	});
});
</script>