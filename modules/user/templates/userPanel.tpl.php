<div id="panelContainer">
	<div class="userData">
		Jesteś zalogowany jako: <strong>__USER_DATA</strong><br/>
		<a href="user-logout.html"><i class="fa fa-lock"></i> wyloguj</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="user-account.html" id="accountLink" title="Twoje konto"><i class="fa fa-user"></i> Twoje konto</a>
	</div>
	<div class="infoData">
		<a href="basket.html" id="basketLink" title="Pozycje w koszyku"><i class="fa fa-shopping-cart"></i> w koszyku: brak produktów</a><br/>
		posiadasz punktów: 1500
	</div>
</div>