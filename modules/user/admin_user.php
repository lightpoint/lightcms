<?php
class User extends View {
	public $page, $limit;
	public $path = 'modules/user/templates/'; //path to templates for admin
	
	public function getUserList() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			
			$list = array(
				'__TABLE'	=>$this->getUserTable(),
				'__PAGINATOR_VALUE' => $this->var->pages,
			);
			
			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}
	
	public function getUserTable() {
		if(empty($this->var->pages)) 
			$_SESSION['pages'] = $this->var->pages = 25;
		
		if($this->var->paginator_value)
			$_SESSION['pages'] = $this->var->paginator_value;
			
		$this->page = $this->var->page;
		if(empty($this->page))
			$this->page = 1;
		$this->page--;
		
		$page_start = $this->page*$this->var->pages;
		
		$this->db->queryString('select SQL_CALC_FOUND_ROWS * from '.__BP.'users');
		
		$this->db->limit($page_start.', '.$this->var->pages);
		$users = $this->db->getData();
	  
		$this->db->queryString('SELECT FOUND_ROWS() AS `found_rows`');
		$this->limit = $this->db->getRow();
		
		$table = new table('myTable', 'tablesorter');
		$tableHeader = array('lp.','Id','Firma', 'Imię i nazwisko', 'E-mail', 'Telefon', 'Data rejestracji', '', '');
		$table->createHeader($tableHeader);
		
		$i = ($this->page*$this->var->pages)+1;
		
		foreach($users as $user) {
			$table->addCell($i);
			$table->addCell($user['USR_ID']);
			$table->addCell($user['USR_FIRM']);
			$table->addCell($user['USR_NAME'].' '.$user['USR_SURNAME']);
			$table->addCell($user['USR_MAIL']);
			$table->addCell($user['USR_TEL_1'].' '.$user['USR_TEL_2'].' '.$user['USR_MOBILE'].' '.$user['USR_FAX']);
			$table->addCell($user['USR_DATE_ADD']);
			$table->addCell($user['USR_ACTIVE']);
			$table->addLink('request.php?action=user&option=details&id='.$user['USR_ID'], 'szczegóły', 'light',null, 'Szczegóły konta');
			$table->addRow();
			$i++;
		}
		return pagination::create($this->limit['found_rows'], $this->page, $this->var->pages, '?action=user').$table->createBody();
	}
	
	public function details() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString("select * from ".__BP."users where USR_ID=".$this->var->id);
			$user = $this->db->getRow();
			
			if($user) {
				$list = array(
					'__USR_ID'=>$user['USR_ID'],
					'__USR_TEL_1'=>$user['USR_TEL_1'],
					'__USR_TEL_2'=>$user['USR_TEL_2'],
					'__USR_MOBILE'=>$user['USR_MOBILE'],
					'__USR_FAX'=>$user['USR_FAX'],
					'__USR_MAIL'=>$user['USR_MAIL'],
					'__USR_CONFIRMED'=>$user['USR_CONFIRMED'],
					'__USR_ACTIVE'=>$user['USR_ACTIVE'],
					'__USR_DATE_ADD'=>$user['USR_DATE_ADD'],
					'__USR_NAME'=>$user['USR_NAME'],
					'__USR_FIRM'=>$user['USR_FIRM'],
					'__USR_SURNAME'=>$user['USR_SURNAME'],
					'__USR_NIP'=>$user['USR_NIP'],
					'__USR_STREET'=>$user['USR_STREET'],
					'__USR_NR_D'=>$user['USR_NR_D'],
					'__USR_NR_M'=>$user['USR_NR_M'],
					'__USR_POSTCODE'=>$user['USR_POSTCODE'],
					'__USR_CITY'=>$user['USR_CITY'],
					'__USR_WWW'=>$user['USR_WWW'],
					'__USR_CONFIRMED'=>$user['USR_CONFIRMED'],
					'__USR_ACTIVE'=>$user['USR_ACTIVE'],
				);
				return Template::parse($list, file_get_contents($this->path.'details.tpl.php'));
			} else return $this->noData();
		} else return $this->goAway();
	}
	
	public function saveUserData() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$update = array(
				'USR_MAIL'=>$this->var->USR_MAIL,
				'USR_TEL'=>$this->var->USR_TEL,
				'USR_ACTIVE'=>$this->var->USR_ACTIVE,
			);
			$this->db->queryString(__BP.'users');
			$this->db->updateQuery($update, 'USR_ID', $this->var->USR_ID);
			
			//save event
			$event_string = array('event'=>'zmiana danych usera', 'user'=>$this->var->USR_ID, 'USR_TEL'=>$this->var->USR_TEL, 'USR_MAIL'=>$this->var->USR_MAIL, 'USR_ACTIVE'=>$this->var->USR_ACTIVE);
			$event = new eventSaver();
			$event->add($this->var->admin_id, json_encode($event_string));
				
		} else return $this->goAway();
	}
	
	public function search() {
		if(empty($this->var->pages)) 
			$_SESSION['pages'] = $this->var->pages = 25;
		
		if($this->var->paginator_value)
			$_SESSION['pages'] = $this->var->paginator_value;
			
		$this->page = $this->var->page;
		if(empty($this->page))
			$this->page = 1;
		$this->page--;
		
		$page_start = $this->page*$this->var->pages;
		
		$this->db->queryString("select SQL_CALC_FOUND_ROWS * from ".__BP."users where USR_TEL like '%".$this->var->id."%' or USR_MAIL like '%".$this->var->id."%'");
		
		
		$this->db->limit($page_start.', '.$this->var->pages);
		$users = $this->db->getData();
	  
		$this->db->queryString('SELECT FOUND_ROWS() AS `found_rows`');
		$this->limit = $this->db->getRow();
		
		$table = new table('myTable', 'tablesorter');
		$tableHeader = array('lp.','Id', 'E-mail','Telefon', 'Data rejestracji', '', '');
		$table->createHeader($tableHeader);
		
		$i = ($this->page*$this->var->pages)+1;
		
		foreach($users as $user) {
			$table->addCell($i, 'lp');
			$table->addCell($user['USR_ID']);
			$table->addCell($user['USR_MAIL']);
			$table->addCell($user['USR_TEL']);
			$table->addCell($user['USR_DATE_ADD']);
			$table->addCell($user['USR_ACTIVE']);
			$table->addLink('request.php?action=user&option=details&id='.$user['USR_ID'], 'szczegóły', 'light',null, 'Szczegóły konta');
			$table->addRow();
			$i++;
		}
		return pagination::create($this->limit['found_rows'], $this->page, $this->var->pages, '?action=user').$table->createBody();
	}
	
	public function getForMenu() {
		$menu_module = $this->var->menu_module;
		$menu_option = $this->var->menu_option;
		
		//rejestracja
		if($menu_module=='user' and $menu_option=='register')
				$selected = " checked";
			else $selected = '';
		$line.="<input type='radio' name='menu' value='user:register:1:Rejestracja'".$selected."> - rejestracja<br/>";
		
		//box logowania
		if($menu_module=='user' and $menu_option=='login')
				$selected = " checked";
			else $selected = '';
		$line.="<input type='radio' name='menu' value='user:login:1:Logowanie'".$selected."> - logowanie<br/>";
		
		return $line;
	}
	
	public function resetUserPassword() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$new_password	= coder::generatePassword();
				$this->db->queryString("select * from ".__BP."users where USR_ID=".$this->var->id);
				//$this->db->query();
				$user = $this->db->getRow();

				if($user) {
					//save event to table users
					$insert = array(
						'PAR_USR_ID'=>$user['USR_ID'],
						'PAR_USR_EMAIL'=>$user['USR_MAIL'],
						'PAR_NEW_PASSWORD'=>coder::hashString($new_password),
						'PAR_IP'=>clientIP::get(),
						'PAR_STATUS'=>0,
					);
					$this->db->queryString(__BP.'password_reminder');
					$this->db->insertQuery($insert);

					//sending mail to user
					$data_template = array (
						'__MAIL'=>$user['USR_ID'],
						'__PASS'=>coder::hashString($new_password),
						'__LINK'=>SC_ROOT_URL,
					);

					$user_remind = new Mailer();

					$files = array('logo.png');
					$user_remind->sendMail($user['USR_MAIL'], '', '', 'Reset hasła ', Template::parse($data_template, file_get_contents('templates/mail/remind.tpl.php')), null, null, $files, 'templates/mail/images/');
					
					//save event
					$event_string = array('event'=>'reset hasla', 'user'=>$this->var->id);
					$event = new eventSaver();
					$event->add($this->var->admin_id, json_encode($event_string));

					//return file_get_contents($this->path.'remindPosted.tpl.php');
					return 'Wysłano link resetujący hasło do użytkownika';
				}
			}
		} else return $this->goAway();
	}
	
	public function changeUserPassword() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$update = array('USR_PASS' => coder::hashString($this->var->pass));
			$this->db->queryString(__BP.'users');
			$this->db->updateQuery($update, 'USR_ID', $this->var->id);
			
			//save event
			$event_string = array('event'=>'zmiana hasla', 'user'=>$this->var->id);
			$event = new eventSaver();
			$event->add($this->var->admin_id, json_encode($event_string));
			
			return "haslo uzytkownika zostalo zmienione";
		} else return $this->goAway();
	}
	
	public function dashboard() {
		$this->db->queryString("select USR_ID, USR_FIRM, USR_NAME, USR_SURNAME, USR_MAIL from ".__BP."users where USR_ACTIVE in (1,2) order by USR_DATE_ADD desc limit 10");
		$users = $this->db->getDataFetch();
		
		if($users) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.','Firma', 'Imię i nazwisko', 'E-mail');
			$table->createHeader($tableHeader);
			$i = 1;
			foreach($users as $user) {
				$link = 'request.php?action=user&option=details&id='.$user['USR_ID'];
				$table->addLink($link, $i, 'light',null, 'Szczegóły konta');
				$table->addLink($link, $user['USR_FIRM'], 'light',null, 'Szczegóły konta');
				$table->addLink($link, $user['USR_NAME'].' '.$user['USR_SURNAME'], 'light',null, 'Szczegóły konta');
				$table->addLink($link, $user['USR_MAIL'], 'light',null, 'Szczegóły konta');
				$table->addRow();
				$i++;
			}
			$tabList = $table->createBody()."<a href='?action=user'>Pełna lista <i class='fa fa-arrow-right'></i></a>";
		} else $tabList = $this->noData();
		$list = array(
			'__MODULE_TITLE'=>'Użytkownicy - 10 ostatnich',
			'__MODULE_ICON'=>'fa fa-users',
			'__MODULE_CONTENT'=>$tabList,
		);
		return Template::parse($list, file_get_contents('modules/dashboard/templates/element.tpl.php'));
	}
}

function user($option=null) {
	$user = new User;
	if($option!=null and method_exists($user, $option))
		return $user->$option();
	else 
		return $user->getUserList();
}
?>