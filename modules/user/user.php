<?php
class User extends View {
	public $path = 'modules/user/templates/'; //path to templates

	//return list of users
	public function getList() {
		return "user-";
	}

	public function panel() {
		if(isset($this->var->user_data_id)) {
			return $this->getUserPanel();
		} else {
			return $this->showLogin();
		}
	}

	private function showLogin() {
		return file_get_contents($this->path.'login.tpl.php');
	}

	public function checkLogin() {
		//user_data_id
		if(isset($this->var->login) and isset($this->var->pass)) {
			$this->db->queryString('select * from '.__BP."users where USR_MAIL='".$this->var->login."' and USR_PASS='".coder::hashString($this->var->pass)."' and USR_ACTIVE in (1,2)");
			
			$user = $this->db->getRow();
			if($user) {
				$_SESSION['user_data_id'] = $user['USR_ID'];
				$_SESSION['user_data_email'] = $user['USR_MAIL'];
				$_SESSION['user_data_name'] = $user['USR_NAME'];
				$_SESSION['user_data_surname'] = $user['USR_SURNAME'];
				$_SESSION['user_data_alias'] = $user['USR_ALIAS'];
				$_SESSION['user_data_agency'] = $user['USR_AGENCY'];
				
				if($this->var->action_redirect) {
					$redirect = '?action='.$this->var->action_redirect;
					if($this->var->option_redirect) 
						$redirect.= '&option='.$this->var->option_redirect;
				}
				
				header('location: index.php');
			} else return 'Błędne dane logowania!';
		} else return 'Wypełnij wszystkie pola!';
	}

	public function getUserPanel() {
		$list = array(
			'__USER_DATA' => $this->var->user_data_name.' '.$this->var->user_data_surname
		);
		
		return Template::parse($list, file_get_contents($this->path.'userPanel.tpl.php'));
	}
	
	public function account() {
		$list = array(
			'__USER_DATA' => $this->var->user_data_name.' '.$this->var->user_data_surname
		);
		
		return Template::parse($list, file_get_contents($this->path.'panel.tpl.php'));
	}

	public function lista() {
		return $_SERVER["HTTP_REFERER"];
	}

	//password change by user
	public function changePasswordold() {
		if($this->var->usr_password1===$this->var->usr_password2) {
			$this->db->queryString("update ".__BP."users set USR_PASS='".coder::hashString($this->var->usr_password1)."' where USR_ID=".$this->var->user_data_id);
			$this->db->execQuery();

			$_SESSION = array();
			session_destroy();
			header('location: user-passwordChanged.html');
		} else $this->error();
	}

	public function passwordChanged() {
		return file_get_contents($this->path.'passwordChange.tpl.php');
	}

	private function checkPrivilages() {
		//TODO: check all privilages from modules ans set visibility
		return true;
	}

	public function setLoginError() {
		$this->saveErrorLogin();
		return file_get_contents($this->path.'loginError.tpl.php');
	}


	public function logout() {
		$_SESSION = array();
		unset($_SESSION);
		header('location: index.php');
	}


	public function updateUserData() {
		$this->db->queryString("select * from ".__BP."users where USR_ID='".$this->var->user_data_id."'");
		$update = $this->db->getRow();
		$list = array(
			'__TEL' =>$update['USR_TEL'],
			'__MAIL' =>$update['USR_MAIL'],
		);
		return Template::parse($list, file_get_contents($this->path.'updateUserData.tpl.php'));
	}

	public function saveUpdateUserData() {
		$this->db->queryString(__BP.'users');
		$list = array(
			'USR_TEL' =>$this->var->USR_TEL,
			'USR_MAIL' =>$this->var->USR_MAIL,
		);

		if(!empty($this->var->usr_password1) and $this->var->usr_password1===$this->var->usr_password2)
			$list['pass'] = md5($this->var->usr_password1);

		$update = $this->db->updateQuery($list, 'numer_id', $this->var->user_data_id);
		header('location: ?');
	}

	public function password_remind() {
		return file_get_contents($this->path.'remind.tpl.php');
	}

	public function remindSave() {
		if($this->var->mail) {
			$new_password	= coder::generatePassword();
			$this->db->queryString("select * from ".__BP."users where USR_MAIL='".$this->var->mail."'");
			//$this->db->query();
			$user = $this->db->getRow();

			if($user) {
				//save event to table users
				$insert = array(
					'PAR_USR_ID'=>$user['USR_ID'],
					'PAR_USR_EMAIL'=>$this->var->mail,
					'PAR_NEW_PASSWORD'=>coder::hashString($new_password),
					'PAR_IP'=>clientIP::get(),
					'PAR_STATUS'=>0,
				);
				$this->db->queryString(__BP.'password_reminder');
				$this->db->insertQuery($insert);
				
				//sending mail to user
				$data_template = array (
					'__MAIL'=>$this->var->mail,
					'__PASS'=>coder::hashString($new_password),
					'__LINK'=>SC_ROOT_URL,
				);

				$user_remind = new Mailer();

				$files = array('top.jpg');
				$user_remind->sendMail($this->var->mail, '', '', 'Reset hasła Twojego konta na - MOC NA START!', Template::parse($data_template, file_get_contents('templates/mail/remind.tpl.php')), null, null, $files, 'templates/mail/images/');

				return file_get_contents($this->path.'remindPosted.tpl.php');
			} else return 'brak użytkownika';
		} else return 'nie podano maila';
	}

	public function resetPassword() {
		$line = array('__LINE'=>$this->var->id);
		return Template::parse($line, file_get_contents($this->path.'resetPassword.tpl.php'));
	}

	public function resetEnd() {
		if($this->var->mail and $this->var->user_pass1==$this->var->user_pass1) {
			//check existing reset request
			$this->db->queryString("select * from ".__BP."password_reminder left join ".__BP."users on PAR_USR_ID=USR_ID where PAR_NEW_PASSWORD='".$this->var->id."' and PAR_USR_EMAIL='".$this->var->mail."'");
			$user = $this->db->getRow();

			if($user) {
				//change password
				$this->db->queryString("update ".__BP."users set USR_PASS='".coder::hashString($this->var->user_pass1)."' where USR_ID=".$user['USR_ID']." and USR_MAIL='".$this->var->mail."'");
				$this->db->execQuery();
				//change status and date click
				$this->db->queryString("update ".__BP."password_reminder set PAR_STATUS=1, PAR_CLICK='".date('Y-m-d h:i:s')."' where PAR_ID=".$user['PAR_ID']);
				$this->db->execQuery();

				//$module = new Module;
				//$movies = $module->load('data','assignMoviesToUser');function moved to server
				$_SESSION['user_data_id'] = $user['USR_ID'];
				$_SESSION['user_data_email'] = $user['USR_MAIL'];
				$_SESSION['user_data_pn'] = $user['USR_TEL'];

				header('location: user-logged.html');
			}
		}
	}

	public function checkMail() {
		$this->db->queryString("select USR_ID from ".__BP."users where USR_MAIL='".$this->var->id."' and USR_ACTIVE in (1,2)");
		$mail = $this->db->getRow();

		if($mail)
			return 'exist';
		else return 'not_exist';
	}

	public function checkPass() {
		if($this->var->user_data_id) {
			$this->db->queryString("select USR_PASS from ".__BP."users where USR_ID=".$this->var->user_data_id);
			$pass = $this->db->getRow();

			if(coder::hashString($this->var->id)==$pass['USR_PASS'])
				return 'match';
			else return 'błędne stare hasło do konta';
		}
	}

	public function changePassword() {
		if($this->var->user_data_id and ($this->var->USR_PASS_1==$this->var->USR_PASS_2)) {
			$this->db->queryString("select USR_PASS from ".__BP."users where USR_ID=".$this->var->user_data_id);
			$pass = $this->db->getRow();
			if(coder::hashString($this->var->USR_PASS_OLD)==$pass['USR_PASS']) {
				$insert_change = array(
					'PAC_USR_ID' => $this->var->user_data_id,
					'PAC_PASS_NEW' => coder::hashString($this->var->USR_PASS_1),
					'PAC_PASS_OLD' => $pass['USR_PASS'],
					'PAC_USR_IP' => clientIP::get());

				$update_change = array('USR_PASS' => coder::hashString($this->var->USR_PASS_1));

				//save to password_change
				$this->db->queryString(__BP.'password_change');
				$this->db->insertQuery($insert_change);

				//save new pass to user
				$this->db->queryString(__BP.'users');
				$this->db->updateQuery($update_change, 'USR_ID', $this->var->user_data_id);

				return file_get_contents($this->path.'passChanged.tpl.php');
			}  else return 'error .2';
		} else return 'error .1';
	}
	
	public function changeData() {
		if($this->var->user_data_id) {
			$phone = str_replace(' ', '', $this->var->telefon);
			$phone = str_replace('-', '', $phone);
			if(strlen($phone)<=9)
				$phone = 	'48'.$phone;
				
			$update = array('USR_MAIL'=>$this->var->email, 'USR_TEL'=>$phone);
			$this->db->queryString(__BP.'users');
			$this->db->updateQuery($update, 'USR_ID', $this->var->user_data_id);
			
			//add to session data
			$_SESSION['user_data_pn'] = $phone;
			$_SESSION['user_data_email'] = $this->var->email;
			
			//save event
			$event_string = array('event'=>'zmiana danych konta', 'user'=>$this->var->user_data_id, 'telefon'=>$this->var->telefon, 'email'=>$this->var->email);
			$event = new eventSaver();
			$event->add($this->var->user_data_id, json_encode($event_string), 'user');
		}
	}
	
	public function delAccountPrompt() {
		if($this->var->user_data_id) {
			$update = array('USR_ACTIVE'=>3);
			$this->db->queryString(__BP.'users');
			$this->db->updateQuery($update, 'USR_ID', $this->var->user_data_id);
			
			//save event
			$event_string = array('event'=>'usuniecie konta', 'user'=>$this->var->user_data_id);
			$event = new eventSaver();
			$event->add($this->var->user_data_id, json_encode($event_string), 'user');
			
			$_SESSION = array();
			session_destroy();
			header('location: /');
		}
	}

	public function register() {
		return file_get_contents($this->path.'register.tpl.php');
	}
	
	public function registerSave() {
		if(1) {
			$user_data = array(
					'USR_PASS' => coder::hashString($this->var->usr_password1),
					'USR_ALIAS' => md5(microtime()),
					'USR_FIRM' => $this->var->firma,
					'USR_CITY' => $this->var->miasto,
					'USR_POSTCODE' => $this->var->kodp,
					'USR_NIP' => $this->var->nip,
					'USR_STREET' => $this->var->ul,
					'USR_NR_D' => $this->var->ndom,
					'USR_NR_D' => $this->var->nlok,
					'USR_TEL_1' => $this->var->tel_1,
					'USR_TEL_2' => $this->var->tel_2,
					'USR_MOBILE' => $this->var->tel_kom,
					'USR_FAX' => $this->var->tel_fax,
					'USR_MAIL' => $this->var->usr_email1,
					'USR_WWW' => $this->var->web,
					'USR_NAME' => $this->var->usr_name,
					'USR_AKU' => $this->var->prompt2,
					'USR_SURNAME' => $this->var->usr_surname,
					'USR_AGENCY' => $this->getAgency($this->var->kodp),
				);
			$this->db->queryString(__BP.'users');
			$usr_id = $this->db->insertQuery($user_data);
			
			//sending email to user
			$data_template = array (
				'__NAME'=>$this->var->usr_name,
				'__LINK'=>SC_ROOT_URL.'user-confirm-'.$this->generateRegisterLink($usr_id).'.html',
				'__NUMBER'=>$usr_id,
				'__EMAIL'=>$this->var->usr_email1,
				'__PASS'=>$this->var->usr_password1,
			);
			
			$user_mail_registration = new Mailer();

			$files = array('top.jpg');
			$user_mail_registration->sendMail($this->var->usr_email1, $this->var->name." ".$this->var->surname, '', 'Rejestracja w systemie', Template::parse($data_template, file_get_contents('templates/mail/registerMail.tpl.php')), null, null, $files, 'templates/mail/images/');
			
			header('location: user-registered.html');
		}
	}
	
	public function registered() {
		return file_get_contents($this->path.'registered.tpl.php');
	}
	
	private function generateRegisterLink($id) {
		$key = md5(microtime());
		$insert = array(
			'LRE_USR_ID'=>$id,
			'LRE_CONTROL_KEY'=>$key,
			'LRE_LOG_ID'=>20,
			'LRE_IP'=>clientIP::get(),
			'LRE_STATUS'=>0
		);
		$this->db->queryString(__BP.'log_register');
		$this->db->insertQuery($insert);
		
		return $key;
	}
	
	//confirm registration from email link
	public function confirm() {
		if($this->var->id) {
			$this->db->queryString("select LRE_USR_ID, LRE_STATUS from ".__BP."log_register where LRE_CONTROL_KEY='".$this->var->id."'");
			$user = $this->db->getRow();
			if($user) {
				if($user['LRE_STATUS']==0) {
					$this->db->queryString("update ".__BP."log_register set LRE_STATUS=1, LRE_DATE_CLICK='".date('Y-m-d H:i:s')."' where LRE_CONTROL_KEY='".$this->var->id."'");
					$this->db->execQuery();//confirmed click
					
					$this->db->queryString("update ".__BP."users set USR_ACTIVE=2 where USR_ID=".$user['LRE_USR_ID']);
					$this->db->execQuery();//change client status 
					
					return file_get_contents($this->path.'confirmed.tpl.php');
				} else return file_get_contents($this->path.'confirmed.tpl.php'); //already confirmed
			} else return $this->pageError(); //no found data with key
		} else return $this->pageError(); //no key in link
	}
	
	private function getAgency($postcode) {
		$this->db->queryString("select agencja from kody_pocztowe where kod='".$postcode."'");
		$agency = $this->db->getRow();
		if($agency)
			return $agency['agencja'];
		else return null;
	}
	
	public function saveForm() {
		if($this->var->email and $this->var->content) {
			//TODO: send or save
			
			
			header('location: kontakt.html');
		}
		header('location: kontakt.html');
	}
}


function user($option=null) {
	$user = new User;
	if($option!=null and method_exists($user, $option))
		return $user->$option();
	else
		return $user->getList();
}
?>