<?php
/* for admin panel */
class Administrator extends View {
	public $path = 'modules/administrator/templates/';

	public static function checkAdmin() {
		$controller = new Controller;
		$var = $controller->GetData();
		//die($var->s_l_login." ".$var->s_l_pass);
		if(U_TYPE=='base') {
			if(!empty($var->s_l_login) and !empty($var->s_l_pass)) {
				$admin_data = new Connector("select * from ".__BP."administrator where ADM_LOGIN='".$var->s_l_login."' and ADM_PASS='".$var->s_l_pass."'");
				$admin = $admin_data->getRow();
				
				if($admin['ADM_ID']!='') {
					$_SESSION['admin_id'] = $admin['ADM_ID'];
					$_SESSION['admin_data'] = $admin['ADM_NAME'].' '.$admin['ADM_SURNAME'];
					$_SESSION['admin_role'] = $admin['ADM_ROLE'];
					$_SESSION['admin_agency'] = $admin['ADM_AGENCY'];
					return true;
				} else return false;
			}
		} elseif(U_TYPE=='file') {
				//die($var->s_l_pass." ".__U_LOGIN);
				if($var->s_l_login==__U_LOGIN and $var->s_l_pass==__U_PASS) {
					$_SESSION['admin_id'] = 1;
					$_SESSION['admin_data'] = 'sysadmin';
					$_SESSION['admin_role'] = 'admin';
					$_SESSION['admin_agency'] = null;
					return true;
				} else return false;
			}
	}
	
	public function dropdown() {
		$list = array(
			'__AVATAR' => 'templates/backend/devoops/img/avatar.png',
			'__ADMIN_DATA' => $this->var->admin_data,
		);
		
		return Template::parse($list, file_get_contents($this->path.'dropdown.tpl.php'));
	}
	
	public function getAdminList() {
		if($this->var->admin_role=='admin') {
			$this->db->queryString("select ADM_SEND_MAIL, ADM_AGENCY, ADM_NAME, ADM_SURNAME, ADM_MAIL, ADM_ROLE, ADM_ACTIVE, ADM_ID, group_concat(ADR_ROLE_ID SEPARATOR ', ')as privileges from ".__BP."administrator left join ".__BP."admin_roles on ADM_ID=ADR_ADM_ID group by ADM_ID order by ADM_ID");
			$admins = $this->db->GetDataFetch();
			
			$i = 1;
			if($admins) {
				$table = new table('myTable', 'tablesorter');
				$tableHeader = array('lp.', 'Dane','Mail','Powiadomienia', 'Typ', 'Uprawnienia', 'Agencja', 'Status','');
				$table->createHeader($tableHeader);
				
				foreach($admins as $admin) {
					$table->addCell($i, 'lp');
					$table->addCell($admin['ADM_NAME']." ".$admin['ADM_SURNAME']);
					$table->addCell($admin['ADM_MAIL']);
					$table->addCell($admin['ADM_SEND_MAIL']);
					$table->addCell($admin['ADM_ROLE']);
					$table->addCell($admin['privileges']);
					$table->addCell($admin['ADM_AGENCY']);
					$table->addCell(($admin['ADM_ACTIVE']=='active') ? 'aktywny' : 'nieaktywny');
					$table->addLink('?action=administrator&option=editAdmin&id='.$admin['ADM_ID'], 'edycja');
					$table->addRow();
					$i++;
				}
				$tableBody = $table->createBody();
			} else $tableBody = '<table id="myTable" class="tablesorter"><tr><td align="center"> - brak danych - </td></tr></table>';
			
			$list = array(
				'__TABLE'	=>$tableBody,
			);
			
			return Template::parse($list, file_get_contents($this->path.'adminList.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addAdmin() {
		return file_get_contents($this->path.'addAdmin.tpl.php');
	}
	
	public function addSave() {
		$this->db->queryString(__BP.'administrator');

		$add_data = array(
			'ADM_NAME'=>$this->var->ADM_NAME,
			'ADM_SURNAME'=>$this->var->ADM_SURNAME,
			'ADM_LOGIN'=>$this->var->ADM_LOGIN,
			'ADM_AGENCY'=>$this->var->ADM_AGENCY,
			'ADM_MAIL'=>$this->var->ADM_MAIL,
			'ADM_PASS'=>coder::hashString($this->var->ADM_PASS),
			'ADM_ROLE'=>$this->var->role,
		);
		
		$this->db->InsertQuery($add_data);

		header('location: admin.php?action=administrator');
	}
	
	public function editAdmin() {
		if($this->var->admin_role=='admin') {
			$this->db->queryString('select * from '.__BP.'administrator where ADM_ID='.$this->var->id);
			//$this->db->query();
			$admin = $this->db->GetRow();
			
			$list = array(
				'__ADM_ID' => $admin['ADM_ID'],
				'__ADM_NAME' => $admin['ADM_NAME'],
				'__ADM_SURNAME' => $admin['ADM_SURNAME'],
				'__ADM_LOGIN' => $admin['ADM_LOGIN'],
				'__ADM_AGENCY' => $admin['ADM_AGENCY'],
				'__ADM_MAIL' => $admin['ADM_MAIL'],
				'__ADM_SEND_MAIL' => $admin['ADM_SEND_MAIL'],
				'__ADM_ROLE' => $admin['ADM_ROLE'],
				'__POSITIONS' =>  $this->getPositions($admin['ADM_ID']),
			);
			
			return Template::parse($list, file_get_contents($this->path.'editAdmin.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSave() {	
		$this->db->queryString(__BP.'administrator');
		$update_data = array(
			'ADM_NAME'=>$this->var->ADM_NAME,
			'ADM_SURNAME'=>$this->var->ADM_SURNAME,
			'ADM_MAIL'=>$this->var->ADM_MAIL,
			'ADM_SEND_MAIL'=>$this->var->ADM_SEND_MAIL,
			'ADM_AGENCY'=>$this->var->ADM_AGENCY,
			'ADM_LOGIN'=>$this->var->ADM_LOGIN,
			'ADM_ROLE'=>$this->var->ADM_ROLE,
		);
		if(!empty($this->var->ADM_PASS))
			$update_data['ADM_PASS'] = coder::hashString($this->var->ADM_PASS);
			
		$this->db->UpdateQuery($update_data, 'ADM_ID', $this->var->ADM_ID);
		
		header('location: admin.php?action=administrator');
	}
	
	public function editSaveRoles() {
		//delete prevoius data
		$this->db->queryString('delete from '.__BP.'admin_roles where ADR_ADM_ID='.$this->var->ADM_ID);
		$this->db->execQuery();
		
		//insert new roles
		foreach($this->var->role as $role=>$value) {
			$r = explode(',',$role);
			$insert = array('ADR_ADM_ID'=>$this->var->ADM_ID, 'ADR_NAME'=>$r[0], 'ADR_ROLE_ID'=>$r[1]);
			$this->db->queryString(__BP.'admin_roles');
			$this->db->insertQuery($insert);

			unset($insert);
		}
		
		header('location: admin.php?action=administrator');
	}
	
	private function getPositions($id=null) {
		if($id) {
			$this->db->queryString('select ADR_ROLE_ID from '.__BP.'admin_roles where ADR_ADM_Id='.$id);
			$roles = $this->db->getDataFetch();
			
			if($roles) {
				foreach($roles as $role)
					$role_list[] = $role['ADR_ROLE_ID'];
			} else $role_list = array();
		} else $role_list = array();
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
	
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if(isset($menu)) {
		    	$list = array(
							'__CH_NAME' => "role[".$menu['title'][0].','.$menu['action'][0]."]",
							'__CH_CHECKED' => (in_array($menu['action'][0],$role_list)) ? 'checked' : '',
							'__CH_TEXT' => $menu['title'][0],
						);
						
					$result.= Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					unset($list);
		    }
		  }
		}

		return $result;
	}
	
	//widget for dasboard
	public function dashboard() {
		$list = array(
			'__MODULE_TITLE'=>'Konta administratorów',
			'__MODULE_ICON'=>'fa fa-user',
			'__MODULE_CONTENT'=>"<a href='?action=administrator'>Lista</a>",
		);
		return Template::parse($list, file_get_contents('modules/dashboard/templates/element.tpl.php'));
	}
}

function administrator($option=null) {
	$admin = new Administrator;
	if($option!=null and method_exists($admin, $option))
		return $admin->$option();
	else 
		return $admin->getAdminList();
}
?>