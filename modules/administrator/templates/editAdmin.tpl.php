<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=administrator&option=getAdminList">Administratorzy</a></li>
			<li><a href="#">Edycja</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Edycja konta</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Dane administratora</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-3 control-label">Imię</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="ADM_NAME"  value="__ADM_NAME" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwisko</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="ADM_SURNAME"  value="__ADM_SURNAME" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Email</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="ADM_MAIL" value="__ADM_MAIL" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Login</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="ADM_LOGIN" value="__ADM_LOGIN" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Hasło</label>
					<div class="col-sm-5">
						<input type="password" class="form-control" name="ADM_PASS" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Uprawnienia</label>
					<div class="col-sm-5">
						<select class="form-control" name="ADM_ROLE" id="ADM_ROLE">
							<option value='user'>user</option>
							<option value='admin'>admin</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Agencja</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="ADM_AGENCY" value="__ADM_AGENCY" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Powiadomienia</label>
					<div class="col-sm-5">
						<select name="ADM_SEND_MAIL" id="ADM_SEND_MAIL">
							<option value="tak">tak</option>
							<option value="nie">nie</option>
						</select>
					</div>
				</div>
				
				<input type="hidden" name="action" value="administrator">
				<input type="hidden" name="option" value="editSave">
				<input type="hidden" name="ADM_ID" value="__ADM_ID">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
				<h4 class="page-header">Uprawnienia</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
				__POSITIONS
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				<input type="hidden" name="action" value="administrator">
				<input type="hidden" name="option" value="editSaveRoles">
				<input type="hidden" name="ADM_ID" value="__ADM_ID">
				</form>
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
	$('#ADM_ROLE').val('__ADM_ROLE');
	$('#ADM_SEND_MAIL').val('__ADM_SEND_MAIL');
});
</script>