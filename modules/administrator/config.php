<?php
$module_info['title'] = 'Administratorzy';
$module_info['description'] = 'Zarządzanie kontami administratorów';
$module_info['author'] = 'lukawar';

$menu['title'][] = 'Konta administratorów';
$menu['action'][] = 'administrator';
$menu['icon'][0] = 'fa fa-user';
$menu['order'][] = '4';

$dashboard = true;
?>