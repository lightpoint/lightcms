<?php
class Logout extends View {
	public function userLogout() {
		$_SESSION = array();
		unset($_SESSION);
		header('location: index.php');
	}
}

function logout($option=null) {
	$logout = new Logout;
	if($option!=null and method_exists($logout, $option))
		return $logout->$option();
	else 
		return $logout->userLogout();
}