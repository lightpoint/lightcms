<?php
$module_info['title'] = 'Galeria';
$module_info['description'] = 'Galerie - tworzenie i zarządzanie';
$module_info['author'] = 'lightpoint';

$menu['title'][] = 'Galeria';
$menu['action'][] = 'gallery';
$menu['icon'][] = 'fa fa-th';

$menu_show['menu'] = true;