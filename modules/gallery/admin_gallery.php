<?php
class Gallery extends View {
	public $path = 'modules/gallery/templates/'; //path to templates for admin
	public $artPath = 'files/gallery/'; //path to gallery file
	
	public function getList() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Alias', 'Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
			$cat_select = "<option value='all'> - pokaż wszystkie - </option>";
			
			$this->db->queryString('select * from '.__BP.'gallery_categories');
			$categories = $this->db->getDataFetch();
					
			foreach($categories as $category) {
				$cat_select.="<option value='".$category['GCA_ID']."'>".$category['GCA_NAME']."</option>";
				$table->addCell($i, 'lp');
				$table->addCell($category['GCA_NAME']);
				$table->addCell($category['GCA_ALIAS']);
				$table->addCell($category['GCA_STATUS']);
				$table->addLink('?action=gallery&option=editCategory&id='.$category['GCA_ID'], 'edycja');
				$table->addRow();
				$i++;
			}
			$tableCategory = $table->createBody();
			
			$tableGallery = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Kategoria', 'Obraz','Status', '&nbsp;&nbsp;','');
			$tableGallery->createHeader($tableHeader);
			
			$i = 1;
			
			if(isset($this->var->cat) and $this->var->cat!='all')
				$cat = ' where GAL_GCA_ID='.$this->var->cat;
			
			$this->db->queryString('select *, GCA_NAME from '.__BP.'gallery left join '.__BP.'gallery_categories on GAL_GCA_ID=GCA_ID'.$cat.' order by GCA_NAME, GAL_ORDER');
			$galleries = $this->db->getDataFetch();
			
			foreach($galleries as $gal) {
				if($i>1) 
					$galup = '<a href="?action=gallery&option=galup&id='.$gal['GAL_ID'].'&ord='.$gal['GAL_ORDER'].'&category='.$gal['GAL_GCA_ID'].'&cat='.$this->var->cat.'"><span><i class="fa fa-chevron-circle-up"></i></span></a>';
				else $galup = '&nbsp;&nbsp;&nbsp;';
				if($i<$this->db->getCount())
					$galdown = '&nbsp;&nbsp;<a href="?action=gallery&option=galdown&id='.$gal['GAL_ID'].'&ord='.$gal['GAL_ORDER'].'&category='.$gal['GAL_GCA_ID'].'&cat='.$this->var->cat.'"><span><i class="fa fa-chevron-circle-down"></i></span></a>';
				else $galdown = '';
					
				$tableGallery->addCell($i, 'lp');
				$tableGallery->addCell($gal['GAL_NAME']);
				$tableGallery->addCell($gal['GCA_NAME']);
				$tableGallery->addLink(GALLERY_FILES_FULL.$gal['GAL_FILE'], $gal['GAL_FILE'],'light');
				$tableGallery->addCell($gal['GAL_STATUS']);
				$tableGallery->addCell($galup.' '.$galdown, 'editCol');
				$tableGallery->addLink('?action=gallery&option=editGallery&id='.$gal['GAL_ID'], 'edycja');
				$tableGallery->addRow();
				$i++;
				
			}
			$tableGallery = $tableGallery->createBody();
			
			
			$list = array(
				'__TABLE_CATEGORY'=>$tableCategory,
				'__TABLE_GALLERY'=>$tableGallery,
				'__SELECT_CATEGORY'=>$cat_select,
				'__SELECT_VALUE'=>isset($this->var->cat)?$this->var->cat:'all',
			);

			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			return file_get_contents($this->path.'addCategory.tpl.php');
		} else return $this->goAway();
	}
	
	public function delGallery() {
		$this->db->queryString('update '.__BP.'gallery set GAL_STATUS=2 where GAL_ID='.$this->var->id);
		$this->db->execQuery();
		
		header('location: admin.php?action=gallery&option='.$this->var->return.'&cat='.$this->var->cat);
	}
	
	public function addSaveCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'gallery_categories');
			
			$cat_insert = array(
				'GCA_NAME' => $this->var->GCA_NAME,
				'GCA_ALIAS' => coder::stripPL($this->var->GCA_NAME),
				'GCA_DESCRIPTION' => $this->var->GCA_DESCRIPTION,
				'GCA_STATUS' => $this->var->GCA_STATUS,
			);
			$this->db->insertQuery($cat_insert);
			
			header('location: admin.php?action=gallery');
		} else return $this->goAway();
	}
	
	public function editCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'gallery_categories where GCA_ID='.$this->var->id);
			$cat = $this->db->getRow();
			
			$list = array(
				'__GCA_NAME' => $cat['GCA_NAME'],
				'__GCA_ALIAS' => $cat['GCA_ALIAS'],
				'__GCA_ID' => $cat['GCA_ID'],
				'__GCA_DESCRIPTION' => $cat['GCA_DESCRIPTION'],
				'__GCA_STATUS' => $this->getCategoryStatus($cat['GCA_STATUS']),
			);
			
			return Template::parse($list, file_get_contents($this->path.'editCategory.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSaveCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'gallery_categories');
			
			$cat_update = array(
				'GCA_NAME' => $this->var->GCA_NAME,
				'GCA_ALIAS' => $this->var->GCA_ALIAS,
				'GCA_DESCRIPTION' => $this->var->GCA_DESCRIPTION,
				'GCA_STATUS' => $this->var->GCA_STATUS,
			);
			$this->db->updateQuery($cat_update,'GCA_ID', $this->var->id);
			
			header('location: admin.php?action=gallery');
		} else return $this->goAway();
	}
	
	public function addGallery() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array('__CATEGORY'=>$this->getCategoriesSelect());
			return Template::parse($list, file_get_contents($this->path.'addGallery.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addSaveGallery() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->image_name) {
				$this->db->queryString('select max(GAL_ORDER) as max_order from '.__BP.'gallery');
				$order = $this->db->getRow();
				
				$this->db->queryString(__BP.'gallery');
				
				$cat_insert = array(
					'GAL_NAME' => $this->var->GAL_NAME,
					'GAL_DESCRIPTION' => $this->var->GAL_DESCRIPTION,
					'GAL_GCA_ID' => $this->var->category,
					'GAL_FILE' => $this->var->image_name,
					'GAL_ORDER' => $order['max_order']+1,
				);
				
				$this->db->insertQuery($cat_insert);
				
				header('location: admin.php?action=gallery');
			}
		} else return $this->goAway();
	}
	
	public function editGallery() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'gallery where GAL_ID='.$this->var->id);
			$gal = $this->db->getRow();
			
			$list = array(
				'__GAL_NAME'=>$gal['GAL_NAME'],
				'__GAL_DESCRIPTION'=>$gal['GAL_DESCRIPTION'],
				'__GAL_FILE'=>GALLERY_FILES_THUMB_2.$gal['GAL_FILE'],
				'__GAL_ID'=>$gal['GAL_ID'],
				'__CATEGORY'=>$this->getCategoriesSelect($gal['GAL_GCA_ID']),
				'__GAL_STATUS'=>$this->getGalleryStatus($gal['GAL_STATUS'])
			);
			
			return Template::parse($list, file_get_contents($this->path.'editGallery.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSaveGallery() {
		$cat_data = new Connector(__BP.'gallery');
		$image_name = '';
		
		$cat_update = array(
			'GAL_NAME'=>$this->var->GAL_NAME,
			'GAL_STATUS'=>$this->var->GAL_STATUS,
			'GAL_DESCRIPTION'=>$this->var->GAL_DESCRIPTION,
			'GAL_GCA_ID'=>$this->var->category);
		
		if(isset($this->var->image_name)) {
			$cat_update['GAL_FILE'] = $this->var->image_name;
		}
			
		$cat_data->updateQuery($cat_update, 'GAL_ID', $this->var->id);
		
		header('location: admin.php?action=gallery');
	}
	
	private function getCategoriesSelect($selected=null) {
		$line = "<select name='category'><option value='0'> - brak - </option>/r";
		$cat_data = new Connector('select * from '.__BP.'gallery_categories');
		$categories = $cat_data->getData();
		
		foreach($categories as $cat) {
			if($cat['GCA_ID']==$selected)
				$selected_str = " selected";
			else $selected_str = '';
			
			$line.= "<option value='".$cat['GCA_ID']."'".$selected_str.">".$cat['GCA_NAME']."</option>/r";
		}
		$line.="</select>";
		
		return $line;
	}
	
	public function galup() {
		$this->db->queryString("SELECT GAL_ORDER as order_max, GAL_ID as order_id FROM ".__BP."gallery WHERE GAL_ORDER<".$this->var->ord." and GAL_GCA_ID=".$this->var->category." order by GAL_ORDER DESC");
		$max = $this->db->getRow();
		
		$this->db->queryString("update ".__BP."gallery set GAL_ORDER=".$this->var->ord." where GAL_ID=".$max['order_id']);
		$this->db->ExecQuery();
		
		$this->db->queryString("update ".__BP."gallery set GAL_ORDER=".$max['order_max']." where GAL_ID=".$this->var->id);
		$this->db->ExecQuery();
		
		header('location: admin.php?action=gallery&cat='.$this->var->cat);
	}
	
	public function galdown() {
		$this->db->queryString("SELECT GAL_ORDER as order_max, GAL_ID FROM ".__BP."gallery WHERE GAL_ORDER>".$this->var->ord." and GAL_GCA_ID=".$this->var->category." order by GAL_ORDER ASC");
		$max = $this->db->getRow();
		
		$this->db->queryString("update ".__BP."gallery set GAL_ORDER=".$max['order_max']." where GAL_ID=".$this->var->id);
		$this->db->ExecQuery();
		
		$this->db->queryString("update ".__BP."gallery set GAL_ORDER=".$this->var->ord." where GAL_ID=".$max['GAL_ID']);
		$this->db->ExecQuery();
		
		header('location: admin.php?action=gallery&cat='.$this->var->cat);
	}
	
	private function getGalleryTile($category=null) {
		if($category) 
			$this->db->queryString('select *, GCA_NAME, GCA_ID from '.__BP.'gallery left join '.__BP.'gallery_categories on GAL_GCA_ID=GCA_ID where GAL_GCA_ID='.$category.' and GCA_STATUS=1 and GAL_STATUS=1 order by GAL_ORDER');
		else $this->db->queryString('select *, GCA_NAME, GCA_ID from '.__BP.'gallery left join '.__BP.'gallery_categories on GAL_GCA_ID=GCA_ID where GAL_STATUS=1 order by GCA_NAME, GAL_ORDER');
			
		$galleries = $this->db->getDataFetch();
		
		$i = 1;
		
		foreach($galleries as $gal) {
			$line.="<div class='tileElement'><div class='tileImage'><img src='".GALLERY_FILES_THUMB_1.$gal['GAL_FILE']."' width='250'></div><div class='tileDescr'><b>".$gal['GAL_NAME']."</b><br/>".$gal['GCA_NAME']."</div><div class='tileControls'><a href='?action=gallery&option=editGallery&id=".$gal['GAL_ID']."'>edycja</a> | <a href=\"javascript:dialog_del('?action=gallery&option=delGallery&id=".$gal['GAL_ID']."&return=viewTiles&cat=".$category."');\">usuń</a></div></div>";
						
			$i++;
		}
		return $line;
	}
	
	public function viewTiles() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array('__GALLERY_TILES' => $this->getGalleryTile($this->var->cat), '__SELECT_CATEGORY' => $this->getCategorySelect($this->var->cat));

			return Template::parse($list, file_get_contents($this->path.'galleryTiles.tpl.php'));
		} else return $this->goAway();
	}
	
	private function getCategoryStatus($id=null) {
		$status = array('aktywne', 'nieaktywne');
		$select = new select('GCA_STATUS', 'GCA_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();
	}
	
	private function getGalleryStatus($id=null) {
		$status = array('aktywny', 'nieaktywny');
		$select = new select('GAL_STATUS', 'GAL_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();
	}
	
	private function getCategorySelect($id=null) {
		$this->db->queryString('select GCA_ID, GCA_NAME from '.__BP.'gallery_categories where GCA_STATUS in (1)');
		$categories = $this->db->getDataFetch();
		$select = new select('GCA_ID', 'GCA_ID', null, null, ' onChange="catFilter()"');
		$select->addNode(' - pokaż wszystkie - ', 'all');
		foreach($categories as $cat) {
			if($id==$cat['GCA_ID'])
				$select->addNode($cat['GCA_NAME'],$cat['GCA_ID'], 'selected');
			else $select->addNode($cat['GCA_NAME'],$cat['GCA_ID']);
		}
		return $select->create();
	}
	
	public function imageLoad() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array(
				'__GAL_M_X' => __GAL_M_X+10,
				'__GAL_M_Y' => __GAL_M_Y+10,
				//'__GAL_ID' => $this->var->id,
			);
			return Template::parse($list, file_get_contents($this->path.'imageLoad.tpl.php'));
		} else return $this->goAway();
	}
	
	public function saveImageLoad() {
		/*$encodedData = str_replace(' ','+',$this->var->image);
  	$decodedData = base64_decode($encodedData);*/
  	
  	//$jpegimage = imagecreatefromjpeg(base64_encode($this->var->image)); 
  	//file_put_contents('files/gallery/test.jpg', $jpegimage);

		list($type, $this->var->image) = explode(';', $this->var->image);
		list(,$this->var->image)      = explode(',', $this->var->image);
		$data = base64_decode($this->var->image);
		
		list($type, $this->var->imageO) = explode(';', $this->var->imageO);
		list(,$this->var->imageO)      = explode(',', $this->var->imageO);
		$dataO = base64_decode($this->var->imageO);
  	
  	if($data)	{
  		$fileName = md5(microtime()).'.jpg';
  		
			//cropped image
  		file_put_contents(GALLERY_FILES_THUMB_1.$fileName, $data);
  		file_put_contents(GALLERY_FILES_FULL.$fileName, $dataO);
  		
  		//resize full image
  		$thumb = new Imagick();
			$thumb->readImage(GALLERY_FILES_FULL.$fileName);    
			$thumb->resizeImage(__GAL_B_X, __GAL_B_Y, Imagick::FILTER_LANCZOS, 1, TRUE);
			$thumb->writeImage(GALLERY_FILES_FULL.$fileName);
			$thumb->clear();
			$thumb->destroy(); 
			
			//resize second thumb
			$thumb = new Imagick();
			$thumb->readImage(GALLERY_FILES_THUMB_1.$fileName);    
			$thumb->resizeImage(__GAL_S_X, __GAL_S_Y, Imagick::FILTER_LANCZOS, 1, TRUE);
			$thumb->writeImage(GALLERY_FILES_THUMB_2.$fileName);
			$thumb->clear();
			$thumb->destroy(); 
			
			$_SESSION['image_name'] = $fileName;
  	}
  	return "<div class='galleryElement'><img src='".GALLERY_FILES_THUMB_2.$fileName."'></div><input type='hidden' name='file_name' value='".$fileName."'>";
	}
	
	public function getForMenu() {
		$menu_module = $this->var->menu_module;
		$menu_option = $this->var->menu_option;
		
		if($menu_module=='gallery')
				$selected = " checked";
			else $selected = '';
		$line="<input type='radio' name='menu' value='gallery:none:1:Galeria:gallery'".$selected."> - galeria<br/>";
		
		return $line;
	}
}

function gallery($option=null) {
	$gallery = new Gallery;
	if($option!=null and method_exists($gallery, $option))
		return $gallery->$option();
	else 
		return $gallery->getList();
}
?>