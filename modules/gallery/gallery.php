<?php
class Gallery extends View {
	public $path = 'files/gallery/'; //path to articles


	public function getList() {
		include "config.php";

		foreach($articlesList as $name=>$file) {
			$line.= "<div><a href='?action=articles&option=".$file."'>".$name."</a></div>";
		}

		return "<div id='menu'>$line</div>";
	}
	
	public function getGallery() {
		return file_get_contents('files/articles/gallery.php');
	}
	
	public function getCategory($cat) {
		$this->db->queryString('select *, GCA_NAME from '.__BP."gallery left join ".__BP."gallery_categories on GAL_GCA_ID=GCA_ID where GCA_ALIAS='$cat' and GAL_STATUS=1 order by GAL_ORDER");
		$galleries = $this->db->getDataFetch();
		
		foreach($galleries as $gal) {
			$title = $gal['GCA_NAME'];
			$line.= "<div class='col-sm-6 col-md-4 col-lg-4 grid'><a href='".$this->path."/full/".$gal['GAL_FILE']."' class='fancybox' rel='".$gal['GCA_NAME']."' title='<b>".$gal['GAL_NAME']."</b><br/>".$gal['GAL_DESCR']."'><img src='".$this->path."/thumb_1/".$gal['GAL_FILE']."'></a></div>";
		}
		
		return "<h3>".$title."</h3>".$line.'<script>$("#menu_gallery").addClass("active");</script>';
	}
	
	public function getAllGallery() {
		$this->db->queryString('select GCA_NAME, GCA_ID from '.__BP.'gallery_categories where GCA_STATUS=1');
		$categories = $this->db->getDataFetch();
		
		foreach($categories as $cat) {
			$this->db->queryString('select * from '.__BP.'gallery where GAL_STATUS=1 and GAL_GCA_ID='.$cat['GCA_ID']);
			
			$galleries = $this->db->getDataFetch();
			if($galleries) {
				$line.="<h3>".$cat['GCA_NAME']."</h3>";
				foreach($galleries as $gal)
					$line.= "<div class='col-sm-6 col-md-4 col-lg-3 view'><a href='".$this->path.'/full/'.$gal['GAL_FILE']."' class='thumbnail fancybox' rel='".$cat['GCA_NAME']."' title='".$gal['GAL_NAME']."'><img src='".$this->path."/thumb_1/".$gal['GAL_FILE']."'></a></div>";
			}
		}
		return $line;
	}

  public function getMenu() {
    $this->db->queryString('select * from '.__BP.'gallery_categories where GCA_STATUS=1');
		
    $gallery = $this->db->getDataFetch();
    $i = 1;

    foreach($gallery as $gal) {
      $line.="<div class='galleryElementContainer  el_".$i."'><a href='galeria.php?cat=".$gal['GCA_ALIAS']."' class='galleryElement'>".$gal['GCA_NAME']."</a></div>";
    }

    return $line;
  }	
  
  public function gallerySide() {
		$this->db->queryString("select GAL_NAME, GAL_FILE from ".__BP."gallery where GAL_STATUS=1 order by rand() limit 3");
		$gallery = $this->db->getDataFetch();
		$line = "<div class='sideHeader'><div class='red_square_big'></div><h2>REALIZACJE</h2></div>";
		if($gallery) {
			foreach($gallery as $gal) {
				$line.= "<div class='side_gal_element'><a href='?action=gallery'><img src=".$this->path.'thumbs/'.$gal['GAL_FILE']."><p>".$gal['GAL_NAME']."</p></div>";
					
			}
		}
		return $line;
	}
}

function gallery($name=null) {
	$gallery = new Gallery;
	if($name!=null)
		return $gallery->getCategory($name);
	else
		return $gallery->getGallery();
}