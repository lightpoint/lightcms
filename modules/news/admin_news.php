<?php
class News extends View {
	public $path = 'modules/news/templates/'; //path to templates for admin
	
	public function getList() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Alias', 'Data dodania', 'Data wyświetlana', 'Data startu', 'Data zakończenia', 'Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
			
			$this->db->queryString('select * from '.__BP.'news order by NEW_DATE_DISPLAY');
			$news = $this->db->getDataFetch();
					
			foreach($news as $n) {
				$table->addCell($i, 'lp');
				$table->addCell($n['NEW_NAME']);
				$table->addCell($n['NEW_ALIAS']);
				$table->addCell($n['NEW_DATE_ADD']);
				$table->addCell($n['NEW_DATE_DISPLAY']);
				$table->addCell($n['NEW_DATE_START']);
				$table->addCell($n['NEW_DATE_END']);
				$table->addCell($n['NEW_STATUS']);
				$table->addLink('admin.php?action=news&option=edit&id='.$n['NEW_ID'], 'edycja',null, null, 'edycja');
				$table->addRow();
				$i++;
				
			}
			$tableCategory = $table->createBody();		
			
			$list = array(
				'__TABLE'=>$tableCategory
			);
			
			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addNews() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			return file_get_contents($this->path.'add.tpl.php');
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$insert = array(
				'NEW_NAME' => $this->var->NEW_NAME,
				'NEW_ALIAS' => coder::stripPL($this->var->NEW_NAME),
				'NEW_DESC_LONG' => $this->var->NEW_DESC_LONG,
				'NEW_DESC_SHORT' => $this->var->NEW_DESC_SHORT,
				'NEW_DATE_DISPLAY' => $this->var->NEW_DATE_DISPLAY,
				'NEW_DATE_START' => $this->var->NEW_DATE_START,
				'NEW_DATE_END' => $this->var->NEW_DATE_END,
				'NEW_STATUS' => $this->var->NEW_STATUS,
			);
			
			$this->db->queryString(__BP.'news');
			$this->db->insertQuery($insert);
			
			header('location: admin.php?action=news');
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'news where NEW_ID='.$this->var->id);
			$news = $this->db->getRow();
			
			$list = array(
				'__NEW_ID' => $news['NEW_ID'],
				'__NEW_NAME' => $news['NEW_NAME'],
				'__NEW_ALIAS' => $news['NEW_ALIAS'],
				'__NEW_START_DATE' => $news['NEW_DATE_START'],
				'__NEW_DISPLAY_DATE' => $news['NEW_DATE_DISPLAY'],
				'__NEW_END_DATE' => $news['NEW_DATE_END'],
				'__NEW_DESC_SHORT' => $news['NEW_DESC_SHORT'],
				'__NEW_DESC_LONG' => $news['NEW_DESC_LONG'],
				'__NEW_STATUS' => $this->getStatus($news['NEW_STATUS'])
			);
			return Template::parse($list, file_get_contents($this->path.'edit.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$update = array(
				'NEW_NAME' => $this->var->NEW_NAME,
				'NEW_ALIAS' => $this->var->NEW_ALIAS,
				'NEW_DESC_LONG' => $this->var->NEW_DESC_LONG,
				'NEW_DESC_SHORT' => $this->var->NEW_DESC_SHORT,
				'NEW_DATE_DISPLAY' => $this->var->NEW_DATE_DISPLAY,
				'NEW_DATE_START' => $this->var->NEW_DATE_START,
				'NEW_DATE_END' => $this->var->NEW_DATE_END,
				'NEW_STATUS' => $this->var->NEW_STATUS,
			);
			
			$this->db->queryString(__BP.'news');
			$this->db->updateQuery($update, 'NEW_ID', $this->var->id);
			
			header('location: admin.php?action=news');
		} else return $this->goAway();
	}
	
	private function getStatus($id=null) {
		$status = array('nowy', 'aktywny', 'nieaktywny');
		$select = new select('NEW_STATUS', 'NEW_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();		
	}
}

function news($option=null) {
	$news = new News;
	if($option!=null and method_exists($news, $option))
		return $news->$option();
	else 
		return $news->getList();
}