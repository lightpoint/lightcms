<?php
class News extends View {
	public $path = 'modules/news/templates/'; //path to templates for admin
	public $artPath = 'files/news/'; //path to articles
	public $backgroundDefault = 'files/news/default.jpg';
	public $backgroundDefaultFront = 'files/news/default_front.jpg';
	
	public function getListMobile() {
		$news_data = new Parser('files/news.xml');
		if(isset($this->var->id)) {
			if($this->var->id=='all') {
				$news_d = $news_data->getDataReverse();
				$i = $news_data->getCount();
				
				foreach($news_d as $news) {
					$descr = substr(strip_tags($news->descr),0 , 350);
					$line.= "<p><strong>".$news->date."</strong><br/>".$descr." <a href='?action=news&option=getNews&id=".$i."'><img src='img/dalej.jpg' width='20' height='20'/></a><br/></p>";
					$i--; 
				}
				return $line;
			} else {
				$news = $news_data->getRow($this->var->id-1);
				return "<p class='promNag'><strong>".$news->name."</strong> (".$news->date.")</p><p>".$news->descr."</p><p>&nbsp</p><p>&nbsp</p><p><a href='?action=news&option=getListMobile' class='nostyle'>&nbsp;&nbsp;zobacz wszyskie aktualności <img src='img/dalej.jpg' width='20' height='20'  align='left'/></a></p>";
			}
		} else {
			$news_c = $news_data->getCount();
			if($news_c>1)
				$news_d = $news_data->getDataReverse();
			else $news_d = $news_data->getData();
			
			$i = 1;
			
			foreach($news_d as $news) {
				$descr = substr(strip_tags($news->descr),0 , 140);
				$line.= "<p><strong>".$news->date."</strong><br/>".$descr." <a href='?action=news&option=getNews&id=".$news_c."'><img src='img/dalej.jpg' width='20' height='20'/></a><br/></p>";
				$i++; 
				$news_c--;
				//if($i>3) break;
			}
			return $line;
		}
	}
	
	public function getList() {
		$this->db->queryString("select * from ".__BP."news order by NEW_DATE_DISPLAY desc");
		$news = $this->db->getDataFetch();
		
		$newData = null;
		$c = 1;
		
		foreach($news as $new) {
			if($new['NEW_BACKGROUND']!='')
				$background = $this->artPath.$new['NEW_BACKGROUND'];
			else $background = $this->backgroundDefaultFront;
			
			if($this->agent=='mobile') {
        $css = 'sb-right';
      } else {
        if(($c%2)==0)
  				$css = 'sb-right';
  			else $css = 'sb-left';
      }
			
			$list = array(
					'__DISPLAY_DATE'=>$new['NEW_DATE_DISPLAY'],
					'__ALIAS'=>$new['NEW_ALIAS'],
					'__TITLE'=>$new['NEW_NAME'],
					'__CONTENT'=>$new['NEW_DESC_LONG'],
					'__BACKGROUND'=>$background,
					'__CSS'=>$css,
				);
				
			$newData.= Template::parse($list, file_get_contents($this->path.'all.tpl.php'));
			$c++;
		}
		
		return $newData;
	}
	
	public function getLast() {
		$this->db->queryString("select * from ".__BP."news order by NEW_DATE_DISPLAY desc");
		$news = $this->db->getRow();
		
		if($news['NEW_BACKGROUND']!='')
			$background = $this->artPath.$news['NEW_BACKGROUND'];
		else $background = $this->backgroundDefaultFront;
		
		$list = array(
				'__DISPLAY_DATE'=>$news['NEW_DATE_DISPLAY'],
				'__ALIAS'=>helper::href(array('news')),
				'__TITLE'=>$news['NEW_NAME'],
				'__CONTENT'=>$news['NEW_DESC_SHORT'],
				'__BACKGROUND'=>$background
			);
			
			return Template::parse($list, file_get_contents($this->path.'last.tpl.php'));
	}
	
	public function getPosition() {
		
	}
	
	public function getArticle($name) {
		if(file_exists($this->artPath.$name))
			return file_get_contents($this->artPath.$name);
		else return $this->pageError();
	}
	
  public function lang() {
    return $this->getList();
  }
}

function news($name=null) {
	$news = new News;
	if($name!=null) {
		if(method_exists($news, $name))
			return $news->getModule($name);
		else
			return $news->getPosition($name);
	} else 
		return $news->getList();
}
?>