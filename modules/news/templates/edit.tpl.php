<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=news">Aktualności</a></li>
			<li><a href="#">Edycja wpisu</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-plus-square-o"></i>
					<span>Edycja wpisu</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Wpisz dane</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="NEW_NAME" required="required" value="__NEW_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Alias</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="NEW_ALIAS" required="required" value="__NEW_ALIAS"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Opis</label>
					<div class="col-sm-5">
						<textarea class="form-control"  name="NEW_DESC_LONG">__NEW_DESC_LONG</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Opis (skrócony)</label>
					<div class="col-sm-5">
						<textarea class="form-control"  name="NEW_DESC_SHORT">__NEW_DESC_SHORT</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Wyświetlana data</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date_time"  name="NEW_DATE_DISPLAY" value="__NEW_DISPLAY_DATE" required="required"/>
					</div>
				</div>
					
				<div class="form-group">
					<label class="col-sm-3 control-label">Data rozpoczęcia</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date_time"  name="NEW_DATE_START" value="__NEW_START_DATE" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Data zakończenia</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date_time"  name="NEW_DATE_END" value="__NEW_END_DATE"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Alternatywne tło</label>
					<div class="col-sm-5">
						<div class="toggle-switch toggle-switch-info">
							<label>
								<input type="checkbox" name="backgr" id="backgr">
								<div class="toggle-switch-inner"></div>
								<div class="toggle-switch-switch"><i class="fa fa-check"></i></div>
							</label>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-9">
						Dodaj tło: <input type="file" name="background_file">
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status</label>
					<div class="col-sm-5">
						__NEW_STATUS
					</div>
				</div>
				
				<input type="hidden" name="action" value="news">
				<input type="hidden" name="option" value="editSave">
				<input type="hidden" name="id" value="__NEW_ID">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(function($){
	$.datepicker.regional['pl'] = {
		closeText: 'Zamknij',
		prevText: '&laquo; Poprzedni',
		nextText: 'Następny &raquo;',
		currentText: 'Dziś',
		monthNames: ['Styczeń','Luty','Marzec','Kwiecieć','Maj','Czerwiec', 'Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
		monthNamesShort: ['Sty','Lu','Mar','Kw','Maj','Cze','Lip','Sie','Wrz','Pa','Lis','Gru'],
		dayNames: ['Niedziela','Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
		dayNamesShort: ['Nie','Pn','Wt','Śr','Czw','Pt','So'],
		dayNamesMin: ['N','Pn','Wt','Śr','Cz','Pt','So'],
		weekHeader: 'Tydz',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['pl']);
});

$(document).ready(function() {
	$('.date_time').datepicker({});
});
</script>