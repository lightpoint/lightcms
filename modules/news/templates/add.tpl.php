<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=news">Aktualności</a></li>
			<li><a href="#?action=news&option=add">Dodaj nowy wpis</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-plus-square-o"></i>
					<span>Dodaj nowy wpis</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<h4 class="page-header">Wpisz dane</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="NEW_NAME" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Opis</label>
					<div class="col-sm-5">
						<textarea class="form-control"  name="NEW_DESC_LONG"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Opis (skrócony)</label>
					<div class="col-sm-5">
						<textarea class="form-control"  name="NEW_DESC_SHORT"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Wyświetlana data</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date_time"  name="NEW_DATE_DISPLAY" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Data rozpoczęcia</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date_time"  name="NEW_DATE_START" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Data zakończenia</label>
					<div class="col-sm-5">
						<input type="text" class="form-control date_time"  name="NEW_DATE_END" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status</label>
					<div class="col-sm-5">
						<select name="NEW_STATUS">
							<option value="nowy">nowy</option>
							<option value="aktywny">aktywny</option>
							<option value="nieaktywny">nieaktywny</option>
						</select>
					</div>
				</div>
				
				<input type="hidden" name="action" value="news">
				<input type="hidden" name="option" value="addSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
jQuery(function($){
	$.datepicker.regional['pl'] = {
		closeText: 'Zamknij',
		prevText: '&laquo; Poprzedni',
		nextText: 'Następny &raquo;',
		currentText: 'Dziś',
		monthNames: ['Styczeń','Luty','Marzec','Kwiecieć','Maj','Czerwiec', 'Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
		monthNamesShort: ['Sty','Lu','Mar','Kw','Maj','Cze','Lip','Sie','Wrz','Pa','Lis','Gru'],
		dayNames: ['Niedziela','Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
		dayNamesShort: ['Nie','Pn','Wt','Śr','Czw','Pt','So'],
		dayNamesMin: ['N','Pn','Wt','Śr','Cz','Pt','So'],
		weekHeader: 'Tydz',
		dateFormat: 'yy-mm-dd',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['pl']);
});

$(document).ready(function() {
	$('.date_time').datepicker({});
});
</script>