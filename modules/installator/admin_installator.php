<?php
class Installator extends View{
	public $path = 'modules/installator/templates/';
	public $files_path = 'system/db_tables/';

	public function showOptions() {
		$tables =  $this->getTablesFromFile();
		$installed = $this->getTables();
		
		$list = array(
			'__BASE_HOST' => __HOST,
			'__BASE_NAME' => __BASE,
			'__BASE_USER' => __USER,
			'__BASE_PREFIX' => __BP,
			'__TABLES_AVAILABLE' => $this->getAvailable($tables, $installed),
			'__TABLES_INSTALLED' => $this->getInstalled($tables, $installed),
		);
		return Template::parse($list, file_get_contents($this->path.'show.tpl.php'));
	}
	
	private function getTables() {
		//$this->db->queryString("SHOW TABLES FROM ".__BASE." where Tables_in_".__BASE." like '".__BP."%'");
		$this->db->queryString("SELECT TABLE_NAME, TABLE_ROWS FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".__BASE."' and TABLE_NAME like '".__BP."%' ORDER BY `TABLES`.`TABLE_ROWS` DESC");
		$data = $this->db->getDataFetch();
		
		foreach($data as $d)
			$result[$d['TABLE_NAME']] = $d['TABLE_ROWS'];
		
		return $result;
	}
	
	private function getAvailable($tables, $installed) {
		foreach($tables as $table) {
			if(array_key_exists(__BP.$table, $installed))
				$class = ' class="installed"';
			else $class = null;
			$line.="<p".$class."><input type='checkbox' name='".$table."'> ".__BP.$table."</p>";
		}
		
		return $line;
	}
	
	private function getInstalled($tables, $installed) {
		foreach($installed as $key=>$value) {
			$line.="<p>&raquo; <strong>".$key."</strong> (".$value." rekordów)</p>";
		}
		
		return $line;
	}
	
	private function getTablesFromFile() {
		$handle = opendir($this->files_path);
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
	
		foreach($catalogs as $file) {
			unset($base);
			include($this->files_path.$file);
			$base_list[] = $base['name'];
		}
		
		/*ksort($result);
		foreach($result as $key=>$value)
			$line.=$value;*/
		return $base_list;
	}
	
	public function setTables() {
		//return $this->var->list[1];
		foreach($this->var->list as $pos) {
			if(file_exists($this->files_path.$pos.'.php')) {
				$lista.=$pos." / ";
				unset($base);
				include($this->files_path.$pos.'.php');
				$sql = str_replace('__BP', __BP, $base['body']);
				$this->db->queryString($sql);
				$this->db->execQuery();
				
			}
		}
		return $lista;
	}
}

function installator($option=null) {
	$install = new installator;
	if($option!=null and method_exists($install, $option))
		return $install->$option();
	else 
		return $install->showOptions();
}
?>