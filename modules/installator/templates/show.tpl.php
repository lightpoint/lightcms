<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=installator">Bazy i tabele</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Konfiguracja bazy</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">
					<p><label>Host:</label> __BASE_HOST</p>
					<p><label>Baza:</label> __BASE_NAME</p>
					<p><label>Użytkownik:</label> __BASE_USER</p>
					<p><label>Prefix:</label> __BASE_PREFIX</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Zainstalowane table</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">
					__TABLES_INSTALLED
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Dostępne table</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">
					__TABLES_AVAILABLE
					<div id="optionsBar"><button class="btn btn-primary btn-label-left" id="prompt"><span><i class="fa fa-plus"></i></span>załaduj</button></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
  $('#prompt').click(function() {
      var names = [];
      $('input:checked').each(function() {
          names.push(this.name);
      });
 			//console.log(names);
 			$.post("request.php", {action: "installator", option: "setTables", list:names})
 			.done(function(response) {
 				alert(response);
 			});
  });
});
</script>