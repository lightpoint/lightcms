<?php
class Przewodnik extends View {
	public $artPath = 'files/articles/'; //path to articles
	
	public function getData() {
		return file_get_contents($this->artPath.$this->var->id.'.php');
	}
	
}

function przewodnik_po_akcesoriach($option=null) {
	$przewodnik = new Pprzewodnik;
	if($option!=null and method_exists($przewodnik, $option))
		return $przewodnik->$option();
	else 
		return null;
}