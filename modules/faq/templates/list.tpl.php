<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=faq">FAQ</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-question"></i>
					<span>Kategorie</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<div id="optionsBar"><a href="?action=faq&option=addCategory" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nową kategorię</a></div>
				<div id="contentPlace">__TABLE_CATEGORY</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-question"></i>
					<span>Pozycje</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<div id="optionsBar">__SELECT_CATEGORY <button id="filterFCatButton" type="button" class="btn btn-success btn-label-left"><span><i class="fa fa-search"></i></span> Fitruj</button> &nbsp;&nbsp;&nbsp;&nbsp;<a href="?action=faq&option=add" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj pozycję</a></div>
				<div id="contentPlace">__TABLE_FAQ</div>
			</div>
		</div>
	</div>
</div>