<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=faq">FAQ</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-question"></i>
					<span>Dodaj nową pozycję</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<h4 class="page-header">Dodaj nową pozycję</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Pytanie</label>
					<div class="col-sm-9">
						<input type="text" class="form-control"  name="FAQ_Q" required="required"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Odpowiedź</label>
					<div class="col-sm-9">
						<textarea name="FAQ_A" id="article_content"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Kategoria</label>
					<div class="col-sm-5">
						__CATEGORY
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status </label>
					<div class="col-sm-5">
						<select name="FAQ_STATUS" id="FAQ_STATUS">
							<option value="1">aktywne</option>
							<option value="2">nieaktywne</option>
						</select>
					</div>
				</div>
				
				<input type="hidden" name="action" value="faq">
				<input type="hidden" name="option" value="addSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="button" id="cancel" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>