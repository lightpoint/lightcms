<?php
class FAQ extends View {
	public $path = 'modules/faq/templates/'; //path to templates for admin
	
	public function getData($alias) {
		if($alias!=null)
			$filter = ' and FCA_ALIAS="'.$alias.'"';
		else $filter = null;
		
		$this->db->queryString('select * from '.__BP.'faq_category  where FCA_STATUS=1'.$filter.' order by FCA_ORDER');
		$faqs = $this->db->getDataFetch();
		
		$template = file_get_contents($this->path.'faq.tpl.php');
		$wrap = file_get_contents($this->path.'wrap.tpl.php');
		
		foreach($faqs as $f) {
			$this->db->queryString('select * from '.__BP.'faq  where FAQ_FCA_ID='.$f['FCA_ID'].' order by FAQ_ORDER');
			$faq = $this->db->getDataFetch();
			$line = null;
			foreach($faq as $fa) {
				$line.= sprintf($template,$fa['FAQ_Q'],$fa['FAQ_A']);
			}
			
			$faq_line.= sprintf($wrap,$f['FCA_NAME'],$line);
		}
		return $faq_line;
	}

}

function faq($option=null) {
	$faq = new FAQ;
	/*if($option!=null and method_exists($faq, $option))
		return $faq->$option();
	else 
		return $faq->getList();*/
	
	return $faq->getData($option);
}