<?php
class FAQ extends View {
	public $path = 'modules/faq/templates/'; //path to templates for admin
	
	public function getList() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Alias', 'Wyświetlanie', 'Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
			
			$this->db->queryString('select * from '.__BP.'faq_category');
			$categories = $this->db->getDataFetch();
					
			foreach($categories as $category) {
				$table->addCell($i, 'lp');
				$table->addCell($category['FCA_NAME']);
				$table->addCell($category['FCA_ALIAS']);
				$table->addCell($category['FCA_TYPE']);
				$table->addCell($category['FCA_STATUS']);
				$table->addLink('?action=faq&option=editCategory&id='.$category['FCA_ID'], 'edycja');
				$table->addRow();
				$i++;
			}
			$tableCategory = $table->createBody();
			
			$tableF = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Kategoria', 'Status', '&nbsp;&nbsp;','');
			$tableF->createHeader($tableHeader);
			
			$i = 1;
			
			if($this->var->sFCatId>0) 
				$cat = ' where FAQ_FCA_ID='.$this->var->sFCatId;
			else $cat = null;
			
			$this->db->queryString('select *, FCA_NAME from '.__BP.'faq left join '.__BP.'faq_category on FAQ_FCA_ID=FCA_ID'.$cat.' order by FCA_NAME, FCA_ORDER');
			$faqs = $this->db->getDataFetch();
			
			foreach($faqs as $faq) {
				if($i>1) 
					$fup = '<a href="?action=faq&option=fup&id='.$faq['FAQ_ID'].'&ord='.$faq['FAQ_ORDER'].'&category='.$faq['FAQ_FCA_ID'].'&cat='.$this->var->cat.'"><span><i class="fa fa-chevron-circle-up"></i></span></a>';
				else $fup = '&nbsp;&nbsp;&nbsp;';
				if($i<$this->db->getCount())
					$fdown = '&nbsp;&nbsp;<a href="?action=faq&option=fdown&id='.$faq['FAQ_ID'].'&ord='.$faq['FAQ_ORDER'].'&category='.$faq['FAQ_FCA_ID'].'&cat='.$this->var->cat.'"><span><i class="fa fa-chevron-circle-down"></i></span></a>';
				else $galdown = '';
					
				$tableF->addCell($i, 'lp');
				$tableF->addCell($faq['FAQ_Q']);
				$tableF->addCell($faq['FCA_NAME']);
				$tableF->addCell($faq['FCA_STATUS']);
				$tableF->addCell($galup.' '.$galdown, 'editCol');
				$tableF->addLink('?action=faq&option=edit&id='.$faq['FAQ_ID'], 'edycja');
				$tableF->addRow();
				$i++;
				
			}
			$tableF = $tableF->createBody();
			
			
			$list = array(
				'__TABLE_CATEGORY'=>$tableCategory,
				'__TABLE_FAQ'=>$tableF,
				'__SELECT_CATEGORY'=> $this->createCategorySelect($this->var->sFCatId)
			);

			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			return file_get_contents($this->path.'addCategory.tpl.php');
		} else return $this->goAway();
	}
	
	public function delGallery() {
		$this->db->queryString('update '.__BP.'gallery set GAL_STATUS=2 where GAL_ID='.$this->var->id);
		$this->db->execQuery();
		
		header('location: admin.php?action=gallery&option='.$this->var->return.'&cat='.$this->var->cat);
	}
	
	public function addSaveCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'faq_category');
			
			$cat_insert = array(
				'FCA_NAME' => $this->var->FCA_NAME,
				'FCA_ALIAS' => coder::stripPL($this->var->FCA_NAME),
				'FCA_TYPE' => $this->var->FCA_TYPE,
				'FCA_STATUS' => $this->var->FCA_STATUS,
			);
			$this->db->insertQuery($cat_insert);
			
			header('location: admin.php?action=faq');
		} else return $this->goAway();
	}
	
	public function editCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'faq_category where FCA_ID='.$this->var->id);
			$cat = $this->db->getRow();
			
			$list = array(
				'__FCA_NAME' => $cat['FCA_NAME'],
				'__FCA_ALIAS' => $cat['FCA_ALIAS'],
				'__FCA_ID' => $cat['FCA_ID'],
				'__FCA_TYPE' => $this->getTypeStatus($cat['FCA_TYPE']),
				'__FCA_STATUS' => $this->getCategoryStatus($cat['FCA_STATUS']),
			);
			
			return Template::parse($list, file_get_contents($this->path.'editCategory.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSaveCategory() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'faq_category');
			
			$cat_update = array(
				'FCA_NAME' => $this->var->FCA_NAME,
				'FCA_ALIAS' => $this->var->FCA_ALIAS,
				'FCA_TYPE' => $this->var->FCA_TYPE,
				'FCA_STATUS' => $this->var->FCA_STATUS,
			);
			$this->db->updateQuery($cat_update,'FCA_ID', $this->var->id);
			
			header('location: admin.php?action=faq');
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array('__CATEGORY' => $this->createCategorySelect($this->var->sFCatId));
			
			return Template::parse($list, file_get_contents($this->path.'add.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'faq');
			
			$cat_insert = array(
				'FAQ_Q' => $this->var->FAQ_Q,
				'FAQ_A' => $this->var->FAQ_A,
				'FAQ_FCA_ID' => $this->var->FCA_ID,
				'FAQ_STATUS' => $this->var->FAQ_STATUS,
			);
			$this->db->insertQuery($cat_insert);
			
			header('location: admin.php?action=faq');
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'faq where FAQ_ID='.$this->var->id);
			$cat = $this->db->getRow();
			
			$list = array(
				'__FAQ_Q' => $cat['FAQ_Q'],
				'__FAQ_A' => $cat['FAQ_A'],
				'__FAQ_ID' => $cat['FAQ_ID'],
				'__CATEGORY' => $this->createCategorySelect($cat['FAQ_FCA_ID']),
				'__STATUS' => $this->getCategoryStatus($cat['FAQ_STATUS']),
			);
			
			return Template::parse($list, file_get_contents($this->path.'edit.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'faq');
			
			$cat_update = array(
				'FAQ_Q' => $this->var->FAQ_Q,
				'FAQ_A' => $this->var->FAQ_A,
				'FAQ_FCA_ID' => $this->var->FCA_ID,
				'FAQ_STATUS' => $this->var->FAQ_STATUS,
			);
			$this->db->updateQuery($cat_update,'FAQ_ID', $this->var->id);
			//$this->db->query();
			header('location: admin.php?action=faq');
		} else return $this->goAway();
	}
	
	public function filter() {
		$_SESSION['sFCatId'] = $this->var->sFCatId;
		header('location: admin.php?action=faq');
	}
	
	private function createCategorySelect($id) {
		$this->db->queryString('select FCA_ID, FCA_NAME from '.__BP.'faq_category where FCA_STATUS=1 order by FCA_ORDER');
		$categories = $this->db->getData();
		
		$line = "<option value='0'> - brak - </option>";
		foreach($categories as $cat) {
			if($cat['FCA_ID']==$id)
				$line.= "<option value='".$cat['FCA_ID']."' selected> - ".$cat['FCA_NAME']." - </option>";
			else $line.= "<option value='".$cat['FCA_ID']."'> - ".$cat['FCA_NAME']." - </option>";
		}
		
		return "<select name='FCA_ID' id='FCA_ID'>".$line.'</select>';
	}
	
	private function getCategoryStatus($id=null) {
		$status = array('active', 'deleted');
		$select = new select('FCA_STATUS', 'FCA_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();
	}
	
	private function getTypeStatus($id=null) {
		$status = array('faq', 'all');
		$select = new select('FCA_TYPE', 'FCA_TYPE');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();
	}
}

function faq($option=null) {
	$faq = new FAQ;
	if($option!=null and method_exists($faq, $option))
		return $faq->$option();
	else 
		return $faq->getList();
}