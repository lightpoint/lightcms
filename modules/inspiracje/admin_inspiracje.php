<?php
class inspiracje extends View {
	public $path = 'modules/inspiracje/templates/';
	
	public function getPage() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Dodał', 'Szkoła','Wiek', 'Data dodania', 'Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;

			
			$this->db->queryString('select * from '.__BP.'inspiracje order by INS_DATE_ADD');
			$inspirations = $this->db->getDataFetch();
			
			foreach($inspirations as $ins) {
				$table->addCell($i, 'lp');
				$table->addCell($ins['INS_NAME']);
				$table->addCell($ins['INS_CLI_ID']);
				$table->addCell($ins['INS_SCHOOL']);
				$table->addCell($ins['INS_GRADE']);
				$table->addCell($ins['INS_DATE_ADD']);
				$table->addCell($ins['INS_STATUS']);
				//$table->addLink('?action=articles&option=add&id='.$ins['INS_ID'], 'dodaj&nbsp;podstronę');
				$table->addLink('?action=inspiracje&option=edit&id='.$ins['INS_ID'], 'edycja');
				$table->addRow();
				$i++;
			}
			$tableBody = $table->createBody();
			
			$list = array(
				'__TABLE'	=>$tableBody,
			);
			
			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array();
			return Template::parse($list, file_get_contents($this->path.'add.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->INS_NAME) {
				$salt = md5(microtime());
				$this->db->queryString(__BP.'inspiracje');
				$alias = coder::stripPL($this->var->INS_NAME);
				$insert = array(
					'INS_NAME' => $this->var->INS_NAME,
					'INS_GRADE' => $this->var->INS_GRADE,
					'INS_SCHOOL' => $this->var->INS_SCHOOL,
					'INS_CLI_ID' => $this->var->admin_id,
					'INS_FILE' => $this->var->image_name,
					'INS_ALIAS' => $alias,
					'INS_CONTENT' => addslashes($this->var->INS_CONTENT),
					'INS_STATUS' => $this->var->INS_STATUS,
					'INS_SALT' => $salt,
				);
				$this->db->insertQuery($insert);
				
				//attachments
	    	foreach ($_FILES["attachment"]["error"] as $key => $error) {
	        if ($error == UPLOAD_ERR_OK) {
	          if(file_exists(INSPIRATION_ATTACHMENT_FILES.'/'.$salt.'/')) {
	            
	          } else {
	            if (!mkdir(INSPIRATION_ATTACHMENT_FILES.'/'.$salt.'/', 0777, true)) {
	              die('Failed to create folders...');
	            }
	          }
	          
	          $this->db->queryString(__BP.'inspiracje_files');
	          $tmp_name = $_FILES["attachment"]["tmp_name"][$key];
	          $name = $_FILES["attachment"]["name"][$key];
	          
	          
	          move_uploaded_file($tmp_name, INSPIRATION_ATTACHMENT_FILES.'/'.$salt.'/'."$name");
	          
	          $insert = array('IFI_INS_ID' => $l, 'IFI_FILE' => $name, 'IFI_INS_SALT' => $salt);
	          $this->db->insertQuery($insert);
	        }
	      }
				
				//save event 
				//eventSaver::add($this->var->admin_id, 'new article - '. $this->var->INS_ALIAS);
			}
			header('location: admin.php?action=inspiracje');
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {	
			$_SESSION['image_name'] = null;
			$this->db->queryString('select * from '.__BP.'inspiracje where INS_ID='.$this->var->id);
			$ins = $this->db->getRow();
			
			//files
      $this->db->queryString('select * from '.__BP.'inspiracje_files where IFI_INS_ID='.$this->var->id.' and IFI_STATUS in (1)');
      $files = $this->db->getDataFetch();
      foreach($files as $f) {
      	if($f['IFI_FILE_TYPE']=='file')
      		$files_data.= '<div><div class="col-sm-10">'.$f['IFI_FILE'].'</div><div class="col-sm-2"><a href="#" class="del-inspiration btn btn-danger btn-label-left" file="'.$f['IFI_ID'].'"><span><i class="fa fa-minus"></i></span>usuń</a></div></div>';
      	else $files_data.= '<div><div class="col-sm-10"><img src="'.INSPIRATION_ATTACHMENT_FILES.'/'.$f['IFI_INS_SALT'].'/'.$f['IFI_FILE'].'" width="50"></div><div class="col-sm-2"><a href="#" class="del-inspiration btn btn-danger btn-label-left" file="'.$f['IFI_ID'].'"><span><i class="fa fa-minus"></i></span>usuń</a></div></div>';
      }
			
			$list = array(
				'__INS_NAME' => $ins['INS_NAME'],
				'__INS_SALT' => $ins['INS_SALT'],
				'__INS_SCHOOL' => $ins['INS_SCHOOL'],
				'__INS_GRADE' => $ins['INS_GRADE'],
				'__INS_ALIAS' => $ins['INS_ALIAS'],
				'__INS_CONTENT' => stripslashes($ins['INS_CONTENT']),
				'__INS_ID' => $ins['INS_ID'],
				'__INS_FILE' => $ins['INS_FILE']==null ? null : '<img src="files/gallery/thumb_2/'.$ins['INS_FILE'].'">',
				'__INS_STATUS' => $this->getStatus($ins['INS_STATUS']),
				'__ATTACHMENTS' => $files_data,
			);
			
			return Template::parse($list, file_get_contents($this->path.'edit.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->INS_NAME) {
				$this->db->queryString(__BP.'inspiracje');
				$update = array(
					'INS_NAME' => $this->var->INS_NAME,
					'INS_SCHOOL' => $this->var->INS_SCHOOL,
					'INS_GRADE' => $this->var->INS_GRADE,
					'INS_ALIAS' => $this->var->INS_ALIAS,
					'INS_CONTENT' => addslashes($this->var->INS_CONTENT),
					'INS_STATUS' => $this->var->INS_STATUS,
				);
				
				if($this->var->image_name) {
					$update['INS_FILE'] = $this->var->image_name;
				}
				
				$this->db->updateQuery($update, 'INS_ID', $this->var->id);
				
				//attachments
	    	foreach ($_FILES["attachment"]["error"] as $key => $error) {
	        if ($error == UPLOAD_ERR_OK) {
	          if(file_exists(INSPIRATION_ATTACHMENT_FILES.'/'.$this->var->salt.'/')) {
	            
	          } else {
	            if (!mkdir(INSPIRATION_ATTACHMENT_FILES.'/'.$this->var->salt.'/', 0777, true)) {
	              die('Failed to create folders: '.INSPIRATION_ATTACHMENT_FILES.'/'.$this->var->salt.'/');
	            }
	          }
	          
	          $this->db->queryString(__BP.'inspiracje_files');
	          $tmp_name = $_FILES["attachment"]["tmp_name"][$key];
	          $name = $_FILES["attachment"]["name"][$key];
	          $fileType = $this->checkFileTypeMagic($tmp_name);
	          if($fileType[0]=='image') {
							
						} else if($fileType[0]=='file') {
	          
	          }
	          move_uploaded_file($tmp_name, INSPIRATION_ATTACHMENT_FILES.'/'.$this->var->salt.'/'."$name");
	          
	          $insert = array('IFI_INS_ID' => $this->var->id, 'IFI_FILE' => $name, 'IFI_INS_SALT' => $this->var->salt, 'IFI_FILE_TYPE' => $fileType[0], 'IFI_FILE_MIME' => $fileType[1]);
	         	$this->db->insertQuery($insert);
	        }
	      }
				
				//save event 
				//eventSaver::add($this->var->admin_id, 'new article - '. $this->var->INS_ALIAS);
			}
			header('location: admin.php?action=inspiracje');
		} else return $this->goAway();
	}
	
	private function getStatus($id=null) {
		$status = array('nowy', 'opublikowany', 'usunięty');
		$select = new select('INS_STATUS', 'INS_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();
	}
	
	public function imageLoad() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array(
				'__GAL_M_X' => __GAL_M_X+10,
				'__GAL_M_Y' => __GAL_M_Y+10,
				//'__GAL_ID' => $this->var->id,
			);
			return Template::parse($list, file_get_contents($this->path.'imageLoad.tpl.php'));
		} else return $this->goAway();
	}
	
	public function saveImageLoad() {
		/*$encodedData = str_replace(' ','+',$this->var->image);
  	$decodedData = base64_decode($encodedData);*/
  	
  	//$jpegimage = imagecreatefromjpeg(base64_encode($this->var->image)); 
  	//file_put_contents('files/gallery/test.jpg', $jpegimage);

		list($type, $this->var->image) = explode(';', $this->var->image);
		list(,$this->var->image)      = explode(',', $this->var->image);
		$data = base64_decode($this->var->image);
		
		list($type, $this->var->imageO) = explode(';', $this->var->imageO);
		list(,$this->var->imageO)      = explode(',', $this->var->imageO);
		$dataO = base64_decode($this->var->imageO);
  	
  	if($data)	{
  		$fileName = md5(microtime()).'.jpg';
  		
			//cropped image
  		file_put_contents(GALLERY_FILES_THUMB_1.$fileName, $data);
  		file_put_contents(GALLERY_FILES_FULL.$fileName, $dataO);
  		
  		//resize full image
  		$thumb = new Imagick();
			$thumb->readImage(GALLERY_FILES_FULL.$fileName);    
			$thumb->resizeImage(__GAL_B_X, __GAL_B_Y, Imagick::FILTER_LANCZOS, 1, TRUE);
			$thumb->writeImage(GALLERY_FILES_FULL.$fileName);
			$thumb->clear();
			$thumb->destroy(); 
			
			//resize second thumb
			$thumb = new Imagick();
			$thumb->readImage(GALLERY_FILES_THUMB_1.$fileName);    
			$thumb->resizeImage(__GAL_S_X, __GAL_S_Y, Imagick::FILTER_LANCZOS, 1, TRUE);
			$thumb->writeImage(GALLERY_FILES_THUMB_2.$fileName);
			$thumb->clear();
			$thumb->destroy(); 
			
			$_SESSION['image_name'] = $fileName;
  	}
  	return "<div class='galleryElement'><img src='".GALLERY_FILES_THUMB_2.$fileName."'></div><input type='hidden' name='file_name' value='".$fileName."'>";
	}	
	
	public function fileLoad() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array(
				'__GAL_M_X' => __GAL_M_X+10,
				'__GAL_M_Y' => __GAL_M_Y+10,
				//'__GAL_ID' => $this->var->id,
			);
			return Template::parse($list, file_get_contents($this->path.'imageLoad.tpl.php'));
		} else return $this->goAway();
	}	
	
	
	public function checkFileTypeMagic($file) {
		$file = fopen($file, 'r');
		$bytes = bin2hex(fread($file, 4));
    //echo '<p>'.$bytes.' - '.var_dump($this->checkMime($bytes)).'</p>';
		fclose($file);
		return $this->checkMime($bytes);
	}
	
	public function checkFileTypeInfo($file) {
		$file_info = new finfo(FILEINFO_MIME);
		echo '<p>'.$file_info->file($file, FILEINFO_MIME_TYPE).'</p>';
		return $file_info->file($file, FILEINFO_MIME_TYPE);
	}
	
	public function delFile() {
		if($this->var->file) {
			$this->db->queryString(__BP.'inspiracje_files');
			$this->db->updateQuery(array('IFI_STATUS'=>'deleted'), 'IFI_ID', $this->var->file);
			return 'ok';
		} else return 'false';
	}
	
	private function checkMime($file) {
		$mime = array(
			'ffd8ffe0' => array('image', 'jpg'),
			'ffd8ffe1' => array('image', 'jpg'),
			'ffd8ffe8' => array('image', 'jpg'),
			'89504e47' => array('image', 'png'),
			'47494638' => array('image', 'gif'),
			'0d444f43' => array('file', 'doc'),
			'cf11e0a1' => array('file', 'doc'),
			'd0cf11e0' => array('file', 'doc'),
			'dba52d00' => array('file', 'doc'),
			'eca5c100' => array('file', 'doc'),
			'fdffffff' => array('file', 'xls'),
			'504b0304' => array('file', 'docx'),
			'a0461df0' => array('file', 'ppt'),
			'25504446' => array('file', 'pdf'),
		);
		return $mime[$file];
	}
}

function inspiracje($option=null) {
	$inspiracje = new inspiracje;
	if($option!=null and method_exists($inspiracje, $option))
		return $inspiracje->$option();
	else 
		return $inspiracje->getPage();
}