<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=inspiracje">Inspiracje</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-sun-o"></i>
					<span>Dodaj nową pozycję</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<h4 class="page-header">Dodaj nową pozycję</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="INS_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Szkoła</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="INS_SCHOOL"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Wiek</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="INS_GRADE"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Treść</label>
					<div class="col-sm-9">
						<textarea name="INS_CONTENT" id="article_content" class="article_content_tbl"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Załaduj miniaturkę</label>
					<div class="col-sm-9">
						<a href="request.php?action=inspiracje&option=imageLoad" class="light btn btn-success btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj</a>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-9"><div id="galleryContainer"></div></div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status </label>
					<div class="col-sm-5">
						<select name="INS_STATUS" id="INS_STATUS">
							<option value="1">nowy</option>
							<option value="2">opublikowany</option>
							<option value="3">usunięty</option>
						</select>
					</div>
				</div>
				
				<input type="hidden" name="action" value="inspiracje">
				<input type="hidden" name="option" value="addSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>