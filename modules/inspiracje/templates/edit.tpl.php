<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=inspiracje">Inspiracje</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-sun-o"></i>
					<span>Edycja pozycji</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<h4 class="page-header">Edycja pozycji</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="INS_NAME" value="__INS_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Szkoła</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="INS_SCHOOL" value="__INS_SCHOOL"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Wiek</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="INS_GRADE" value="__INS_GRADE"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Alias</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="INS_ALIAS" value="__INS_ALIAS"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Treść</label>
					<div class="col-sm-9">
						<textarea name="INS_CONTENT" id="article_content" class="article_content_tbl">__INS_CONTENT</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Załaduj miniaturkę</label>
					<div class="col-sm-9">
						<a href="request.php?action=inspiracje&option=imageLoad" class="light btn btn-success btn-label-left"><span><i class="fa fa-plus"></i></span>załaduj nową</a>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-9"><div id="galleryContainer">__INS_FILE</div></div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status </label>
					<div class="col-sm-5">
						__INS_STATUS
					</div>
				</div>
				
					<h4 class="page-header">Dodatkowe pliki</h4>
					<div class="form-group">
					<label class="col-sm-3 control-label">Załączniki</label>
					<div class="col-sm-9">
						<div class="well">
							<div class="row">
								__ATTACHMENTS
							</div>
						</div>
            <p><input type="file" name="attachment[]" class="lesson_attachment"></p>
					</div>
          
          <label class="col-sm-3 control-label">&nbsp;</label><div class="col-sm-9" id="attachments_container"></div>
				</div>
				
				<input type="hidden" name="action" value="inspiracje">
				<input type="hidden" name="option" value="editSave">
				<input type="hidden" name="id" value="__INS_ID">
				<input type="hidden" name="salt" value="__INS_SALT">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>