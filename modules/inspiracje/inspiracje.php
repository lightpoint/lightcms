<?php
class inspiracje extends View {
	public $path = 'modules/inspiracje/templates/';
	
	public function getPage() {
		
	}
	
	public function getAll_() {
		$this->db->queryString('select * from '.__BP.'inspiracje where INS_STATUS=2 order by INS_DATE_ADD');
		$ins = $this->db->getDataFetch();
		
		if($ins) {
			foreach($ins as $i) {
				$line.='<a class="lesson-tile" href="inspiracja-'.$i['INS_ALIAS'].'.html"><div class="lesson-tile-image" style="background-image: url(http://makewonder.pl/files/gallery/thumb_1/'.$i['INS_FILE'].');"> <!--<div class="lesson-categories-list"><span class="label label-premium"><i class="fa fa-lock"></i>Premium</span></div>--></div>
  <div class="lesson-tile-bottom">
    <div class="lesson-title-wrap">
      <div class="lesson-title"> 
        <h4 class="ng-binding">'.$i['INS_NAME'].'</h4>
        <p class="ng-binding"></p>
      </div>
    </div>
    <div class="lesson-author">
      <div class="author-info">
        <div class="author-grades">Szkoła <span class="ng-binding">'.$i['INS_SCHOOL'].'</span></div>
        <div class="author-grades">Wiek <span class="ng-binding">'.$i['INS_GRADE'].'</span></div>
      </div>
    </div>
  </div></a>';
  		}
		}
		return $line;
	}
	
	public function getAll() {
		$this->db->queryString('select * from '.__BP.'inspiracje where INS_STATUS=2 order by INS_DATE_ADD');
		$ins = $this->db->getDataFetch();
		
		if($ins) {
			foreach($ins as $i) {
				$line.= '<div class="row"><div class="col s3"><img src="http://makewonder.pl/files/gallery/thumb_1/'.$i['INS_FILE'].'" class="responsive-img img-bordered"></div><div class="col s9"><h2 class="ng-binding">'.$i['INS_NAME'].'</h2><div class="author-grades" style="display: none;">Szkoła <span class="ng-binding">'.$i['INS_SCHOOL'].'</span></div>
        <div class="author-grades" style="display: none;">Wiek <span class="ng-binding">'.$i['INS_GRADE'].'</span></div>
        <p>'.stripslashes($i['INS_CONTENT']).'</p>';
        
        $this->db->queryString('select * from '.__BP.'inspiracje_files where IFI_STATUS=1 and IFI_INS_ID='.$i['INS_ID']);
        //$this->db->query();
        $files = $this->db->getDataFetch();
        
        if($files) {
					foreach($files as $f) {//http://nauczyciele.makewonder.pl/inspiracje.html
						if($f['IFI_FILE_TYPE']=='file') {
							$line.='<div><a href="http://makewonder.pl/'.INSPIRATION_ATTACHMENT_FILES.'/'.$f['IFI_INS_SALT'].'/'.$f['IFI_FILE'].'" target="_blank">'.$f['IFI_FILE'].'</a></div>';
						} else /*if($f['IFI_FILE_TYPE']=='image')*/ {
							$line.='<div class="col s4"><img class="materialboxed" src="http://makewonder.pl/'.INSPIRATION_ATTACHMENT_FILES.'/'.$f['IFI_INS_SALT'].'/'.$f['IFI_FILE'].'" width="150"></div>';
						}
					}
				}
				
				$line.='</div></div>';
				
				//$line.='<a class="lesson-tile" href="inspiracja-'.$i['INS_ALIAS'].'.html"><div class="lesson-tile-image" style="background-image: url(http://makewonder.pl/files/gallery/thumb_1/'.$i['INS_FILE'].');"> <!--<div class="lesson-categories-list"><span class="label label-premium"><i class="fa fa-lock"></i>Premium</span></div>--></div>  <div class="lesson-tile-bottom">    <div class="lesson-title-wrap">      <div class="lesson-title">         <h4 class="ng-binding">'.$i['INS_NAME'].'</h4>        <p class="ng-binding"></p>      </div>    </div>    <div class="lesson-author">      <div class="author-info">        <div class="author-grades">Szkoła <span class="ng-binding">'.$i['INS_SCHOOL'].'</span></div>        <div class="author-grades">Wiek <span class="ng-binding">'.$i['INS_GRADE'].'</span></div></div></div></div></a>';
  		}
		}
		return $line;
	}
	
	public function getOne() {
		if($this->var->id) {
			$this->db->queryString('select * from '.__BP."inspiracje where INS_ALIAS='".$this->var->id."' and INS_STATUS=2");
			
			$inspiracja = $this->db->getRow();
			
			if($inspiracja) {
				$line = '<section class="lesson-header-section">
	    <div class="container">
	      <div class="col"><a class="all-lessons-link" href="inspiracje.html"><i class="fa fa-chevron-left"></i>&nbsp; Zobacz wszystkie inspiracje</a>
	        <h1 class="lesson-title ">'.$inspiracja['INS_NAME'].'</h1>
	        
	      </div><img class="lesson-main-image" src="http://makewonder.pl/files/gallery/full/'.$inspiracja['INS_FILE'].'">
	    </div>
	  </section>
	  <section class="lesson-info-section">
	    <div class="container">
	      <div class="col">
	        <div class="row">
	          <h4>Szkoła</h4>
	          <p class="lesson-description ">'.$inspiracja['INS_SCHOOL'].'</p>
	        </div>
	        <div class="row">
	          <h4 class="inline">Klasa</h4><br/>
	          <div class="categories-list-n">'.$inspiracja['INS_GRADE'].'</div>
	        </div>
	       
	      </div>
	      <div class="col">
	        <h4>Opis</h4>'.stripslashes($inspiracja['INS_CONTENT']).'</div>
	    </div>
	  </section>';
	  		return $line;
	  	}
		}
	}
		
}

function inspiracje($option=null) {
	$inspiracje = new inspiracje;
	if($option!=null and method_exists($inspiracje, $option))
		return $inspiracje->$option();
	else 
		return $inspiracje->getPage();
}