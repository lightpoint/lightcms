<?php
$module_info['title'] = 'Inspiracje';
$module_info['description'] = 'Inspiracje - tworzenie i zarządzanie';
$module_info['author'] = 'expansja';

$menu['title'][] = 'Inspiracje';
$menu['action'][] = 'inspiracje';
$menu['icon'][] = 'fa fa-sun-o';

$menu_show['menu'] = true;