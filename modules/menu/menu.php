<?php
class AMenu extends View {
	public $path = 'modules/menu/templates/';
	public $placeHolder = array('top', 'right');
	public $placeHolderTemplate = array(
		'top'=>"<div class='topMenu' style='width:%s'><a id='menu_%s' href='%s'>%s</a>",
		'right'=>"<div class='rightMenu'><a href='%s'>%s</a>",
	);
	public $placeEmptyTemplate = "<div class='topMenu emptyMenu'>%s";
  
	public function getNav() {
		$menu = '<li><a href="/" target="_self" class="logo">Wonder Workshop</a></li>
		  <li class="nav-dropdown" id="wat-robots"><span class="nav-link">Roboty <i class="fa fa-caret-down"></i></span>
		  	<ul class="wat-dropdown-menu">
          <li><a href="'.helper::href(array('dash')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_dash.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_dash_2x.png 2x" alt="Dash Icon"><span>Dash</span></a></li>
          <li><a href="'.helper::href(array('dot')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_dot.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_dot_2x.png 2x" alt="Dot Icon"><span>Dot</span></a></li>
        </ul>
		  </li>

		  <li class="nav-dropdown"><span class="nav-link">Aplikacje <i class="fa fa-caret-down"></i></span>
		  	<ul class="wat-dropdown-menu">
          <li><a href="'.helper::href(array('aplikacje_wonder')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_wonder.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_wonder_2x.png 2x" alt="Wonder Icon"><span>Wonder</span></a></li>
          <li><a href="'.helper::href(array('aplikacje_blockly')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_blockly.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_blockly_2x.png 2x" alt="Blockly Icon"><span>Blockly</span></a></li>
          <li><a href="'.helper::href(array('aplikacje_xylo')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_xylo.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_xylo_2x.png 2x" alt="Xylo Icon"><span>Xylo</span></a></li>
          <li><a href="'.helper::href(array('aplikacje_path')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_path.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_path_2x.png 2x" alt="Path Icon"><span>Path</span></a></li>
          <li><a href="'.helper::href(array('aplikacje_go')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_go.png" srcset="templates/frontend/makewonder_1.0/img/icons 2x" alt="Go Icon"><span>Go</span></a></li>
        </ul>
		  </li>
		  <li class="nav-dropdown"><span class="nav-link">Jak zacząć? <i class="fa fa-caret-down"></i></span>
		  	<ul class="wat-dropdown-menu">
          <li><a href="'.helper::href(array('jak_zaczac')).'"><img src="templates/frontend/makewonder_1.0/img/icons/icon_dash.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_dash_2x.png 2x" alt="Dash Icon"><span>Pierwsze kroki</span></a></li>
          <li><a href="'.helper::href(array('przewodnik_po_akcesoriach')).'"><img src="templates/frontend/makewonder_1.0/img/icons/BuildingBrick.png" srcset="templates/frontend/makewonder_1.0/img/icons/BuildingBrick_2x.png 2x" alt="Dot Icon"><span>Przewodnik po akcesoriach</span></a></li>
          <li><a href="https://www.makewonder.com/play/ideas/" target="_blank"><img src="templates/frontend/makewonder_1.0/img/icons/icon_kid_portal.png" srcset="templates/frontend/makewonder_1.0/img/icons/icon_kid_portal_2x.png 2x" alt="Dot Icon"><span>Pomysły na zabawę [eng]</span></a></li>
        </ul>
		  </li>
		  <li><a href="http://nauczyciele.makewonder.pl" class="nav-link" target="_blank">Dla nauczycieli</a></li>
		  <li class="wat-action-buttons"><div class="wat-button-group"><a role="button" class="wat-button" onClick="_gaq.push(['."'".'trackPageview'."'".', '."'".'Klikniecie'."'".', '."'".'Sklep'."'".', '."'".'Menu'."'".']);" href="http://wonderpolska.pl/" target="_blank">Sklep</a></div></li>';

		return $menu;
  }
  
  public function getMobileNav() {
		$menu = '<h3 class="active"><a href="/" target="_self">Home</a></h3>
      <h3><a href="'.helper::href(array('dash')).'" target="_self">Dash</a></h3>
      <h3><a href="'.helper::href(array('dot')).'" target="_self">Dot</a></h3>
      <h3><a href="'.helper::href(array('aplikacje')).'" target="_self">Aplikacje</a></h3>
      <ul>
        <li><a href="'.helper::href(array('aplikacje_go')).'">Go</a></li>
        <li><a href="'.helper::href(array('aplikacje_blockly')).'" class=" ">Blockly</a></li>
        <li><a href="'.helper::href(array('aplikacje_path')).'" class=" ">Path</a></li>
        <li><a href="'.helper::href(array('aplikacje_xylo')).'" class=" ">Xylo</a></li>
        <li><a href="'.helper::href(array('aplikacje_wonder')).'" class=" ">Wonder</a></li>
      </ul>
      <h3><a href="'.helper::href(array('jak_zaczac')).'">Jak zacząć?</a></h3>
      <h3><a onClick="_gaq.push(['."'".'_trackEvent'."'".', '."'".'Klikniecie'."'".', '."'".'Sklep'."'".', '."'".'Menu'."'".']);" href="http://www.wonderpolska.pl/">Sklep</a></h3>';

    return $menu;
	}
	
	public function getFootMenu() {
		$menu = '<div footer-accordion="" hide-default="" class="footer-link-section-title">Produkty Wonder</div>
	    <ul>
	    	<li> <a onClick="_gaq.push(['."'".'_trackEvent'."'".', '."'".'Klikniecie'."'".', '."'".'Sklep'."'".', '."'".'Dolne menu'."'".']);" href="http://wonderpolska.pl/" target="_blank">Sklep</a></li>
	      <li> <a href="'.helper::href(array('dash')).'" target="_self">Dash</a></li>
	      <li> <a href="'.helper::href(array('dot')).'" target="_self">Dot</a></li>
	      <li> <a href="'.helper::href(array('aplikacje')).'">Aplikacje na roboty</a></li>
	      <li> <a href="'.helper::href(array('przewodnik_po_akcesoriach')).'" target="_self">Przewodnik po akcesoriach</a></li>
	    </ul>
	  </div>
	  <div class="footer-link-section">
	    <div footer-accordion="" hide-default="" class="footer-link-section-title">Przydatne informacje</div>
	    <ul>
	      <li> <a href="'.helper::href(array('jak_zaczac')).'" target="_self">Jak zacząć</a></li>  
	      <li> <a href="'.helper::href(array('pomoc')).'" target="_self">Pomoc / FAQ</a></li>
	      <li> <a href="'.helper::href(array('gwarancja_producenta')).'" target="_self">Gwarancja</a></li>
	      <li> <a href="'.helper::href(array('polityka_prywatnosci')).'" target="_self">Polityka prywatności</a></li>
	    </ul>
	  </div>
	  <div class="footer-link-section">
	    <div footer-accordion="" hide-default="" class="footer-link-section-title">Firma</div>
	    <ul>
	      <li> <a href="'.helper::href(array('kontakt')).'" target="_self">Kontakt</a></li>
	      <li> <a href="'.helper::href(array('historia')).'" target="_self">Historia Wonder</a></li>
	      <li> <a href="https://www.makewonder.com/mediakit" target="_blank">Dla mediów</a></li>
	    </ul>
	  </div>';
	  //<li> <a href="'.helper::href(array('gwarancja_satysfakcji')).'" target="_self">Gwarancja satysfakcji</a></li>
	  return $menu;
	}
	
	public function getFormNewsletter() {
		$form = '<div class="footer-link-section-title newsletter-field">Dołącz do naszego Newslettera
                  <p>Bądź na bieżąco z naszymi produktami, spotkaniami i innymi atrakcjami</p>
                </div>
                
                <form method="POST" name="footerNewsletterForm" id="footerNewsletterForm" class="ng-pristine ng-invalid ng-invalid-required ng-invalid-date" action="http://wonderpolska.pl/settings.php">
                  
                  <div style="position: absolute; left: -5000px;">
                    <input type="text" name="b_6c1539b72e697a4dc1503b46b_e5770b11b0" tabindex="-1" value="" ng-model="newsLetterBot" class="ng-pristine ng-valid">
                  </div>
                  <div class="row">
                    <input ng-model="newsletterEmail" type="email" ng-disabled="newsletterSignedUp" placeholder="e-mail" required="" class="newsletter-input ng-pristine ng-invalid ng-invalid-required ng-valid-email" name="mailing_email">
                  </div><br/>
                  <div class="row dob-button">
                    <input type="text" required="" class="newsletter-input ng-pristine ng-invalid ng-invalid-required ng-invalid-date" placeholder="imię" name="mailing_name">
                    <input id="mailing_action" name="mailing_action" value="add" type="hidden">
                    <button type="submit" class="newsletter-button no-shadow-button"><span ng-hide="loading" class="">Dołącz</span></button>
                  </div>
                </form>';
                
     $form2 = '<form  method="POST" ng-submit="signupNewsletter()" name="heroSignupForm" ng-hide="newsletterSignedUp" class="home-signup-wrapper ng-pristine ng-invalid ng-invalid-date">
                    <div style="position: absolute; left: -5000px;">
                      <input type="text" name="b_6c1539b72e697a4dc1503b46b_e5770b11b0" tabindex="-1" value="" ng-model="bot" class="ng-pristine ng-valid">
                    </div>
                    <input type="email" placeholder="Email:" ng-model="email" class="ng-pristine ng-valid ng-valid-email">
                    
                    <button ng-disabled="heroSignupForm.$invalid" disabled="disabled"><span ng-hide="loading" class="">Zapisz mnie!</span><span ng-show="loading" class="ng-hide"><i class="fa fa-spinner fa-spin"></i></span></button>
                  </form>';
    return $form;
	}
	
	public function getFormNewsletter2() {
		$form = '<div ng-hide="newsletterSignedUp" class="footer-link-section-title newsletter-field">Dołącz do naszego Newslettera
                  <p>Bądź na bieżąco z naszymi produktami, spotkaniami i innymi atrakcjami</p>
                </div>
                <div ng-show="newsletterSignedUp" class="footer-link-section-title ng-hide"> 
                  <p> <strong>Dziękujemy za dopisanie się do newslettera!</strong></p>
                </div>
                <form method="POST" ng-submit="joinNewsletter()" ng-valid-required="" name="footerNewsletterForm" id="footerNewsletterForm" ng-hide="newsletterSignedUp" class="ng-pristine ng-invalid ng-invalid-required ng-invalid-date">
                  <p ng-show="under13" class="info newsletter-error fx-fade-normal ng-hide">Nie jesteś osobą pełnoletnią. Poproś rodziców, lub opiekuna, by wypełnili formularz.</p>
                  <p ng-show="newsLetterError" class="error newsletter-error fx-fade-normal ng-binding ng-hide"></p>
                  <div style="position: absolute; left: -5000px;">
                    <input type="text" name="b_6c1539b72e697a4dc1503b46b_e5770b11b0" tabindex="-1" value="" ng-model="newsLetterBot" class="ng-pristine ng-valid">
                  </div>
                  <div class="row">
                    <input ng-model="newsletterEmail" type="email" ng-disabled="newsletterSignedUp" placeholder="e-mail" required="" class="newsletter-input ng-pristine ng-invalid ng-invalid-required ng-valid-email">
                  </div><small>Wpisz datę urodzenia, by potwierdzić, że nie jesteś robotem. Nie będziemy widzieć ani zapisywać tej informacji!</small>
                  <div class="row dob-button">
                    <input ng-model="dob" dob-mask="" type="text" ng-disabled="newsletterSignedUp" required="" class="newsletter-input ng-pristine ng-invalid ng-invalid-required ng-invalid-date" placeholder="DD/MM/RRRR">
                    <button ng-disabled="newsletterSignedUp || footerNewsletterForm.$invalid" class="newsletter-button no-shadow-button" disabled="disabled"><span ng-hide="loading" class="">Dołącz</span><span ng-show="loading" class="ng-hide"><i class="fa fa-spinner fa-spin"></i></span></button>
                  </div>
                </form>';
                
     $form2 = '<form  method="POST" ng-submit="signupNewsletter()" name="heroSignupForm" ng-hide="newsletterSignedUp" class="home-signup-wrapper ng-pristine ng-invalid ng-invalid-date">
                    <div style="position: absolute; left: -5000px;">
                      <input type="text" name="b_6c1539b72e697a4dc1503b46b_e5770b11b0" tabindex="-1" value="" ng-model="bot" class="ng-pristine ng-valid">
                    </div>
                    <input type="email" placeholder="Email:" ng-model="email" class="ng-pristine ng-valid ng-valid-email">
                    
                    <button ng-disabled="heroSignupForm.$invalid" disabled="disabled"><span ng-hide="loading" class="">Zapisz mnie!</span><span ng-show="loading" class="ng-hide"><i class="fa fa-spinner fa-spin"></i></span></button>
                  </form>';
    return $form;
	}
	
	public function getNavTeachersTop() {
		$line = '<a ui-sref="home" ui-sref-active="active" class="portal-name active" href="http://nauczyciele.makewonder.pl/">portal dla nauczycieli</a>
          <nav>
            <ul>
              <li> <a href="inspiracje.html" class="">Inspiracje nauczycieli</a></li>
              <li> <a href="scenariusze-lekcji.html" class="">Scenariusze lekcji</a></li>
              <li> <a href="oferta-robotow.html" tabindex="1">Oferta</a></li>
            </ul>
          </nav>
          <div class="header-sign-up">
            <a style="display: none;" href="#modal1" class="waves-effect waves-light btn modal-trigger">Zaloguj się</a>
          </div>
          <div ng-hide="user.isLoggedIn()" class="header-shopping-cart-spacer">
            <div class="header-store-button"><a role="button" href="https://wonderpolska.pl/" class="button orange-button pill-button">Sklep</a></div>
          </div>';
          
    return $line;
	}
	
	
	public function getList($option) {
		if(in_array($option,$this->placeHolder)) {
			$this->db->queryString("select * from ".__BP."menus where MEN_TYPE='".$option."' and MEN_STATUS=1 and MEN_PARENT=0 order by MEN_ORDER");
			$menu_d = $this->db->getDataFetch();
			$m_c = $this->db->getCount();
			$element_width = 100/$m_c;
			
			if(array_key_exists($option, $this->placeHolderTemplate)) {
				foreach($menu_d as $md) {
					if($md['MEN_STYLE']=='text' or $md['MEN_STYLE']=='gallery')
						$line.= sprintf($this->placeHolderTemplate[$option], $element_width.'%', $md['MEN_MODULE_ALIAS'], $md['MEN_HREF'], $md['MEN_NAME']);
					elseif($md['MEN_STYLE']=='none')
						$line.= sprintf($this->placeEmptyTemplate, $md['MEN_NAME']);
					elseif($md['MEN_STYLE']=='box') {
						$module = new Module();
						$result = $module->load($md['MEN_MODULE'],$md['MEN_MODULE_ALIAS']);
						$line.= sprintf($result, '', $result);
					}
					
					//add child menu
					$this->db->queryString("select * from ".__BP."menus where MEN_TYPE='".$option."' and MEN_STATUS=1 and MEN_PARENT=".$md['MEN_ID']." order by MEN_ORDER");
					$menu_ch = $this->db->getDataFetch();
					
					if($menu_ch!=null) {
						$line.="<div class='menu_ch'>";
						foreach($menu_ch as $m_ch) {
							$line.="<div class='m_element'><a href='".$m_ch['MEN_HREF']."'>".$m_ch['MEN_NAME']."</a></div>";
						}
						
						$line.="</div>";
					}
					$line.="</div>";
				}
				return $line;
			} else {
				foreach($menu_d as $md) {
					if($md['MEN_STYLE']=='text')
						$line.= "<li><a href='".$md['MEN_HREF']."'>".$md['MEN_NAME']."</a></li>";
					elseif($md['MEN_STYLE']=='box') {
						$module = new Module();
						$result = $module->load($md['MEN_MODULE'],$md['MEN_MODULE_ALIAS']);
						$line.= "<li>$result</li>";
					}
					elseif($md['MEN_STYLE']=='banner') {
						$module = new Module();
						$result = $module->load($md['MEN_MODULE'],$md['MEN_MODULE_ALIAS']);
						$line.= "<li>$result</li>";
					}
				}
				return "<ul class='$option'>".$line."</ul>";
			}
		} else return $this->moduleError('menu', $option);
	}
	
	public function getClass() {
		if($this->var->action==gallery and $this->var->option) {
			$this->db->queryString("select GCA_FILE from ".__BP."gallery_categories where GCA_ALIAS='".$this->var->option."'");
			$class_temp = $this->db->getRow();
			$class = $class_temp['GCA_FILE'];
			if($class!=null and $class!='')
				return $class;
			else return "back1";
		} else return "back1";
	}
}

function menu($option=null) {
	$menu = new AMenu;
	if($option!=null and method_exists($menu, $option))
		return $menu->$option();
	else 
		return $menu->getList();
}
?>