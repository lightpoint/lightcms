<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=menu">Lista modułów na stronie</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-ellipsis-v"></i>
					<span>Edycja pozycji menu</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
					<div id="contentPlace">
					
						<div class="form-group">
							<label class="col-sm-3 control-label">Nazwa</label>
							<div class="col-sm-5">
								<input type="text" class="form-control"  name="MEN_NAME" value="__MEN_NAME"/>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Położenie</label>
							<div class="col-sm-5">
								__MENU_TYPE
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-sm-3 control-label">Moduł</label>
							<div class="col-sm-5">
								__MENU_LIST
							</div>
						</div>
						
					</div>
					
					<input type="hidden" name="action" value="menu">
					<input type="hidden" name="option" value="saveEditMenu">
					<input type="hidden" name="MEN_ID" value="__MEN_ID">
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" class="btn btn-primary">Zapisz</button>
							<button type="submit" class="btn btn-secondary">Anuluj</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>