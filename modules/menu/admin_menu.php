<?php
class AMenu extends View {
	public $path = 'modules/menu/templates/';
	//public $placeHolder = array('top', 'bottom', 'left', 'right', 'center');
	public $placeHolder = array('top', 'right');

	//return the list of menus with all data
	public function getList() {
		$this->db->queryString("select * from ".__BP."menus where MEN_PARENT=0 order by MEN_ORDER, MEN_TYPE");
		$menus = $this->db->getDataFetch();
		
		$i = 1;
		if($menus) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Położenie','Moduł','Kolejność','','');
			$table->createHeader($tableHeader);
			
			foreach($menus as $menu) {
				if($i>1) 
					$menuup = '<a href="?action=menu&option=menuup&id='.$menu['MEN_ID'].'&ord='.$menu['MEN_ORDER'].'"><img src="templates/backend/default/img/au.png"></a>';
				else $menuup = '&nbsp;&nbsp;&nbsp;';
				if($i<$menu_data->getCount)
					$menudown = '<a href="?action=menu&option=menudown&id='.$menu['MEN_ID'].'&ord='.$menu['MEN_ORDER'].'"><img src="templates/backend/default/img/adown.png"></a>';
				else $menudown = '';
				
				$table->addCell($i, 'lp');
				$table->addLink('', $menu['MEN_NAME'], 'fancybox fancybox.ajax');
				$table->addLink('', $menu['MEN_TYPE'], 'fancybox fancybox.ajax');
				$table->addLink('', $menu['MEN_MODULE'], 'fancybox fancybox.ajax');
				$table->addCell($menuup.' '.$menudown);
				$table->addLink('?action=menu&option=addChild&id='.$menu['MEN_ID'], 'dodaj&nbsp;podstronę');
				$table->addLink('?action=menu&option=editMenu&id='.$menu['MEN_ID'].'&menu_module='.$menu['MEN_MODULE'].'&menu_option='.$menu['MEN_MODULE_ALIAS'], 'edycja');
				$table->addRow();
				
				//add child rows
				$this->db->queryString("select * from ".__BP."menus where MEN_PARENT=".$menu['MEN_ID']." order by MEN_ORDER, MEN_TYPE");
				$menus_ch = $this->db->getDataFetch();
				$i_ch = 1;
				if($menus_ch!=null) {
					foreach($menus_ch as $m_ch) {
						$table->addCell('');
						$table->addLink('', $i_ch.". ".$m_ch['MEN_NAME'], 'fancybox fancybox.ajax');
						$table->addLink('', $m_ch['MEN_TYPE'], 'fancybox fancybox.ajax');
						$table->addLink('', $m_ch['MEN_MODULE'], 'fancybox fancybox.ajax');
						//$table->addCell($menuup.' '.$menudown);
						$table->addCell('');
						$table->addCell('');
						$table->addLink('?action=menu&option=editMenu&id='.$m_ch['MEN_ID'].'&menu_module='.$m_ch['MEN_MODULE'].'&menu_option='.$m_ch['MEN_MODULE_ALIAS'], 'edycja');
						$table->addRow();
						$i_ch++;	
					}	
				}
				
				$i++;
			}
			$tableBody = $table->createBody();
		} else $tableBody = '<table id="myTable" class="tablesorter"><tr><td align="center"> - brak danych - </td></tr></table>';
		
		$list = array(
			'__TABLE'	=>$tableBody,
		);
		
		return Template::parse($list, file_get_contents($this->path.'menusList.tpl.php'));
	}
	
	//create window for adding the new one
	public function addMenu() {
		$list = array(
			'__MENU_TYPE'	=>$this->createMenuSelect(),
			'__MENU_LIST'	=>$this->getMenuList(),
		);
		
		return Template::parse($list, file_get_contents($this->path.'addMenu.tpl.php'));
	}
	
	private function createMenuSelect($position=null) {
		foreach($this->placeHolder as $ph) {
			if($position==$php)
				$selected = " selected='selected'";
			else $selected = '';
			
			$line.= "<option value='".$ph."'".$selected.">".$ph."</option>";
		}
		return '<select name="MEN_TYPE" id="MEN_TYPE">'.$line."</select>";
	}
	
	private function getMenuList() {
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
		
		foreach($catalogs as $module) {
			unset($menu_show);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if($menu_show['menu']==true) {
					$module = new Module('admin_');
		    	$result.= "<h4>".$module_info['title']."</h4>";
					$result.= $module->load($menu['action'][0],'getForMenu');
		    }
		  }
		}
		
		if($this->var->menu_module=='none')
				$selected = " checked";
			else $selected = '';
		$result.= "<br/><input type='radio' name='menu' value='none:none:1:none' $selected> - <strong>brak modułu</strong>";
		
		return $result;
	}
	
	public function saveMenu() {
		$this->db->queryString("select max(MEN_ORDER) as max_order from ".__BP."menus");
		$max = $this->db->getRow();
		
		$menu = explode(':', $this->var->menu);
		
		if($menu[1]=='none')
			$href = '?action='.$menu[0];
		else $href = '?action='.$menu[0].'&option='.$menu[1];
		
		$insert_data = array(
			'MEN_NAME'=>$this->var->MEN_NAME,
			'MEN_TYPE'=>$this->var->MEN_TYPE,
			'MEN_MODULE'=>$menu[0],
			'MEN_MODULE_ALIAS'=>$menu[1],
			'MEN_MODULE_ID'=>$menu[2],
			'MEN_MODULE_TITLE'=>$menu[3],
			'MEN_STYLE'=>$menu[4],
			'MEN_HREF'=>$href,
			'MEN_ORDER'=>(int)$max['max_order'] + 1,
			
		);
		
		if($menu[0]=='none')
			$insert_data['MEN_STYLE'] = 'none';
			
		if($menu[0]=='gallery')
			$insert_data['MEN_MODULE_ALIAS'] = 'gallery';
		
		$this->db->queryString(__BP.'menus');
		$this->db->insertQuery($insert_data);
		//$menu_data->query();
		header('location: ?action=menu');
	}
	
	public function editMenu() {
		$this->db->queryString("select MEN_TYPE, MEN_NAME from ".__BP."menus where MEN_ID=".$this->var->id);
		$menu = $this->db->getRow();
		
		$list = array(
			'__MENU_TYPE'	=>$this->createMenuSelect($menu['MEN_TYPE']),
			'__MENU_LIST'	=>$this->getMenuList(),
			'__MEN_NAME'=>$menu['MEN_NAME'],
			'__MEN_ID'=>$this->var->id
		);
		
		return Template::parse($list, file_get_contents($this->path.'editMenu.tpl.php'));
	}
	
	public function saveEditMenu() {
		$menu = explode(':', $this->var->menu);
		
		if($menu[1]=='none')
			$href = '?action='.$menu[0];
		else $href = '?action='.$menu[0].'&option='.$menu[1];
		
		$insert_data = array(
			'MEN_NAME'=>$this->var->MEN_NAME,
			'MEN_TYPE'=>$this->var->MEN_TYPE,
			'MEN_MODULE'=>$menu[0],
			'MEN_MODULE_ALIAS'=>$menu[1],
			'MEN_MODULE_ID'=>$menu[2],
			'MEN_MODULE_TITLE'=>$menu[3],
			'MEN_STYLE'=>$menu[4],
			'MEN_HREF'=>$href,
		);
		
		if($menu[0]=='none')
			$insert_data['MEN_STYLE'] = 'none';
			
		if($menu[0]=='gallery')
			$insert_data['MEN_MODULE_ALIAS'] = 'gallery';
		
		$this->db->queryString(__BP.'menus');
		$this->db->updateQuery($insert_data,'MEN_ID',$this->var->MEN_ID);
		//$menu_data->query();
		header('location: ?action=menu');
	}
	
	public function addChild() {
		$this->db->queryString("select MEN_TYPE from ".__BP."menus where MEN_ID=".$this->var->id);
		$menu_type = $this->db->getRow();
		
		$list = array(
			'__MENU_TYPE'	=>$menu_type['MEN_TYPE'],
			'__MENU_LIST'	=>$this->getMenuList(),
			'__MENU_PARENT' =>$this->var->id, 
		);
		
		return Template::parse($list, file_get_contents($this->path.'addMenuChild.tpl.php'));		
	}
	
	public function saveMenuChild() {
		$this->db->queryString("select max(MEN_ORDER) as max_order from ".__BP."menus where MEN_PARENT=".$this->var->MEN_PARENT);
		$max = $this->db->getRow();
		
		$menu = explode(':', $this->var->menu);
		
		$insert_data = array(
			'MEN_NAME'=>$this->var->MEN_NAME,
			'MEN_TYPE'=>$this->var->MEN_TYPE,
			'MEN_MODULE'=>$menu[0],
			'MEN_MODULE_ALIAS'=>$menu[1],
			'MEN_MODULE_ID'=>$menu[2],
			'MEN_MODULE_TITLE'=>$menu[3],
			'MEN_HREF'=>'?action='.$menu[0].'&option='.$menu[1],
			'MEN_ORDER'=>(int)$max['max_order'] + 1,
			'MEN_PARENT'=>$this->var->MEN_PARENT,
		);
		
		if($menu[0]=='none')
			$insert_data['MEN_STYLE'] = 'none';
		
		$this->db->queryString(__BP.'menus');
		$this->db->insertQuery($insert_data);
		//$menu_data->query();
		header('location: ?action=menu');
	}
	
	public function menuup() {
		$this->db->queryString("SELECT MEN_ORDER as order_max, MEN_ID as order_id FROM ".__BP."menus WHERE MEN_ORDER<".$this->var->ord." order by MEN_ORDER DESC");
		$max = $this->db->getRow();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$this->var->ord." where MEN_ID=".$max['order_id']);
		$this->db->execQuery();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$max['order_max']." where MEN_ID=".$this->var->id);
		$this->db->execQuery();
		
		header('location: admin.php?action=menu');
	}
	
	public function menudown() {
		$this->db->queryString("SELECT MEN_ORDER as order_max, MEN_ID FROM ".__BP."menus WHERE MEN_ORDER>".$this->var->ord." order by MEN_ORDER ASC");
		$this->db->getRow();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$max['order_max']." where MEN_ID=".$this->var->id);
		$this->db->execQuery();
		
		$this->db->queryString("update ".__BP."menus set MEN_ORDER=".$this->var->ord." where MEN_ID=".$max['MEN_ID']);
		$this->db->execQuery();
		
		header('location: admin.php?action=menu');
	}
}

function menu($option=null) {
	$menu = new AMenu;
	if($option!=null and method_exists($menu, $option))
		return $menu->$option();
	else 
		return $menu->getList();
}