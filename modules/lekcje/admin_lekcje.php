<?php
class lekcje extends View {
	public $path = 'modules/lekcje/templates/';
	
	public function getPage() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa','Data dodania', 'Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;

			$this->db->queryString('select * from '.__BP.'lekcje order by LEK_DATE_ADD');
			$lessons = $this->db->getDataFetch();
			
			foreach($lessons as $less) {
				$table->addCell($i, 'lp');
				$table->addCell($less['LEK_NAME']);
				$table->addCell($less['LEK_DATE_ADD']);
				$table->addCell($less['LEK_STATUS']);
				//$table->addLink('?action=articles&option=add&id='.$art['ART_ID'], 'dodaj&nbsp;podstronę');
				$table->addLink('?action=lekcje&option=edit&id='.$less['LEK_ID'], 'edycja');
				$table->addRow();
				$i++;
			}
			$tableBody = $table->createBody();
			
			$list = array(
				'__TABLE'	=>$tableBody,
			);
			
			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array(
				'__LESSON_SUBJECTS' => $this->getLessonSubjects(),
				'__LESSON_ACCESSORIES' => $this->getLessonAccessories(),
				'__LESSON_AUTHOR' => $this->getLessonAuthors(),
			);
			return Template::parse($list, file_get_contents($this->path.'add.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->LEK_NAME) {
				$this->db->queryString(__BP.'lekcje');
        $salt = md5(microtime());
        
				$lessons = array(
					'LEK_NAME' => $this->var->LEK_NAME,
					'LEK_NAME' => coder::stripPL($this->var->LEK_NAME),
					'LEK_CONTENT' => addslashes($this->var->LEK_CONTENT),
          'LEK_CONTENT_2' => addslashes($this->var->LEK_CONTENT_2),
          'LEK_CONTENT_3' => addslashes($this->var->LEK_CONTENT_3),
          'LEK_GROUP' => addslashes($this->var->LEK_GROUP),
          'LEK_CLASS_MIN' => $this->var->LEK_CLASS_MIN,
          'LEK_CLASS_MAX' => $this->var->LEK_CLASS_MAX,
          'LEK_TIME' => addslashes($this->var->LEK_TIME),
          'LEK_TIME_UNIT' => addslashes($this->var->LEK_TIME_UNIT),
          //'LEK_TIME_UNIT' => addslashes($this->var->LEK_TIME_UNIT),
					'LEK_FILE' => $this->var->image_name,
					'LEK_TYPE' => $this->var->LEK_TYPE,
					'LEK_STATUS' => $this->var->LEK_STATUS,
					'LEK_AUTHOR' => $this->var->LEK_AUTHOR,
          'LEK_SALT' => $salt,
				);
				$l = $this->db->insertQuery($lessons);
      
      	//attachments
	    	foreach ($_FILES["attachment"]["error"] as $key => $error) {
	        if ($error == UPLOAD_ERR_OK) {
	          if(file_exists(LESSONS_ATTACHMENT_FILES.'/'.$salt.'/')) {
	            
	          } else {
	            if (!mkdir(LESSONS_ATTACHMENT_FILES.'/'.$salt.'/', 0777, true)) {
	              die('Failed to create folders...');
	            }
	          }
	          
	          $this->db->queryString(__BP.'lekcje_files');
	          $tmp_name = $_FILES["attachment"]["tmp_name"][$key];
	          $name = $_FILES["attachment"]["name"][$key];
	          move_uploaded_file($tmp_name, LESSONS_ATTACHMENT_FILES.'/'.$salt.'/'."$name");
	          
	          $insert = array('LEF_LEK_ID' => $l, 'LEF_FILE' => $name, 'LEF_LEK_SALT' => $salt);
	          $this->db->insertQuery($insert);
	        }
	      }
	      
	      //subjects
	      foreach($this->var->lesson_subject as $id=>$value) {
					$this->db->queryString(__BP.'lekcje_subject_id');
					$insert = array('LSI_LEK_ID' => $l, 'LSI_LSU_ID' => $id);
	        $this->db->insertQuery($insert);
				}
				
				//accessories
	      foreach($this->var->lesson_accessory as $id=>$value) {
					$this->db->queryString(__BP.'lekcje_acc_id');
					$insert = array('LAI_LEK_ID' => $l, 'LAI_LAC_ID' => $id);
	        $this->db->insertQuery($insert);
				}
				
				//helpers
	      foreach($this->var->lesson_helpers as $id=>$value) {
					$this->db->queryString(__BP.'lekcje_helpers');
					$insert = array('LHE_LEK_ID' => $l, 'LHE_CONTENT' => $value);
	        $this->db->insertQuery($insert);
	        //echo $id.' '.$value.'<br/>';
				}
				
				//objectives
				foreach($this->var->lesson_objective as $id=>$value) {
					$this->db->queryString(__BP.'lekcje_objectives');
					$insert = array('LOB_LEK_ID' => $l, 'LOB_CONTENT' => $value);
	        $this->db->insertQuery($insert);
	        //echo $id.' '.$value.'<br/>';
				}
      }
			header('location: admin.php?action=lekcje&option=edit&id='.$l);
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {	
			$_SESSION['image_name'] = null;
			$this->db->queryString('select * from '.__BP.'lekcje where LEK_ID='.$this->var->id);
			$lek = $this->db->getRow();
      
      //helpers
      $this->db->queryString('select * from '.__BP.'lekcje_helpers where LHE_LEK_ID='.$this->var->id.' and LHE_STATUS=1');
      $helpers = $this->db->getDataFetch();
      foreach($helpers as $h)
      	$helpers_data.= '<div><div class="col-sm-10"><input type="text" class="form-control" name="lesson_helpers[]" value="'.$h['LHE_CONTENT'].'"></div><div class="col-sm-2"><a href="#" class="del-input btn btn-danger btn-label-left"><span><i class="fa fa-minus"></i></span>usuń</a></div></div>';
      	
      //objectives
      $this->db->queryString('select * from '.__BP.'lekcje_objectives where LOB_LEK_ID='.$this->var->id.' and LOB_STATUS=1');
      $objectives = $this->db->getDataFetch();
      foreach($objectives as $o)
      	$objectives_data.= '<div><div class="col-sm-10"><input type="text" class="form-control" name="lesson_objective[]" value="'.$o['LOB_CONTENT'].'"></div><div class="col-sm-2"><a href="#" class="del-input btn btn-danger btn-label-left"><span><i class="fa fa-minus"></i></span>usuń</a></div></div>';
      
      //files
      $this->db->queryString('select * from '.__BP.'lekcje_files where LEF_LEK_ID='.$this->var->id.' and LEF_STATUS in (1,2)');
      $files = $this->db->getDataFetch();
      foreach($files as $f)
      	$files_data.= '<div><div class="col-sm-10">'.$f['LEF_FILE'].'</div><div class="col-sm-2"><a href="#" class="del-attachment btn btn-danger btn-label-left" file="'.$f['LEF_ID'].'"><span><i class="fa fa-minus"></i></span>usuń</a></div></div>';
      
      
      //$this->db->queryString('select * from '.__BP.'lekcje_files where LEF_LEK_ID='.$this->var->id);
			//$att = $this->db->getRow();
			
			$list = array(
				'__LEK_NAME' => $lek['LEK_NAME'],
				'__LEK_CONTENT' => stripslashes($lek['LEK_CONTENT']),
				'__LEK_2_CONTENT_2' => stripslashes($lek['LEK_CONTENT_2']),
				'__LEK_3_CONTENT_3' => stripslashes($lek['LEK_CONTENT_3']),
				'__LEK_GROUP' => $lek['LEK_GROUP'],
				'__LEK_CLASS_MIN' => $lek['LEK_CLASS_MIN'],
				'__LEK_CLASS_MAX' => $lek['LEK_CLASS_MAX'],
				'__LEK_ALIAS' => $lek['LEK_ALIAS'],
				'__LEK_TIME' => $lek['LEK_TIME'],
				'__LEK_UNIT_TIME' => $lek['LEK_TIME_UNIT'],
				'__LEK_TYPE' => $lek['LEK_TYPE'],
				'__LEK_STATUS' => $lek['LEK_STATUS'],
				'__LEK_SALT' => $lek['LEK_SALT'],
				'__LEK_ID' => $lek['LEK_ID'],
				'__LEK_FILE' => $lek['LEK_FILE']==null ? null : '<img src="files/gallery/thumb_2/'.$lek['LEK_FILE'].'">',
				'__LESSON_HELPERS' => $helpers_data,
				'__LESSON_OBJECTIVES' => $objectives_data,
				'__LESSON_FILES' => $files_data,
				'__LESSON_SUBJECTS' => $this->getLessonSubjects($this->var->id),
				'__LESSON_ACCESSORIES' => $this->getLessonAccessories($this->var->id),				
				'__LESSON_AUTHOR' => $this->getLessonAuthors($lek['LEK_AUTHOR']),				
			);
			
			return Template::parse($list, file_get_contents($this->path.'edit.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString(__BP.'lekcje');
      
			$lessons = array(
				'LEK_NAME' => $this->var->LEK_NAME,
				'LEK_ALIAS' => $this->var->LEK_ALIAS,
				'LEK_CONTENT' => addslashes($this->var->LEK_CONTENT),
        'LEK_CONTENT_2' => addslashes($this->var->LEK_CONTENT_2),
        'LEK_CONTENT_3' => addslashes($this->var->LEK_CONTENT_3),
        'LEK_GROUP' => addslashes($this->var->LEK_GROUP),
        'LEK_CLASS_MIN' => $this->var->LEK_CLASS_MIN,
        'LEK_CLASS_MAX' => $this->var->LEK_CLASS_MAX,
        'LEK_TIME' => addslashes($this->var->LEK_TIME),
        'LEK_TIME_UNIT' => addslashes($this->var->LEK_TIME_UNIT),
        //'LEK_TIME_UNIT' => addslashes($this->var->LEK_TIME_UNIT),
				'LEK_TYPE' => $this->var->LEK_TYPE,
				'LEK_STATUS' => $this->var->LEK_STATUS,
				'LEK_AUTHOR' => $this->var->LEK_AUTHOR,
			);
			
			if($this->var->image_name) {
				$lessons['LEK_FILE'] = $this->var->image_name;
			}

			$this->db->updateQuery($lessons, 'LEK_ID', $this->var->id);
      
    	//attachments
    	foreach ($_FILES["attachment"]["error"] as $key => $error) {
        if ($error == UPLOAD_ERR_OK) {
          if(file_exists(LESSONS_ATTACHMENT_FILES.'/'.$this->var->salt.'/')) {
            
          } else {
            if (!mkdir(LESSONS_ATTACHMENT_FILES.'/'.$this->var->salt.'/', 0777, true)) {
              die('Failed to create folders: '.LESSONS_ATTACHMENT_FILES.'/'.$this->var->salt.'/');
            }
          }
          
          $this->db->queryString(__BP.'lekcje_files');
          $tmp_name = $_FILES["attachment"]["tmp_name"][$key];
          $name = $_FILES["attachment"]["name"][$key];
          move_uploaded_file($tmp_name, LESSONS_ATTACHMENT_FILES.'/'.$this->var->salt.'/'."$name");
          
          $insert = array('LEF_LEK_ID' => $this->var->id, 'LEF_FILE' => $name, 'LEF_LEK_SALT' => $this->var->salt);
          $this->db->insertQuery($insert);
        }
      }
      
      //subjects
      $this->db->queryString('delete from '.__BP.'lekcje_subject_id where LSI_LEK_ID='.$this->var->id);
      $this->db->execQuery();
      foreach($this->var->lesson_subject as $id=>$value) {
				$this->db->queryString(__BP.'lekcje_subject_id');
				$insert = array('LSI_LEK_ID' => $this->var->id, 'LSI_LSU_ID' => $id);
        $this->db->insertQuery($insert);
			}
			
			//accessories
			$this->db->queryString('delete from '.__BP.'lekcje_acc_id where LAI_LEK_ID='.$this->var->id);
      $this->db->execQuery();
      foreach($this->var->lesson_accessory as $id=>$value) {
				$this->db->queryString(__BP.'lekcje_acc_id');
				$insert = array('LAI_LEK_ID' => $this->var->id, 'LAI_LAC_ID' => $id);
        $this->db->insertQuery($insert);
			}
			
			//helpers
			$this->db->queryString('delete from '.__BP.'lekcje_helpers where LHE_LEK_ID='.$this->var->id);
      $this->db->execQuery();
      foreach($this->var->lesson_helpers as $id=>$value) {
				$this->db->queryString(__BP.'lekcje_helpers');
				$insert = array('LHE_LEK_ID' => $this->var->id, 'LHE_CONTENT' => $value);
        $this->db->insertQuery($insert);
        //echo $id.' '.$value.'<br/>';
			}
			
			//objectives
			$this->db->queryString('delete from '.__BP.'lekcje_objectives where LOB_LEK_ID='.$this->var->id);
      $this->db->execQuery();
			foreach($this->var->lesson_objective as $id=>$value) {
				$this->db->queryString(__BP.'lekcje_objectives');
				$insert = array('LOB_LEK_ID' => $this->var->id, 'LOB_CONTENT' => $value);
        $this->db->insertQuery($insert);
        //echo $id.' '.$value.'<br/>';
			}
			
			$this->saveLessonToFile($this->var->id);
			
			header('location: admin.php?action=lekcje');
		} else return $this->goAway();
	}
	
	public function imageLoad() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array(
				'__GAL_M_X' => __GAL_M_X+10,
				'__GAL_M_Y' => __GAL_M_Y+10,
				//'__GAL_ID' => $this->var->id,
			);
			return Template::parse($list, file_get_contents($this->path.'imageLoad.tpl.php'));
		} else return $this->goAway();
	}
	
	public function saveImageLoad() {
		/*$encodedData = str_replace(' ','+',$this->var->image);
  	$decodedData = base64_decode($encodedData);*/
  	
  	//$jpegimage = imagecreatefromjpeg(base64_encode($this->var->image)); 
  	//file_put_contents('files/gallery/test.jpg', $jpegimage);

		list($type, $this->var->image) = explode(';', $this->var->image);
		list(,$this->var->image)      = explode(',', $this->var->image);
		$data = base64_decode($this->var->image);
		
		list($type, $this->var->imageO) = explode(';', $this->var->imageO);
		list(,$this->var->imageO)      = explode(',', $this->var->imageO);
		$dataO = base64_decode($this->var->imageO);
  	
  	if($data)	{
  		$fileName = md5(microtime()).'.jpg';
  		
			//cropped image
  		file_put_contents(GALLERY_FILES_THUMB_1.$fileName, $data);
  		file_put_contents(GALLERY_FILES_FULL.$fileName, $dataO);
  		
  		//resize full image
  		$thumb = new Imagick();
			$thumb->readImage(GALLERY_FILES_FULL.$fileName);    
			$thumb->resizeImage(__GAL_B_X, __GAL_B_Y, Imagick::FILTER_LANCZOS, 1, TRUE);
			$thumb->writeImage(GALLERY_FILES_FULL.$fileName);
			$thumb->clear();
			$thumb->destroy(); 
			
			//resize second thumb
			$thumb = new Imagick();
			$thumb->readImage(GALLERY_FILES_THUMB_1.$fileName);    
			$thumb->resizeImage(__GAL_S_X, __GAL_S_Y, Imagick::FILTER_LANCZOS, 1, TRUE);
			$thumb->writeImage(GALLERY_FILES_THUMB_2.$fileName);
			$thumb->clear();
			$thumb->destroy(); 
			
			$_SESSION['image_name'] = $fileName;
  	}
  	return "<div class='galleryElement'><img src='".GALLERY_FILES_THUMB_2.$fileName."'></div><input type='hidden' name='file_name' value='".$fileName."'>";
	}		
	
	private function getLessonSubjects($id = null) {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'lekcje_subject where LSU_STATUS=1');
			$lessons = $this->db->getDataFetch();
			
			if($id==null) {
				$list = array();
			} else {
				$this->db->queryString('select LSI_LSU_ID from '.__BP.'lekcje_subject_id  where LSI_STATUS=1 and LSI_LEK_ID='.$id);
				//$this->db->query();
				$lekcje_checked = $this->db->getDataFetch();
				foreach($lekcje_checked as $lekcje_node)
					$list[] = $lekcje_node['LSI_LSU_ID'];
			}
			
			foreach($lessons as $l) {
				if(in_array($l['LSU_ID'],$list))
					$checked = 'checked';
				else $checked = null;
				
				$line.= '<div class="col-sm-3" style="background:'.$l['LSU_COLOR'].'"> <input type="checkbox" name="lesson_subject['.$l['LSU_ID'].']"'.$checked.'> '.$l['LSU_NAME'].' </div>';			
			}
			
			return $line;
			//return null;
		} else return $this->goAway();
	}
	
	private function getLessonAccessories($id = null) {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($id==null) 
				$this->db->queryString('select * from '.__BP.'lekcje_accessories');
			else //makewonder__
				$this->db->queryString('SELECT * FROM '.__BP.'lekcje_accessories left join '.__BP.'lekcje_acc_id on LAI_LAC_ID=LAC_ID where (LAI_LEK_ID='.$id.' or LAI_LEK_ID is null)');
			$acesories = $this->db->getDataFetch();
			
			foreach($acesories as $a) {
				if($a['LAI_LEK_ID']==$id)
					$checked = 'checked';
				else $checked = null;
				$line.= '<div class="col-sm-4"> <input type="checkbox" name="lesson_accessory['.$a['LAC_ID'].']"'.$checked.'> '.$a['LAC_NAME'].'<br/><img src="nauczyciele/img/accessories/'.$a['LAC_IMAGE'].'" height="100px"></div>';			
			}
			
			return $line;
		} else return $this->goAway();
	}
	
	private function saveLessonToFile($id) {
		//if($id) {
			
			$this->db->queryString('select * from '.__BP."lekcje left join ".__BP."administrator on LEK_AUTHOR=ADM_ID where LEK_ID='".$id."' and LEK_STATUS=2");
			$lekcja = $this->db->getRow();
			
			if($lekcja) {
				//subjects
				$this->db->queryString('select * from '.__BP.'lekcje_subject left join '.__BP.'lekcje_subject_id on LSI_LSU_ID=LSU_ID where LSU_STATUS=1 and LSI_LEK_ID='.$lekcja['LEK_ID']);
				$subjects = $this->db->getDataFetch();
				
				if($subjects) {
					foreach($subjects as $sub)
						$sub_data.= '<span class="label" style="background-color: '.$sub['LSU_COLOR'].';">'.$sub['LSU_NAME'].'</span>';
				} else $sub_data = null;
				
				//accesories
				$this->db->queryString('select * from '.__BP.'lekcje_accessories left join '.__BP.'lekcje_acc_id on LAI_LAC_ID=LAC_ID where LAC_STATUS=1 and LAI_LEK_ID='.$lekcja['LEK_ID']);
				$accessories = $this->db->getDataFetch();
				
				if($accessories) {
					foreach($accessories as $acc)
						$acc_data.= '<li class="ng-scope"><img src="http://www.makewonder.pl/nauczyciele/img/accessories/'.$acc['LAC_IMAGE'].'" alt="'.$acc['LAC_NAME'].'"><div class="ng-binding">'.$acc['LAC_NAME'].'</div></li>';
				} else $acc_data = null;
				
				//files
				$this->db->queryString('select * from '.__BP.'lekcje_files where LEF_STATUS=1 and LEF_LEK_ID='.$lekcja['LEK_ID']);
				$files = $this->db->getDataFetch();
				
				if($files) {
					foreach($files as $file)
						$files_data.= '<li class="ng-scope"><a href="http://www.makewonder.pl/files/attachments/lessons/'.$file['LEF_LEK_SALT'].'/'.$file['LEF_FILE'].'" target="_blank" class="ng-binding"><i class="fa fa-cloud-download"></i>'.$file['LEF_FILE'].'</a></li>';
				} else $files_data = null;
				
				//objectives
				$this->db->queryString('select * from '.__BP.'lekcje_objectives where LOB_STATUS=1 and LOB_LEK_ID='.$lekcja['LEK_ID']);
				$objectives = $this->db->getDataFetch();
				
				if($objectives) {
					foreach($objectives as $obj)
						$obj_data.= '<div>'.$obj['LOB_CONTENT'].'</div>';
				} else $obj_data = null;
				
				//helpers
				$this->db->queryString('select * from '.__BP.'lekcje_helpers where LHE_STATUS=1 and LHE_LEK_ID='.$lekcja['LEK_ID']);
				$helpers = $this->db->getDataFetch();
				
				if($helpers) {
					foreach($helpers as $h)
						$h_data.= '<div>'.$h['LHE_CONTENT'].'</div>';
				} else $h_data = null;
				
				$line = '<section class="lesson-header-section">
    <div class="container">
      <div class="col"><a class="all-lessons-link" href="scenariusze-lekcji.html"><i class="fa fa-chevron-left"></i>&nbsp; Zobacz wszystkie scenariusze</a>
        <h1 class="lesson-title ">'.$lekcja['LEK_NAME'].'</h1>';
        
        if($lekcja['LEK_AUTHOR'])
        	$line.= '<div class="lesson-author">
          <div class="author-img">
            <div class="img ng-scope" style="background-image: url(http://makewonder.pl/files/attachments/avatar/'.$lekcja['ADM_AVATAR'].');"></div>
          </div>
          <div class="author-name"><strong class="ng-binding">'.$lekcja['ADM_NAME'].' '.$lekcja['ADM_SURNAME'].'</strong></div>
        </div>';
        
      $line.= '</div><img class="lesson-main-image" src="http://makewonder.pl/files/gallery/full/'.$lekcja['LEK_FILE'].'">
    </div>
  </section>
  <section class="lesson-info-section">
    <div class="container">
      <div class="col">
        <div class="row">
          <h4>Opis</h4>
          <p class="lesson-description ">'.$lekcja['LEK_CONTENT'].'</p>
        </div>
        <div class="row">
          <h4 class="inline">Przedmioty</h4><br/>
          <div class="categories-list-n">'.$sub_data.'</div>
        </div>
        <div class="row clear">
          <div class="col-4">
            <h4>Wielkość grupy</h4>
            <p class="ng-binding">'.$lekcja['LEK_GROUP'].'</p>
          </div>
          
          <div class="col-4">
            <p class="ng-binding">&nbsp;</p>
          </div>
          
          <div class="col-4">
            <h4>Czas</h4>
            <p class="ng-binding">'.$lekcja['LEK_TIME'].'</p>
          </div>
          
          <div class="col-12">
            <h4>Klasy</h4>
            <p class="ng-binding">'.$lekcja['LEK_CLASS_MIN'].' - '.$lekcja['LEK_CLASS_MAX'].'</p>
          </div>
        </div>
      </div>
      <div class="col">
        <h4>Cele lekcji</h4>'.$lekcja['LEK_CONTENT_2'].'</div>
    </div>
  </section>
  <div class="protected-content">
    <section class="lesson-details-section">
      <div class="container">
        <div class="row">
          <div class="col">
            <h3>Co będziesz potrzebować</h3>
            <div class="row">
              <h4>Roboty i akcesoria</h4>
              <ul class="products-list">'.$acc_data.'</ul>
            </div>
            <div class="row">
              <h4 class="h4-orange">Załączniki do lekcji</h4>
              <ul class="downloads-list">'.$files_data.'</ul>
            </div>
            <div class="row">
              <h4>Potrzebne materiały</h4>'.$h_data.'</div>
          </div>
          <div class="col">
            <h3>Realizowane obszary podstawy programowej</h3>
            <div class="ng-isolate-scope"><div class="ng-binding">'.$obj_data.'</div></div>
          </div>
        </div>
      </div>
    </section>
    <section class="lesson-sections-section">
      <div class="container">
        <div class="row">
          <div class=""><div class="ng-binding">'.$lekcja['LEK_CONTENT_3'].'</div></div>
        </div>
      </div>
    </section>
  </div>';
  			file_put_contents('files/attachments/lessons/'.$lekcja['LEK_ALIAS'].'.php', stripslashes($line));
			}
	//	}
	}
	
	private function getLessonAuthors($id=null) {
		$this->db->queryString('select ADM_ID, ADM_NAME, ADM_SURNAME, ADM_AVATAR from '.__BP.'administrator where ADM_AGENCY="A"');
		$admins = $this->db->getDataFetch();
		$select = new select('LEK_AUTHOR', 'LEK_AUTHOR');
		
		foreach($admins as $adm) {
			if($id==$adm['ADM_ID'])
				$select->addNode($adm['ADM_NAME'].' '.$adm['ADM_SURNAME'], $adm['ADM_ID'], 'selected');
			else $select->addNode($adm['ADM_NAME'].' '.$adm['ADM_SURNAME'], $adm['ADM_ID']);
		}
		return $select->create();
	}
	
	public function delAttachment() {
		if($this->var->file) {
			$this->db->queryString(__BP.'lekcje_files');
			$this->db->updateQuery(array('LEF_STATUS'=>'deleted'), 'LEF_ID', $this->var->file);
			return 'ok';
		} else return 'false';
	}
}

function lekcje($option=null) {
	$lekcje = new lekcje;
	if($option!=null and method_exists($lekcje, $option))
		return $lekcje->$option();
	else 
		return $lekcje->getPage();
}