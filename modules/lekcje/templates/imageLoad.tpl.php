<style>
      .cropit-image-preview {
        background-color: #f8f8f8;
        background-size: cover;
        border: 5px solid #ccc;
        border-radius: 3px;
        margin: 50px 0;
        width:__GAL_M_Xpx;
        height: __GAL_M_Ypx;
        cursor: move;
        left: 25%;
      }

      .cropit-image-background {
        opacity: .2;
        cursor: auto;
      }

      .image-size-label {
        margin: 20px 0 0 0;
      }

      input {
        position: relative;
        z-index: 10;
        display: block;
      }

    </style>

    <div class="image-editor">
    	<div class="qq-upload-button-selector qq-upload-button-a btn btn-success" style="width: auto; position: relative; overflow: hidden; direction: ltr;">
					<div><i class="fa fa-upload icon-white"></i> Załaduj zdjęcie</div>
				<input class="cropit-image-input" qq-button-id="659515d4-a3c6-4dca-91d2-e6c3bfb4151d" multiple="" type="file" name="qqfile" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
      
      <div class="cropit-image-preview-container">
        <div class="cropit-image-preview"></div>
      </div>
      <div class="image-size-label">Zmień rozmiar</div>
      <input type="range" class="cropit-image-zoom-input">

    </div>
    
    <div id="cropit-buttons-container">
    
    </div>
    <p>&nbsp;</p>
    <div class="form-group">
			<div class="col-sm-9">
				<div class="buttons">
					<button type="button" id="export" class="btn btn-primary">Zapisz</button>
					<button type="button" class="btn btn-secondary lightClose">Anuluj</button>
				</div>
				<div class="spinner" style="display: none;"><i class="fa fa-refresh fa-spin"></i> ładuję ...</div>
			</div>
		</div>

    <script>
      $(function() {
        $('.image-editor').cropit({
          exportZoom: 1,
          imageBackground: true,
          imageBackgroundBorderWidth: 90,
          imageState: {
            src: 'files/attachments/empty.png'
          }
        });

        $('#export').click(function() {
        	$('.buttons').css('display', 'none');
        	$('.spinner').css('display', 'block');
          var imageData = $('.image-editor').cropit('export', {type: 'image/jpeg', quality: 10});
          var imageOriginal = $('.image-editor').cropit('imageSrc');
          //var imageOriginal = $('.image-editor').cropit('export');
          
          //window.open(imageData);
          $.post("request.php",{action: 'lekcje', option: 'saveImageLoad', image: imageData, imageO: imageOriginal})
					.done(function(response) {
						$('#galleryContainer').html(response);
						$(this).lightClose();
					});
        });
      });
    </script>