<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=lekcje">Lekcje</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-puzzle-piece"></i>
					<span>edycja pozycji</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<h4 class="page-header">Edycja pozycji</h4>
        <form name="adminEdit" action="admin.php" method="post" class="form-horizontal" enctype="multipart/form-data">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="LEK_NAME" value="__LEK_NAME"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Opis</label>
					<div class="col-sm-9">
						<textarea name="LEK_CONTENT" id="LEK_CONTENT" class="article_content_tbl">__LEK_CONTENT</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Podstawa edukacyjna</label>
					<div class="col-sm-9">
						<textarea name="LEK_CONTENT_2" id="LEK_CONTENT_2" class="article_content_tbl">__LEK_CONTENT_2</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Przebieg lekcji</label>
					<div class="col-sm-9">
						<textarea name="LEK_CONTENT_3" id="LEK_CONTENT_3" class="article_content_tbl">__LEK_CONTENT_3</textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Przedmioty</label>
					<div class="col-sm-9">
						<div class="well">
							<div class="row">
								__LESSON_SUBJECTS
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Wielkość grupy</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="LEK_GROUP" value="__LEK_GROUP"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Klasy</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="LEK_CLASS" value="__LEK_CLASS"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Czas</label>
					<div class="col-sm-1">
						<input type="text" class="form-control" name="LEK_TIME" value="__LEK_TIME"/>
					</div>
					
					<div class="col-sm-2">
						<select name="LEK_TIME_UNIT" class="form-control" id="LEK_TIME_UNIT"><option value=''>brak</option><option value='minut'>minut</option><option value='h'>h</option><option value='dni'>dni</option></select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Akcesoria</label>
					<div class="col-sm-9">
						<div class="well">
							<div class="row">
								__LESSON_ACCESSORIES
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Pomoce</label>
					<div class="col-sm-9">
						<div id="helper-container" class="row">
							__LESSON_HELPERS
						</div>
						<p><br/><a href="#" class="add-helper btn btn-success btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj</a></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Cele lekcji</label>
					<div class="col-sm-9">
						<div id="subject-container" class="row">
							__LESSON__OBJECTIVES
						</div>
						<p><br/><a href="#" class="add-subject btn btn-success btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj</a></p>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Załaduj miniaturkę</label>
					<div class="col-sm-9">
						<a href="request.php?action=lekcje&option=imageLoad" class="light btn btn-success btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj</a>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<div class="col-sm-9"><div id="galleryContainer"></div></div>
				</div>
				
        <div class="form-group">
					<label class="col-sm-3 control-label">Dodaj załączniki</label>
					<div class="col-sm-9">
              <p><input type="file" name="attachment[]" class="lesson_attachment"></p>
					</div>
          
          <label class="col-sm-3 control-label">&nbsp;</label><div class="col-sm-9" id="attachments_container"></div>
				</div>
            
				<div class="form-group">
					<label class="col-sm-3 control-label">Typ </label>
					<div class="col-sm-5">
						<select name="LEK_TYPE" id="LEK_TYPE">
							<option value="1">darmowe</option>
							<option value="2">premium</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Status </label>
					<div class="col-sm-5">
						<select name="LEK_STATUS" id="LEK_STATUS">
							<option value="1">nowe</option>
							<option value="2">opublikowane</option>
							<option value="3">usunięte</option>
						</select>
					</div>
				</div>
				
				<input type="hidden" name="action" value="lekcje">
				<input type="hidden" name="option" value="addSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
			</div>
		</div>
	</div>
</div>