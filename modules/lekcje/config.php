<?php
$module_info['title'] = 'Lekcje';
$module_info['description'] = 'Lekcje - tworzenie i zarządzanie';
$module_info['author'] = 'expansja';

$menu['title'][] = 'Lekcje';
$menu['action'][] = 'lekcje';
$menu['icon'][] = 'fa fa-puzzle-piece';

$menu_show['menu'] = true;