<?php
class lekcje extends View {
	public $path = 'modules/lekcje/templates/';
	public function getPage() {
		return null;
	}
	
	public function getLesson() {
		if(file_exists('../files/attachments/lessons/'.$this->var->id.'.php')) {
			return file_get_contents('../files/attachments/lessons/'.$this->var->id.'.php');
		} else return $this->getLessonTemp();
	}
	
	public function getLessonTemp() {
		if($this->var->id) {
			$this->db->queryString('select * from '.__BP."lekcje where LEK_ALIAS='".$this->var->id."' and LEK_STATUS=2");
			$lekcja = $this->db->getRow();
			
			if($lekcja) {
				//subjects
				$this->db->queryString('select * from '.__BP.'lekcje_subject left join '.__BP.'lekcje_subject_id on LSI_LSU_ID=LSU_ID where LSU_STATUS=1 and LSI_LEK_ID='.$lekcja['LEK_ID']);
				$subjects = $this->db->getDataFetch();
				
				if($subjects) {
					foreach($subjects as $sub)
						$sub_data.= '<span class="label" style="background-color: '.$sub['LSU_COLOR'].';">'.$sub['LSU_NAME'].'</span>';
				} else $sub_data = null;
				
				//accesories
				$this->db->queryString('select * from '.__BP.'lekcje_accessories left join '.__BP.'lekcje_acc_id on LAI_LAC_ID=LAC_ID where LAC_STATUS=1 and LAI_LEK_ID='.$lekcja['LEK_ID']);
				$accessories = $this->db->getDataFetch();
				
				if($accessories) {
					foreach($accessories as $acc)
						$acc_data.= '<li class="ng-scope"><img src="http://www.makewonder.pl/nauczyciele/img/accessories/'.$acc['LAC_IMAGE'].'" alt="'.$acc['LAC_NAME'].'"><div class="ng-binding">'.$acc['LAC_NAME'].'</div></li>';
				} else $acc_data = null;
				
				//files
				$this->db->queryString('select * from '.__BP.'lekcje_files where LEF_STATUS=1 and LEF_LEK_ID='.$lekcja['LEK_ID']);
				$files = $this->db->getDataFetch();
				
				if($files) {
					foreach($files as $file)
						$files_data.= '<li class="ng-scope"><a href="http://www.makewonder.pl/files/attachments/lessons/'.$file['LEF_LEK_SALT'].'/'.$file['LEF_FILE'].'" target="_blank" class="ng-binding"><i class="fa fa-cloud-download"></i>'.$file['LEF_FILE'].'</a></li>';
				} else $files_data = null;
				
				//objectives
				$this->db->queryString('select * from '.__BP.'lekcje_objectives where LOB_STATUS=1 and LOB_LEK_ID='.$lekcja['LEK_ID']);
				$objectives = $this->db->getDataFetch();
				
				if($objectives) {
					foreach($objectives as $obj)
						$obj_data.= '<div>'.$obj['LOB_CONTENT'].'</div>';
				} else $obj_data = null;
				
				//helpers
				$this->db->queryString('select * from '.__BP.'lekcje_helpers where LHE_STATUS=1 and LHE_LEK_ID='.$lekcja['LEK_ID']);
				$helpers = $this->db->getDataFetch();
				
				if($helpers) {
					foreach($helpers as $h)
						$h_data.= '<div>'.$h['LHE_CONTENT'].'</div>';
				} else $h_data = null;
				
				$line = '<section class="lesson-header-section">
    <div class="container">
      <div class="col"><a class="all-lessons-link" href="scenariusze-lekcji.html"><i class="fa fa-chevron-left"></i>&nbsp; Zobacz wszystkie scenariusze</a>
        <h1 class="lesson-title ">'.$lekcja['LEK_NAME'].'</h1>';
        
        if($lekcja['LEK_AUTHOR'])
        	$line.= '<div class="lesson-author">
          <div class="author-img">
            <div class="img ng-scope" style="background-image: url(http://makewonder.pl/files/attachments/avatar/'.$lekcja['ADM_AVATAR'].');"></div>
          </div>
          <div class="author-name"><strong class="ng-binding">'.$lekcja['ADM_NAME'].' '.$lekcja['ADM_SURNAME'].'</strong></div>
        </div>';
        
      $line.= '</div><img class="lesson-main-image" src="http://makewonder.pl/files/gallery/full/'.$lekcja['LEK_FILE'].'">
    </div>
  </section>
  <section class="lesson-info-section">
    <div class="container">
      <div class="col">
        <div class="row">
          <h4>Opis</h4>
          <p class="lesson-description ">'.$lekcja['LEK_CONTENT'].'</p>
        </div>
        <div class="row">
          <h4 class="inline">Przedmioty</h4><br/>
          <div class="categories-list-n">'.$sub_data.'</div>
        </div>
        <div class="row clear">
          <div class="col-4">
            <h4>Wielkość grupy</h4>
            <p class="ng-binding">'.$lekcja['LEK_GROUP'].'</p>
          </div>
          <div class="col-4">
            <h4>Klasy</h4>
            <p class="ng-binding">'.$lekcja['LEK_CLASS_MIN'].' - '.$lekcja['LEK_CLASS_MAX'].'</p>
          </div>
          <div class="col-4">
            <h4>Czas</h4>
            <p class="ng-binding">'.$lekcja['LEK_TIME'].'</p>
          </div>
        </div>
      </div>
      <div class="col">
        <h4>Cele lekcji</h4>'.$lekcja['LEK_CONTENT_2'].'</div>
    </div>
  </section>
  <div class="protected-content">
    <section class="lesson-details-section">
      <div class="container">
        <div class="row">
          <div class="col">
            <h3>Co będziesz potrzebować</h3>
            <div class="row">
              <h4>Roboty i akcesoria</h4>
              <ul class="products-list">'.$acc_data.'</ul>
            </div>
            <div class="row">
              <h4 class="h4-orange">Załączniki do lekcji</h4>
              <ul class="downloads-list">'.$files_data.'</ul>
            </div>
            <div class="row">
              <h4>Potrzebne materiały</h4>'.$h_data.'</div>
          </div>
          <div class="col">
            <h3>Realizowane obszary podstawy programowej</h3>
            <div class="ng-isolate-scope"><div class="ng-binding">'.$obj_data.'</div></div>
          </div>
        </div>
      </div>
    </section>
    <section class="lesson-sections-section">
      <div class="container">
        <div class="row">
          <div class=""><div class="ng-binding">'.$lekcja['LEK_CONTENT_3'].'</div></div>
        </div>
      </div>
    </section>
  </div>';
  			return $line;
			} else return $this->pageError();
		}
		
	}
	
	public function getAll() {
		$this->db->queryString('select LEK_ALIAS, LEK_FILE, LEK_NAME, LEK_AUTHOR, LEK_CLASS_MIN, LEK_CLASS_MAX, ADM_NAME, ADM_SURNAME, ADM_AVATAR from '.__BP.'lekcje left join '.__BP.'administrator on LEK_AUTHOR=ADM_ID where LEK_STATUS=2');
		//$this->db->query();
		$lekcje = $this->db->getDataFetch();
		
		if($lekcje) {
			foreach($lekcje as $l) {
				$line.='<a class="lesson-tile" href="scenariusz-'.$l['LEK_ALIAS'].'.html"><div class="lesson-tile-image" style="background-image: url(http://makewonder.pl/files/gallery/thumb_1/'.$l['LEK_FILE'].');"> <!--<div class="lesson-categories-list"><span class="label label-premium"><i class="fa fa-lock"></i>Premium</span></div>--></div>
  <div class="lesson-tile-bottom">
    <div class="lesson-title-wrap">
      <div class="lesson-title"> 
        <h4 class="ng-binding">'.$l['LEK_NAME'].'</h4>
        <p class="ng-binding"></p>
      </div>
    </div>
    <div class="lesson-author">';
        if($l['LEK_AUTHOR']!=0) {
					$line.='<div class="author-img">
        		<div class="img ng-scope" style="background-image: url(http://makewonder.pl/files/attachments/avatar/'.$l['ADM_AVATAR'].');"></div>
      		</div>
      <div class="author-info">
        <div class="author-name"><span></span><strong class="ng-binding">'.$l['ADM_NAME'].' '.$l['ADM_SURNAME'].'</strong></div>
        <div class="author-grades">Klasy <span class="ng-binding">'.$l['LEK_CLASS_MIN'].' - '.$l['LEK_CLASS_MAX'].'</span></div>';
				} else $line.='<div class="author-info"><div class="author-grades">Klasy <span class="ng-binding">'.$l['LEK_CLASS_MIN'].' - '.$l['LEK_CLASS_MAX'].'</span></div>';
    		$line.='</div>
    </div>
  </div></a>';
  		}
		}
		return $line;
	}

}

function lekcje($option=null) {
	$lekcje = new lekcje;
	if($option!=null and method_exists($lekcje, $option))
		return $lekcje->$option();
	else 
		return $lekcje->getPage();
}