<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=options&option=system">Konfiguracja systemu</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Zainstalowane moduły</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">
					__MODULE_TABLE
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Opcje</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">
				<div id="contentPlace">
					<p><a href="modules/options/templates/info.php" target="_blank">Konfiguracja serwera</a></p>
					<p><a href="?action=installator">Bazy i tabele</a></p>
					<p><a href="?action=options&option=productsTypes">Typy w produktach</a></p>
					<p><a href="?action=options&option=productsUnits">Jednostki w produktach</a></p>
				</div>
			</div>
		</div>
	</div>
</div>