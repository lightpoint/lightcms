<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=options&option=system">Konfiguracja</a></li>
			<li><a href="?action=options&option=productsTypes">Typy produktów</a></li>
			<li><a href="?action=options&option=productsTypesEdit">Edycja pozycji</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Edycja pozycji</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">		
				<h4 class="page-header">DANE</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Nazwa</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="PRT_NAME" value="__PRT_NAME"/>
					</div>
				</div>
				
				<input type="hidden" name="action" value="options">
				<input type="hidden" name="option" value="productsTypesEditSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="submit" class="btn btn-secondary">Anuluj</button>
					</div>
				</div>
				</form>	
			
		</div>
		<div class="box-content">		
			<h4 class="page-header">PARAMETRY</h4>
			<div id="optionsBar"><a href="?action=options&option=productsTypesParametrAdd&id=__PRT_ID" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nowy parametr</a></div>
				<div id="contentPlace">
					__MODULE
				</div>	
			</div>
		</div>
	</div>
</div>