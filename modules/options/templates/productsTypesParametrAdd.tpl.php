<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=options&option=system">Konfiguracja</a></li>
			<li><a href="?action=options&option=productsTypes">Typy produktów</a></li>
			<li><a href="?action=options&option=productsTypesEdit">Edycja pozycji</a></li>
			<li><a href="?action=options&option=productsTypesParametrAdd">Nowy parametr</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-cog"></i>
					<span>Nowy parametr</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-content">		
				<h4 class="page-header">Dane</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Nazwa</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="PTP_NAME"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Wartość</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="PTP_VALUE"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-3 control-label">Modyfikator*</label>
						<div class="col-sm-5">
							<textarea name="PTP_MODIFIER" class="form-control"></textarea>
						</div>
					</div>
					<input type="hidden" name="action" value="options">
					<input type="hidden" name="option" value="productsTypesParametrAddSave">
					<input type="hidden" name="PRT_ID" value="__PRT_ID">
					<div class="form-group">
						<div class="col-sm-9 col-sm-offset-3">
							<button type="submit" class="btn btn-primary">Zapisz</button>
							<button type="submit" class="btn btn-secondary">Anuluj</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>