<?php
class Options extends View {
	public $path = 'modules/options/templates/';
	public function getList() {
		$list = array('__MODULE_TABLE'=>$this->getModulesForDashboard());
		return Template::parse($list, file_get_contents($this->path.'view.tpl.php'));
	}
	
	public function profile() {
		$this->db->queryString('select * from '.__BP.'administrator where ADM_ID='.$this->var->admin_id);
		$admin = $this->db->getRow();
		
		$list = array(
			'__ADM_NAME'=>$admin['ADM_NAME'],
			'__ADM_SURNAME'=>$admin['ADM_SURNAME'],
			'__ADM_MAIL'=>$admin['ADM_MAIL'],
			'__ADM_LOGIN'=>$admin['ADM_LOGIN'],
		);
		return Template::parse($list, file_get_contents($this->path.'profile.tpl.php'));
	}
	
	public function getModulesForDashboard() {
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
		unset($list);
		
		foreach($catalogs as $module) {
			unset($menu);
			unset($dashboard);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if(isset($dashboard)) {
		    	$list[$menu['action'][0]] = array(
							'action' => $menu['action'][0],
							'title' => $menu['title'][0],
						);
		    }
		  }
		}
		
		//get modules for list
		$this->db->queryString("select ADD_ACTION, ADD_ORDER from ".__BP."admin_dashboard where ADD_ADM_ID=".$this->var->admin_id);
		$cad = $this->db->getDataFetch();
		if($cad) {
			foreach($cad as $c)
				$cad_list[$c['ADD_ACTION']] = $c['ADD_ORDER'];
		} else $cad_list = array();
		
		//get privileges for admin
		$this->db->queryString("select ADR_ROLE_ID from ".__BP."admin_roles where ADR_ADM_ID=".$this->var->admin_id);
		$car = $this->db->getDataFetch();
		if($car) {
			foreach($car as $cr)
				$car_list[] = $cr['ADR_ROLE_ID'];
		} else $car_list = array();
		
		$i = 1;
		$table = new table('myTable', 'tablesorter');
		$tableHeader = array('lp.', 'Moduł', 'Włączony', 'Pozycja', '');
		$table->createHeader($tableHeader);
		
		foreach($list as $l) {
			if(in_array($l['action'], $car_list) or $this->var->admin_role=='admin') {
				
				if(array_key_exists($l['action'], $cad_list))
					$bold = $cad_list[$l['action']];
				else $bold = null;
				
				$table->addCell($i, 'lp');
				$table->addCell($l['title'], $bold!=null ? 'td_bold' : '');
				$table->addCell($bold!=null ? 'tak' : 'nie', $bold!=null ? 'td_bold' : '');
				$table->addCell($bold!=null ? "<input type='text' name='m_order[".$l['action']."]' value='".$bold."'>" : '');
				$table->addCell($bold!=null ? "<input type='checkbox' name='m_action[".$l['action']."]' checked='checked'>" : "<input type='checkbox' name='m_action[".$l['action']."]'>");
				$table->addRow();
				$i++;
			}
		}
		
		return $table->createBody();
	}
	
	public function moduleSave() {
		$this->db->queryString("delete from ".__BP."admin_dashboard where ADD_ADM_ID=".$this->var->admin_id);
		$this->db->execQuery();
		
		if($this->var->m_action) {
			foreach($this->var->m_action as $key=>$value) {
				$list = array(
					'ADD_ADM_ID'=>$this->var->admin_id,
					'ADD_ACTION'=>$key,
					'ADD_ORDER'=>$this->var->m_order[$key]
				);
				$this->db->queryString(__BP.'admin_dashboard');
				$this->db->insertQuery($list);
				unset($list);
			}
		}
		header("location: admin.php?action=options");
	}
	
	public function profileSave() {
		if($this->var->ADM_NAME) {
			$update = array(
				'ADM_NAME'=>$this->var->ADM_NAME,
				'ADM_SURNAME'=>$this->var->ADM_SURNAME,
				'ADM_MAIL'=>$this->var->ADM_MAIL,
			);
			$this->db->queryString(__BP.'administrator');
			$this->db->updateQuery($update, 'ADM_ID', $this->var->admin_id);	
		}
		
		header("location: admin.php");
	}
	
	public function passwordChange() {
		if($this->var->ADM_PASS1 and $this->var->ADM_PASS2) {
			$update = array('ADM_PASS'=>coder::hashString($this->var->ADM_PASS1), 'ADM_LOGIN' => $this->var->ADM_LOGIN);
			
			$this->db->queryString(__BP.'administrator');
			$this->db->updateQuery($update, 'ADM_ID', $this->var->admin_id);
			header("location: logout.php");
		}
	}
	
	public function getInstalledModules() {
		$handle = opendir('modules');
		$catalogs = '';
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
	
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists("modules/".$module."/config.php"))  {
		    include("modules/".$module."/config.php");
		    if(isset($menu)) {
		    	$list = array(
							'__AUTHOR' => $module_info['author'],
							'__TEXT' => $module_info['description'],
							'__CLASS_ICON' => $menu['icon'][0],
							'__TITLE' => $module_info['title'],
							'__DASHBOARD' => null,
						);
					 
					 //$result[$menu['order'][0]] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 $result[] = Template::parse($list, file_get_contents($this->path.'pos.tpl.php'));
					 unset($list);
		    }
		  }
		}
		
		ksort($result);
		foreach($result as $key=>$value)
			$line.=$value;
		return $line;
	}
	
	public function system() {
		/*ob_start();
		phpinfo();
		$variable = ob_get_contents();
		ob_get_clean();*/

		if($this->var->admin_role=='admin') {
			$list = array(
				'__MODULE_TABLE' => $this->getInstalledModules(),
				'__PHP_INFO' => $variable,
			);
			return Template::parse($list, file_get_contents($this->path.'system.tpl.php'));
		} else return $this->goAway();
	}
	
	/**
	* configurations for PRODUCT TYPES
	*/
	public function productsTypes() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString('select * from '.__BP.'products_types');
			$types = $this->db->getDataFetch();
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Status', '', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
			foreach($types as $prt) {
				$table->addCell($i, 'lp');
				$table->addCell($prt['PRT_NAME']);
				$table->addCell($prt['PRT_STATUS']);
				$table->addCell();
				$table->addLink('admin.php?action=options&option=productsTypesEdit&id='.$prt['PRT_ID'], 'edycja',null, null, 'edycja');
				$table->addRow();
				$i++;
			}
			
			$list = array(
				'__MODULE' => $table->createBody(),
			);
			return Template::parse($list, file_get_contents($this->path.'productTypes.tpl.php'));
		} else return $this->goAway();
	}
	
	public function productsTypesAdd() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			
			$list = array(
				
			);
			return Template::parse($list, file_get_contents($this->path.'productsTypesAdd.tpl.php'));
		} else return $this->goAway();
	}
	
	public function productsTypesAddSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->PRT_NAME) {
				$this->db->queryString(__BP.'products_types');
				$prt = $this->db->insertQuery(array('PRT_NAME' => $this->var->PRT_NAME));
				
				header('location: ?action=options&option=productsTypesEdit&id='.$prt);
			} else header('location: ?action=options&option=productsTypes');
		} else return $this->goAway();
	}
	
	public function productsTypesEdit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString("select * from ".__BP."products_types where PRT_STATUS=1 and PRT_ID=".$this->var->id);
			$types = $this->db->getRow();
			
			if($types) {
				$this->db->queryString("select * from ".__BP."products_types_pos where PTP_STATUS=1 and PTP_PRT_ID=".$this->var->id);
				$parameters = $this->db->getDataFetch();
				
				$table = new table('myTable', 'tablesorter');
				$tableHeader = array('lp.', 'Nazwa', 'Wartość', 'Status', '', '');
				$table->createHeader($tableHeader);
				
				$i = 1;
				foreach($parameters as $prt) {
					$table->addCell($i, 'lp');
					$table->addCell($prt['PTP_NAME']);
					$table->addCell($prt['PTP_VALUE']);
					$table->addCell($prt['PTP_STATUS']);
					$table->addCell();
					$table->addLink('admin.php?action=options&option=productsTypesParametrEdit&id='.$prt['PTP_ID'], 'edycja',null, null, 'edycja');
					$table->addRow();
					$i++;
				}
				
				$list = array(
					'__PRT_NAME' => $types['PRT_NAME'],
					'__PRT_ID' => $types['PRT_ID'],
					'__MODULE' => $table->createBody(),
				);
				return Template::parse($list, file_get_contents($this->path.'productsTypesEdit.tpl.php'));
			}
		} else return $this->goAway();
	}
	
	public function productsTypesParametrAdd() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
				$list = array('__PRT_ID' => $this->var->id);
				return Template::parse($list, file_get_contents($this->path.'productsTypesParametrAdd.tpl.php'));
			
		} else return $this->goAway();
	}
	
	public function productsTypesParametrAddSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->PTP_VALUE) {
				$insert = array(
					'PTP_NAME' => $this->var->PTP_NAME,
					'PTP_VALUE' => $this->var->PTP_VALUE,
					'PTP_MODIFIER' => $this->var->PTP_MODIFIER,
					'PTP_PRT_ID' => $this->var->PRT_ID,
				);
				
				$this->db->queryString(__BP."products_types_pos");
				$this->db->insertQuery($insert);
				
				header('location: ?action=options&option=productsTypesEdit&id='.$this->var->PRT_ID);
			}
		} else return $this->goAway();
	}
	
	public function productsTypesParametrEdit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$this->db->queryString("select * from ".__BP."products_types_pos where PTP_STATUS=1 and PTP_ID=".$this->var->id);
				$ptp = $this->db->getRow();
				
				$list = array(
					'__PRT_ID' => $this->var->id,
					'__PTP_NAME' => $ptp['PTP_NAME'],
					'__PTP_VALUE' => $ptp['PTP_VALUE'],
					'__PTP_MODIFIER' => $ptp['PTP_MODIFIER'],
				);
				return Template::parse($list, file_get_contents($this->path.'productsTypesParametrEdit.tpl.php'));
			}
		} else return $this->goAway();
	}
	
	public function productsTypesParametrEditSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->PTP_NAME) {
				$this->db->queryString(__BP.'products_type_pos');
				$update = array(
					'PTP_NAME' => $this->var->PTP_NAME,
					'PTP_VALUE' => $this->var->PTP_VALUE,
					'PTP_STATUS' => $this->var->PTP_STATUS,
				);
				
				$this->db->updateQuery($update, 'PTP_ID', $this->var->PTP_ID);
				
				header('location: ?action=options&option=productsTypesEdit&id=');
			}
		} else return $this->goAway();
	}
	
	public function deleteParametr() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$this->db->queryString("update ".__BP."products_types_pos set PTP_STATUS=2 where PTP_ID=".$this->var->id);
				$this->db->execQuery();
				
				header('location: ?action=options&option=productsTypesEdit&id=');
			}
		} else return $this->goAway();
	}
	
	public function deleteType() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$this->db->queryString("update ".__BP."products_types set PRT_STATUS=2 where PRT_ID=".$this->var->id);
				$this->db->execQuery();
				
				header('location: ?action=options&option=productsTypes');
			}
		} else return $this->goAway();
	}
	
	public function productsUnits() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString("select * from ".__BP."units where UNI_STATUS=1");
			$units = $this->db->getDataFetch();
			$i = 1;
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa', 'Jednostka', 'Waga', '', '');
			$table->createHeader($tableHeader);
			
			if($units) {
				foreach($units as $unit) {					
					$table->addCell($i, 'lp');
					$table->addCell($unit['UNI_NAME']);
					$table->addCell($unit['UNI_ALIAS']);
					$table->addCell($unit['UNI_WEIGHT']);
					$table->addLink('admin.php?action=options&option=productsUnitEdit&id='.$unit['UNI_ID'], 'edycja',null, null, 'edycja');
					$table->addLink('admin.php?action=options&option=productsUnitDelt&id='.$unit['UNI_ID'], 'usuń',null, null, 'usuń');
					$table->addRow();
					$i++;
				}
			}
			
			$list = array(
				'__MODULE' => $table->createBody()
			);
			
			return Template::parse($list, file_get_contents($this->path.'productsUnits.tpl.php'));
		} else return $this->goAway();
	}
	
	public function productsUnitAdd() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array();
			return Template::parse($list, file_get_contents($this->path.'productsUnitsAdd.tpl.php'));
		} else return $this->goAway();
	}
	
	public function productsUnitAddSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->UNI_NAME) {
				$insert = array(
					'UNI_NAME' => $this->var->UNI_NAME,
					'UNI_ALIAS' => $this->var->UNI_ALIAS,
					'UNI_WEIGHT' => $this->var->UNI_WEIGHT,
				);
				$this->db->queryString(__BP.'units');
				$this->db->insertQuery($insert);
				
				header('location: ?action=options&option=productsUnits');
			}
		} else return $this->goAway();
	}
	
	public function productsUnitEdit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString("select * from ".__BP."units where UNI_STATUS=1 and UNI_ID=".$this->var->id);
			$unit = $this->db->getRow();
			$list = array(
				'__UNI_NAME' => $unit['UNI_NAME'],
				'__UNI_ALIAS' => $unit['UNI_ALIAS'],
				'__UNI_WEIGHT' => $unit['UNI_WEIGHT'],
				'__UNI_ID' => $unit['UNI_ID'],
			);
			return Template::parse($list, file_get_contents($this->path.'productsUnitsAdd.tpl.php'));
		} else return $this->goAway();
	}
	
	public function productsUnitEditSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$update = array(
					'UNI_NAME' => $this->var->UNI_NAME,
					'UNI_ALIAS' => $this->var->UNI_ALIAS,
					'UNI_WEIGHT' => $this->var->UNI_WEIGHT
				);
			
				$this->db->queryString(__BP.'units');
				$this->db->updateQuery($update, 'UNI_ID', $this->var->id);
			}
			
			header('location: ?action=options&option=productsUnits');
		} else return $this->goAway();
	}
	
	public function productsUnitDelete() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {			
				$this->db->queryString('delete from '.__BP.'units where UNI_ID='.$this->var->id);
				$this->db->execQuery();
			}
			
			header('location: ?action=options&option=productsUnits');
		} else return $this->goAway();
	}
}

function options($option=null) {
	$options = new Options;
	if($option!=null and method_exists($options, $option))
		return $options->$option();
	else 
		return $options->getList();
}
?>