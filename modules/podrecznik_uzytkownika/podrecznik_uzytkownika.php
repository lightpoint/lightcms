<?php
class Podrecznik extends View {
	public $artPath = 'files/articles/'; //path to articles
	
	public function getData() {
		return file_get_contents($this->artPath.$this->var->id.'.php');
	}
	
}

function podrecznik_uzytkownika($option=null) {
	$podrecznik = new Podrecznik;
	if($option!=null and method_exists($podrecznik, $option))
		return $podrecznik->$option();
	else 
		return null;
}