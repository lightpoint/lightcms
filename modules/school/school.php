<?php
class school extends View {
	public $path = '../modules/school/templates/';
	public function getPage() {
		
	}
	
	public function schoolForm() {
		$list = array('__ROLE' => $this->getRole(), '__HEARD' => $this->getHeard());
		return Template::parse($list, file_get_contents($this->path.'form.tpl.php'));
	}
	
	private function getRole() {
		$status = array('nauczyciel', 'integrator', 'specjalista', 'dyrektor szkoły', 'animator zajęć', 'dyrektor okręgowy', 'inny');
		$select = new select('role', 'role');
		$select->addNode('funkcja',null);
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();		
	}
	
	private function getHeard() {
		$status = array('od znajomego', 'profil facebook', 'informacja na blogu', 'na konferencji', 'email', 'inne');
		$select = new select('heard', 'heard');
		$select->addNode('Skąd się o nas dowiedziałeś?',null);
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();		
	}
	
	public function save() {
		$this->db->queryString('makewonder__school');
		$insert = array(
			'SCH_NAME' => $this->var->name,
			'SCH_LAST_NAME' => $this->var->last_name,
			'SCH_EMAIL' => $this->var->email,
			'SCH_SCHOOL' => $this->var->school,
			'SCH_WOJEWODZTWO' => $this->var->district,
			'SCH_FUNCTION' => $this->var->role,
			'SCH_HEARD' => $this->var->heard,
			'SCH_PHONE' => $this->var->phoneNumber,
		);
		$this->db->insertQuery($insert);
		
		header('location: dziekujemy-za-rejetracje.html');
	}
}

function school($option=null) {
	$school = new school;
	if($option!=null and method_exists($school, $option))
		return $school->$option();
	else 
		return $school->getPage();
}