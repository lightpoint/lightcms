<?php
class school extends View {
	public $path = 'modules/school/templates/';
	public function getPage() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$this->db->queryString("select * from ".__BP."school order by SCH_DATE_ADD");
			
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Data dodania', 'Imię i nazwisko', 'Szkoła', 'Województwo', 'Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
			$schools = $this->db->getDataFetch();
			
			foreach($schools as $sch) {
				$table->addCell($i, 'lp');
				$table->addCell($sch['SCH_DATE_ADD']);
				$table->addCell($sch['SCH_NAME'].' '.$sch['SCH_LAST_NAME']);
				$table->addCell($sch['SCH_SCHOOL']);
				$table->addCell($sch['SCH_WOJEWODZTWO']);
				$table->addCell($sch['SCH_STATUS']);
				
				$table->addLink('?action=school&option=edit&id='.$sch['SCH_ID'], 'szczegóły');
				$table->addRow();
				$i++;
				
			}
			$tableBody = $table->createBody();
			
			$list = array('__TABLE' => $tableBody);
			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}

}

function school($option=null) {
	$school = new school;
	if($option!=null and method_exists($school, $option))
		return $school->$option();
	else 
		return $school->getPage();
}