<div id="modal1" class="modal">
	<div class="modal-content">
		<div class="row">
    <div class="col s12">
      <ul class="tabs">
        <li class="tab col s3"><a href="#test1">Zaloguj się </a></li>
        <li class="tab col s3"><a class="active" href="#test2">Zarejestruj się</a></li>
			</ul>
    </div>
		<div id="test1" class="col s12">Test 1</div>
		<div id="test2" class="col s12">
        <div class="request-info-modal">
          <form class="ng-pristine ng-invalid ng-invalid-required ng-valid-email" method="post" action="ajax.php?action=school&option=save">
				    <section class="top-section">
				      <h3>Zaproś Dasha i Dota<br/>do swojej klasy.</h3>
				    </section>
				    <section class="body-section">
				      <div class="form-body">
				        <div class="row">
				          <input type="text" placeholder="Imię" name="name" required="required" class="span-6 ng-pristine ng-untouched ng-invalid ng-invalid-required">
				          <input type="text" placeholder="Nazwisko" name="last_name" class="span-6 ng-pristine ng-untouched ng-valid">
				        </div>
				        <div class="row">
				          <input type="email" placeholder="Email" name="email" required="required" class="span-12 ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required">
				        </div>
				        <div class="row">
				          <input type="text" placeholder="Nazwa szkoły" name="school" required="required" class="span-12 ng-pristine ng-untouched ng-invalid ng-invalid-required">
				        </div>
				        <div class="row">
				          <input type="text" placeholder="Województwo" name="district" required="required" class="span-12 ng-pristine ng-untouched ng-invalid ng-invalid-required">
				        </div>
				        <div class="row">
				          __ROLE
				        </div>
				        <div class="row">
				          __HEARD
				        </div>
				        <div class="row">
				          <input type="text" placeholder="Telefon" name="phoneNumber" class="span-12 ng-pristine ng-untouched ng-valid">
				        </div>
				      </div>
				    </section>
				    <section class="bottom-section">
				      <button ng-disabled="loading || requested" class="pill-button">Zapisz</button>
				    </section>
				    <input type="hidden" name="action" value="school">
				    <input type="hidden" name="option" value="save">
				  </form>
				  </div>
        </div>
    </div>
	</div>
</div>


