<div id="modal1" class="modal">
	<div class="modal-content">
  	<div id="tabs-container">
    	<ul class="tabs-menu">
        <li class="current"><a href="#tab-1">Zaloguj się</a></li>
        <li><a href="#tab-2">Zarejestruj się</a></li>
    	</ul>
    </div>
    <div class="tab">
    	<div id="tab-1" class="tab-content">
      	<form method="post" action="ajax.php?action=user&option=login">
      		<section class="top-section">
				  	<h3>Zaloguj się na swoje konto.</h3>
				  </section>
				  <section class="body-section">
      			<div class="form-body">
      				<div class="row">
				        <input type="email" placeholder="Email" name="email" required="required" class="span-12">
				      </div>
				      <div class="row">
				        <input type="password" placeholder="Hasło" name="email" required="required" class="span-12">
				      </div>
      			</div>
      		</section>
      		<section class="bottom-section">
			      <button ng-disabled="loading || requested" class="pill-button">Zaloguj</button>
			    </section>
      	</form>
      </div>
      <div id="tab-2" class="tab-content">
        
          <form class="" method="post" action="ajax.php?action=school&option=save">
				    <section class="top-section">
				      <h3>Zaproś Dasha i Dota do swojej klasy.</h3>
				    </section>
				    <section class="body-section">
				      <div class="form-body">
				        <div class="row">
				          <input type="text" placeholder="Imię" name="name" required="required" class="span-6">
				          <input type="text" placeholder="Nazwisko" name="last_name" class="span-6">
				        </div>
				        <div class="row">
				          <input type="email" placeholder="Email" name="email" required="required" class="span-12">
				        </div>
				        <div class="row">
				          <input type="password" placeholder="Hasło" name="haslo1" required="required" class="span-12">
				        </div>
				        <div class="row">
				          <input type="password" placeholder="Powtórz hasło" name="haslo2" required="required" class="span-12">
				        </div>
				        <div class="row">
				          <input type="text" placeholder="Nazwa szkoły" name="school" required="required" class="span-12">
				        </div>
				        <div class="row">
				          <input type="text" placeholder="Województwo" name="district" required="required" class="span-12">
				        </div>
			
				        <div class="row">
				          <input type="text" placeholder="Telefon" name="phoneNumber" class="span-12">
				        </div>
				      </div>
				    </section>
				    <section class="bottom-section">
				      <button ng-disabled="loading || requested" class="pill-button">Zapisz</button>
				    </section>
				    <input type="hidden" name="action" value="school">
				    <input type="hidden" name="option" value="save">
				  </form>
				  </div>
        </div>
	</div>
</div>