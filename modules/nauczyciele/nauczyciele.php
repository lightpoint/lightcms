<?php
class nauczyciele extends View {
	public $path = 'modules/nauczyciele/templates/';
	public function getPage() {
		
	}
	
	public function inspiracje() {
		return file_get_contents('../files/articles/inspiracje.php');
	}
	
	public function cennik() {
		return file_get_contents('../files/articles/cennik.php');
	}
	
	public function oferta() {
		return file_get_contents('../files/articles/oferta.php');
	}
	
	public function dziekujemy() {
		return file_get_contents('../files/articles/dziekujemy.php');
	}

}

function nauczyciele($option=null) {
	$nauczyciele = new nauczyciele;
	if($option!=null and method_exists($nauczyciele, $option))
		return $nauczyciele->$option();
	else 
		return $nauczyciele->getPage();
}