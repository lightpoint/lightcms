<?php
class loader extends View {
	public $path = 'modules/loader/templates/';
	
	public function checkFileTypeMagic($file) {
		//$file = fopen('image.gif', 'r');
		$file = fopen($file, 'r');
		$bytes = bin2hex(fread($file, 6));
		if($bytes == '474946383761' || $bytes == '474946383961')
	    print 'To plik gif';
		else 
    print 'To NIE JEST gif';

		fclose($file);
		return $bytes;
	}
	
	public function checkFileTypeInfo ($file) {
		//$file1 = 'image.jpg';
		//$file2 = 'phpcode.jpg';
		
		$file_info = new finfo(FILEINFO_MIME);
		
		//print $file_info->file($file1, FILEINFO_MIME_TYPE); 
		//print $file_info->file($file2, FILEINFO_MIME_TYPE);
		print $file_info->file($file, FILEINFO_MIME_TYPE);
		
		return $file_info->file($file, FILEINFO_MIME_TYPE);
	}
	
	/* calling loaders */
	
	public function imageLoad() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array(
				'__GAL_M_X' => __GAL_M_X+10,
				'__GAL_M_Y' => __GAL_M_Y+10,
				//'__GAL_ID' => $this->var->id,
			);
			return Template::parse($list, file_get_contents($this->path.'imageLoad.tpl.php'));
		} else return $this->goAway();
	}
	
	public function fileLoad() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array(
				//'__GAL_M_X' => __GAL_M_X+10,
				//'__GAL_M_Y' => __GAL_M_Y+10,
				//'__GAL_ID' => $this->var->id,
			);
			return Template::parse($list, file_get_contents($this->path.'fileLoad.tpl.php'));
		} else return $this->goAway();
	}
	
	public function saveFileLoad() {
		$tmp_name = $_FILES["attachment"]["tmp_name"][$key];
    $name = $_FILES["attachment"]["name"][$key];
    move_uploaded_file($tmp_name, LESSONS_ATTACHMENT_FILES.'/'.$salt.'/'."$name");	
	}
	
	public function moveFilesArray($catalog) {
    $files = array();
    $fdata = $_FILES["fileName"];
    if (is_array($fdata["name"])) {
      for ($i = 0; $i < count($fdata['name']); ++$i) {
        $files[] = array(
            'name' => $fdata['name'][$i],
            'tmp_name' => $fdata['tmp_name'][$i],
        );
      }
    } else {
      $files[] = $fdata;
    }

    foreach ($files as $file) {
      // uploaded location of file is $file['tmp_name']
      // original filename of file is $file['file']
      
      //$tmp_name = $_FILES["attachment"]["tmp_name"][$key];
      //$name = $_FILES["attachment"]["name"][$key];
      move_uploaded_file($file['tmp_name'], $catalog.$file['file']);
    }
	}
	
	
}

function loader($option=null) {
	$loader = new loader;
	if($option!=null and method_exists($loader, $option))
		return $loader->$option();
	else 
		return $loader->getPage();
}