<style>

      input {
        position: relative;
        z-index: 10;
        display: block;
      }

    </style>

    <div class="image-editor">
    	<div class="qq-upload-button-selector qq-upload-button-a btn btn-success" style="width: auto; position: relative; overflow: hidden; direction: ltr;">
					<div><i class="fa fa-upload icon-white"></i> Załaduj plik</div>
				<input class="cropit-image-input" type="file" name="fileName" id="fileName" style="position: absolute; right: 0px; top: 0px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
      
      <div class="cropit-image-preview-container">
        <div class="preview"></div>
      </div>
      

    </div>
    
    <p>&nbsp;</p>
    <div class="form-group">
			<div class="col-sm-9">
				<div class="buttons">
					<button type="button" id="export" class="btn btn-primary">Zapisz</button>
					<button type="button" class="btn btn-secondary lightClose">Anuluj</button>
				</div>
				<div class="spinner" style="display: none;"><i class="fa fa-refresh fa-spin"></i> ładuję ...</div>
			</div>
		</div>

    <script>
    	$("document").ready(function(){
    		$('#fileName').change(function() {
				  $('.preview').html('<p>' + $(this).val() + '</p>');
				});
				
			});
			
      $(function() {
        $('#export').click(function() {
        	//$('.preview').append(fileList);
        	$('.buttons').css('display', 'none');
        	$('.spinner').css('display', 'block');
        
          $.post("request.php",{action: 'loader', option: 'saveFileLoad', files: $('#fileName').val()})
					.done(function(response) {
						$(this).lightClose();
					});
					
        });
      });
    </script>