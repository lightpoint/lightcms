<?php
class Article extends View {
	public $artPath = 'files/articles/'; //path to articles
	
	public function getList() {
		include "config.php";
		
		foreach($articlesList as $name=>$file) {
			$line.= "<div><a href='/articles-".$file.".html'>".$name."</a></div>";
		}
		
		return "<div id='menu'>$line</div>";
	}
	
	public function getArticle($name) {
		if(file_exists($this->artPath.$name.'.php'))
			return file_get_contents($this->artPath.$name.'.php');
		else return $this->pageError();
	}
	
}

function articles($name=null) {
	$articles = new Article;
	if($name!=null)
		return $articles->getArticle($name);
	else 
		return $articles->getList();
}