<?php
$module_info['title'] = 'Artykuły';
$module_info['description'] = 'Artykuły tekstowe - tworzenie i zarządzanie';
$module_info['author'] = 'lukawar';
$menu['order'][] = '1';
$menu_show['menu'] = true;

$menu['title'][0] = 'Artykuły';
$menu['action'][0] = 'articles';
$menu['icon'][0] = 'fa fa-pencil-square-o';