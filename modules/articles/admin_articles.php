<?php
class Articles extends View {
	public $path = 'modules/articles/templates/'; //path to templates for admin
	public $artPath = 'files/articles/'; //path to articles
	
	public function getArticlesList() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Nazwa','Alias','Kategoria', 'Status', '');
			$table->createHeader($tableHeader);
			
			$i = 1;
			if($this->var->sCatId>0) 
				$category = ' and ART_CAT_ID='.$this->var->sCatId;
			else $category = null;
			
			$this->db->queryString('select * from '.__BP.'articles left join '.__BP.'category on ART_CAT_ID=CAT_ID where ART_PARENT=0'.$category.' order by ART_ORDER');
			$articles = $this->db->getDataFetch();
			
			foreach($articles as $art) {
				$table->addCell($i, 'lp');
				$table->addCell($art['ART_NAME']);
				$table->addCell($art['ART_ALIAS']);
				$table->addCell($art['CAT_NAME']);
				$table->addCell($art['ART_STATUS']);
				//$table->addLink('?action=articles&option=add&id='.$art['ART_ID'], 'dodaj&nbsp;podstronę');
				$table->addLink('?action=articles&option=edit&id='.$art['ART_ID'], 'edycja');
				$table->addRow();
				$i++;
				
				//check for childs
				/*$this->db->queryString('select * from '.__BP.'articles where ART_PARENT='.$art['ART_ID'].' order by ART_ORDER');
				$ch_articles = $this->db->getDataFetch();
				
				if($ch_articles) {
					$chi = 1;
					foreach($ch_articles as $chart) {
						$table->addCell(null);
						$table->addCell($chi.'. '.$chart['ART_NAME']);
						$table->addCell($chart['ART_ALIAS']);
						$table->addCell($chart['ART_STATUS']);
						$table->addCell(null);
						$table->addLink('?action=articles&option=edit&id='.$chart['ART_ID'], 'edycja');
						$table->addRow();
						$chi++;
					}
					
				}*/
			}
			$tableBody = $table->createBody();
			
			$list = array(
				'__TABLE'	=>$tableBody,
				'__ART_CAT_ID' => $this->createCategorySelect($this->var->sCatId)
			);
			
			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
		} else return $this->goAway();
	}
	
	public function add() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$list = array('__ART_CAT_ID' => $this->createCategorySelect(0));
			return Template::parse($list, file_get_contents($this->path.'add.tpl.php'));
		} else return $this->goAway();
	}
	
	public function addSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->ART_NAME) {
				$this->db->queryString(__BP.'articles');
				$article_name = $this->checkFile(coder::stripPL($this->var->ART_NAME));
				$insert = array(
					'ART_PARENT' => $this->var->ART_PARENT,
					'ART_NAME' => $this->var->ART_NAME,
					'ART_ALIAS' => $article_name,
					'ART_CONTENT' => addslashes($this->var->ART_CONTENT),
					'ART_STATUS' => $this->var->ART_STATUS,
					'ART_CAT_ID' => $this->var->ART_CAT_ID,
				);
				$this->db->insertQuery($insert);
				
				file_put_contents($this->artPath.$article_name.'.php', stripslashes($this->var->ART_CONTENT));
				
				//save event 
				eventSaver::add($this->var->admin_id, 'new article - '. $this->var->ART_ALIAS);
				//save backup content
				$this->setBackup($article_name.'.php', $this->var->ART_CONTENT); 
			}
			header('location: admin.php?action=articles');
		} else return $this->goAway();
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {	
			$this->db->queryString('select * from '.__BP.'articles where ART_ID='.$this->var->id);
			$art = $this->db->getRow();
			
			$list = array(
				'__ART_NAME' => $art['ART_NAME'],
				'__ART_ALIAS' => $art['ART_ALIAS'],
				'__ART_CONTENT' => stripslashes($art['ART_CONTENT']),
				'__ART_ID' => $art['ART_ID'],
				'__ART_STATUS' => $this->getStatus($art['ART_STATUS']),
				'__ART_CAT_ID' => $this->createCategorySelect($art['ART_CAT_ID']),
			);
			
			return Template::parse($list, file_get_contents($this->path.'edit.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSave() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->ART_NAME) {
				$this->db->queryString(__BP.'articles');

				$update = array(
					'ART_PARENT' => $this->var->ART_PARENT,
					'ART_NAME' => $this->var->ART_NAME,
					'ART_ALIAS' => $this->var->ART_ALIAS,
					'ART_CONTENT' => addslashes($this->var->ART_CONTENT),
					'ART_STATUS' => $this->var->ART_STATUS,
					'ART_CAT_ID' => $this->var->ART_CAT_ID,
				);
				$this->db->updateQuery($update, 'ART_ID', $this->var->ART_ID);
				//$this->db->query();
				
				file_put_contents($this->artPath.$this->var->ART_ALIAS.'.php', stripslashes($this->var->ART_CONTENT));
				
				//save event 
				eventSaver::add($this->var->admin_id, 'edit article - '. $this->var->ART_ALIAS);
				//save backup content
				$this->setBackup($this->var->ART_NAME.'.php', $this->var->ART_CONTENT); 
			}
			header('location: admin.php?action=articles');
		} else return $this->goAway();
	}
	
	private function setBackup($file, $content) {
		$path = $this->artPath."backup/".date('Ymd_His_');
		file_put_contents($path.$file, $content);
	}
	
	private function checkFile($file) {
		if(file_exists($this->artPath.$file.'.php')) {
			$file.='_';
			return $this->checkFile($file);
		} else return $file;
	}
	
	private function getStatus($id=null) {
		$status = array('nowy', 'opublikowany', 'nieopublikowany');
		$select = new select('ART_STATUS', 'ART_STATUS');
		foreach($status as $s) {
			if($id==$s)
				$select->addNode($s, $s, 'selected');
			else $select->addNode($s, $s);
		}
		return $select->create();
	}
	
	private function getParents($id=null) {
		$this->db->queryString('select ART_ID, ART_NAME from '.__BP.'articles where ART_PARENT=0');
		$articles = $this->db->getDataFetch();
		
		$select = new select('ART_PARENT', 'ART_PARENT');
		
		$select->addNode(' - brak - ', 0);
		foreach($articles as $art) {
			if($id==$art['ART_ID'])
				$select->addNode($art['ART_NAME'], $art['ART_ID'], 'selected');
			else $select->addNode($art['ART_NAME'], $art['ART_ID']);
		}
		return $select->create();
	}
	
	private function createCategorySelect($id) {
		$this->db->queryString('select CAT_ID, CAT_NAME from '.__BP.'category where CAT_STATUS=1 order by CAT_ORDER');
		$categories = $this->db->getData();
		
		$line = "<option value='0'> - brak - </option>";
		foreach($categories as $cat) {
			if($cat['CAT_ID']==$id)
				$line.= "<option value='".$cat['CAT_ID']."' selected> - ".$cat['CAT_NAME']." - </option>";
			else $line.= "<option value='".$cat['CAT_ID']."'> - ".$cat['CAT_NAME']." - </option>";
		}
		
		return "<select name='ART_CAT_ID' id='ART_CAT_ID'>".$line.'</select>';
	}
	
	public function filter() {
		$_SESSION['sCatId'] = $this->var->sCatId;
		header('location: admin.php?action=articles');
	}
	
	 public function getForMenu() {
		$menu_module = $this->var->menu_module;
		$menu_option = $this->var->menu_option;
		
		$this->db->queryString('select ART_ID, ART_NAME, ART_ALIAS from '.__BP.'articles where ART_STATUS in (1,2)');
		$articles = $this->db->getDataFetch();
				
		foreach($articles as $article) {
			if($menu_module=='articles' and $menu_option==$article['ART_ALIAS'])
				$selected = " checked";
			else $selected = '';
				
			$line.="<input type='radio' name='menu' value='articles:".$article['ART_ALIAS'].":".$article['ART_ID'].":".$article['ART_NAME'].":text'".$selected."> - ".$article['ART_NAME']."<br/>";
		}
		
		return $line;
	}
}

function articles($option=null) {
	$articles = new Articles;
	
	if($option!=null and method_exists($articles, $option)) {	
		return $articles->$option();
	} else {
		return $articles->getArticlesList();
	}
}