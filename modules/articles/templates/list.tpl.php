<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=articles">Artykuły tekstowe</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Edycja tekstów</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<div id="optionsBar"><a href="?action=articles&option=add" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nowy artykuł tekstowy</a>&nbsp;&nbsp;&nbsp; __ART_CAT_ID <button id="filterCatButton" type="button" class="btn btn-success btn-label-left"><span><i class="fa fa-search"></i></span> Fitruj</button></div>
				<div id="contentPlace">__TABLE</div>
			</div>
		</div>
	</div>
</div>