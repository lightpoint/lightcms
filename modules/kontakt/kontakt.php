<?php
class Kontakt extends View {
	public $path = 'modules/kontakt/templates/';
	public function getPage() {
		return file_get_contents($this->path.'form.tpl.php');
	}
	
	public function formMakewonder() {
		return file_get_contents($this->path.'form_makewonder.tpl.php');
	}
	
	public function sendFromM() {
		$data = explode('|',$this->var->ytype);
		$data_template = array (
					'__EMAIL'=>$this->var->email,
					'__NAME'=>$this->var->yname,
					'__CONTENT'=>$this->var->content,
					'__SUBJECT'=>$this->var->subject,
					'__TYPE'=>$data[1],
				);

		
		$user_remind = new Mailer();
		
		$files = array('logo.png');
		$user_remind->sendMail('wojtek@expansja.pl', '', '', 'Pytanie ze strony internetowej', Template::parse($data_template, file_get_contents('templates/mail/mail.tpl.php')), null, null, $files, 'templates/mail/images/', $this->var->email, $this->var->yname);
		$result['result'] = true;
		$result['info'] = $this->info();
		
		$list = array(
			'__SEND'=>$this->info()
		);
		return Template::parse($list, file_get_contents($this->path.'send.tpl.php'));
	}
	
	public function send() {
		$data = explode('|',$this->var->ytype);
		$data_template = array (
					'__EMAIL'=>$this->var->email,
					'__NAME'=>$this->var->yname,
					'__CONTENT'=>$this->var->content,
					'__SUBJECT'=>$this->var->subject,
					'__TYPE'=>$data[1],
				);

		
		$user_remind = new Mailer();
		
		$files = array('logo.png');
		$user_remind->sendMail($data[0], '', '', 'Pytanie ze strony internetowej', Template::parse($data_template, file_get_contents('templates/mail/mail.tpl.php')), null, null, $files, 'templates/mail/images/', $this->var->email, $this->var->yname);
		$result['result'] = true;
		$result['info'] = $this->info();
		
		$list = array(
			'__SEND'=>$this->info()
		);
		return Template::parse($list, file_get_contents($this->path.'send.tpl.php'));
	}
	
	public function sendFromShop() {
		$data = explode('|',$this->var->ytype);
		$data_template = array (
					'__EMAIL'=>$this->var->email,
					'__NAME'=>$this->var->yname,
					'__CONTENT'=>$this->var->content,
					'__SUBJECT'=>$this->var->subject,
					'__TYPE'=>$data[1],
				);

		
		$user_remind = new Mailer();
		
		$files = array('logo.png');
		$user_remind->sendMail($data[0], '', '', 'Pytanie ze sklepu internetowego Wonder Polska', Template::parse($data_template, file_get_contents('templates/mail/mail.tpl.php')), null, null, $files, 'templates/mail/images/', $this->var->email, $this->var->yname);
		$result['result'] = true;
		$result['info'] = $this->info();
		
		$list = array(
			'__SEND'=>$this->info()
		);
		//return Template::parse($list, file_get_contents($this->path.'send.tpl.php'));
		header('location: http://wonderpolska.pl/Potwierdzenie-wyslania-cabout-pol-28.html');
	}
	
	public function info() {
		return '<h3>Twoje zapytanie zostało wysłane, dziękujemy.</h3>';
	}
	
	public function addContact() {
		$this->db->queryString(__BP.'newsletter_address');
		$this->db->insertQuery(array('NAD_EMAIL'=>$this->var->id));

		return "<p>Twój email został zapisany, dziękujemy!</p>";
	}
}

function kontakt($option=null) {
	$kontakt = new Kontakt;
	if($option!=null and method_exists($kontakt, $option))
		return $kontakt->$option();
	else 
		return $kontakt->getPage();
}