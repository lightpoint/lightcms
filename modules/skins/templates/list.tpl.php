<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=skins&option=list">Skórki</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-puzzle-piece"></i>
					<span>Lista skórek</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>
			<div class="box-subcontent">
				<div id="contentPlace">__TABLE</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$('button.btn-primary').click(function() {
		document.location.href = '?action=skins&option=setDefault&id='+$(this).attr('name');
	});
			
});
</script>