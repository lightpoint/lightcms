<?php
class Skins extends View {
	public $path = 'modules/skins/templates/';

	public function getList() {
		$list = array('__TABLE'=>$this->getSkins());
		
		return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
	}
	
	public function getSkins() {
		$handle = opendir('templates/frontend');
		$catalogs = '';
		
		$skin = $this->db->dict('SKIN_NAME');
		
		while($catalog = readdir($handle)) {
		  if ($catalog != '.' AND $catalog !='..')
		    $catalogs[] = $catalog;
		}
		
		foreach($catalogs as $module) {
			unset($menu);
		  if(file_exists("templates/frontend/".$module."/config.php"))  {
		    include("templates/frontend/".$module."/config.php");
		    if(isset($config_file)) {
		    	if($config_file['alias']==$skin['DIC_VALUE']) {
		    		$active = ' box-active';
		    		$select_button = " disabled='disabled'";
		    	} else { 
		    		$active = null;
		    		$select_button = null;
		    	}
		    	$result.="<div class='col-md-4'><div class='box'><div class='box-content".$active."'><strong>".$config_file['name']."</strong><br/>".$config_file['description']."<br/><img src='".$config_file['thumbnail']."'><br/>autor: ".$config_file['author']."<br/>data dodania: ".$config_file['date_add']."<br/><br/><button type='button' name='".$config_file['alias']."' class='btn btn-primary'".$select_button."><i class='fa fa-puzzle-piece'></i> Wybierz</button> <a class='btn btn-success' target='_blank' href='index.php?siteTemplateLayoutPath_hs7fn2a=".$config_file['alias']."'><i class='fa fa-eye'></i> Podgląd</a></div></div></div>";
		    }
		  }
		}
		return $result;
	}
	
	public function setDefault() {
		if($this->var->id) {
			$this->db->setDict('SKIN_NAME', $this->var->id);
			$_SESSION['siteTemplateLayoutPath_hs7fn2a'] = null;
			header('location: ?action=skins');
		}
	}
	
}

function Skins($option=null) {
	$skins = new Skins;
	if($option!=null and method_exists($skins, $option))
		return $skins->$option();
	else 
		return $skins->getList();
}
?>