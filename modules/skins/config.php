<?php
$module_info['title'] = 'Skórki';
$module_info['description'] = 'Zarządzanie skórkami strony';
$module_info['author'] = 'Expansja';

$menu['title'][] = 'Skórki';
$menu['action'][] = 'skins';
$menu['icon'][0] = 'fa fa-puzzle-piece';
$menu['order'][] = '2';

$dashboard = true;
?>