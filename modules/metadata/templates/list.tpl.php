<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=metadata">Metadane</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-google-plus-square"></i>
					<span>Metadane</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<div id="optionsBar"><a href="?action=metadata&option=add" class="btn btn-primary btn-label-left"><span><i class="fa fa-plus"></i></span>dodaj nowy wpis</a></div>
				<div id="contentPlace">__TABLE</div>
			</div>
		</div>
	</div>
</div>