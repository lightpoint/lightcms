<div class="row">
	<div id="breadcrumb" class="col-md-12">
		<ol class="breadcrumb">
			<li><a href="?action=metadata">Metadane</a></li>
		</ol>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12">
		<div class="box">
			<div class="box-header">
				<div class="box-name">
					<i class="fa fa-pencil-square-o"></i>
					<span>Dodaj nową pozycję</span>
				</div>
				<div class="box-icons">
					<a class="expand-link">
						<i class="fa fa-expand"></i>
					</a>
				</div>
				<div class="no-move"></div>
			</div>

			<div class="box-content">
				<h4 class="page-header">Dodaj nową pozycję</h4>
				<form name="adminEdit" action="admin.php" method="post" class="form-horizontal">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Strona</label>
					<div class="col-sm-5">
						__PAGE
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Tytuł</label>
					<div class="col-sm-5">
						<input type="text" class="form-control"  name="PAM_TITLE"/>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Opis</label>
					<div class="col-sm-9">
						<textarea name="PAM_DESCRIPTION"></textarea>
					</div>
				</div>
				
				<input type="hidden" name="action" value="metadata">
				<input type="hidden" name="option" value="addSave">
				<div class="form-group">
					<div class="col-sm-9 col-sm-offset-3">
						<button type="submit" class="btn btn-primary">Zapisz</button>
						<button type="button" class="btn btn-secondary cancel">Anuluj</button>
					</div>
				</div>

				</form>	
			</div>
		</div>
	</div>
</div>