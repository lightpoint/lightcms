<?php
class Metadata extends View {
	public function getDefault() {
		$this->db->queryString('select PAM_TITLE, PAM_DESCRIPTION from '.__BP.'pages_meta where PAM_PAG_ID=1');
		$meta = $this->db->getRow();
		
		return '<title>'.$meta['PAM_TITLE'].'</title><meta name="description" content="'.$meta['PAM_DESCRIPTION'].'">';
	}
	
	public function getData($name) {
		$this->db->queryString('select PAM_TITLE, PAM_DESCRIPTION from '.__BP.'pages_meta left join '.__BP.'pages on PAG_ID=PAM_PAG_ID where PAG_NAME="'.$name.'"');
		$meta = $this->db->getRow();
		if($meta)
			return '<title>'.$meta['PAM_TITLE'].'</title><meta name="description" content="'.$meta['PAM_DESCRIPTION'].'">';
		else return $this->getDefault();
	}
	
}

function metadata($name=null) {
	$metadata = new Metadata;
	if($name!=null)
		return $metadata->getData($name);
	else 
		return $metadata->getDefault();
}