<?php
class Metadata extends View {
	public $path = 'modules/metadata/templates/';
	
	public function getList() {
		$table = new table('myTable', 'tablesorter');
		$tableHeader = array('lp.', 'Nazwa', 'Tytuł', '');
		$table->createHeader($tableHeader);
		
		/*$this->db->queryString('select * from '.__BP.'pages');
		$pages = $this->db->getDataFetch();*/
		
		$i = 1;
		
		$this->db->queryString('select * from '.__BP.'pages_meta left join '.__BP.'pages on PAG_ID=PAM_PAG_ID');
		$meta = $this->db->getDataFetch();
		
		foreach($meta as $m) {
			$table->addCell($i, 'lp');
			$table->addCell($m['PAG_NAME']);
			$table->addCell($m['PAM_TITLE']);
			
			$table->addLink('?action=metadata&option=edit&id='.$m['PAM_ID'], 'edycja');
			$table->addRow();
			$i++;
			
		}
		$tableBody = $table->createBody();
		
		$list = array('__TABLE'	=>$tableBody);
		
		return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
	}
	
	public function add() {
		$list = array('__PAGE' => $this->createSelect());
		return Template::parse($list, file_get_contents($this->path.'add.tpl.php'));
	}
	
	public function addSave() {
		if($this->var->PAM_TITLE) {
			$this->db->queryString(__BP.'pages_meta');

			$insert = array(
				'PAM_TITLE' => $this->var->PAM_TITLE,
				'PAM_DESCRIPTION' => $this->var->PAM_DESCRIPTION,
				'PAM_PAG_ID' => $this->var->PAM_PAG_ID
			);
			$this->db->insertQuery($insert);
		}
		header('location: admin.php?action=metadata');
	}
	
	public function edit() {
		if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {	
			$this->db->queryString('select * from '.__BP.'pages_meta left join '.__BP.'pages on PAG_ID=PAM_PAG_ID where PAM_ID='.$this->var->id);
			$meta = $this->db->getRow();
			
			$list = array(
				'__PAGE' => $meta['PAG_NAME'],
				'__PAM_TITLE' => $meta['PAM_TITLE'],
				'__PAM_DESCRIPTION' => $meta['PAM_DESCRIPTION'],
				'__PAM_ID' => $meta['PAM_ID'],
				
			);
			
			return Template::parse($list, file_get_contents($this->path.'edit.tpl.php'));
		} else return $this->goAway();
	}
	
	public function editSave() {
		$this->db->queryString(__BP.'pages_meta');

		$update = array(
			'PAM_TITLE' => $this->var->PAM_TITLE,
			'PAM_DESCRIPTION' => $this->var->PAM_DESCRIPTION,
		);
		$this->db->updateQuery($update, 'PAM_ID', $this->var->id);			
		
		header('location: admin.php?action=metadata');
	}
	
	private function createSelect($id=null) {
		$this->db->queryString('select * from '.__BP.'pages where PAG_ID not in (select PAM_PAG_ID from '.__BP.'pages_meta)');
		$pages = $this->db->getDataFetch();
		
		$select = new select('PAN_PAG_ID', 'PAN_PAG_ID');
		foreach($pages as $page) {
			if($id==$page['PAG_ID'])
				$select->addNode($page['PAG_NAME'], $page['PAG_ID'], 'selected');
			else $select->addNode($page['PAG_NAME'], $page['PAG_ID']);
		}
		return $select->create();
	}
}

function metadata($option=null) {
	$metadata = new Metadata;
	if($option!=null and method_exists($metadata, $option))
		return $metadata->$option();
	else 
		return $metadata->getList();
}