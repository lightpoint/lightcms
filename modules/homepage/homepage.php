<?php
class Homepage extends View {
	public function getPage() {
		$data_module = new Module();
		$news = $data_module->load('news', 'getLast');
		$helper = new helper();
		$list = array(
			'__NEWS'=>$news, 
			'__INSTAGRAM'=>$this->getInstagram(),
		);
			
		return Template::parse($list, file_get_contents('files/pages/homepage'.$this->var->lang_sufix.'.page.php'));
	}	
	
	public function getInstagram() {
		$this->db->queryString('select * from '.__BP.'instagram_cached where INS_STATUS="active" order by rand() limit 21');
		$images = $this->db->getDataFetch();
		
		$i = 1;
		
		foreach($images as $image) {
			$line.="<div class='col s6 m2 l2 rs2 rm3 rl3 img-container'><a class='fancybox' href='".$image['INS_HIGH_RES']."'><img class='responsive-img-inst' src='".$image['INS_LOW_RES']."'></a></div>";
			if($i==8)
				if($this->var->lang_sufix=='_en')
					$line.="<div class='col s12 m4 l4 rs2 rm3 rl3' style='overflow: hidden;'><div class='box-blue'><p align='center'>SHOW HOW YOU FIND YOUR BALANCE<br/>USE HASHTAG #jurajskawodamineralna</p></div></div>";
				else $line.="<div class='col s12 m4 l4 rs2 rm3 rl3' style='overflow: hidden;'><div class='box-blue'><p align='center'>POKAŻ JAK ZNAJDUJESZ SWOJĄ RÓWNOWAGĘ<br/>UŻYWAJ HASHTAG #jurajskawodamineralna</p></div></div>";
				
			if($i==14) {
				$line.="<div class='col s6 m2 l2 rs2 rm3 rl3' style='overflow: hidden;'>
				<div class='col s12 m12 l12 box-centered'>";
				if($this->var->lang_sufix=='_en')
					$line.="JOIN TO US";
				else 
					$line.= "DOŁĄCZ DO NAS";
				
				$line.="</div><div class='col s6 m4 l6 box-centered'><a href='https://www.facebook.com/Jurajska.NaturalnaWodaMineralna?fref=ts' target='_blank'><img src='templates/frontend/jurajska_1.0/img/icons/fb.png' class='ip-img'></a></div>
					<div class='col s6 m4 l6 box-centered'><a href='http://instagram.com/jurajska_woda_mineralna/' target='_blank'><img src='templates/frontend/jurajska_1.0/img/icons/instagram.png' class='ip-img'></a></div>
					<div class='col s6 m4 l6 box-centered'><img src='templates/frontend/jurajska_1.0/img/icons/yt.png' class='ip-img'></div>
				</div>";
			}
			$i++;
		}
		
		return $line;
	}
	
	public function getInstagramUserMedia() {
		$media = $this->getInstagram();
		
		foreach ($media->data as $data) {
    	$line.= "<img src=\"{$data->images->standard_resolution->url}\" width='200px'>";
  	}
  	
  	return $line;
	}
}

function homepage($option=null) {
	$homepage = new Homepage;
	if($option!=null and method_exists($homepage, $option))
		return $homepage->$option();
	else 
		return $homepage->getPage();
}