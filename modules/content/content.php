<?php
class Content extends View {
	public $path = 'modules/content/templates/';
	
	public function showModule() {
		if($this->var->action!='index') {
			if($this->var->user_logged_in and in_array($this->var->id_status, $this->blocked_status) and $this->var->option!='logout') { //check if logged and blocked pages
				return $this->loadStartPage();
			} elseif($this->var->user_logged_in and in_array($this->var->id_status, $this->update_status) and $this->var->option!='logout') { //check if update page
				$loader = 'user';
				require_once "modules/user/user.php";
				return $loader('updateUserData');
			} else {
				if(file_exists(SUB_CATALOG."modules/".$this->var->action."/".$this->var->action.".php")) {
					$loader = $this->var->action;
					require_once SUB_CATALOG."modules/".$this->var->action."/".$this->var->action.".php";
					return $loader($this->var->option);
				} else return $this->pageError(SUB_CATALOG."modules/".$this->var->action."/".$this->var->action.".php");
			}
		} else return $this->loadStartPage();
	} 
	
	private function loadStartPage() {
		return file_get_contents($this->artPath.'homepage.php');
	}
}

function content() {
	$content = new Content;
	return $content->showModule();
}