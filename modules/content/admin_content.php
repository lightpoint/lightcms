<?php
class ViewContent extends Layout {
	public $path = 'modules/content/templates/';
	function __construct() {
		$controller = new Controller;
		$this->var = $controller->GetData();
	}
	
	public function load() {
		//echo $this->var->action;
		if($this->var->action!='index') {
			$loader = $this->var->action;
			require_once "modules/".$this->var->action."/admin_".$this->var->action.".php";
			return $loader($this->var->option);
		} else return $this->loadStartPage();
	}
	
	private function loadStartPage() {	
		$module = new Module($this->type);
		return $module->load('dashboard','view');
	}
	
}

function content() {
	$result = new ViewContent;
	return $result->load();
}


?>