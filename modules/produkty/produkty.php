<?php
class Produkty extends View {
	public function getPage() {
		return file_get_contents('files/pages/products/produkty'.$product.$this->var->lang_sufix.'.page.php');
	}	
	
	public function getProduct($product) {
   // echo 'files/pages/'.$product.$this->var->lang_sufix.'.page.php';
		if(file_exists('files/pages/products/'.$product.$this->var->lang_sufix.'.page.php'))
			return file_get_contents('files/pages/products/'.$product.$this->var->lang_sufix.'.page.php');
    elseif(file_exists('files/pages/products/'.$product.'.page.php'))
      return file_get_contents('files/pages/products/'.$product.'.page.php');
		else return $this->noProduct();
	}	
}

function produkty($option=null) {
	$produkty = new Produkty;
	if($option!=null and $option!='lang')
		return $produkty->getProduct($option);
  elseif($option=='lang')
    return $produkty->getPage();
	else 
		return $produkty->getPage();
}