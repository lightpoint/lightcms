<?php
class Dashboard extends View{
	public $path = 'modules/dashboard/templates/';
	
	public function view(){
		$this->db->queryString("select * from ".__BP."admin_dashboard where ADD_ADM_ID=".$this->var->admin_id." order by ADD_ORDER");
		$cads = $this->db->getDataFetch();
		
		if($cads) {
			$module = new Module('admin_');
			foreach($cads as $c) {
				$result.= $module->load($c['ADD_ACTION'],'dashboard');
			}
			$list = array('__DASHBOARDS'=>$result);
		} else $list = array('__DASHBOARDS'=>"<p align='center'>Brak zdefiniowanych widgetów. Możesz zdefiniować je w <a href='?action=options'>tym miejscu</a>.</p>");
		
		return Template::parse($list, file_get_contents($this->path.'view.tpl.php'));
		//return Template::parse($list, file_get_contents('templates/backend/devoops/ajax/charts_flot.html'));
	}
}

function dashboard($option=null) {
	$dashboard = new Dashboard;
	if($option!=null and method_exists($dashboard, $option))
		return $dashboard->$option();
	else 
		return $dashboard->view();
}