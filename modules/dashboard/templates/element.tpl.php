<div class="col-xs-12 col-sm-6" style="min-height: 100px;">
	<div class="box">
		<div class="box-header">
			<div class="box-name">
				<i class="__MODULE_ICON"></i>
				<span>__MODULE_TITLE</span>
			</div>
			<div class="box-icons">
				<a class="collapse-link">
					<i class="fa fa-chevron-up"></i>
				</a>
				<a class="expand-link">
					<i class="fa fa-expand"></i>
				</a>
				<a class="close-link">
					<i class="fa fa-times"></i>
				</a>
			</div>
			<div class="no-move"></div>
		</div>
		<div class="box-content">
			<div id="contentPlace">
				__MODULE_CONTENT
			</div>
		</div>
	</div>
</div>