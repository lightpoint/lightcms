<?php
error_reporting(0);
require "../../system/instagram.php";
//use MetzWeb\Instagram\Instagram;

// initialize class
$instagram = new Instagram(array(
 'apiKey'      => 'e885daea355549dd9d93bb2f7fe66a33',
		  'apiSecret'   => '575c3bb85ef64f3d93bf3ac30e77c569',
		  'apiCallback' => 'http://expansja.pl/strony/jurajska/modules/instagram/success.php'
));
$loginUrl = $instagram->getLoginUrl();

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Instagram - OAuth Login</title>
    <link rel="stylesheet" type="text/css" href="assets/style.css">
    <style>
      .login {
        display: block;
        font-size: 20px;
        font-weight: bold;
        margin-top: 50px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <header class="clearfix">
        <h1>Instagram <span>display your photo stream</span></h1>
      </header>
      <div class="main">
        <ul class="grid">
          <li><img src="assets/instagram-big.png" alt="Instagram logo"></li>
          <li>
            <a class="login" href="<?php echo $loginUrl ?>">» Login with Instagram</a>
            <h4>Use your Instagram account to login.</h4>
          </li>
        </ul>
        <!-- GitHub project -->
        <footer>
          <p>created by <a href="https://github.com/cosenary/Instagram-PHP-API">cosenary's Instagram class</a>, available on GitHub</p>
        </footer>
      </div>
    </div>
  </body>
</html>