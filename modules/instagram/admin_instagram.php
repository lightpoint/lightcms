<?php
class InstagramClass extends View {
	public $path = 'modules/instagram/templates/'; //path to templates for admin
	public $instagram;
	public function login() {
		$this->instagram = new Instagram(array(
		  'apiKey'      => __INS_API_KEY,
		  'apiSecret'   => __INS_API_SECRET,
		  'apiCallback' => __INS_API_CALLBACK
		));
		return $this->instagram;
	}	
	
	public function getList() {
	//	if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			$table = new table('myTable', 'tablesorter');
			$tableHeader = array('lp.', 'Miniatura', 'Data dodania', 'Data pobrania', 'Uzytkownik','Tagi','');
			$table->createHeader($tableHeader);
			
			$i = 1;
			
			$this->db->queryString('select * from '.__BP.'instagram_cached');
			$images = $this->db->getDataFetch();
					
			foreach($images as $img) {
				$table->addCell($i, 'lp');
				$table->addCell('<a href="'.$img['INS_HIGH_RES'].'" class="fancybox"><img src="'.$img['INS_THUMBNAIL'].'"></a>');
				$table->addCell($img['INS_DATE']);
				$table->addCell($img['INS_DATE_CACHED']);
				$table->addCell($img['INS_USERNAME']);
				$table->addCell($img['INS_TAGS']);
				if($img['INS_STATUS']=='inactive')
					$table->addCell('<a href="request.php?action=instagram&option=activate&id='.$img['INS_UID'].'" class="btn btn-success btn-label-left btn-request"><span><i class="fa fa-instagram"></i></span>uaktywnij</a><br/><a href="request.php?action=instagram&option=delete&id='.$img['INS_UID'].'" class="btn btn-danger btn-label-left btn-request"><span><i class="fa fa-minus-square"></i></span>usuń</a>','ins-buttons');
				else $table->addCell('<a href="request.php?action=instagram&option=deactivate&id='.$img['INS_UID'].'" class="btn btn-warning btn-label-left btn-request btn-request"><span><i class="fa fa-instagram"></i></span>dezaktywuj</a><br/><a href="request.php?action=instagram&option=delete&id='.$img['INS_UID'].'" class="btn btn-danger btn-label-left btn-request"><span><i class="fa fa-minus-square"></i></span>usuń</a>','ins-buttons');
				$table->addRow($img['INS_UID'].' '.$img['INS_STATUS']);
				$i++;
			}
			
			$list = array('__INS_LIST' => $table->createBody());

			return Template::parse($list, file_get_contents($this->path.'list.tpl.php'));
	//	} else return $this->goAway();
	}
	
	public function activate() {
		//if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$this->db->queryString(__BP.'instagram_cached');
				$update = array('INS_STATUS' => 'active');
				$this->db->updateQuery($update, 'INS_UID', $this->var->id);
				
				return json_encode(array('id'=>$this->var->id,'status'=>'active'));
			}
	//	} else return $this->goAway();
	}
	
	public function deactivate() {
	//	if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$this->db->queryString(__BP.'instagram_cached');
				$update = array('INS_STATUS' => 'inactive');
				$this->db->updateQuery($update, 'INS_UID', $this->var->id);
				
				return json_encode(array('id'=>$this->var->id,'status'=>'inactive'));
			}
	//	} else return $this->goAway();
	}
	
	public function delete() {
	//	if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			if($this->var->id) {
				$this->db->queryString('delete from '.__BP.'instagram_cached where INS_UID="'.$this->var->id.'"');
				$this->db->execQuery();
				
				return json_encode(array('id'=>$this->var->id,'status'=>'delete'));
			}
	//	} else return $this->goAway();
	}
	
	public function success() {
	//	if($this->checkAdminRole($this->var->admin_id,__CLASS__, __FUNCTION__)) {
			//empting table
			$this->db->queryString('select IST_NEXT_URL from '.__BP.'instagram_status order by IST_ID desc');
			//$this->db->query();
			$next_id = $this->db->getRow();
			
			$this->login();
			$tag = __INS_KEYWORDS;
  
			$max_id = $next_id['IST_NEXT_URL'];
			
			for($i = 1; $i <= 2; $i++) {
				if($max_id) {
					$clientID = $this->instagram->getApiKey();

				  $call = new stdClass;
				  $call->pagination->next_max_id = $max_id;
				  $call->pagination->next_url = "https://api.instagram.com/v1/tags/{$tag}/media/recent?client_id={$clientID}&max_tag_id={$max_id}";
					$result = $this->instagram->getTagMedia(__INS_KEYWORDS,$auth=false,array('max_tag_id'=>$max_id));
				} else {
					$result = $this->instagram->getTagMedia(__INS_KEYWORDS);
				}
				
				//var_dump($result);
				$max_id = $result->pagination->next_max_tag_id;
			  foreach ($result->data as $media) {
			  	$this->db->queryString(__BP.'instagram_cached');
	        $insert = array(
	        	'INS_THUMBNAIL' => addslashes($media->images->thumbnail->url),
	        	'INS_LOW_RES' => addslashes($media->images->low_resolution->url),
	        	'INS_HIGH_RES' => addslashes($media->images->standard_resolution->url),
	        	'INS_TAGS' => addslashes(str_replace('#', ' #', $media->caption->text)),
	        	'INS_UID' => $media->id,
	        	'INS_USERNAME' => addslashes($media->user->username));
	        
	        $this->db->prepareInsertQuery($insert);
	        $this->db->execQuery();
			  }
			  //save next_max_id for next iterations
	      $this->db->queryString(__BP.'instagram_status');
	      $this->db->insertQuery(array('IST_NEXT_URL' => $max_id));
			}
			header('location: ?action=instagram');
	//	} else return $this->goAway();
	}
	
	public function actionSuccess() {
		$this->login();
		$code = $this->var->code;
		var_dump($code);
		if(isset($code)) {
		  $data = $this->instagram->getOAuthToken($code);
		  $this->instagram->setAccessToken($data);
		  $result = $this->instagram->getUserMedia();
		  
		  var_dump($result->data);
		  foreach ($result->data as $media) {
            $content = "<li>";

            // output media
            if ($media->type === 'video') {
              // video
              $poster = $media->images->low_resolution->url;
              $source = $media->videos->standard_resolution->url;
              $content .= "<video class=\"media video-js vjs-default-skin\" width=\"250\" height=\"250\" poster=\"{$poster}\"
                           data-setup='{\"controls\":true, \"preload\": \"auto\"}'>
                             <source src=\"{$source}\" type=\"video/mp4\" />
                           </video>";
            } else {
              // image
              $image = $media->images->low_resolution->url;
              $content .= "<img class=\"media\" src=\"{$image}\"/>";
            }

            // create meta section
            $avatar = $media->user->profile_picture;
            $username = $media->user->username;
            $comment = $media->caption->text;
            $content .= "<div class=\"content\">
                           <div class=\"avatar\" style=\"background-image: url({$avatar})\"></div>
                           <p>{$username}</p>
                           <div class=\"comment\">{$comment}</div>
                         </div>";

            // output media
            echo $content . "</li>";
        }
		} else {
		  if (isset($_GET['error'])) {
		    echo 'An error occurred: ' . $_GET['error_description'];
		  }
		}
	}
}

function instagram($option=null) {
	$instagram = new InstagramClass;
	if($option!=null and method_exists($instagram, $option))
		return $instagram->$option();
	else 
		return $instagram->getList();
}