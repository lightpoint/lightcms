<?php
session_start();
//error_reporting(E_ERROR);
error_reporting(E_ALL);

require_once "../../system/config.php";
require_once "../../system/connector.php";
require_once "../../system/system.php";
require_once "../../system/view.php";
require_once "../../system/controller.php";
require_once "../../system/instagram.php";
require_once "../administrator/admin_administrator.php";

$db = new Connector;
$db->BaseConnection();

$_SESSION['type'] = 'admin_';
$admin = Administrator::checkAdmin();

if($admin) {
	$instagram = new Instagram(array(
		  'apiKey'      => 'e885daea355549dd9d93bb2f7fe66a33',
		  'apiSecret'   => '575c3bb85ef64f3d93bf3ac30e77c569',
		  'apiCallback' => 'http://expansja.pl/strony/jurajska/modules/instagram/instagramExport.php'
		));
		
	$code = $_GET['code'];

	if (isset($code)) {
	  $data = $instagram->getOAuthToken($code);
	  $instagram->setAccessToken($data);
	  $result = $instagram->getUserMedia();
		
		var_dump($result->data);	
			
		foreach ($result->data as $media) {
      $content = "<li>";

      // output media
      if ($media->type === 'video') {
        // video
        $poster = $media->images->low_resolution->url;
        $source = $media->videos->standard_resolution->url;
        $content .= "<video class=\"media video-js vjs-default-skin\" width=\"250\" height=\"250\" poster=\"{$poster}\"
                     data-setup='{\"controls\":true, \"preload\": \"auto\"}'>
                       <source src=\"{$source}\" type=\"video/mp4\" />
                     </video>";
      } else {
        // image
        $image = $media->images->low_resolution->url;
        $content .= "<img class=\"media\" src=\"{$image}\"/>";
      }

      // create meta section
      $avatar = $media->user->profile_picture;
      $username = $media->user->username;
      $comment = $media->caption->text;
      $content .= "<div class=\"content\">
                     <div class=\"avatar\" style=\"background-image: url({$avatar})\"></div>
                     <p>{$username}</p>
                     <div class=\"comment\">{$comment}</div>
                   </div>";

      // output media
      echo $content . "</li>";
    }
	} else {

	  
	  if (isset($_GET['error'])) {
	    echo 'An error occurred: ' . $_GET['error_description'];
	  }

	}
} else header('location: ../../admin.php');
