<?php
class Instagram extends View {
	public function getPage() {
		$this->db->queryString('select * from '.__BP.'instagram_cache where INS_STATUS="active" order by rand() limit 20')	;
		$images = $this->db->getDataFetch();
		
		foreach($images as $image) {
			$line.="<div class='col s6 m2 l2'><img src='".$image['INS_HIGH_RES']."' width='100%'></div>";
		}
		
		return $line;
	}	
}

function instagram($option=null) {
	$instagram = new Instagram;
	if($option!=null and method_exists($instagram, $option))
		return $instagram->$option();
	else 
		return $instagram->getPage();
}