<?php
session_start();
error_reporting(E_ERROR);
//error_reporting(E_ALL);

require_once "system/config.php";
require_once "system/connector.php";
require_once "system/system.php";
require_once "system/view.php";
require_once "system/controller.php";
require_once "system/functions.php";
require_once "system/components.php";
require_once "system/parser.php";
require_once "modules/administrator/admin_administrator.php";
require_once "system/mailer.php";
require_once "system/instagram.php";

$db = new Connector;
$db->BaseConnection();

$_SESSION['type'] = 'admin_';
$admin = Administrator::checkAdmin();

if($admin)
	$layout = new Layout(V_A_TEMPLATE_PATH, 'admin_');
else Layout::showLogin(V_A_TEMPLATE_PATH);