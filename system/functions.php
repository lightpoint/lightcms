<?php
/*
Loading image
usage:
  $image_file = new ImageLoader(x,y,$fileName,$file,$pth,$type);
  $image_ext = $image_file->load();
x,y  dimensions
$fileName  - file name
$file - file from $_FILES['file']['tmp_name'] - only 'file'
$path - path to load
$prop - keep proportions, default true
return extension
*/

class ImageLoader {
	function __construct($x, $y, $fileName, $fileFull, $path, $prop='true') {
		$this->x = $x;
		$this->y = $y;
		$this->fileName = $fileName;
		$this->fileFull = $fileFull;
		$this->path = $path;
		$this->prop = $prop;
	}
	
	private function getType($fileType) {
		$mimetypes = $this->privBuildMimeArray();
		if (isset($mimetypes[$fileType])) 
        	 return $mimetypes[$fileType];
       	else return 'false';
	}
	
	public function load() {
		$fileType = $this->getType($_FILES[$this->fileFull]['type']);
		//die($fileType." asd");
		if($fileType=='false') {
			return $fileType;
		} else {
			switch ($fileType) {
	    		case '.jpg';
	    		case '.jpe';
	    		case '.jpeg';
	    			$source = @imagecreatefromjpeg($_FILES[$this->fileFull]['tmp_name']);
	    			break;
	    		case '.png':
	    			$source = @imagecreatefrompng($_FILES[$this->fileFull]['tmp_name']);
	    			break;
	    		case '.gif':
	    			$source = @imagecreatefromgif($_FILES[$this->fileFull]['tmp_name']);
	    			break;
	    		default : return false;
			}
			
	    if($this->prop=='true') {
	  		if(imagesy($source)>imagesx($source)) {
	  			if (imagesy($source)>$this->y)
	  				$new_height = $this->y;
	  			else $new_height = imagesy($source);
	  				$new_width = floor(imagesx($source)*($new_height/imagesy($source)));
	      	} else {
	  			if (imagesx($source)>$this->x)
	  				$new_width = $this->x;
	  			else $new_width = imagesx($source);
	  				$new_height = floor(imagesy($source)*($new_width/imagesx($source)));

	  		}
	      $dest = imagecreatetruecolor($new_width,$new_height);
	      if($fileType == ".gif" or $fileType == ".png"){
			    imagecolortransparent($dest, imagecolorallocatealpha($dest, 0, 0, 0, 127));
			    imagealphablending($dest, false);
			    imagesavealpha($dest, true);
			  }
			  imagecopyresampled($dest,$source,0,0,0,0,$new_width,$new_height,imagesx($source),imagesy($source));
			  //imagefilledrectangle($desc, 0, 0, $xw, $yw, $source);
	    } else {
	      if(imagesy($source)<imagesx($source)) {
	  			if (imagesy($source)>$this->y)
	  				$new_height = $this->y;
	  			else $new_height = imagesy($source);
	  				$new_width = floor(imagesx($source)*($new_height/imagesy($source)));
	      	} else {
	  			if (imagesx($source)>$this->x)
	  				$new_width = $this->x;
	  			else $new_width = imagesx($source);
	  				$new_height = floor(imagesy($source)*($new_width/imagesx($source)));
	  		}
	      $xw = $new_width-$this->x;
	      $yw = $new_height-$this->y;
	      $dest = imagecreatetruecolor($this->x,$this->y);
	      if($fileType == ".gif" or $fileType == ".png"){
			    imagecolortransparent($dest, imagecolorallocatealpha($dest, 0, 0, 0, 127));
			    imagealphablending($dest, false);
			    imagesavealpha($dest, true);
			  }
			  imagecopyresampled($dest,$source,0,0,$xw,$yw,$new_width,$new_height,imagesx($source),imagesy($source));
			  //imagefilledrectangle($desc, 0, 0, $xw, $yw, $source);
	    }
			//die($this->path.$this->fileFull.$fileType);
			switch ($fileType) {
	    		case '.jpg';
	    		case '.jpe';
	    		case '.jpeg';
	          
	    			imagejpeg($dest,$this->path.$this->fileName.$fileType,100);
	    			break;
	    		case '.png':
	    			imagepng($dest,$this->path.$this->fileName.$fileType);
	    			break;
	    		case '.gif':
	    			imagegif($dest,$this->path.$this->fileName.$fileType,100);
	    			break;
			}

			return $fileType;
		} 
	}
	
	private function privBuildMimeArray() { 
      return array( 
         "image/bmp"=>".bmp", 
         "image/gif"=>".gif", 
         "image/ief"=>".ief", 
         "image/jpeg"=>".jpg",
         "image/png"=>".png", 
         "image/tiff"=>".tiff", 
         "image/tif"=>".tif");
	}	
}