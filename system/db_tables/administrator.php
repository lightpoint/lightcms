<?php
$base['name'] = 'administrator';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPadministrator` (
  `ADM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADM_NAME` varchar(200) NOT NULL,
  `ADM_SURNAME` varchar(200) NOT NULL,
  `ADM_LOGIN` varchar(200) NOT NULL,
  `ADM_PASS` varchar(200) NOT NULL,
  `ADM_MAIL` varchar(200) NOT NULL,
  `ADM_ROLE` enum('admin','user') NOT NULL DEFAULT 'admin',
  `ADM_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ADM_AGENCY` varchar(2) NOT NULL,
  `ADM_SEND_MAIL` enum('nie','tak') NOT NULL DEFAULT 'nie',
  `ADM_ACTIVE` enum('active','deleted') NOT NULL,
  PRIMARY KEY (`ADM_ID`),
  KEY `ADM_AGENCY` (`ADM_AGENCY`),
  KEY `ADM_SEND_MAIL` (`ADM_SEND_MAIL`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


INSERT INTO `__BPadministrator` (`ADM_ID`, `ADM_NAME`, `ADM_SURNAME`, `ADM_LOGIN`, `ADM_PASS`, `ADM_MAIL`, `ADM_ROLE`, `ADM_DATE_ADD`, `ADM_AGENCY`, `ADM_SEND_MAIL`, `ADM_ACTIVE`) VALUES
(1, 'Łukasz', 'Warguła', 'lukawar', 'abb7dcbcf57aed9ba61726ddf0846ca4fa521a9f1f9732b763be239994e7777e', 'lukasz@lightpoint.pl', 'admin', '0000-00-00 00:00:00', 'A', 'tak', 'active');";