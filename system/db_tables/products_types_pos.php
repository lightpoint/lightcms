<?php
$base['name'] = 'products_types_pos';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPproducts_types_pos` (
`PTP_ID` int(11) NOT NULL,
  `PTP_PRT_ID` int(11) NOT NULL,
  `PTP_NAME` varchar(255) NOT NULL,
  `PTP_VALUE` varchar(100) NOT NULL,
  `PTP_MODIFIER` varchar(255) DEFAULT NULL,
  `PTP_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

INSERT INTO `__BPproducts_types_pos` (`PTP_ID`, `PTP_PRT_ID`, `PTP_NAME`, `PTP_VALUE`, `PTP_MODIFIER`, `PTP_STATUS`) VALUES
(1, 1, 'XXL', 'XXL', '', 'active'),
(2, 1, 'XL', 'XL', '', 'active'),
(3, 1, 'L', 'L', '', 'active'),
(4, 1, 'M', 'M', '', 'active'),
(5, 1, 'S', 'S', '', 'active'),
(6, 2, 'Czarny', 'Czarny', '', 'active'),
(7, 2, 'Biały', 'Biały', '{/value/}-200', 'active'),
(8, 2, 'Czerwony', 'Czerwony', '', 'active');

ALTER TABLE `__BPproducts_types_pos`
 ADD PRIMARY KEY (`PTP_ID`), ADD KEY `PTP_PRT_ID` (`PTP_PRT_ID`,`PTP_STATUS`);
 
ALTER TABLE `__BPproducts_types_pos`
MODIFY `PTP_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;";