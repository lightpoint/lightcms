<?php
$base['name'] = 'dict';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPdict` (
  `DIC_KEY` varchar(20) NOT NULL,
  `DIC_VALUE` varchar(100) NOT NULL,
  `DIC_INT` int(11) DEFAULT NULL,
  `DIC_LNG` varchar(3) NOT NULL DEFAULT 'pl',
  `DIC_DESCRIPTION` varchar(30) NOT NULL,
  `DIC_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`DIC_KEY`),
  KEY `DIC_VALUE` (`DIC_VALUE`),
  KEY `DIC_LNG` (`DIC_LNG`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `__BPdict` (`DIC_KEY`, `DIC_VALUE`, `DIC_INT`, `DIC_LNG`, `DIC_DESCRIPTION`, `DIC_DATE`) VALUES
('SHARE_SEND_MESSAGE', 'Wiadomość została wysłana.', 0, 'pl', 'Message after share', '2014-11-20 08:22:06'),
('SKIN_NAME', 'default', 0, 'pl', 'Skin folder name', '2015-02-11 14:24:50');";