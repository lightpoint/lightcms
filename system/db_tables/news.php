<?php
$base['name'] = 'news';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPnews` (
`NEW_ID` int(11) NOT NULL,
  `NEW_NAME` varchar(250) NOT NULL,
  `NEW_ALIAS` varchar(250) NOT NULL,
  `NEW_DESC_SHORT` mediumtext NOT NULL,
  `NEW_DESC_LONG` mediumtext NOT NULL,
  `NEW_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NEW_DATE_DISPLAY` date NOT NULL,
  `NEW_DATE_START` date NOT NULL,
  `NEW_DATE_END` date NOT NULL,
  `NEW_STATUS` enum('nowy','aktywny','nieaktywny') NOT NULL DEFAULT 'nowy'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";