<?php
$base['name'] = 'category';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPcategory` (
`CAT_ID` int(11) NOT NULL,
  `CAT_PARENT` int(11) NOT NULL,
  `CAT_NAME` varchar(200) NOT NULL,
  `CAT_ALIAS` varchar(100) NOT NULL,
  `CAT_DESCRIPTION` tinytext NOT NULL,
  `CAT_ORDER` int(11) NOT NULL,
  `CAT_STATUS` enum('nowy','aktywny','nieaktywny') NOT NULL DEFAULT 'nowy'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";