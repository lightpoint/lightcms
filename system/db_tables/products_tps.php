<?php
$base['name'] = 'products_tps';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPproducts_tps` (
`TPS_PTP_ID` int(11) NOT NULL,
  `TPS_PRO_ID` int(11) NOT NULL,
  `TPS_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active',
  `TPS_PRT_ID` int(11) NOT NULL,
  `TPS_ORDER` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `__BPproducts_tps` (`TPS_PTP_ID`, `TPS_PRO_ID`, `TPS_STATUS`, `TPS_PRT_ID`, `TPS_ORDER`) VALUES
(1, 9, 'active', 1, 0),
(3, 9, 'active', 1, 0),
(6, 9, 'active', 2, 0);

ALTER TABLE `__BPproducts_tps`
 ADD KEY `TPS_PTP_ID` (`TPS_PTP_ID`,`TPS_PRO_ID`,`TPS_STATUS`,`TPS_ORDER`), ADD KEY `TPS_PRT_ID` (`TPS_PRT_ID`);";