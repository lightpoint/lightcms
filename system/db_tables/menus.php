<?php
$base['name'] = 'menus';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPmenus` (
  `MEN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MEN_PARENT` int(11) NOT NULL,
  `MEN_TYPE` enum('left','right','top','bottom','center') COLLATE latin1_general_ci NOT NULL DEFAULT 'left',
  `MEN_NAME` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `MEN_ORD` int(11) NOT NULL,
  `MEN_MODULE` varchar(250) COLLATE latin1_general_ci NOT NULL,
  `MEN_MODULE_ID` int(11) NOT NULL,
  `MEN_MODULE_ALIAS` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `MEN_MODULE_TITLE` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `MEN_HREF` varchar(150) COLLATE latin1_general_ci NOT NULL,
  `MEN_STYLE` varchar(30) COLLATE latin1_general_ci NOT NULL,
  `MEN_ORDER` int(11) NOT NULL,
  `MEN_STATUS` enum('aktywny','nieaktywny','usunięty') COLLATE latin1_general_ci NOT NULL DEFAULT 'aktywny',
  PRIMARY KEY (`MEN_ID`),
  KEY `MEN_TYPE` (`MEN_TYPE`,`MEN_ORD`,`MEN_MODULE`,`MEN_MODULE_ID`),
  KEY `MEN_PARENT` (`MEN_PARENT`),
  KEY `MEN_ORDER` (`MEN_ORDER`),
  KEY `MEN_MODULE_ALIAS` (`MEN_MODULE_ALIAS`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;";