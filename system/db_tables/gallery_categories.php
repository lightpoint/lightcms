<?php
$base['name'] = 'gallery_categories';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPgallery_categories` (
  `GCA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GCA_NAME` varchar(200) NOT NULL,
  `GCA_ALIAS` varchar(200) NOT NULL,
  `GCA_DESCRIPTION` tinytext NOT NULL,
  `GCA_ORDER` int(11) NOT NULL,
  `GCA_STATUS` enum('aktywne','nieaktywne') NOT NULL DEFAULT 'aktywne',
  PRIMARY KEY (`GCA_ID`),
  KEY `GCA_ALIAS` (`GCA_ALIAS`,`GCA_ORDER`,`GCA_STATUS`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";