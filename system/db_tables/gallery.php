<?php
$base['name'] = 'gallery';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPgallery` (
  `GAL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GAL_GCA_ID` int(11) NOT NULL,
  `GAL_NAME` varchar(200) NOT NULL,
  `GAL_DESCRIPTION` text NOT NULL,
  `GAL_FILE` varchar(100) NOT NULL,
  `GAL_ORDER` int(11) NOT NULL,
  `GAL_STATUS` enum('aktywny','nieaktywny') NOT NULL DEFAULT 'aktywny',
  PRIMARY KEY (`GAL_ID`),
  KEY `GAL_GCA_ID` (`GAL_GCA_ID`,`GAL_STATUS`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";