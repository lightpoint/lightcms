<?php
$base['name'] = 'log_register';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPlog_register` (
`LRE_ID` int(11) NOT NULL,
  `LRE_USR_ID` int(11) NOT NULL,
  `LRE_CONTROL_KEY` varchar(50) NOT NULL,
  `LRE_LOG_ID` tinyint(4) NOT NULL,
  `LRE_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LRE_DATE_CLICK` datetime NOT NULL,
  `LRE_IP` varchar(20) NOT NULL,
  `LRE_STATUS` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";