<?php
$base['name'] = 'products';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPproducts` (
`PRO_ID` int(11) NOT NULL,
  `PRO_CAT_ID` int(11) NOT NULL,
  `PRO_NAME` varchar(200) NOT NULL,
  `PRO_ALIAS` varchar(100) NOT NULL,
  `PRO_SUBNAME` varchar(100) NOT NULL,
  `PRO_DESC_SHORT` tinytext NOT NULL,
  `PRO_DESC_LONG` mediumtext NOT NULL,
  `PRO_PRICE` float(12,2) NOT NULL,
  `PRO_PRICE_BUY` float(12,2) NOT NULL,
  `PRO_PRICE_SELL` float(12,2) NOT NULL,
  `PRO_COUNT` tinyint(4) NOT NULL,
  `PRO_FILE` varchar(50) NOT NULL,
  `PRO_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PRO_STATUS` enum('nowy','aktywny','nieaktywny') NOT NULL DEFAULT 'nowy'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";