<?php
$base['name'] = 'products_types';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPproducts_types` (
`PRT_ID` int(11) NOT NULL,
  `PRT_NAME` varchar(200) NOT NULL,
  `PRT_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `__BPproducts_types` (`PRT_ID`, `PRT_NAME`, `PRT_STATUS`) VALUES
(1, 'Rozmiary', 'active'),
(2, 'Kolory', 'active');

ALTER TABLE `__BPproducts_types`
 ADD PRIMARY KEY (`PRT_ID`), ADD KEY `PRT_STATUS` (`PRT_STATUS`);";