<?php
$base['name'] = 'orders';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPorders` (
`ORD_ID` int(11) NOT NULL,
  `ORD_USR_ID` int(11) NOT NULL,
  `ORD_DATE_ADD` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ORD_DATE_SEND` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ORD_STATUS` enum('nowe','zatwierdzone','przetwarzane','wysłane','usunięte') NOT NULL DEFAULT 'nowe',
  `ORD_USER_DATA` mediumtext NOT NULL,
  `ORD_PROD_DATA` mediumtext NOT NULL,
  `ORD_VALUE` float(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";