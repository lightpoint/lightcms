<?php
$base['name'] = 'password_reminder';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPpassword_reminder` (
`PAR_ID` int(11) NOT NULL,
  `PAR_USR_ID` int(11) NOT NULL,
  `PAR_USR_EMAIL` varchar(100) NOT NULL,
  `PAR_NEW_PASSWORD` varchar(150) NOT NULL,
  `PAR_IP` varchar(40) NOT NULL,
  `PAR_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `PAR_CLICK` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `PAR_STATUS` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";