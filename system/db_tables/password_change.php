<?php
$base['name'] = 'password_change';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPpassword_change` (
`PAC_ID` int(11) NOT NULL,
  `PAC_USR_ID` int(11) NOT NULL,
  `PAC_PASS_NEW` varchar(255) NOT NULL,
  `PAC_PASS_OLD` varchar(255) NOT NULL,
  `PAC_USR_IP` int(30) NOT NULL,
  `PAC_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";