<?php
$base['name'] = 'admin_roles';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPadmin_roles` (
  `ADR_ADM_ID` int(11) NOT NULL,
  `ADR_NAME` varchar(50) NOT NULL,
  `ADR_ROLE_ID` varchar(30) NOT NULL,
  `ADR_ROLE_OPTION` varchar(30) NOT NULL,
  `ADR_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ADR_ORDER` tinyint(4) NOT NULL,
  KEY `ADR_ADM_ID` (`ADR_ADM_ID`,`ADR_ROLE_ID`),
  KEY `ADR_ORDER` (`ADR_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2;";