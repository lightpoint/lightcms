<?php
$base['name'] = 'products_gallery';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPproducts_gallery` (
`PRG_ID` int(11) NOT NULL,
  `PRG_PRO_ID` int(11) NOT NULL,
  `PRG_FILE` varchar(100) NOT NULL,
  `PRG_STATUS` enum('aktywny','nieaktywny','usunięty') NOT NULL DEFAULT 'aktywny',
  `PRG_ORDER` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";