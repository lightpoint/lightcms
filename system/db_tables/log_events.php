<?php
$base['name'] = 'log_events';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPlog_events` (
  `LOE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOE_DATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LOE_USER` int(11) NOT NULL,
  `LOE_EVENT` mediumtext CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `LOE_FROM` enum('admin','user') NOT NULL,
  `LOE_IP` varchar(30) NOT NULL,
  PRIMARY KEY (`LOE_ID`),
  KEY `LOE_USER` (`LOE_USER`),
  KEY `LOE_FROM` (`LOE_FROM`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";