<?php
$base['name'] = 'units';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPunits` (
`UNI_ID` int(11) NOT NULL,
  `UNI_NAME` varchar(30) NOT NULL,
  `UNI_ALIAS` varchar(20) NOT NULL,
  `UNI_WEIGHT` float(8,2) NOT NULL,
  `UNI_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4;

INSERT INTO `__BPunits` (`UNI_ID`, `UNI_NAME`, `UNI_ALIAS`, `UNI_WEIGHT`, `UNI_STATUS`) VALUES
(1, 'Kilogram', 'kg', 1.00, 'active'),
(2, 'Sztuka', 'szt.', 1.00, 'active'),
(3, 'Litr', 'l', 1.00, 'active');

ALTER TABLE `__BPunits`
 ADD PRIMARY KEY (`UNI_ID`), ADD KEY `UNI_STATUS` (`UNI_STATUS`);";