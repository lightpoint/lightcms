<?php
$base['name'] = 'articles';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BParticles` (
  `ART_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ART_PARENT` int(11) NOT NULL DEFAULT '0',
  `ART_LEVEL` tinyint(4) NOT NULL DEFAULT '0',
  `ART_NAME` varchar(200) NOT NULL,
  `ART_ALIAS` varchar(200) NOT NULL,
  `ART_CONTENT` longtext NOT NULL,
  `ART_ORDER` int(11) NOT NULL,
  `ART_STATUS` enum('nowy','opublikowany','ukryty') NOT NULL DEFAULT 'nowy',
  PRIMARY KEY (`ART_ID`),
  KEY `ART_PARENT` (`ART_PARENT`,`ART_ALIAS`,`ART_ORDER`,`ART_STATUS`),
  KEY `ART_LEVEL` (`ART_LEVEL`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";