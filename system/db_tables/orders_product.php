<?php
$base['name'] = 'orders_product';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPorders_product` (
`ORP_ID` int(11) NOT NULL,
  `ORP_ORD_ID` int(11) NOT NULL,
  `ORP_PRO_ID` int(11) NOT NULL,
  `ORP_PRICE` float(12,2) NOT NULL,
  `ORP_STATUS` enum('active','deleted') NOT NULL DEFAULT 'active',
  `ORP_TYPE` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";