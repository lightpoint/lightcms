<?php
$base['name'] = 'admin_dashboard';
$base['body'] = "CREATE TABLE IF NOT EXISTS `__BPadmin_dashboard` (
  `ADD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ADD_ADM_ID` int(11) NOT NULL,
  `ADD_ACTION` varchar(50) NOT NULL,
  `ADD_OPTION` varchar(50) NOT NULL,
  `ADD_ORDER` tinyint(4) NOT NULL,
  PRIMARY KEY (`ADD_ID`),
  KEY `ADD_ADM_ID` (`ADD_ADM_ID`),
  KEY `ADD_ACTION` (`ADD_ACTION`),
  KEY `ADD_ORDER` (`ADD_ORDER`)
) ENGINE=InnoDB DEFAULT CHARSET=latin2 AUTO_INCREMENT=1 ;";