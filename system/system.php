<?php
class Layout {
	public $var, $site;

	public function __construct($layout_path, $type='') {
		global $view;

		//create link after login
		if($_GET['action']!='user' and $_POST['action']!='user') {
			$_SESSION['link_request']['action'] = $_GET['action'];
			$_SESSION['link_request']['option'] = $_GET['option'];
		} //else header('location: /');

		//no mod-rewrite version
		//$_SESSION['link_request'] = $_SERVER['SERVER_NAME'].$_SERVER['QUERY_STRING'];

		$this->layout_path = $layout_path;

		$this->type = $type;

		$this->view = new View();
		$layout_p = $this->view->setLayout($this->layout_path);

		$this->site = file_get_contents($layout_p);
		$this->parse_content();
	}

	public function parse_content() {
		$module_content = array();
		$module_list = array();

		$data_module = new Module($this->type);
		preg_match_all("|{/[^>]+/}|U",  $this->site, $out, PREG_PATTERN_ORDER);
		foreach($out[0] as $module_str) {
			$module = str_replace(array('{/','/}'), array('',''), $module_str);
			$module = explode(':', $module);


			$module_content[] = $data_module->load($module[0],$module[1]);
			$module_list[] = $module_str;
		}

		$config = new config_options;
		if($config->testingSite==true) {
			$controller = new Controller;

			$this->all_data = "<div class=infoBox>".$controller->displayDataArray()."</div>";
		}

		$this->site = str_replace($module_list, $module_content, $this->site);
		$this->site.= $this->all_data;

	}

	public static function showLogin() {
		echo file_get_contents(V_A_TEMPLATE_PATH."login.tpl.php");
	}

	public function __destruct() {
		echo $this->site;
	}
}

class Module {
	public function __construct($type=null) {
		$this->type = $type;
	}

	public function load($module, $option=null, $id=null) {
		if(defined($module)) {
			return constant($module);
		} else if(!file_exists("modules/".$module."/".$this->type.$module.".php")) {
			return "<div class='errorInfo'>can't find module - ".$module." (type=".$this->type.") ".$_SERVER['REQUEST_URI']."</div>";
		}
		require_once("modules/".$module."/".$this->type.$module.".php");
		if($option!=null)
			return $module($option);
		else return $module();
	}
}

class Model {
	public $path = 'modules/';
	public function __construct($name, $type=null) {
		$this->name = $name;
		$this->type = $type;
		return (object)$this->getModel();
	}
	
	public function getModel() {
		if(file_exists($this->path.$this->name.'/'.$this->type.$this->name.'.php')) {
			include_once($this->path.$this->name.'/'.$this->type.$this->name.'.php');
			if(class_exists($this->name)) {
				$this->model =  new	$this->name;
				return $this->model;
			} else return null;
		} else return null;
	}
	
	public function get($function, $param=null) {
		return $this->model->$function();
	}
}