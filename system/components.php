<?php
//create tables with rows, cells
class table {
	public function __construct($id=null, $class=null) {
		if($class!='') $class = "class='$class'";
		if($id!='') $id = "id='$id'";
		$this->htmlHead = "<table $class $id>";
	}
	
	public function createHeader($th) {
		foreach($th as $h) {
			if($h=='')
				$this->td.="<th width='30px'>&nbsp;</th>";
			else $this->td.="<th>".$h."</th>";
		}
		$this->html.=$this->addHeaderRow();
	}
	
	public function createBody() {
	  $this->html = $this->htmlHead."<tbody>".$this->html."</tbody></table>";
    return $this->html;
	}

	public function addLink($href, $link, $class=null, $tdClass=null, $title=null, $option=null) {
		if($class!='') $class = " class='$class'";
		if($tdClass!='') $tdClass = " class='$tdClass'";
		$this->td.="<td$tdClass><a href='$href'$class title='$title' $option>$link</a></td>";
	}

	public function getBody() {
		return $this->html;
	}
	
	public function setCellColspan($data, $colspan, $class=null) {
		$this->td.= "<td colspan='".$colspan."' class='".$class."'>".$data."</td>";
	}

	public function addCell($td, $class=null) {
		$cl = null;
		if($class) $cl = " class='$class'";
		$this->td.="<td$cl>".$td."</td>";
	}

	public function addRow($class=null) {
		if($class) $cl = " class='$class'";
			else $cl = null;
		$this->html.="<tr$cl>".$this->td.'</tr>';
		$this->td = '';
	}
	
	private function addHeaderRow() {
		$this->htmlHead.='<thead><tr>'.$this->td.'</tr></thead>';
		$this->td = '';
	}
	
	private function createFoot() {
		$this->html.="</table>";
	}
}

//create breadcrumb with paths & separator
class breadcrumbs {
	public $separator = '&nbsp;&raquo&nbsp;';
	
	public function __construct() {
		$this->crumbs = array();
	}
	
	public function addCrumb($name, $link=null, $class=null) {
		if($class)
			$class_name = " class='$class'";
		if($link)
			$this->crumbs[] = "<a href='$link'$class_name>$name</a>";
		else $this->crumbs[] = $name;
	}
	
	public function create() {
		$i = 0;
		foreach($this->crumbs as $crumb) { 
			if($i>0)
				$breadCrumb.= $this->separator.$crumb;
			else $breadCrumb.= $crumb;
			$i++;
		}
		return "<div id='breadcrumb'>".$breadCrumb."</div>";
	}
}

class pagination {
	static function create($all, $page, $elements, $module, $pageSize=null) {
		$pageSelect = array(10, 20, 50, 100); //paging elements
		$pageSelectText = 'liczba elementów na stronie: ';
		
		if($all>0) {
			$pagination_elements = ceil($all/$elements);
			if($page>0)
				$result.='<div class="pagin active"><a href="'.$module.'&page='.$page.'&elements='.$elements.'">&laquo;</a></div>';

			if($pagination_elements<=14) {
				for($i = 1; $i <= $pagination_elements; $i++) {
					$i_p = $i;
					
					if($i==($page+1))
						$result.='<div class="pagin current">'.$i_p.'</div>';
					else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
				}
			} else {
				if($page<9) {
					for($i = 1; $i <= 10; $i++) {
						$i_p = $i;
						
						if($i==($page+1))
							$result.='<div class="pagin current">'.$i_p.'</div>';
						else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}	
					$result.='<div class="pagin active">...</div>';
					for($i = $pagination_elements-3; $i <= $pagination_elements; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
				} elseif($page>=($pagination_elements-10)) {
					for($i = 1; $i <= 3; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}	
					$result.='<div class="pagin active">...</div>';
					for($i = $pagination_elements-10; $i <= $pagination_elements; $i++) {
						$i_p = $i;
						
						if($i==($page+1))
							$result.='<div class="pagin current">'.$i_p.'</div>';
						else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
				} else {
					for($i = 1; $i <= 3; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}	
					$result.='<div class="pagin active">...</div>';
					
					for($i = $page-5; $i <= $page+5; $i++) {
						$i_p = $i;
						
						if($i==($page+1))
							$result.='<div class="pagin current">'.$i_p.'</div>';
						else $result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
					
					$result.='<div class="pagin active">...</div>';
					for($i = $pagination_elements-3; $i <= $pagination_elements; $i++) {
						$i_p = $i;
						
						$result.='<div class="pagin active"><a href="'.$module.'&page='.$i_p.'&elements='.$elements.'">'.$i_p.'</a></div>';
					}
				}
				
			}
			
			if($page<$pagination_elements-1) {
				$next = $page+2;
				$result.='<div class="pagin active"><a href="'.$module.'&page='.$next.'&elements='.$elements.'">&raquo;</a></div>';
			}
			
			$result ='<div id="pagin_container">'.$result.'</div>';
		} else $result = null;
		
		if($pageSize==true) {
			$pageSelectBody = new select('pageSize', 'pageSize');
			foreach($pageSelect as $ps) {
				if($ps==$elements)
					$pageSelectBody->addNode($ps, $ps, 'selected');
				else $pageSelectBody->addNode($ps, $ps);
			}
			return $pageSelectText.$pageSelectBody->create().$result;
		} else return $result;
	}
}

//parse templates & add data to placeholders
class Template {
	static function parse($data, $file) {
		if(is_array($data) and $file!=null) {
			//$file = preg_replace("|/*[^>]+*/|U",  '', $file);
			$first = array();
			$second = array();
			foreach($data as $placeholder=>$value) {
				$first[] = $placeholder;
				$second[] = $value;
			}
			return str_replace($first, $second, $file);
		} else return View::pageError();
	}
}

//create select
class select {
	public function __construct($id, $name=null, $class=null, $multiple=null, $attr=null) {
		$this->id = $id;
		$this->name = $name;
		$this->class = $class;
		$this->attr = $attr;
		
		if($multiple=='multiple')
			$multiple = " multiple='multiple'";
		
		$this->html = "<select id='".$this->id."' name='".$this->name."' class='".$this->class."'".$multiple.$attr.">__NODES\n\r</select>";
	}
	
	public function addNode($name, $value, $selected=null) {
		if($selected=='selected')
			$selected = " selected='selected'";
		else $selected = '';
			
		$this->node.="<option value='".$value."'".$selected.">".$name."</option>\n\r";
	}
	
	public function create() {
		return str_ireplace('__NODES',$this->node,$this->html);
	}
}

//get clint ip
class clientIP {
	static function get() {
		$ip = 0;
		if(!empty($_SERVER['HTTP_CLIENT_IP']))
			$ip = $_SERVER['HTTP_CLIENT_IP'];
			
		if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipList = explode (', ', $_SERVER['HTTP_X_FORWARDED_FOR']);
			
			if($ip) {
				array_unshift($ipList, $ip);
				$ip = 0;
			}
			
			foreach($ipList as $v)
				if(!ereg('^(192\.168|172\.16|10|224|240|127|0)\.', $v))
					return $v;
					
		return $ip ? $ip : $_SERVER['REMOTE_ADDR'];
	}
}

class coder {
	static function hashString($string) {
		$salt = 'f8j2S5jX8at2Do0saV_ansPw4w';
		$result = hash('sha256', $string.$salt);
		return $result;
	}
	
	static function stripPL_a($str, $replace = '_') {
	  $str = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	  $charsArr =  array('^', "'", '"', '`', '~');
	  $str = str_replace($charsArr, '', $str );
	  $return = trim(ereg_replace(' +',' ',preg_replace('/[^a-zA-Z0-9s]/','',strtolower($str))));
	  return str_replace(' ', $replace, $return);
  }
  
  static function stripPL($string){
  	//$trips = array'Ę'=>'e','ę'=>'e', 'Ó'=>'o', 'ó'=>'o','Ą'=>'a', 'ą'=>'a', 'Ś'=>'s', 'ś'=>'s', 'Ł'=>'l','ł'=>'l','Ź'=>'z', 'ź'=>'z', 'Ż'=>'z', 'ż'=>'z', 'Ć'=>'c','ć'=>'c', 'Ń'=>'n', 'ń'=>'n', ' '=>'_'
  	$strip1 = array('ę', 'ó', 'ą', 'ś', 'ł', 'ż', 'ź', 'ć', 'ń', 'Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń', ' ', '&', '!');
  	$strip2 = array('e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n', 'e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n', '_', '_and_', '');
		
		return strtolower(str_replace($strip1, $strip2, $string));
	}
	
	static function generatePassword(){
    $pattern='qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    for($i=0; $i<8; $i++) {
    	$key.=$pattern{rand(0,strlen($pattern)-1)};
    }
    
    $key = ucfirst($key).rand(0, 99);
    return $key;
	}
  
}

class helper {
  static function addhttp($url) {
    if(!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
  }
  
  static function href($parameters, $hash=null) {
		$p = array('action', 'option', 'id', 'idp');
		$link = null;
		$counter = 0;
		$link__temp = array();
		if(is_array($parameters)) {
			foreach($parameters as $par) {
				if($par!=null) {
					$link_temp[$p[$counter]] = $par;
					$counter++;
				}
			}
			
			if(__URL_SHORT_LINK) {
					$link = implode('-', $link_temp);
					$link.='.html';
				} else {
					foreach($link_temp as $key=>$value)
						$link.= '&'.$key.'='.$value;
					$link[0] = '?';
				}
			if($hash)
				$link.='#'.$hash;
			return $link;
		} else return '#';
	}
	
	public function langText($lang, $key) {
    global $langText;
		return $langText[$lang][$key];
	}
	
	public function languages() {
		$controller = new Controller;
		$this->var = $controller->trimData();
		//var_dump($this->var);
		if($this->var->s_lang=='pl') {
			return '<img src="templates/frontend/jurajska_1.0/img/icons/pl_a.png">&nbsp;&nbsp;&nbsp;<a href="'.helper::href(array($this->var->action, $this->var->option, $this->var->cat, $this->var->id, 'lang', 'en')).'"><img src="templates/frontend/jurajska_1.0/img/icons/en.png"></a>';
		} elseif($this->var->s_lang=='en') {
			return '<a href="'.helper::href(array($this->var->action, $this->var->option, $this->var->cat, $this->var->id, 'lang', 'pl')).'"><img src="templates/frontend/jurajska_1.0/img/icons/pl.png"></a>&nbsp;&nbsp;&nbsp;<img src="templates/frontend/jurajska_1.0/img/icons/en_a.png">';
		}
	}
	
}