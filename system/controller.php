<?php
class Controller {
	
	public function __construct() {
		$this->session = $_SESSION;	
		$this->post = $_POST;
		$this->get = $_GET;
		$this->data = array_merge((array)$this->session,(array)$this->post,(array)$this->get);
	}
	
	public function getData() {
		return (object)$this->data; //return without filtering
	}
	
	public function trimData() {
		$data = $this->getData();
		$trim = array('lang', 'pl', 'en');
	
		if(in_array($data->option, $trim))
			$data->option = null;
		if(in_array($data->cat, $trim))
			$data->cat = null;
		if(in_array($data->id, $trim))
			$data->id = null;
			
		return (object)$data;
	}
	
	public function getMesData() {
		return mysql_escape_string((object)$this->data);
	}
	
	public function getHscData() {
		return htmlspecialchars((object)$this->data, ENT_QUOTES);
	}
	
	public function displayData() {
		var_dump($this->data);
	}
	
	public function displayDataArray() {
		$line = null;
		if(count($this->data)>0)
			foreach($this->data as $key=>$value)
				if(is_array($value)) {
					//$line.='<p>'.$key.'=>'.print_r($value).'</p>';
					foreach($value as $p_key=>$p_value)
						$line.='<p>'.$key.'=>'.$p_key.'['.$p_value.']</p>';
				} else $line.='<p>'.$key.'=>'.$value.'</p>';
		return $line;
	}
}
?>