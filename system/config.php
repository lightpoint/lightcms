<?php
class config_options {
	public $mainentance = false;
	public $errorSite = false;
	public $testingSite = false;
	public $mobileAgentDetect = false;
}


//paths
define('SC_ROOT_URL','http://localhost/makewonder/');
define('SC_HOME_PATH','/');

define('SC_IMAGES_URL',SC_ROOT_URL.'/images');
define('SC_IMAGES_THUMB_URL',SC_ROOT_URL.'/images/thumb');

//products images
define('PRODUCT_FILES_FULL','files/product/full/');
define('PRODUCT_FILES_THUMB_1','files/product/thumb_1/');
define('PRODUCT_FILES_THUMB_2','files/product/thumb_2/');

define('PRODUCT_FULL_X', 1200);
define('PRODUCT_FULL_Y', 900);

define('PRODUCT_THUMB_1_X', 300);
define('PRODUCT_THUMB_1_Y', 300);

define('PRODUCT_THUMB_2_X', 100);
define('PRODUCT_THUMB_2_Y', 100);

//files gallery
define('GALLERY_FILES_FULL','files/gallery/full/');
define('GALLERY_FILES_THUMB_1','files/gallery/thumb_1/');
define('GALLERY_FILES_THUMB_2','files/gallery/thumb_2/');

define('LESSONS_ATTACHMENT_FILES','files/attachments/lessons/');
define('INSPIRATION_ATTACHMENT_FILES','files/attachments/inspiracje/');

define('__GAL_B_X', 1200);
define('__GAL_B_Y', 900);

define('__GAL_M_X', 320);
define('__GAL_M_Y', 320);

define('__GAL_S_X', 100);
define('__GAL_S_Y', 100);

//languages
define('SC_LNG_DEFAULT', 'pl');

//views
define('V_TEMPLATE','makewonder_1.0/');
define('V_PATH_TEMPLATE','templates/frontend/');
define('V_COMPILE','/templates/_c'); //compiled views for OPT

//admin paths
define('V_A_TEMPLATE','/devoops/');
define('V_A_TEMPLATE_PATH','templates/backend'.V_A_TEMPLATE);

//OPT config
define('OPT_PATH','lib/Opl');
define('OPT_LIB_PATH','lib');

//options
//login user - base/file
define('U_TYPE', 'base');

//base
define("__HOST", "mysql-expansja.ogicom.pl");
define("__BASE", "db214672");
define("__USER", "db214672");
define("__PASS", "gouda1Hipsta");

//table prefix
define("__BP", "makewonder__");

//user file configuration
define('__U_LOGIN','admin');
define('__U_PASS','dfc4c26a10daebacb0fe122423d4232c6e370621c24cb18f1d1ba9eb110176c7');

//short link configuration
define("__URL_SHORT_LINK", true); // mod rewrite uses '-'
define("__URL_ADD_SLASH", false); //add slashes to generated link

//system informations
define('__VER_REGISTERED', 'Expansja');
define('__VER_NUMBER', '1.2');
define('__VER_NAME', 'LightCMS');

//admin - temporary
define('__ADMIN_NAME', 'lukasz');
define('__ADMIN_MAIL', 'lukasz@expansja.pl');


//mailer config
define('__M_HOST', 'smtp-expansja.ogicom.pl');
define('__M_USER', 'noreply-wp.expansja');
define('__M_PASS', 'powiadoM13');
define('__M_PORT', 587);
define('__M_SMTP_AUTH', 1);
define('__M_SSL_POLACZENIE', 0);
define('__M_HTML', 1);
define('__M_FROM_EMAIL', 'noreply@wonderpolska.pl');
define('__M_FROM_NAME', 'MAKEWONDER.PL');