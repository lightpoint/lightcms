<?php
/* parser for xml using SimpleXML class by lukawar */
class Parser {
  public function __construct($file) {
    $this->file = $file;
  }

  public function getData() {
    //return $this->parseFile($this->file);
    return $this->loadFile($this->file);
  }

	public function getDataReverse() {
    //return $this->parseFile($this->file);
    return $this->parseFileReverse($this->file);
  }

  public function getRow($index) {
    $nodes = (array)$this->loadFile($this->file);
    $node = array_values($nodes);
    return $node[0][$index];
  }

	/* update selected node with number index
	 * new parse(file)
	 * nodeValue(nodeChild, data)
	 * updateData(node index)	*/
	public function updateData($index) {
		$xmlfile = $this->parseFile($this->file);
		//var_dump($xmlfile);
		foreach($this->line as $nodeLine=>$data)
			$xmlfile->item[(int)$index]->$nodeLine = $data;

		file_put_contents($this->file, $xmlfile->asXML());
	}

	public function updateDataG($index) {
		$xmlfile = $this->parseFile($this->file);
		var_dump($xmlfile);
		foreach($this->line as $nodeLine=>$data)
			$xmlfile->gallery[(int)$index]->$nodeLine = $data;

		file_put_contents($this->file, $xmlfile->asXML());
	}

	/* add node into xml file
	 * new parser(file)
	 * nodeValue(nodeChild, data)
	 * addData(nodeParent) */
  public function addData($node) {
		$xmlstr = $this->getContentXML();
		$sxe = new SimpleXMLElement($xmlstr);

		$XMLnode = $sxe->addChild($node);

		foreach($this->line as $nodeLine=>$data)
			$XMLnode->addChild($nodeLine, $data);

		file_put_contents($this->file, $sxe->asXML());
  }

	//values of silngle node for add and updete functions
	public function nodeValue($node, $data) {
		$this->line[$node] = $data;
	}

	//delete selected node with number index
  public function delData5($index) {
		//$doc = new SimpleXMLElement($this->getContentXML());
		$nodes = (array)$this->loadFile($this->file);
    $doc = array_values($nodes);
    //echo $doc[0][2]."--";
    var_dump($doc);
    unset($doc[0][$index-1]);

    //die(var_dump($doc));
		//$dom = dom_import_simplexml($doc->category[$index]);
		//$dom->parentNode->removeChild($dom);
		//echo $doc->asXml();
		file_put_contents($this->file, $doc->asXML());
  }

  public function delData($index) {
  	$xmlfile = $this->parseFile($this->file);
		unset($xmlfile->item[$index-1]);
		file_put_contents($this->file, $xmlfile->asXML());
  }

  public function delDataG($index) {
  	$xmlfile = $this->parseFile($this->file);
		unset($xmlfile->gallery[$index-1]);
		file_put_contents($this->file, $xmlfile->asXML());
  }

	public function getCount() {
		$data =  $this->loadFile($this->file);
		return count($data);
	}

  private function parseFile($xmlFile) {
    $this->XMLfile = file_get_contents($xmlFile);

	  return simplexml_load_string($this->XMLfile);
  }

	private function parseFileReverse($xmlFile) {
    $this->XMLfile = file_get_contents($xmlFile);

	  $xml = simplexml_load_string($this->XMLfile);
		$reverseArray = (array) $xml;
		$reverseArray = array_reverse($reverseArray["item"]);
		return $reverseArray;
  }

	private function loadFile($xmlFile) {
		if(file_exists($xmlFile)) {
			$this->XMLfile = simplexml_load_file($xmlFile);
			return $this->XMLfile;
		}
		else return false;
	}

	//return data of xml
	public function getContentXML() {
		return file_get_contents($this->file);
	}
}
?>