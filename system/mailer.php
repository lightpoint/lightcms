<?php
include_once('system/mail_class/class.phpmailer.php');
include_once('system/mail_class/class.smtp.php');
include_once('system/mail_class/EmailSendClass.php');
include_once('system/mail_class/emailCheck.php');

class Mailer {
	public function sendMail($to, $name, $cc, $subject, $message, $_messageFile, $_arrAtahment, $arrAtahmentHtml, $atPath, $replyTo, $replyToName) {
		$objEmailSendClass = new EmailSendClass();
		if(emailCheck($to)){	
			foreach($arrAtahmentHtml as $a) {
				$_arrAtahmentHtml['path'][] = $atPath.$a;
				$_arrAtahmentHtml['cid'][] = $a;
				$_arrAtahmentHtml['name'][] = $a;
				$_arrAtahmentHtml['type'][] = 'image/jpg';
			}
			$objEmailSendClass->wyczyscDane();		
			
			$objEmailSendClass->ustawServer(__M_HOST, __M_USER, __M_PASS, __M_PORT, __M_SMTP_AUTH, __M_SSL_POLACZENIE, __M_HTML);
				$objEmailSendClass->ustawEmail($to, $name, __M_FROM_EMAIL, __M_FROM_NAME, $subject, $message, $_messageFile, '', '', $_arrAtahment, $_arrAtahmentHtml, $replyTo, $replyToName);
			
			if($objEmailSendClass->sendEmail()){

			} else echo "błąd wysyłania";
		}
	}
}
?>