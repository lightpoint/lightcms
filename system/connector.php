<?php
class Connector extends PDO {//connecting with PDO
	public $pdo, $pdoConnection, $queryString;
	
  public function __construct($queryString=null) {
    $this->queryString = $queryString;
  }

	public function queryString($queryString) {
		$this->queryString = $queryString;
	}
	
  public function BaseConnection() { //create PDO object 
		$this->pdo = new PDO("mysql:host=".__HOST.";dbname=".__BASE, __USER, __PASS);
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->pdo->query('SET NAMES utf8');
		//$this->pdo->query("SET GLOBAL log_output = 'TABLE'");
		//$this->pdo->query("SET GLOBAL general_log = 'ON'");
		//$this->pdo->query('SET CHARACTER_SET utf8_unicode_ci');
		return $this->pdo;
  }

  public function insertQuery($istring) {
    foreach($istring as $key=>$is) {
      $label[] = $key;
      $value[] = "'".$is."'";
    }

    $this->queryString = "insert into ".$this->queryString." (".implode(",",$label).") values (".implode(",",$value).")";
    return $this->ExecQuery();
    return $this->pdo->lastInsertId();
  }
  
  public function prepareInsertQuery($istring) {
    foreach($istring as $key=>$is) {
      $label[] = $key;
      $value[] = "'".$is."'";
    }

    $this->queryString = "insert into ".$this->queryString." (".implode(",",$label).") values (".implode(",",$value).")";
  }
	
  /* istring - array with elements
     id - identyficator
     id_value - value of identyficator */
  public function updateQuery($istring, $id, $id_value) {
    foreach($istring as $key=>$is)
      $value[] =$key.'="'.$is.'"';
    $this->queryString = "update ".$this->queryString." set ".implode(",",$value)." where $id='$id_value'";
    $this->ExecQuery();
  }
	
	/* id - identyficator
     id_value - value of identyficator */
	public function delQuery($id, $id_value) {
		$this->queryString = "delete from ".$this->queryString." where $id='$id_value'";
    $this->ExecQuery();
	}

  public function GetData() { //get query from base
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();

		try {
   		$this->cell = $this->pdoConnection->query($this->queryString);
   	} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
 
    return $this->cell;
  }
  
  public function GetDataFetch() { //get query from base
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		
		try {
	  	$this->cell = $this->pdoConnection->query($this->queryString);
	  } catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
	  
	  $this->getCount = $this->cell->rowCount();
    
    if($this->getCount>0)
    	return $this->cell;
    else return false;
  }

  public function ExecQuery() {
		if(null === $this->pdoConnection) 
    	$this->pdoConnection = $this->BaseConnection();
			
		try {
    	$this->cell = $this->pdoConnection->exec($this->queryString);
    } catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
    return $this->pdo->lastInsertId();
  }

  public function RunQuery() {
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		try {
			$this->cell = $this->pdoConnection->query($this->queryString);
		} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
    return $this->pdo->lastInsertId();
  }

  public function GetRow() { //get one row from base
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		
		try {
   		$this->cell = $this->pdoConnection->query($this->queryString." limit 1");
   	} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
 
    return $this->cell->fetch();
  }
  
  public function dict($dict, $lang='pl') { //get dict value from from base
		if(null === $this->pdoConnection)
    	$this->pdoConnection = $this->BaseConnection();
		
		try {
   		$this->cell = $this->pdoConnection->query("select * from ".__BP."dict where DIC_KEY='".$dict."' and DIC_LNG='".$lang."' limit 1");
   	} catch(PDOException $Exception ) {
	  	throw new Exception($Exception->getMessage());
	  }
 
    return $this->cell->fetch();
  }
  
  /* id - DIC_KEY value
     value - data value 
     row - row name */
  public function setDict($id, $value, $row='DIC_VALUE') {
    $this->queryString = "update ".__BP."dict set ".$row."='".$value."' where DIC_KEY='$id'";
    $this->ExecQuery();
  }
  
  public function limit($l) {
		$this->queryString.= ' limit '.$l;
	}
  
  public function getCount() { //
  	return $this->getCount;
  }
  
  public function query() { //show query
  	echo "<p>".$this->queryString."</p>";
  }
  
  public function showQuery() { //return query 
  	return $this->queryString;
  }
	
  public function close() {
	$this->pdo = null;
  }

  function __destruct() {
    //$this->cell->closeCursor();
  }
}