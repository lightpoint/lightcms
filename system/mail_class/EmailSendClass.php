<?php
include_once('class.phpmailer.php');
include_once('class.smtp.php');

class EmailSendClass extends PHPMailer{
	
	private $objMail;
	
	public function __construct(){
		$this->objMail = new PHPMailer();
		//$this->objMail = $objMail;
		
	}
	
	public function wyczyscDane(){
		//	czyszczenie danych
		$this->objMail->ClearAddresses();
		$this->objMail->ClearCCs();
		$this->objMail->ClearBCCs();
		$this->objMail->ClearReplyTos();
		$this->objMail->ClearAllRecipients();
		$this->objMail->ClearAttachments();
		$this->objMail->ClearCustomHeaders();
	}
	
	public function ustawServer($strHost, $strUser, $strPass, $strPort, $boolSmtpAuth=true, $boolSsl=false, $boolHtml=true){
		
		//xxxxxxxxxxxxxxxxxx SMTP xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
		$this->objMail->IsSMTP();
		
		$this->objMail->Host			= $strHost;	// sets GMAIL as the SMTP server
		$this->objMail->Username		= $strUser;	// GMAIL username
		$this->objMail->Password		= $strPass;	// GMAIL password
		$this->objMail->Port			= $strPort;	// set the SMTP port for the GMAIL server
		$this->objMail->SMTPAuth		= $boolSmtpAuth;	// enable SMTP authentication
		if($boolSsl){
			$this->objMail->SMTPSecure	= "ssl";	// sets the prefix to the servier
		}
		
		$this->objMail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
		$this->objMail->WordWrap   = 50; // set word wrap
		
		$this->objMail->IsHTML($boolHtml);	// send as HTML
	}
	
	public function ustawEmail($strTo, $strToName, $strFrom, $strFromName, $strSubject, $strMessage='', $strMessageFile='', $arrToBCC='', $arrToBCCName='', $arrAttachment=array(), $arrAttachmentHtml=array(), $strReplyTo='', $strReplyToName=''){
		
		$this->objMail->AddAddress($strTo, $strToName);
		$this->objMail->From		= $strFrom;
		$this->objMail->FromName	= $strFromName;
		$this->objMail->Subject		= $strSubject;
		
		
		
		if(is_array($arrAttachmentHtml)){
			for($x=0, $xx=count($arrAttachmentHtml['path']); $x<$xx; $x++){
				if($this->objMail->AddEmbeddedImage($arrAttachmentHtml['path'][$x], $arrAttachmentHtml['cid'][$x], $arrAttachmentHtml['name'][$x], 'base64', $arrAttachmentHtml['type'][$x])){
//					echo 'dodano plik<br />';
				}
				else {
//					echo 'nie dodano<br />';
				}
			}
		}
		
		if(is_array($arrAttachment) && count($arrAttachment)>0){
			foreach ($arrAttachment as $strZalacznik){
				$this->objMail->AddAttachment($strZalacznik);
			}
		}
		
		if($strMessage!='') { $body = $strMessage; }
		elseif ($strMessageFile!='') { $body = $this->objMail->getFile($strMessageFile); }
		$this->objMail->MsgHTML($body);
		
		if(is_array($arrToBCC) && count($arrToBCC)>0){
			foreach ($arrToBCC as $ToBCCkey=>$strToBCC){
				$this->objMail->AddBCC($strToBCC, $arrToBCCName[$ToBCCkey]);
			}
		}
		
		if($strReplyTo==''){
			$strReplyTo = $strFrom;
			$strReplyToName = $strFromName;
		}
		$this->objMail->AddReplyTo($strReplyTo, $strReplyToName);
	}
	
	public function sendEmail(){
		$result = $this->objMail->Send();
		return $result;
	}
	
	public function addBCC($mail,$name=''){
		$this->objMail->AddBCC($mail,'WE');
	}
	
}



?>