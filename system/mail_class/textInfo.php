<?php
//MAIL DO ADMINA - USER WYSYŁA ZGŁOSZENIE NOWE
$textInfo[1]['admin'] = '<p>Z konta <strong>__BUNDLE_USER</strong> właśnie zgłoszono pakiet faktur o numerze <strong>__BUNDLE_NUMBER</strong></p>
<p>Zgłoszenie ma status: <strong>NOWE</strong></p>
<p>Jeśli:</p>
<p><strong>ZGŁOSZENIE KOMPLETNE</strong></p>
<ul><li>zmień status na "PRZYJĘTE DO REALIZACJI" (klient otrzyma maila z informacją o realizacji zamówienia)</li></ul>
<p><strong>ZGŁOSZENIE NIEKOMPLETNE</strong></p>
<ul><li>zmień status na "W OCZEKIWANIU NA POTWIERDZENIE PŁATNOŚCI" (klient otrzyma maila z prośbą o dosłanie brakujących dokumentów)</li></ul>
<p><strong>ZGŁOSZENIE BŁĘDNE</strong></p>
<ul><li>zmień status na "ODRZUCONE" (klient otrzyma maila z prośbą o ponowne wysłanie zgłoszenia)</li></ul>';
$textSubject[1]['admin'] = 'Nowe zgłoszenie';

//MAIL DO USERA PO WYSŁANIU ZGŁOSZENIA NOWE
$textInfo[1]['user'] = '<p>Witaj <strong>__BUNDLE_IMIE</strong>,</p>
<p>Z Twojego konta <strong>__BUNDLE_USER</strong> właśnie wysłano pakiet faktur o numerze <strong>__BUNDLE_NUMBER</strong></p>
<p>Zgłoszenie ma status: <strong>NOWE</strong>.</p>
<p>Po weryfikacji dokumentów przez Organizatora, poinformujemy Cię mailowo o statusie zgłoszenia.</p>';
$textSubject[1]['user'] = 'Wysłano nowe zgłoszenie';
		
//MAIL DO ADMINA - USER DOSYŁA DOKUMENTY DOSŁANO POTWIERDZENIE PŁATNOŚCI
$textInfo[2]['admin'] = '<p>Użytkownik <strong>__BUNDLE_USER</strong> właśnie wysłał brakujące dokumenty do zgłoszenia o numerze <strong>__BUNDLE_NUMBER</strong></p>
<p>Zgłoszenie ma status: <strong>DOSŁANO POTWIERDZENIE PŁATNOŚCI</strong></p>
<p><strong>ZGŁOSZENIE KOMPLETNE</strong></p>
<ul><li>zmień status na "PRZYJĘTE DO REALIZACJI" (klient otrzyma maila z informacją o realizacji zamówienia)</li></ul>
<p><strong>ZGŁOSZENIE NIEKOMPLETNE</strong></p>
<ul><li>zmień status na "W OCZEKIWANIU NA POTWIERDZENIE PŁATNOŚCI" (klient otrzyma maila z prośbą o dosłanie brakujących dokumentów)</li></ul>
<p><strong>ZGŁOSZENIE BŁĘDNE</strong></p>
<ul><li>zmień status na "ODRZUCONE" (klient otrzyma maila z prośbą o ponowne wysłanie zgłoszenia)</li></ul>';
$textSubject[2]['admin'] = 'Dosłano dokumenty do zgłoszenia';
		
//MAIL DO USERA PO ZMIANIE STATUSU PRZEZ AUTOMAT NA DOSŁANO POTWIERDZENIE PŁATNOŚCI
$textInfo[2]['user'] = '<p>Witaj <strong>__BUNDLE_IMIE</strong>,</p>
<p>Z Twojego konta __BUNDLE_USER właśnie wysłano brakujące dokumenty do  zgłoszenia o numerze <strong>__BUNDLE_NUMBER</strong>.</p>
<p>Zgłoszenie ma status: <strong>DOSŁANO POTWIERDZENIE ZAPŁATY</strong>.</p>
<p>Po weryfikacji dokumentów przez Organizatora, poinformujemy Cię mailowo o statusie zgłoszenia.</p>';
$textSubject[2]['user'] = 'Zmiana statusu zgłoszenia';

//MAIL DO USERA PO ZMIANIE STATUSU PRZED ADMINA NA PRZYJĘTE DO REALIZACJI
$textInfo[3]['user'] = '<p>Witaj <strong>__BUNDLE_IMIE</strong>,</p>
<p>Twoje zgłoszenie o numerze <strong>__BUNDLE_NUMBER</strong> przeszło pomyślnie proces weryfikacji i zmieniło status na: <strong>PRZYJĘTE DO REALIZACJI.</strong></p>
<p>Już niedługo poinformujemy Cię o wysyłce wybranej nagrody.</p>
<p style="color:green;"><strong>__BUNDLE_INFO</strong></p>';
$textSubject[3]['user'] = 'Zmiana statusu zgłoszenia';

		
//MAIL DO USERA PO ZMIANIE STATUSU PRZEZ ADMINA NA W OCZEKIWANIU NA POTWIERDZENIE PŁATNOŚCI
$textInfo[4]['user'] = '<p>Witaj <strong>__BUNDLE_IMIE</strong>,</p>
<p>Twoje zgłoszenie o numerze <strong>__BUNDLE_NUMBER</strong> zmieniło status na: <strong>W OCZEKIWANIU NA POTWIERDZENIE PŁATNOŚCI</strong> i wymaga dosłania niezbędnych dokumentów.</p>
<p>Aby dosłać brakujące dokumenty, zaloguj się na swoje konto w programie "Złoto Dla Zuchwałych", wejdź w Twoje zgłoszenie o numerze <strong>__BUNDLE_NUMBER</strong>, a następnie załącz potwierdzenie płatności i kliknij "wyślij".</p>
<p>W przypadku pytań czy wątpliwości skontaktuj się z Organizatorem.</p>
<p style="color:green;"><strong>__BUNDLE_INFO</strong></p>';
$textSubject[4]['user'] = 'Zmiana statusu zgłoszenia';

//MAIL DO USERA PO ZMIANIE STATUSU PRZEZ ADMINA NA ODRZUCONE
$textInfo[5]['user'] = '<p>Witaj <strong>__BUNDLE_IMIE</strong>,</p>
<p>Twoje zgłoszenie o numerze <strong>__BUNDLE_NUMBER</strong> nie przeszło pomyślnie procesu weryfikacji zmieniło status na: <strong>ODRZUCONE</strong>.</p>
<p style="color:green;"><strong>Uzasadnienie: __BUNDLE_INFO</strong></p>
<p>W celu weryfikacji zgłoszenia, zaloguj się na swoje konto w programie "Złoto Dla Zuchwałych", wejdź w Twoje zgłoszenie o numerze <strong>__BUNDLE_NUMBER</strong>, a następnie nanieś niezbędne poprawki i kliknij "wyślij".</p>
<p>W przypadku pytań czy wątpliwości skontaktuj się z Organizatorem.</p>';
$textSubject[5]['user'] = 'Twoje zgłoszenie zostało odrzucone';

//MAIL DO USERA PO ZMIANIE STATUSU PRZEZ ADMINA NA WYSŁANE
$textInfo[6]['user'] = '<p>Witaj <strong>__BUNDLE_IMIE</strong>,</p>
<p>zgłoszenie akcji o numerze <strong>__BUNDLE_NUMBER</strong> zmieniło status na: <strong>WYSŁANE</strong>.</p>
<p>Wybrana przez Ciebie nagroda została wysłana.</p>
<p>Dziękujemy za udział w akcji.</p>
<p style="color:green;"><strong>__BUNDLE_INFO</strong></p>';
$textSubject[6]['user'] = 'Zmiana statusu zgłoszenia';

		
//MAIL DO USERA PO ZMIANIE STATUSU PRZEZ ADMINA NA UZUPEŁNIENIE BRAKÓW
$textInfo[7]['user'] = '<p>Witaj <strong>__BUNDLE_IMIE</strong>,</p>
<p>Twoje zgłoszenie o numerze <strong>__BUNDLE_NUMBER</strong> zmieniło status na: <strong>UZUPEŁNIENIE BRAKÓW</strong> i wymaga dosłania niezbędnych dokumentów.</p>
<p>Aby dosłać brakujące dokumenty, zaloguj się na swoje konto w programie "Złoto Dla Zuchwałych", wejdź w Twoje zgłoszenie o numerze <strong>__BUNDLE_NUMBER</strong>, a następnie załącz potwierdzenie płatności i kliknij "wyślij".</p>
<p>W przypadku pytań czy wątpliwości skontaktuj się z Organizatorem.</p>
<p style="color:green;"><strong>__BUNDLE_INFO</strong></p>';
$textSubject[7]['user'] = 'Zmiana statusu zgłoszenia';