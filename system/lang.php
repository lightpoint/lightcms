<?php
//ofirmie
$langText['pl']['__LANG_OFIRMIE_ROWNOWAGA'] = 'JURAJSKA. TWOJA RÓWNOWAGA';
$langText['en']['__LANG_OFIRMIE_ROWNOWAGA'] = 'JURAJSKA. YOUR STABILITY';

//nav
$langText['pl']['__LANG_NAV_OFIRMIE'] = 'O FIRMIE';
$langText['en']['__LANG_NAV_OFIRMIE'] = 'ABOUT US';
$langText['pl']['__LANG_NAV_PRODUKTY'] = 'NASZE PRODUKTY';
$langText['en']['__LANG_NAV_PRODUKTY'] = 'PRODUCTS';
$langText['pl']['__LANG_NAV_CSR'] = 'ODPOWIEDZIALNOŚĆ SPOŁECZNA';
$langText['en']['__LANG_NAV_CSR'] = 'CSR';
$langText['pl']['__LANG_NAV_TR'] = 'TWOJA RÓWNOWAGA';
$langText['en']['__LANG_NAV_TR'] = 'YOUR BALANCE';
$langText['pl']['__LANG_NAV_KONTAKT'] = 'KONTAKT';
$langText['en']['__LANG_NAV_KONTAKT'] = 'CONTACT';
$langText['pl']['__LANG_NAV_KARIERA'] = 'KARIERA';
/*$langText['en']['__LANG_NAV_KARIERA'] = 'CAREER';*/
$langText['pl']['__LANG_NAV_NEWS'] = 'AKTUALNOŚCI';
/*$langText['en']['__LANG_NAV_NEWS'] = 'NEWS';*/
$langText['pl']['__LANG_NAV_MEDIA'] = 'DLA MEDIÓW';
$langText['en']['__LANG_NAV_MEDIA'] = 'FOR PRESS';
$langText['pl']['__LANG_NAV_JURA'] = 'WODA Z JURY';
$langText['en']['__LANG_NAV_JURA'] = 'WATER FROM JURA';
$langText['pl']['__LANG_NAV_LOGIN'] = 'ZALOGUJ SIĘ';
$langText['en']['__LANG_NAV_LOGIN'] = 'LOG IN';
$langText['pl']['__LANG_NAV_RODZINA'] = 'RODZINA';
$langText['en']['__LANG_NAV_RODZINA'] = 'FAMILY';
$langText['pl']['__LANG_NAV_JUNIOR'] = 'JURAJSKA JUNIOR';
$langText['en']['__LANG_NAV_JUNIOR'] = 'JURAJSKA JUNIOR';

//tr
$langText['pl']['__LANG_TR_ROWNOWAGA_BOX'] = 'TWOJA RÓWNOWAGA';
$langText['en']['__LANG_TR_ROWNOWAGA_BOX'] = 'YOUR BALANCE';